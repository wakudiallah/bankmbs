<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('FILESYSTEM_DRIVER', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
             'root' => storage_path('app/public/storage/uploads/file'), 
              
             'visibility' => 'public',
             'directoryPerm' => 0777
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public/storage/uploads/file/'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        /* 'sftp' => [
            'driver' => 'sftp',
            'host' => 'moaqs.com',
            'port'=>4434,
            'username' => 'mom',
            'password' => 'netxpert123',
           //'privateKey' => storage_path('/key/insko23.ppk'),
        ],*/
           'xftp' => [
            'driver' => 'ftp',
            'host' => '60.54.119.233',
            //'port'=>221,
            'username' => 'novrizal',
            'password' => 'novrizal',
            'root' => '/'
        ],

        'sftp' => [
            'driver' => 'sftp',
            'host' => 'moaqs.com',
            'port'=>22,
            'username' => 'mom_mb',
            'password' => 'netxpert',
            'root' => '/',
            'timeout' => 1250,
        ],


        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'url' => env('AWS_URL'),
        ],

        'ftp' => [
           'driver'   => 'ftp',
            'host'     => 'ftp.insko.my',
            'username' => 'mommbsb@insko.my',
            'password' => 'netxpert123',
            'root' => '/',
             'directoryPerm' => 0777
        ],
         'ssftp' => [
           'driver'   => 'ssftp',
            'host'     => '192.168.0.123',
            'port'=>4434,
            'username' => 'novrizal',
            'password' => 'novrizal',
            'root' => '/'
        ],
        
    ],

];
