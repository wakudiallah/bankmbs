<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
     

        protected $hidden = ['created_at', 'updated_at','create_by'];

public function employment() {
		return $this->belongsTo('App\Model\Employment','employment_id');	
	}
}
