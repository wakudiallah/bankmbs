<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Branch  extends Model
{
   
   protected $table = 'branch';  

    public function term()
    {
        return $this->hasOne('App\Model\Term','id_branch');	
    }
    
    public function User() {
        return $this->hasMany('App\Model\User','id_branch');	
	}
    
    

}
