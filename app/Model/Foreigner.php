<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Foreigner  extends Model
{
   
   protected $table = 'dec_foreigner_exchanges';  

   public function Country()
    {
        return $this->hasOne('App\Model\Country','country_code','country');  
    }

}
