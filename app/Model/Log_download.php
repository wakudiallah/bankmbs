<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Log_download  extends Model
{
   
   protected $table = 'log_download';  

    public function term()
    {
        return $this->hasOne('App\Model\Term','id_praapplication');	
    }
    
     public function user()
    {
        return $this->hasOne('App\Model\User','id','id_user');	
    }



}
