<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\AntiAttrition;
class SettlementInfo extends Model
{
	protected $table = 'settlement_infos';

    protected $fillable = [
        //'id_settlement',
'MaxAge',
'DueDate',
'BalOutStanding',
'FullSettlement',
'ProfitEarned',
'Rebate',
'PaidInstallment',
'RemInstallment',
'DSRUtilise',
'DSRUnutilise', 
'CalculateDate',
'TaskIDNo',
'ACID',
'CustIDNo',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    public function newloanoffer() {
        //return $this->hasOne('App\Model\AntiAttrition','ACID','ACID2'); 
        return $this->hasMany('App\Model\AntiAttrition','ACID','ACID2')->groupBy('ACID');
    }
}
