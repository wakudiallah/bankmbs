<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
     

        protected $hidden = ['created_at', 'updated_at','create_by'];

public function package() {
		return $this->belongsTo('App\Model\Package','id_package');	
	}
}
