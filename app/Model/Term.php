<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Term  extends Model
{
   
   protected $table = 'term';  
    use SoftDeletes;

    protected $dates = ['deleted_at'];
    public $timestamps = true;

   	public function MarOfficer() {
		return $this->belongsTo('App\Model\User','referral_id','id');	
	}


   	public function Basic() {
		return $this->belongsTo('App\Model\Basic','id_praapplication','id_praapplication');	
	}
	
   public function Log_download() {
		return $this->hasMany('App\Model\Log_download','id_praapplication','id_praapplication');	
	}
	
   public function Branch() {
		return $this->belongsTo('App\Model\Branch','id_branch','branch_code');	
	}
	
   public function Contact_v() {
		return $this->belongsTo('App\Model\Contact_v','id_praapplication','id_praapplication');	
	}
	
	public function PraApplication() {
		return $this->belongsTo('App\Model\PraApplication','id_praapplication','id');	
	}
	
	public function LoanAmmount() {
		return $this->belongsTo('App\Model\LoanAmmount','id_praapplication','id_praapplication');	
	}

	public function PraApp() {
		return $this->belongsTo('App\Model\PraApplication','id_praapplication','id');	
	}


public function LogLogin() {
		return $this->belongsTo('App\Model\Log_download','id_praapplication','id_praapplication');	
	}
	
	public function Financial() {
		return $this->belongsTo('App\Model\Financial','id_praapplication','id_praapplication');	
	}

	 public function MO() {
        return $this->belongsTo('App\MO','referral_id','id');   
    }




	
	

     

}
