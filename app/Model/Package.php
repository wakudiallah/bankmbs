<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
	 protected $table = 'packages';  
	  protected $fillable = ['effective_rate', 'flat_rate', 'name', 'created_by'];
   protected $hidden = ['created_at', 'updated_at','create_by'];

 public function Type() {
		return $this->belongsTo('App\Model\Loan','id_type','id');	
	}
   use SoftDeletes;

	protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
}
