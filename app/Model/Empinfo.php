<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Empinfo  extends Model
{
   
   protected $table = 'empinfo';  

public function employment() {
		return $this->belongsTo('App\Model\Employment','employment_id');	
	}

	public function employer() {
		return $this->belongsTo('App\Model\Employer','employer_id');	
	}

	public function Nature() {
		return $this->belongsTo('App\Model\Occupations','nature_business','occupation_code');	
	}
	public function PositionDesc() {
		return $this->belongsTo('App\Model\Position','position','position_code');	
	}
	 public function StateOffice()
    {
        return $this->hasOne('App\Model\State2','state_code','state_code');  
    }
}
