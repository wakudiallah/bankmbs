<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $table = 'addresses';

     public function AddTypes()
    {
        return $this->hasOne('App\Model\PrAddType','AddTyCode','AddType');   
    }
}
