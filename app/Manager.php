<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manager extends Model
{
    protected $table = 'managers'; 
    public $incrementing = false;  
    
    protected $guarded = ["id"]; 
	protected $dates   = ['deleted_at'];
	public $timestamps = true;
   
    public function User() {
        return $this->hasOne('App\Model\User','id');	
	}
}
