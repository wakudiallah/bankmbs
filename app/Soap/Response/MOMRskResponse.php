<?php

namespace App\Soap\Response;

class MOMRskResponse
{
  /**
   * @var string
   */
  protected $MOMRskResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMRskResult)
  {
    $this->MOMRskResult = $MOMRskResult;
  }

  /**
   * @return string
   */
  public function getMOMRskResult()
  {
    return $this->MOMRskResult;
  }
}