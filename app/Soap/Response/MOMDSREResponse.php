<?php

namespace App\Soap\Response;

class MOMDSREResponse
{
  /**
   * @var string
   */
  protected $MOMDSREResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMDSREResult)
  {
    $this->MOMDSREResult = $MOMDSREResult;
  }

  /**
   * @return string
   */
  public function getMOMDSREResult()
  {
    return $this->MOMDSREResult;
  }
}