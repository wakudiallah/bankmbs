<?php

namespace App\Soap\Response;

class MOMBscResponse
{
  /**
   * @var string
   */
  protected $MOMBscResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMBscResult)
  {
    $this->MOMBscResult = $MOMBscResult;
  }

  /**
   * @return string
   */
  public function getMOMBscResult()
  {
    return $this->MOMBscResult;
  }
}