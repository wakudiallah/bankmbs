<?php

namespace App\Soap\Response;

class MOMDSRCResponse
{
  /**
   * @var string
   */
  protected $MOMDSRCResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMDSRCResult)
  {
    $this->MOMDSRCResult = $MOMDSRCResult;
  }

  /**
   * @return string
   */
  public function getMOMDSRCResult()
  {
    return $this->MOMDSRCResult;
  }
}