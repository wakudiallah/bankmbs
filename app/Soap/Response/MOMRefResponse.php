<?php

namespace App\Soap\Response;

class MOMRefResponse
{
  /**
   * @var string
   */
  protected $MOMRefResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMRefResult)
  {
    $this->MOMRefResult = $MOMRefResult;
  }

  /**
   * @return string
   */
  public function getMOMRefResult()
  {
    return $this->MOMRefResult;
  }
}