<?php

namespace App\Soap\Response;

class MOMFinResponse
{
  /**
   * @var string
   */
  protected $MOMFinResult;

  /**
   * GetConversionAmountResponse constructor.
   *
   * @param string
   */
  public function __construct($MOMFinResult)
  {
    $this->MOMFinResult = $MOMFinResult;
  }

  /**
   * @return string
   */
  public function getMOMFinResult()
  {
    return $this->MOMFinResult;
  }
}