<?php

namespace App\Soap\Request;

class GetDSRequest
{
  /**
   * @var string
   */
  protected $CommandString;

  /**
   * @var string
   */
  
  /**
   * @var string
   */
  
  /**
   * @var string
   */

  /**
   * GetConversionAmount constructor.
   *
   * @param string $CurrencyFrom
   * @param string $CurrencyTo
   * @param string $RateDate
   * @param string $Amount
   */
  public function __construct($CommandString)
  {
    $this->CommandString        = $CommandString;
  }

  /**
   * @return string
   */
  public function getCommandString()
  {
    return $this->CommandString;
  }

  /**
   * @return string
 
 
}