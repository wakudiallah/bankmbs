<?php

namespace App\Soap\Request;

class MOMRef
{
  /**
   * @var string
   */
  protected $AppNo;
  /**
   * @var string
   */
  protected $txnCode;
  /**
   * @var string
   */
  protected $ActCode;
   /**
   * @var string
   */
  protected $usr;
   /**
   * @var string
   */
  protected $pwd;
  /**
   * @var string
   */
  protected $nm1;

  /**
   * @var string
   */
  protected $hp1;

  /**
   * @var string
   */
  protected $hm1;

  /**
   * @var string
   */
  protected $rel1;

  /**
   * @var string
   */
   protected $nm2;

  /**
   * @var string
   */
  protected $hp2;

  /**
   * @var string
   */
  protected $hm2;

  /**
   * @var string
   */
  protected $rel2;

  /**
   * @var string
   */
  
  /**
   * @var string
   */

  /**
   * GetConversionAmount constructor.
   *
   */
  public function __construct($AppNo, $txnCode, $ActCode, $usr, $pwd, $nm1, $hm1, $hp1, $rel1, $nm2, $hm2, $hp2, $rel2)
  {
    $this->AppNo        = $AppNo;
    $this->txnCode     = $txnCode;
    $this->ActCode     = $ActCode;
     $this->usr        = $usr;
    $this->pwd     = $pwd;
    $this->nm1        = $nm1;
    $this->hm1     = $hm1;
    $this->hp1     = $hp1;
    $this->rel1       = $rel1;
     $this->nm2        = $nm2;
    $this->hm2     = $hm2;
    $this->hp2     = $hp2;
    $this->rel2       = $rel2;

  }

  /**
   * @return string
   */
  public function getnm1()
  {
    return $this->nm1;
  }

  /**
   * @return string
   */
  public function gethm1()
  {
    return $this->hm1;
  }

  /**
   * @return string
   */
  public function gethp1()
  {
    return $this->hp1;
  }

  /**
   * @return string
   */
  public function getrel1()
  {
    return $this->rel1;
  }

   public function getnm2()
  {
    return $this->nm2;
  }

  /**
   * @return string
   */
  public function gethm2()
  {
    return $this->hm2;
  }

  /**
   * @return string
   */
  public function gethp2()
  {
    return $this->hp2;
  }

  /**
   * @return string
   */
  public function getrel2()
  {
    return $this->rel2;
  }

  /**
   * @return string
   */
 

  /**
   * @return string
   */
 
}