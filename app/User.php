<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'users';
    public $incrementing = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    use Notifiable;
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function Branch() {
        return $this->belongsTo('App\Model\Branch','id_branch','id');   
    }

     public function MarOfficer() {
        return $this->belongsTo('App\MarketingOfficer','id','id');   
    }

     public function Manager() {
        return $this->belongsTo('App\Manager','id','id');   
    }

    public function MO() {
        return $this->hasOne('App\Manager','id','manager_id');  
    }

      public function Officer() {
        return $this->belongsTo('App\MO','id','id');   
    }

    
}
