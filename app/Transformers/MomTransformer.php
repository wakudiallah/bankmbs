<?php

namespace App\Transformers;

use App\MO;
use League\Fractal\TransformerAbstract;

class MomTransformer extends TransformerAbstract
{
	public function transform (MO $mo)
	{
		return[
			'name' => $mo->desc_mo,
			'id_mo'=> $mo->id_mo,
			'manager'=> $mo->maanger_id,
			//'token'=> $user->api_token,
		];
	}
}