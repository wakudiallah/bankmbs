<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MarketingOfficer extends Model
{
     protected $table = 'marketing_officers'; 
     public $incrementing = false;  

    public function term()
    {
        return $this->hasMany('App\Model\Term','referral_id');	
    }
    
    public function User() {
        return $this->hasOne('App\Model\User','id');	
	}
	public function Manager() {
        return $this->hasOne('App\Manager','id','manager_id');	
	}
	public function MO() {
        return $this->hasOne('App\MO','id_mo','mo_id');	
	}
}
