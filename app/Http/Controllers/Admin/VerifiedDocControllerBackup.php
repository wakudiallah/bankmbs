<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Model\PraApplication;
use App\Model\Basic;
use App\Model\User;
use App\Model\Contact;
use App\Model\Empinfo;
use App\Model\LoanAmmount;
use App\Model\LoanCalculator;
use App\Model\LoanCalculator2;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\Document;
use App\Model\Basic_v;
use App\Model\Contact_v;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount_v;
use App\Model\Spouse_v;
use App\Model\Reference_v;
use App\Model\Financial_v;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\Log_download;
use App\Model\Settlement;

use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;

use App\Http\Controllers\Controller;
use DB;
use Input;
use Auth;
use Mail;
use Image;
Use File;
use Storage;

class VerifiedDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
	 $this->middleware('auth');	
      $this->middleware('admin');
	} 
	
    public function index()
    {
        
         
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
      
       
    }
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

    	date_default_timezone_set("Asia/Kuala_Lumpur");
        $today = date('Y-m-d H:i:s');
      
    
		    
		if($id == '2') {

			// Loan Calculator Here
			$id_praapplication = $request->input('id_praapplication');

			// DSR_A

			if($request->sector=="Government") {
					$dsr_a = Dsr_a::where('id_praapplication', $id_praapplication)
			          ->update(['sector' => $request->sector,  
			          	'gender' => $request->gender, 
			          	'age' => $request->age, 
			          	'category' => $request->category, 
			          	'existing_mbsb' => $request->existing_mbsb, 
			           	'basic_salary1' => $request->basic_salary1,
			           	'basic_salary2' => $request->basic_salary2,
			           	'basic_salary3' => $request->basic_salary3,
			           	'fixed_allowances1' => $request->fixed_allowances1,
			           	'fixed_allowances2' => $request->fixed_allowances2,
			           	'fixed_allowances3' => $request->fixed_allowances3,
			           	'total_earnings1' => $request->total_earnings1,
			           	'total_earnings2' => $request->total_earnings2,
			           	'total_earnings3' => $request->total_earnings3,
			           	'variable_income' => $request->variable_income,
			           	'gmi' => $request->gmi,
			           	'epf1' => $request->epf1,
			           	'epf2' => $request->epf2,
			           	'epf3' => $request->epf3,
			           	'socso1' => $request->socso1,
			           	'socso2' => $request->socso2,
			           	'socso3' => $request->socso3,
			           	'income_tax1' => $request->income_tax1,
			           	'income_tax2' => $request->income_tax2,
			           	'income_tax3' => $request->income_tax3,
			           	'other_contribution1' => $request->other_contribution1,
			           	'other_contribution2' => $request->other_contribution2,
			           	'other_contribution3' => $request->other_contribution3,
			           	'total_deduction1' => $request->total_deduction1,
			           	'total_deduction2' => $request->total_deduction2,
			           	'total_deduction3' => $request->total_deduction3,
			           	'nmi' => $request->nmi,
			           	'additional_income' => $request->additional_income,
			           	'ite' => $request->ite,
			           	'sec1_verified_by' => $request->sec1_verified_by,
			           	'sec1_verified_time' => $today,
			           	'total_gross_income' => $request->total_gross_income,
			           	'total_net_income' => $request->total_net_income

			           	]);


			}
			else {

				$dsr_a = Dsr_a::where('id_praapplication', $id_praapplication)
			          ->update(['sector' => $request->sector,  
			          	'gender' => $request->gender, 
			          	'age' => $request->age, 
			        
			           	'basic_salary1' => $request->basic_salary1,
			           	'basic_salary2' => $request->basic_salary2,
			           	'basic_salary3' => $request->basic_salary3,
			           	'fixed_allowances1' => $request->fixed_allowances1,
			           	'fixed_allowances2' => $request->fixed_allowances2,
			           	'fixed_allowances3' => $request->fixed_allowances3,
			           	'total_earnings1' => $request->total_earnings1,
			           	'total_earnings2' => $request->total_earnings2,
			           	'total_earnings3' => $request->total_earnings3,
			           	'variable_income' => $request->variable_income,
			           	'gmi' => $request->gmi,
			           	'epf1' => $request->epf1,
			           	'epf2' => $request->epf2,
			           	'epf3' => $request->epf3,
			           	'socso1' => $request->socso1,
			           	'socso2' => $request->socso2,
			           	'socso3' => $request->socso3,
			           	'income_tax1' => $request->income_tax1,
			           	'income_tax2' => $request->income_tax2,
			           	'income_tax3' => $request->income_tax3,
			           	'other_contribution1' => $request->other_contribution1,
			           	'other_contribution2' => $request->other_contribution2,
			           	'other_contribution3' => $request->other_contribution3,
			           	'total_deduction1' => $request->total_deduction1,
			           	'total_deduction2' => $request->total_deduction2,
			           	'total_deduction3' => $request->total_deduction3,
			           	'nmi' => $request->nmi,
			           	'additional_income' => $request->additional_income,
			           	'ite' => $request->ite,
			           	'sec1_verified_by' => $request->sec1_verified_by,
			           	'sec1_verified_time' => $request->today,
			           	'total_gross_income' => $request->total_gross_income,
			           	'total_net_income' => $request->total_net_income

			           	]);


			}
			
          	$dsr_b = Dsr_b::where('id_praapplication', $id_praapplication)
          ->update(['financing_amount' => $request->financing_amount,  
          	'tenure' => $request->tenure, 
           	'rate' => $request->rate,
           	'monthly_repayment' => $request->monthly_repayment,
           	'existing_loan' => $request->existing_loan,
           	'total_group_exposure' => $request->total_group_exposure
           	]);

          $dsr_c = Dsr_c::where('id_praapplication', $id_praapplication)
          ->update(['mbsb_housing' => $request->mbsb_housing,  
          	'mbsb_hire' => $request->mbsb_hire, 
           	'mbsb_personal' => $request->mbsb_personal,
           	'de_angkasa' => $request->de_angkasa,
           	'de_govhousing' => $request->de_govhousing,
           	'de_koperasi' => $request->de_koperasi,
           	'de_nonbiro' => $request->de_nonbiro,
           	'de_otherloan' => $request->de_otherloan,
           	'de_other' => $request->de_other,
           	'ccris_housing' => $request->ccris_housing,
           	'ccris_creditcard' => $request->ccris_creditcard,
           	'ccris_hire' => $request->ccris_hire,
           	'ccris_otherdraft' => $request->ccris_otherdraft,
           	'ccris_other' => $request->ccris_other,
           	'total_commitment' => $request->total_commitment
           	]);

          $dsr_d = Dsr_d::where('id_praapplication', $id_praapplication)
          ->update(['sec2_verified_by' => $request->sec2_verified_by,  
          	'sec2_verified_time' => $today
           	]);

           $dsr_e = Dsr_e::where('id_praapplication', $id_praapplication)
          ->update(['total_debt' => $request->total_debt,  
          	'net_income' => $request->net_income, 
           	'dsr' => $request->dsr,
           	'dsr_bnm' => $request->dsr_bnm,
           	'ndi' => $request->ndi,
           	'decision' => $request->decision,
           	'data_save' => $today
           	]);




			$name = $request->input('name');
	
			$basic = Basic::where('id_praapplication', $id_praapplication)
			->update(['name' => $name, 'new_ic' => $request->new_ic]);

			$loan_amount = $request->input('loan_amount');

           $loanammount = loanAmmount::where('id_praapplication', $id_praapplication)
          ->update(['loanammount' => $loan_amount ]);

          if($request->input('id_tenure_hidden')==0) {
          	$loanammount = loanAmmount::where('id_praapplication', $id_praapplication)
          ->update(['id_tenure' => '20' ]);
          }




		
		}
		else if($id == '3') {

			// Loan Calculator Here
			$id_praapplication = $request->input('id_praapplication');
			
			$expire1 = str_replace('/', '-', $request->input('expire1'));
			$expire1_r =  date('Y-m-d', strtotime($expire1));
			$expire2 = str_replace('/', '-', $request->input('expire2'));
			$expire2_r =  date('Y-m-d', strtotime($expire2));
			$expire3 = str_replace('/', '-', $request->input('expire3'));
			$expire3_r =  date('Y-m-d', strtotime($expire3));
			$expire4 = str_replace('/', '-', $request->input('expire4'));
			$expire4_r =  date('Y-m-d', strtotime($expire4));
			
		$riskrating = RiskRating::where('id_praapplication', $id_praapplication)
          ->update(['salary_deduction' => $request->rr_salary_deduction,  
          	'age' => $request->rr_age, 
           	'education_level' => $request->rr_edu_level,
           	'employment' => $request->rr_emp,
           	'position' => $request->rr_position,
           	'marital_status' => $request->rr_marital,
           	'spouse_employment' => $request->rr_spouse_emp,
           	'property_loan' => $request->rr_property_loan,
           	'adverse_ccris' => $request->rr_adverse_ccris,
           	'total_score' => $request->total_score,
           	'grading' => $request->grading,
           	'decision' => $request->final_decision,
           	'scoring_by' => Auth::user()->name,
           	'date_scoring' => $today,
           	'reviewed_by' => Auth::user()->name,
           	'date_reviewed' => $today
           	]);
		  
			
		
		}
	}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function upload(Request $request,$id) {
		$user = Auth::user();
		$id_praapplication = $request->input('id_praapplication');
		$pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
		$nametest  = $pra->id;
		if ($request->hasFile('file'.$id)) {
			$name = $request->input('document'.$id);
			$file = $request->file('file'.$id);
			$tipe_file   = $_FILES['file'.$id]['type'];
			if($tipe_file == "application/pdf" || $tipe_file == "image/jpeg" || $tipe_file == "image/png" || $tipe_file == "image/bmp" || $tipe_file == "image/gif") {
				$filename = str_random(25).'-'.$name.'-'.$file->getClientOriginalName();
				//Storage::disk('ftp')->put($nametest, fopen($request->file('file'), 'r+'));
				 if($tipe_file != "application/pdf") {

                    if(!(File::exists('storage/uploads/file/'.$nametest.'/'))) {
                        $create_folder =  File::makeDirectory('storage/uploads/file/'.$nametest.'/', 0775);
                    }

                      $img = Image::make($request->file('file'.$id));

                      $path = public_path('storage/uploads/file/'.$nametest.'/'. $filename);
                 
                      $img->resize(800, null, function ($constraint) {
                          $constraint->aspectRatio();
                      })->encode('jpg', 25)->save($path);
                 }
                 else {
                    $destinationPath = 'storage/uploads/file/'.$nametest.'/';
                    $file->move($destinationPath, $filename);
                 }
				$document = new Document;
				$document->name = $name;
				$document->type = $id;
				$document->upload = $filename; 
				$document->id_praapplication = $id_praapplication; 
				$document->save();
				
				return response()->json(['file' => "$filename"]);
			}
		}  
    }

	public function Term(Request $request) {

          $id_praapplication = $request->input('id_praapplication');
          $status = $request->input('status');
          $verification_remark = $request->input('remark');
		  $branch = $request->input('branch');
          $id = $id_praapplication;
          $user = Auth::user();
          $user_aktif = Auth::user()->name;


          
      
		 if ($status==77) {
			    // Set Time Log
		  		date_default_timezone_set("Asia/Kuala_Lumpur");
				$time_name = date('Ymd');
				$time_log = date('Y-m-d H:i:s');
				$log_download = new Log_download;
				
				$user = Auth::user();
				$id_user =  $user->id;
				$log_download->id_praapplication   =  $id;
				$log_download->id_user   =  $id_user;
				$log_download->Activity   =  'DSR PASS';
				$log_download->downloaded_at   =  $time_log;
				$log_download->log_remark   =  $verification_remark;
				// $log_download->log_reason  =  $reason;
				$log_download->save();
				$hasil = "DSR PASS";
				$terms = Term::where('id_praapplication', $id_praapplication)->limit('1')->first();
				if($terms->referral_id=='0'){
					$term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '77', 'verification_remark' => $verification_remark]);

					$cust = PraApplication::latest('created_at')->where('id',  $id_praapplication )->limit('1')->first();
     
		      		$email_cust = $cust->email;
		      		$name_cust = $cust->fullname;
		       		
		       		Mail::send('mail.cust_resultdocument', compact('email_cust','name_cust'), function ($message) use ($email_cust, $name_cust) {
				       $message->from('mbsb@netxpert.com.my', 'MBSB Personal Financing-i');
				       $message->subject('[MBSB] Document Approved');
				       $message->to($email_cust, $name_cust);
				   });
				}
				else{
					$term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '77', 'verification_remark' => $verification_remark]);
				}

				
            	

          }
		  else if ($status==88) {
			    // Set Time Log
		  		date_default_timezone_set("Asia/Kuala_Lumpur");
				$time_name = date('Ymd');
				$time_log = date('Y-m-d H:i:s');
				$log_download = new Log_download;
				
				$user = Auth::user();
				$id_user =  $user->id;
				$log_download->id_praapplication   =  $id;
				$log_download->id_user   =  $id_user;
				$log_download->Activity   =  'DSR REJECT';
				$log_download->downloaded_at   =  $time_log;
				$log_download->log_remark   =  $verification_remark;
				// $log_download->log_reason  =  $reason;
				$log_download->save();
				$hasil = "DSR REJECT";
				
            	$term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '88', 'verification_remark' => $verification_remark]);

            	if($term->referral_id=='0'){
	            	$cust = PraApplication::latest('created_at')->where('id',  $id_praapplication )->limit('1')->first();
		      		$email_cust = $cust->email;
		      		$name_cust = $cust->name;
		       		
		       		Mail::send('mail.cust_resultdocument_reject', compact('email_cust','name_cust'), function ($message) use ($email_cust, $name_cust) {
				       $message->from('mbsb@netxpert.com.my', 'MBSB Personal Financing-i');
				       $message->subject('[MBSB] Document Approved');
				       $message->to($email_cust, $name_cust);
				   });
		       	}
           
          }
          		$basic = Basic::latest('created_at')->where('id_praapplication',$id_praapplication)->limit('1')->first();
          		$pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();

          		
		 return response()->json(['status' => $hasil]);
	
     
          
    }	
}

