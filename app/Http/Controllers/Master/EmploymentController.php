<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Employment;
use App\Model\Package;
use App\ScoreRating;
use App\ScoreCard;
use Auth;
use Alert;
class EmploymentController extends Controller
{
     public function index()
    {
        $user = Auth::user();
        if ($user->role<>4) {
            return redirect('admin');   
        }
        $emp = Employment::where('status','1')->get();
        $package = Package::where('status','1')->get();
        return view('admin.master.emp', compact('emp','user','package')); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $name  	  = $request->input('name');
        $package  = $request->input('package');

        $id = Auth::user()->id;
 
        $emp             = new Employment ;
        $emp->name       = $name;
        $emp->package_id = $package;
        $emp->create_by  = $id;
   
        $emp->save();
    	Alert::success('Data added successfuly');
        return redirect('admin/master/job-sector')->with('message', 'New Package Successfully Added');
    }

    public function savescorerating(Request $request)
    {
       
        $sector  = $request->input('sector');

        $id = Auth::user()->id;
 
        $sector                 = new ScoreRating ;
        $sector->sector           = $request->input('sector');
        $sector->variable      = $request->input('variable');
        $sector->float_value = $request->input('float1');
        $sector->float_value2 = $request->input('float2');
        $sector->value      = $request->input('value');
        $sector->score      = $request->input('score');
   
        $sector->save();
    
        return redirect('admin/master/scorerating')->with('message', 'New Score Rating Successfully Added');
    }
     public function savescorecard(Request $request)
    {
       

        $id = Auth::user()->id;
 
        $sector                 = new ScoreCard ;
        $sector->grade           = $request->input('grade');
        $sector->score      = $request->input('score');
        $sector->score2 = $request->input('score2');
        $sector->decision = $request->input('decision');
   
        $sector->save();
    
        return redirect('admin/master/scorecard')->with('message', 'New Score Card Successfully Added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_scorerating(Request $request, $id)
    {
        $sector = ScoreRating::find($id);
        $sector->sector           = $request->input('sector');
        $sector->variable      = $request->input('variable');
        $sector->float_value = $request->input('float1');
        $sector->float_value2 = $request->input('float2');
        $sector->value      = $request->input('value');
        $sector->score      = $request->input('score');
        $sector->save();
        
        return redirect('admin/master/scorerating')->with('message', 'Score Rating Successfully Updated');
    }

     public function update_scorecard(Request $request, $id)
    {
        $sector = ScoreCard::find($id);
        $sector->grade           = $request->input('grade');
        $sector->score      = $request->input('score');
        $sector->score2 = $request->input('score2');
        $sector->decision = $request->input('decision');
        $sector->save();
        
        return redirect('admin/master/scorecard')->with('message', 'Score Card Successfully Updated');
    }

    public function update(Request $request, $id)
    {
        $package = Package::find($id);
        $package->name           = $request->input('name');
        $package->flat_rate      = $request->input('flat_rate');
        $package->effective_rate = $request->input('effective_rate');
        $package->save();
        
        return redirect('admin/master/package')->with('message', 'Package Successfully Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
         $ids           = $request->input('id');
        $package = Package::where('id',$ids)->delete();   
                        
        return redirect('/admin/master/package')->with(['delete' => 'Package successfully deleted']);
    }
}
