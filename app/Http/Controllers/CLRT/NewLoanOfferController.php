<?php

namespace App\Http\Controllers\CLRT;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\MO;
use App\Manager;
use App\Model\AntiAttrition;
use App\Model\SettlementInfo;
use App\Model\Address;
use App\Model\PraApplication;
use App\Model\State;
use App\User;
use App\JobRun;
use App\Http\Requests;
use Auth;
use DB;

class NewLoanOfferController extends Controller
{
    public function newloanoffer()
    {
        $user           = Auth::user();
        $jobrun         = JobRun::get();
        $state          = State::get();
        //$antiattrition  = AntiAttrition::where('mo_id', $user->mo_id)->orderBy('created_at','DESC')->get();
		return view('clrt.newloanoffer', compact('user','antiattrition','jobrun','state')); 	  
    }

    public function newloanoffer_view(Request $request)
    {
        $user       = Auth::user();

        $task       = $request->input('task');
        $jobsector  = $request->input('jobsector');
        $net1       = $request->input('net1');
        $net2       = $request->input('net2');
        $name_state = $request->input('state');
        $state      = State::get();
          
        $jobrun     = JobRun::get();
        $newloanoffer = AntiAttrition::where('TaskID',$task)->where('CustWrkSec',$jobsector)->where('NetProceed','>=',$net1)->where('NetProceed','<=',$net2)->where('State','=',$name_state)->orderBy('created_at', 'asc')->groupby('CustIDNo')->get();
         
        return view('clrt.newloanoffer_view', compact('user','antiattrition','jobrun','newloanoffer','state','name_state'));
    }

    public function detail_loan_offer($id)
    {
         $user = Auth::user();
         $detail = AntiAttrition::where('ACID',$id)->limit('1')->first();
         $settlement = SettlementInfo::where('ACID2',$id)->limit('1')->first();
         $loan_offer = AntiAttrition::where('ACID',$id)->get();
         $address = Address::where('Acid',$id)->get();
        $mo = MO::get();
        $state = $detail->States->state_code;
        $manager = Manager::get();
        return view('clrt.newloanoffer_detail', compact('user','antiattrition','jobrun','detail','loan_offer','settlement','address','mo','manager','id','state'));
    }

     public function detail_loan_offer_sms($id)
    {
         $user = Auth::user();
         $detail = AntiAttrition::where('ACID',$id)->limit('1')->first();
         $settlement = SettlementInfo::where('ACID2',$id)->limit('1')->first();
         $loan_offer = AntiAttrition::where('ACID',$id)->get();
         $address = Address::where('Acid',$id)->get();
         $praapplication = PraApplication::where('id',$id);
        $mo = MO::get();
        $manager = Manager::get();
        return view('clrt.newloanoffer_detail_sms', compact('user','antiattrition','jobrun','detail','loan_offer','settlement','address','mo','manager','id'));
    }

     public function detail_loan_offer_assigned($id)
    {
         $user = Auth::user();
         $detail = AntiAttrition::where('ACID',$id)->limit('1')->first();
         $settlement = SettlementInfo::where('ACID2',$id)->limit('1')->first();
         $loan_offer = AntiAttrition::where('ACID',$id)->get();
         $address = Address::where('Acid',$id)->get();
         $praapplication = PraApplication::where('id',$id);
        $mo = MO::get();
        $manager = Manager::get();
        return view('clrt.newloanoffer_assign', compact('user','antiattrition','jobrun','detail','loan_offer','settlement','address','mo','manager','id'));
    }

     public function select_tenure($id)
    {
         $user = Auth::user();
         $detail = AntiAttrition::where('ACID',$id)->limit('1')->first();
         $settlement = SettlementInfo::where('ACID2',$id)->limit('1')->first();
         $loan_offer = AntiAttrition::where('ACID',$id)->get();
         $address = Address::where('Acid',$id)->get();
         $praapplication = PraApplication::where('id',$id);
        $mo = MO::get();
        $manager = Manager::get();
        return view('clrt.select_tenure', compact('user','antiattrition','jobrun','detail','loan_offer','settlement','address','mo','manager','id'));
    }

     public function otp_timeout($id)
    {
         $user = Auth::user();
         $detail = AntiAttrition::where('ACID',$id)->limit('1')->first();
         $settlement = SettlementInfo::where('ACID2',$id)->limit('1')->first();
         $loan_offer = AntiAttrition::where('ACID',$id)->get();
         $address = Address::where('Acid',$id)->get();
          $mo = MO::get();
        $manager = Manager::get();
        return view('clrt.newloanoffer_timeout', compact('user','antiattrition','jobrun','detail','loan_offer','settlement','address','mo','manager','id'));
    }

     public function request_ramcy($id)
    {
         $user = Auth::user();
         $detail = AntiAttrition::where('ACID',$id)->limit('1')->first();
         $settlement = SettlementInfo::where('ACID2',$id)->limit('1')->first();
         $loan_offer = AntiAttrition::where('ACID',$id)->get();
         $address = Address::where('Acid',$id)->get();
          $mo = MO::get();
        $manager = Manager::get();
        return view('clrt.newloanoffer_ramcy', compact('user','antiattrition','jobrun','detail','loan_offer','settlement','address','mo','manager','id'));
    }

     public function multi()
    {
        $user = Auth::user();
        $mo = MO::get();
        $manager = Manager::get();
        //$antiattrition = AntiAttrition::where('mo_id', $user->mo_id)->orderBy('created_at','DESC')->get();
        $antiattrition = AntiAttrition::wherenull('mo_id')->orderBy('created_at','DESC')->get();
        $newoffer = SettlementInfo::where('status','0')->orderBy('created_at','DESC')->get();
        
		return view('mo.clrt.multi', compact('user','antiattrition','mo','manager','newoffer')); 	  
    }

   

    /**
     * Get Ajax Request and restun Data
     *
     * @return \Illuminate\Http\Response
     */
    public function myformAjax($id)
    {
        $cities = DB::table("mo")
                    ->where("id_manager",$id)
                    ->pluck("desc_mo","id_mo");
        return json_encode($cities);
    }

    public function assign_to_mo(Request $request){
        $user = Auth::user();
        $new = array_map(null, $request->CustIDNo, $request->ci);

        foreach($new as $new) {

             $anti = AntiAttrition::where('CustIDNo',$new[0])->update([
                "mo_id"       => $request->city,
                "assign_by"   => $user->id

            ]);
        }

        return redirect('/anti-attrition-multi');
    }
}
