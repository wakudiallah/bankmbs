<?php

namespace App\Http\Controllers\LoanForm;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Model\PraApplication;
use App\Model\Basic;
use App\Model\Contact;
use App\Model\Empinfo;
use App\Model\LoanAmmount;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\i_financing;
use App\Model\Document;
use App\Model\Basic_v;
use App\Model\Contact_v;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount_v;
use App\Model\Spouse_v;
use App\Model\Reference_v;
use App\Model\Financial_v;
use App\Model\i_financing_v;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\Log_download;
use App\Model\Settlement;
use App\Model\Credit;
use App\Model\Pep;
use App\Mail_System;
use App\Dsr_b;

use App\Http\Controllers\Controller;
use DB;
use Input;
use Auth;
use Mail;
use Image;
use File;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\Foreigner;
use App\Model\Subscription;

class LoanFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
        
      
       
    }
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         if($id == '1') {
        $id_praapplication = $request->input('id_praapplication');
        $country = $request->input('country');
        $country_others = $request->input('country_others');
        $country_origin = $request->input('country_origin');
        $country_dob = $request->input('country_dob');
        $citizen = $request->input('citizen');
        $dob2 = $request->input('dob');
        $dob =  date('Y-m-d', strtotime($dob2));
        $name = $request->input('name');
        $title = $request->input('title');
        $title_others = $request->input('title_others');
        $gender = $request->input('gender');
        $marital = $request->input('marital');
        $dependents = $request->input('dependents');
        $race = $request->input('race');
        $ownership = $request->input('ownership');
        $education = $request->input('education');
        $bumiputera = $request->input('bumiputera');
        $nationality = $request->input('nationality');
         $resident = $request->input('resident');


        $race_others = $request->input('race_others');
        $religion = $request->input('religion');
        $religion_others = $request->input('religion_others');

        // address line
        $address  = $request->input('address');
        $address2 = $request->input('address2');
        $address3 = $request->input('address3');
        $postcode = $request->input('postcode');
        $state    = $request->input('state');
        $state_code    = $request->input('state_code');
         $corres_state2 = $request->input('corres_state2');
         $state_dob = $request->input('state_dob');



        $police_number = $request->input('police_number');

              $basic = Basic::where('id_praapplication', $id_praapplication)
      ->update(['country' => $country, 'country_others' => $country_others,  'country_origin' => $country_origin, 'country_dob' => $country_dob,  'citizen' => $citizen, 'name' => $name, 'title' => $title, 'title_others' => $title_others, 'gender' => $gender, 'marital' => $marital, 'dependents' => $dependents, 'ownership' => $ownership, 'race' => $race, 'race_others' => $race_others   , 'religion' => $religion , 'religion_others' => $religion_others ,'address' => $address , 'address2' => $address2 , 'address3' => $address3 ,'police_number' => $police_number , 'postcode'=>$postcode,'state'=>$state, 'education'=>$education,'bumiputera'=>$bumiputera,'nationality'=>$nationality,'corres_state1'=> $corres_state2,'state_dob'=>$state_dob,'state_code'=>$state_code,'resident'=>$resident]);
    }
      
      else if($id == '2') {


        $id_praapplication = $request->input('id_praapplication');
        $empname = $request->input('emp_name');
        $employment_id = $request->input('employment_id');
        $employer_id = $request->input('employer_id');
         // address line
        $address = $request->input('address');
        $address2 = $request->input('address2');
        $address3 = $request->input('address3');
        $office_phone = $request->input('office_phone');

        $joined2 =  $request->input('joined');
        $joined2 = str_replace('/', '-', $joined2);
        $joined =  date('Y-m-d', strtotime($joined2));
        $position = $request->input('position');
        $occupation = $request->input('occupation');
        $empstatus = $request->input('empstatus');
        $nature_business = $request->input('nature_business');
         $postcode = $request->input('postcode3');
        $state    = $request->input('state3');
        $state3 =  $request->input('state_code3');

         $mbsb_staff = $request->input('mbsb_staff');
        $staff_no = $request->input('staff_no');
              $empinfo = Empinfo::where('id_praapplication', $id_praapplication)
      ->update(['empname' => $empname,  'address' => $address, 'address2' => $address2 , 'address3' => $address3, 'joined' => $joined, 'position' => $position, 'occupation' => $occupation,  'empstatus' => $empstatus ,  'office_phone' => $office_phone, 'postcode'=> $postcode, 'state'=>$state,'nature_business'=>$nature_business, 'state_code'=>$state3, 'mbsb_staff' => $mbsb_staff, 'staff_no' => $staff_no]);

    }

    else if($id == '3') {


        $id_praapplication = $request->input('id_praapplication');
        //spouse
        $name = $request->input('name');
          $homephone =  $request->input('homephone');
            $mobilephone = $request->input('mobilephone');
        $emptype =  $request->input('emptype');
         $emptype_others = $request->input('emptype_others');

              $spouse = Spouse::where('id_praapplication', $id_praapplication)
      ->update(['name' => $name,  'emptype' => $emptype , 'homephone' => $homephone, 'mobilephone' => $mobilephone,  'emptype_others' => $emptype_others]);

        //reference
        $name1 = $request->input('name1');
        $mobilephone1 =  $request->input('mobilephone1');
        $home_phone1 =  $request->input('home_phone1');
        $relationship1 =  $request->input('relationship1');
        $name2 = $request->input('name2');
        $mobilephone2 =  $request->input('mobilephone2');
         $home_phone2 =  $request->input('home_phone2');
        $relationship2 =  $request->input('relationship2');
     

        $reference = Reference::where('id_praapplication', $id_praapplication)->update(['name1' => $name1, 'mobilephone1' => $mobilephone1, 'relationship1' => $relationship1, 'name2' => $name2, 'mobilephone2' => $mobilephone2, 'relationship2' => $relationship2, 'home_phone1' => $home_phone1,'home_phone2' => $home_phone2]);


        
    }

     else if($id == '4') {
  
  
          $id_praapplication = $request->input('id_praapplication');
          $income = $request->input('income');
          $other_income =  $request->input('other_income');      
          $total_income = $request->input('total_income');
         
                $financial = Financial::where('id_praapplication', $id_praapplication)
        ->update(['income' => $income,  'other_income' => $other_income, 'total_income' => $total_income ]);
  
      }
   
        else if($id == '5') {

        $id_praapplication = $request->input('id_praapplication');
        $package = $request->input('package');
        $loanammount = $request->input('loanammount');
        $maxloan = $request->input('maxloan');
        $method = $request->input('method');
        $id_tenure =  $request->input('id_tenure');
          $memberno =  $request->input('memberno');
     
              //$loanammount = loanammount::where('id_praapplication', $id_praapplication)
      //->update(['package' => $package,  'id_tenure' => $id_tenure]);

    }

     
      

      else if($id == '5') {


        $id_praapplication = $request->input('id_praapplication');
        $loanammount = $request->input('loanammount');
        $id_tenure =  $request->input('tenure');

        
     
              $loanammount = loanammount::where('id_praapplication', $id_praapplication)
      ->update([  'id_tenure' => $id_tenure ]);
    
        
    }


     else if($id == '6') {


        $id_praapplication = $request->input('id_praapplication');
        $bank1 = $request->input('bank1');
        $bank2 = $request->input('bank2');
        $bank3 = $request->input('bank3');
        $bank4 = $request->input('bank4');
        $bank5 = $request->input('bank5');
        $bank6 = $request->input('bank6');
       
     
              $i_financing = I_financing::where('id_praapplication', $id_praapplication)
      ->update(['bank1' => $bank1,  'bank2' => $bank2, 'bank3' => $bank3, 
        'bank4' => $bank4, 'bank5' => $bank5, 'bank6' => $bank6 ]);

    }

    else if($id == '99') {


        $id_praapplication = $request->input('id_praapplication');
        $loanamount = $request->input('loanamount');
        $id_tenure = $request->input('tenure');
        $maxloan = $request->input('maxloan');
        $package = $request->input('package');
        $rate = $request->input('interests');
        $installment = $request->input('installment');
       
     $date = date('Y-m-d H:i:s');
      $lat = $request->input('lat');
                  $lng = $request->input('lng');
                  $location = $request->input('location');

              $loanammount = LoanAmmount::where('id_praapplication', $id_praapplication)
      ->update(['id_tenure' => $id_tenure, 'maxloan' => $maxloan, 
        'package' => $package,'rate' => $rate,'installment' => $installment]);
       $terma = Term::where('id_praapplication', $id_praapplication)->first();

       $ref_id = $terma->referral_id;

       $dsr = Dsr_b::where('id_praapplication', $id_praapplication)
      ->update(['financing_amount' => $loanamount, 'tenure' => $id_tenure, 
       'rate' => $rate,'monthly_repayment' => $installment]);

      if($ref_id=='0'){
        
       $term = Term::where('id_praapplication', $id_praapplication)
       ->update(['file_created' => $date,'lat'=> $lat,'lng' => $lng,'location' => $location,'mo_stage'=>'1','status'=>'0']);
      }
      
      else{
         $term = Term::where('id_praapplication', $id_praapplication)
       ->update(['file_created' => $date,'lat'=> $lat,'lng' => $lng,'location' => $location,'mo_stage'=>'1','status'=>'1']);
      }

      

       return response()->json(['status' =>200, 'id_tenure' => $id_tenure ,'package' => $package,'rate' => $rate,'installment' => $installment,'financing_amount' => $loanamount]);


       /* $id_praapplication = $request->input('id_praapplication');
        $loanamount = $request->input('loanamount');
        $id_tenure = $request->input('tenure');
        $maxloan = $request->input('maxloan');
        $package = $request->input('package');
        $rate = $request->input('rate');
        $installment = $request->input('installment');
       
     
              $loanammount = LoanAmmount::where('id_praapplication', $id_praapplication)
      ->update(['id_tenure' => $id_tenure, 'maxloan' => $maxloan, 
        'package' => $package,'rate' => $rate,'installment' => $installment]);

      $dsr = Dsr_b::where('id_praapplication', $id_praapplication)
      ->update(['financing_amount' => $loanamount, 'tenure' => $id_tenure, 
       'rate' => $rate,'monthly_repayment' => $installment]);

       return response()->json(['status' =>200, 'id_tenure' => $id_tenure ,'package' => $package,'rate' => $rate,'installment' => $installment,'financing_amount' => $loanamount]);*/

    }
   
         

    
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
 public function upload(Request $request,$id)
    {
        $user_id            = $request->input('user_id');
        $id_praapplication  = $request->input('id_praapplication');
        $pra                = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
        $nametest           = $pra->id;
        
       
        $ic = $pra->ic_number;
        $ic_number2 =  str_replace('/', '', $ic);

        if ($request->hasFile('file'.$id)) {
            $name = $request->input('document'.$id);
            $file = $request->file('file'.$id);
            $tipe_file   = $_FILES['file'.$id]['type'];
                //if($tipe_file == "application/pdf") {

            $base           = base64_encode(file_get_contents($request->file('file'.$id)));
            $filename       = str_random(15).'-'.$name.'-'.$file->getClientOriginalName();
            $destinationPath = 'storage/uploads/file/'.$nametest.'/';
            $file->move($destinationPath, $filename);

                
        $document = new Document;
        $document->name = $name;
        $document->type = $id;
        $document->upload = $filename; 
        $document->id_praapplication = $id_praapplication; 
        $document->base64 = $base; 
        $document->save();

        return response()->json(['file' => "$filename"]);


              // }
            

                }  
    }
    public function uploads(Request $request,$id)
    {
                  $user = Auth::user();
                 $id_praapplication = $request->input('id_praapplication');

                   $pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
      
                   $nametest  = $pra->id;

                 
                  if ($request->hasFile('file'.$id)) {
                  $name = $request->input('document'.$id);
                 $file = $request->file('file'.$id);
                $tipe_file   = $_FILES['file'.$id]['type'];
                if($tipe_file == "application/pdf" || $tipe_file == "image/jpeg" || $tipe_file == "image/png" || $tipe_file == "image/bmp" || $tipe_file == "image/gif") {


                 $filename = str_random(25).'-'.$name.'-'.$file->getClientOriginalName();

                 if($tipe_file != "application/pdf") {

                    if(!(File::exists('storage/uploads/file/'.$nametest.'/'))) {
                        $create_folder =  File::makeDirectory('storage/uploads/file/'.$nametest.'/', 0777);
                    }

                      $img = Image::make($request->file('file'.$id));

                      $path = public_path('storage/uploads/file/'.$nametest.'/'. $filename);
                 
                      $img->resize(800, null, function ($constraint) {
                          $constraint->aspectRatio();
                      })->encode('jpg', 25)->save($path);
                 }
                 else {
                    $destinationPath = 'storage/uploads/file/'.$nametest.'/';
                    $file->move($destinationPath, $filename);
                 }
                


                  
                $document = new Document;
                $document->name = $name;
                $document->type = $id;
                $document->upload = $filename; 
                $document->id_praapplication = $id_praapplication; 
                $document->save();
                 return response()->json(['file' => "$filename"]);

               }
            

                }  
    }
    
     public function term(Request $request) {


          $basic_v = new Basic_v ;
          $contact_v = new Contact_v ;
          $empinfo_v = new Empinfo_v ;
          $loanammount_v = new LoanAmmount_v ;
          $spouse_v = new Spouse_v ;
       

          $reference_v = new Reference_v ;
          $financial_v = new Financial_v ;      
          $id_praapplication = $request->input('id_praapplication');
          $id = $id_praapplication;

      // Cek apakah aplikasi baru submit atau aplikasi lama a.k.a route back
      $cek = Log_download::where('id_praapplication', $id)->where('activity', 'Application Submitted')->get();
    
      if(count($cek)=='0') {  // Jika Aplikasi baru submit
          date_default_timezone_set("Asia/Kuala_Lumpur");
        $file_created_time = date('Y-m-d H:i:s');
        
        $term = Term::where('id_praapplication', $id_praapplication)
        ->update(['status' => '99', 
          'verification_result' => '0',
          'purchase_application' => $request->input('purchase_application'),
          'appointment_mbsb' => $request->input('appointment_mbsb'),
          'credit_transactions' => $request->input('credit_transactions'),
          'consent_for' => $request->input('consent_for'),
          'high_networth' => $request->input('high_networth'),
          'politically' => $request->input('politically'),
          'for_goods' => $request->input('for_goods'),
          'product_disclosure' => $request->input('product_disclosure'),
          ]);

        // cek kredit
        $count_credit = Credit::where('id_praapplication', $id_praapplication)->count();
        if($count_credit<1) {
          $credit = new Credit ;
          $credit->fullname = $request->input('fullname');
          $credit->mykad =  $request->input('mykad');
          $credit->passport =  $request->input('passport');
          $credit->relationship =  $request->input('relationship');
          $credit->id_praapplication = $id_praapplication;
          $credit->save();
        }
        else {
          $credit = Credit::where('id_praapplication', $id_praapplication)->update(['fullname' => $request->input('fullname'), 
              'mykad' => $request->input('mykad'),
              'passport' => $request->input('passport'),
              'relationship' => $request->input('relationship')]);
        }

        // cek pep
        $count_pep = Pep::where('id_praapplication', $id_praapplication)->count();
        if($count_pep<1) {
          $pep = new Pep ;
          $pep->name1 = $request->input('name1');
          $pep->relationship1 =  $request->input('relationship1');
          $pep->status1 =  $request->input('status1');
          $pep->prominent1 =  $request->input('prominent1');
          //2
          $pep->name2 = $request->input('name2');
          $pep->relationship2 =  $request->input('relationship2');
          $pep->status2 =  $request->input('status2');
          $pep->prominent2 =  $request->input('prominent2');
          //3
          $pep->name3 = $request->input('name3');
          $pep->relationship3 =  $request->input('relationship3');
          $pep->status3 =  $request->input('status3');
          $pep->prominent3 =  $request->input('prominent3');

          $pep->id_praapplication = $id_praapplication;
          $pep->save();
        }
        else {
          $pep = Pep::where('id_praapplication', $id_praapplication)->update(['name1' => $request->input('name1'), 
              'relationship1' => $request->input('relationship1'),
              'status1' => $request->input('status1'),
              'prominent1' => $request->input('prominent1'),
              'name2' => $request->input('name2'), 
              'relationship2' => $request->input('relationship2'),
              'status2' => $request->input('status2'),
              'prominent2' => $request->input('prominent2'),
              'name3' => $request->input('name3'), 
              'relationship3' => $request->input('relationship3'),
              'status3' => $request->input('status3'),
              'prominent3' => $request->input('prominent3'),
              'id_praapplication' => $request->input('id_praapplication')

              ]);
        }

         

        // Set Time Log
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Application Submitted';
        $log_download->downloaded_at   =  $file_created_time;
        //$log_download->log_remark   =  $remark_admin;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
            
        $contact_v->id_praapplication   =  $id;
        $basic_v->id_praapplication   =  $id;
        $empinfo_v->id_praapplication   =  $id;
        $loanammount_v->id_praapplication   =  $id;
        $spouse_v->id_praapplication   =  $id;
          
        $reference_v->id_praapplication   =  $id;
        $financial_v->id_praapplication   =  $id;
        $contact_v->save();
        $basic_v->save();
        $empinfo_v->save();
        $loanammount_v->save();
        $spouse_v->save();
               
        $reference_v->save();
        $financial_v->save();
 
       /* $email = "novrizaleka@gmail.com";
        $fullname = "Personal Loan Co-Opbank Persatuan";
          // Send to Admin Email
          
          Mail::send('mail.notification_application_submitted', compact('id'), function ($message) use ($email, $fullname) {
          $message->from('mbsb@ezlestari.com.my', 'novrizaleka@gmail,com');
          $message->subject('New Application Submitted');
          $message->to($email, $fullname);
        
          });
        
          // Email ke bos
          $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id )->limit('1')->first();
          $jumlahpinjam = $loanammount->loanammount;
          $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id )->limit('1')->first();
          $majikan = $empinfo->empname;
          $basic = Basic::latest('created_at')->where('id_praapplication',  $id )->limit('1')->first();
          $nama = $basic->name;
          $ic = $basic->new_ic;
          Mail::send('mail.notification_application_submitted_boss', compact('id','nama','ic','majikan','jumlahpinjam'), function ($message) use ($email, $fullname) {
          $message->from('novrizaleka@gmail.com', 'novrizaleka@gmail.com');
          $message->subject('Stage 2 - New Application Submitted');
          $message->to('novrizaleka@gmail.com', 'Che Onn Ismail');
        
          }); */
        
      }
      else {
        
          $term = Term::where('id_praapplication', $id_praapplication)
          ->update(['status' => '99', 
            'verification_result' => '0',
            'purchase_application' => $request->input('purchase_application'),
            'appointment_mbsb' => $request->input('appointment_mbsb'),
            'credit_transactions' => $request->input('credit_transactions'),
            'consent_for' => $request->input('consent_for'),
            'high_networth' => $request->input('high_networth'),
            'politically' => $request->input('politically'),
            'for_goods' => $request->input('for_goods'),
            'product_disclosure' => $request->input('product_disclosure')

            ]);

            // cek kredit
            $count_credit = Credit::where('id_praapplication', $id_praapplication)->count();
            if($count_credit<1) {
              $credit = new Credit ;
              $credit->fullname = $request->input('fullname');
              $credit->mykad =  $request->input('mykad');
              $credit->passport =  $request->input('passport');
              $credit->relationship =  $request->input('relationship');
              $credit->id_praapplication = $id_praapplication;
              $credit->save();
            }
            else {
               $credit = Credit::where('id_praapplication', $id_praapplication)->update(['fullname' => $request->input('fullname'), 
              'mykad' => $request->input('mykad'),
              'passport' => $request->input('passport'),
              'relationship' => $request->input('relationship')]);
            }

            // cek pep
            $count_pep = Pep::where('id_praapplication', $id_praapplication)->count();
            if($count_pep<1) {
              $pep = new Pep ;
              $pep->name1 = $request->input('name1');
              $pep->relationship1 =  $request->input('relationship1');
              $pep->status1 =  $request->input('status1');
              $pep->prominent1 =  $request->input('prominent1');
              //2
              $pep->name2 = $request->input('name2');
              $pep->relationship2 =  $request->input('relationship2');
              $pep->status2 =  $request->input('status2');
              $pep->prominent2 =  $request->input('prominent2');
              //3
              $pep->name3 = $request->input('name3');
              $pep->relationship3 =  $request->input('relationship3');
              $pep->status3 =  $request->input('status3');
              $pep->prominent3 =  $request->input('prominent3');

              $pep->id_praapplication = $id_praapplication;
              $pep->save();
            }
            else {
              $pep = Pep::where('id_praapplication', $id_praapplication)->update(['name1' => $request->input('name1'), 
                  'relationship1' => $request->input('relationship1'),
                  'status1' => $request->input('status1'),
                  'prominent1' => $request->input('prominent1'),
                  'name2' => $request->input('name2'), 
                  'relationship2' => $request->input('relationship2'),
                  'status2' => $request->input('status2'),
                  'prominent2' => $request->input('prominent2'),
                  'name3' => $request->input('name3'), 
                  'relationship3' => $request->input('relationship3'),
                  'status3' => $request->input('status3'),
                  'prominent3' => $request->input('prominent3'),
                  'id_praapplication' => $request->input('id_praapplication')

                  ]);
            }

            $subs = Subscription::where('id_praapplication', $id_praapplication)->update(['sub_wealth' => $request->input('sub_wealth'), 
              'gpa' => $request->input('gpa'),
               'hasanah' => $request->input('hasanah'),
              'annur' => $request->input('annur'),
              'wasiat' => $request->input('wasiat')]);

         $foreigner = Foreigner::where('id_praapplication', $id_praapplication)->update(['foreign1' => $request->input('foreigner1'), 
              'foreign2' => $request->input('foreigner1'),
              'foreign3' => $request->input('foreigner2'),
              'country' => $request->input('for_country'),
              'foreign4' => $request->input('foreigner4')]);

           // Set Time Log
            /*date_default_timezone_set("Asia/Kuala_Lumpur");
            $time_name = date('Ymd');
            $time_log = date('Y-m-d H:i:s');
     rm       $log_download = new Log_download;
            
            $user = Auth::user();
            $id_user =  $user->id;
            $log_download->id_praapplication   =  $id;
            $log_download->id_user   =  $id_user;
            $log_download->Activity   =  'Route Back Application Submitted';
            $log_download->downloaded_at   =  $time_log;
            //$log_download->log_remark   =  $remark_admin;
            // $log_download->log_reason  =  $reason;
            $log_download->save();
          */
          

          
         
          
          /*$email = "mbsb@ezlestari.com.my";
          $fullname = "Personal Loan Co-Opbank Persatuan";
          // Send to Admin Email
          
          Mail::send('mail.notification_application_submitted', compact('id'), function ($message) use ($email, $fullname) {
          $message->from('mbsb@ezlestari.com.my', 'mbsb@ezlestari.com.my');
          $message->subject('Route Back Application Submitted');
          $message->to($email, $fullname);
          });*/
  
      }
      
          
      
      
      
          
        return redirect('home')->with('message', 'Thank You, Your Application Successfully Submitted. ');
		  }


    public function requestedit($id) {
         
          $user = Auth::user();
          $term = Term::where('id_praapplication', $id)->update(['edit' => '1' ]);
          $email = $user->email;
          $basic = Basic::latest('created_at')->where('id_praapplication', $id)->limit('1')->first(); 
          $fullname  = $basic->name;
          $new_ic  = $basic->new_ic;
		  
		   // Set Time Log
		  		date_default_timezone_set("Asia/Kuala_Lumpur");
				$time_name = date('Ymd');
				$time_log = date('Y-m-d H:i:s');
				$log_download = new Log_download;
				
				$user = Auth::user();
				$id_user =  $user->id;
				$log_download->id_praapplication   =  $id;
				$log_download->id_user   =  $id_user;
				$log_download->Activity   =  'Request Edit/Route Back';
				$log_download->downloaded_at   =  $time_log;
				//$log_download->log_remark   =  $remark_admin;
				// $log_download->log_reason  =  $reason;
				$log_download->save();
          
         /*  Mail::send('mail.requestedit', compact('fullname'), function ($message) use ($email, $fullname) {
          $message->from('loan@ezlestari.com.my', 'Personal loan Co-Opbank Persatuan');
          $message->subject('Request Edit Application Form');
          $message->to($email, $fullname);
          });
          
          // Kasih notif ke admin juga
          $email_admin = "loan@ezlestari.com.my";
          $fullname_admin ="Personal loan Co-Opbank Persatuan";
          Mail::send('mail.requestedit_admin', compact('email','fullname','new_ic'), function ($message) use ($email_admin, $fullname_admin) {
          $message->from('no-reply@ezlestari.com.my', 'no-reply');
          $message->subject('New Request for Edit Application Form');
          $message->to($email_admin, $fullname_admin);
          }); */
          return redirect('home')->with('message', 'Request for edit have been sent, please wait for admin approval');
    }

     public function approvetedit($id) {
		$user = Auth::user();
          $term = Term::where('id_praapplication', $id)->update(['edit' => '1' ]);
          $basic = Basic::latest('created_at')->where('id_praapplication', $id)->limit('1')->first();
          $pra = PraApplication::latest('created_at')->where('id',$id)->limit('1')->first();
          $fullname  = $basic->name;
          $email  = $pra->email;
		
          $term = Term::where('id_praapplication', $id)->update(['edit' => '0',  'status' => '0', 'verification_result' => '4']);
		  // Set Time Log
		  		date_default_timezone_set("Asia/Kuala_Lumpur");
				$time_name = date('Ymd');
				$time_log = date('Y-m-d H:i:s');
				$log_download = new Log_download;
				
				$user = Auth::user();
				$id_user =  $user->id;
				$log_download->id_praapplication   =  $id;
				$log_download->id_user   =  $id_user;
				$log_download->Activity   =  'Approve Request Route Back from User';
				$log_download->downloaded_at   =  $time_log;
				//$log_download->log_remark   =  $remark_admin;
				// $log_download->log_reason  =  $reason;
				$log_download->save();
        
        /*  Mail::send('mail.requestedit_approve', compact('fullname'), function ($message) use ($email, $fullname) {
          $message->from('loan@ezlestari.com.my', 'Personal loan Co-Opbank Persatuan');
          $message->subject('Request Edit Application Form has Approved');
          $message->to($email, $fullname);
          }); */

          return redirect('admin')->with('message', 'request for edit have been approved');
    }
	
		public function routeback(Request $request) {
				$user = Auth::user();
				$id_praapplication = $request->input('id_praapplication');

				$cus_name = $request->input('cus_name');
				$cus_ic = $request->input('cus_ic');

				$reason = $request->input('reason');
				$cus_email = $request->input('cus_email');

				// Set Time Activity
		
						date_default_timezone_set("Asia/Kuala_Lumpur");
						$time_name = date('Ymd');
						$time_download = date('Y-m-d H:i:s');
						$log_download = new Log_download;
						
						$user = Auth::user();
						$id_user =  $user->id;
						$log_download->id_praapplication   =  $id_praapplication;
						$log_download->id_user   =  $id_user;
						$log_download->Activity   =  'Route Back to User';
						$log_download->downloaded_at   =  $time_download;
						$log_download->log_reason  =  $reason;
						$log_download->save();
					
					
						// Send to Customer Email
						/*
						Mail::send('mail.notifikasi_routeback_customer', compact('cus_name','cus_ic','reason'), function ($message) use ($cus_email, $cus_name) {
						$message->from('loan@ezlestari.com.my', 'loan@ezlestari.com.my');
						$message->subject('[EZLESTARI.COM.MY] Route Back Application');
						$message->to($cus_email, $cus_name);
						
				
						}); */
						
				//$name  = $request->input('Name');
				$term = Term::where('id_praapplication', $id_praapplication)->update(['edit' => '0',  'status' => '0', 'verification_result' => '4']);
				return redirect('routeback_status')->with('success', 'Application has Routed Back to User');
				

		}


  public function uploaddoc(Request $request)
    {
        
        $user = Auth::user();
        $id_praapplication = $request->input('id_praapplication');
        $icnumber = $request->input('icnumber');

        $debt_consolidation = $request->input('debt_consolidation');
       
          $financial = Financial::where('id_praapplication', $id_praapplication)
        ->update(['debt_consolidation' => $debt_consolidation ]);

        
        if($debt_consolidation>0) {
        
          // settlement
          $setgroup = array_map(null, $request->set_institution, $request->set_installment, $request->set_amount); 
            foreach ($setgroup as $del) {
              $delete = Settlement::where('id_praapplication', $id_praapplication)->delete();
            }
            
            foreach ($setgroup as $val) {
             /* $val[2] = str_replace('/', '-', $val[2]);
              $valx =  date('Y-m-d', strtotime($val[2])); */
              $settlement = new Settlement;
              $settlement->institution = $val[0];
              $settlement->installment = $val[1]; 
              $settlement->amount = $val[2];
              $settlement->id_praapplication = $id_praapplication;
              $settlement->save();
            } 

        }
        
        $pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
        if ($icnumber<>$pra->icnumber) {
          return redirect('upload')->with('error', 'Wrong IC Number. Please try again ');
        }
        $email  = $pra->email;

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        
        $doc_status = Term::where('id_praapplication', $id_praapplication)->update(['time_upload' => $time_log, 'status' => '1', 'file_created' => $time_log]);
        // $term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '1' ]);
                
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id_praapplication;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Customer Upload Document';
        $log_download->downloaded_at   =  $time_log;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
        
        // Send to Customer Email
        
        $cus_name = $pra->fullname;
        $to = $pra->email;

        $main_sender = Mail_System::where('id',1)->first();
        $main_sender_name= $main_sender->name;
        $main_sender_email= $main_sender->email;


        $noreply_sender = Mail_System::where('id',2)->first();
        $noreply_sender_name = $noreply_sender->name;
        $noreply_sender_email = $noreply_sender->email;
        
        Mail::send('mail.cus_submitdokumen', compact('cus_name'), function ($message) use ($main_sender_email, $main_sender_name, $cus_name, $to) {
        $message->from($main_sender_email, $main_sender_name);
        $message->subject('Documents Sucessfully Uploaded');
        $message->to($to,$cus_name);
        
    
        });

          // Email ke bos
          $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first();
          $jumlahpinjam = $loanammount->loanammount;
          $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first();
          $majikan = $empinfo->empname;
          $basic = Basic::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first();
          $nama = $basic->name;
          $ic = $basic->new_ic;

          Mail::send('mail.notification_upload_document_boss', compact('id','nama','ic','majikan','jumlahpinjam'), function ($message) use ($main_sender_email, $main_sender_name) {
          $message->from($main_sender_email, $main_sender_name);
          $message->subject('[DCA MBSB] Stage 1 - Application Request');
          $message->to('novrizaleka@gmail.com', 'Che Onn Ismail');
        
          }); 

         ?>
          <script>alert("Dokumen anda telah berjaya dihantar");location.href="../home";</script>
         <?php
        
    }


    
}
//
