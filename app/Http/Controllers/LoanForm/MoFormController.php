<?php

namespace App\Http\Controllers\LoanForm;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests;
use App\Model\PraApplication;
use App\Model\Basic;
use App\Model\Branch;
use App\Model\Contact;
use App\Model\Empinfo;
use App\Model\LoanAmmount;
use App\Model\Spouse;
use App\Model\Reference;
use App\Model\Financial;
use App\Model\i_financing;
use App\Model\Document;
use App\Model\Basic_v;
use App\Model\Contact_v;
use App\Model\Empinfo_v;
use App\Model\LoanAmmount_v;
use App\Model\Spouse_v;
use App\Model\Reference_v;
use App\Model\Financial_v;
use App\Model\i_financing_v;
use App\Model\Document_v;
use App\Model\Term;
use App\Model\Log_download;
use App\Model\Settlement;
use App\Model\Credit;
use App\Model\Pep;
use App\Model\Employment;
use App\Model\Loan;
use App\Model\Tenure;
use App\Model\Commitments;
use App\Model\User;
use App\Mail_System;
use App\Model\Country;
use App\Model\Occupations;
use App\Model\Position;
use App\Model\Relationship;
use App\Model\Title;
use App\Dsr_a;
use App\Dsr_b;
use App\Dsr_c;
use App\Dsr_d;
use App\Dsr_e;
use App\RiskRating;
use App\Http\Controllers\Controller;
use DB;
use Input;
use Auth;
use Mail;
use Image;
use File;
use DateTime;
use Hash;
use Rhumsaa\Uuid\Uuid;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\MOMBsc;
use App\Soap\Response\MOMBscResponse;
use App\Soap\Response\MOMEmpInfResponse;
use App\Soap\Request\MOMRef;
use App\Soap\Response\MOMRefResponse;
use App\Soap\Response\MOMSpResponse;
use App\Soap\Response\MOMCommitResponse;
use App\Soap\Response\MOMFinResponse;

use App\Soap\Response\MOMDSRAResponse;
use App\Soap\Response\MOMDSRBResponse;
use App\Soap\Response\MOMDSRCResponse;
use App\Soap\Response\MOMDSREResponse;

use App\Soap\Response\MOMLnResponse;
use App\Soap\Response\MOMRskResponse;
use App\Soap\Response\MOMTrmResponse;

use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;

use Storage;
use App\Model\Waps;
use OneSignal;
use App\Model\CodeIDType;
use App\Model\CodeJobStatus;
use App\Model\CodeWorkSec;
use App\Model\Foreigner;
use App\Model\Subscription;

class MoFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         
    }
 protected $soapWrapper;

  /**
   * SoapController constructor.
   *
   * @param SoapWrapper $soapWrapper
   */
  public function __construct(SoapWrapper $soapWrapper)
  {
    $this->soapWrapper = $soapWrapper;
  }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
      

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function add_document($id_pra, $view=FALSE)
    {
        
        $user = Auth::user();
        if ($user->role==2 AND $user->role==3 ) {
                    return redirect('admin');   
        }
        else if ($user->role==0) {
                    return redirect('home');    
        }
        $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_result = Term::where('id_praapplication', $id_pra)->first()->verification_result;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $name = strtoupper($basic->name);
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
         $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
         $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
         $icnumber= $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;
        $statusx = 1;

   
          $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

         $credit = Credit::where('id_praapplication', $id_pra)->first(); 
           $pep = Pep::where('id_praapplication', $id_pra)->first(); 
           $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 

    $cal_bulan = $financial->first()->cal_bulan;
    if ($cal_bulan=='0000-00-00') {
     $cal_bulan = ''; 
    }
    else {
     $cal_bulan = date('F Y', strtotime($cal_bulan)); 
    }
    
    
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term

        $today = date("d/m/Y");
        //$view = "verify";
        $country = Country::get(); //term
        $occupation = Occupations::get();
        $nature_business = Occupations::get();
        $position = Position::get();
        $relationship = Relationship::get();
        $relationship2 = Relationship::get();
        $title = Title::get();
        $branch = Branch::get();

          $dsr_b        = Dsr_b::where('id_praapplication',$id_pra

        )->first();
      
        return view('mo.document', compact('view','name','cal_bulan','user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                     'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','today','credit','pep','commitments','country','occupation','position','relationship','relationship2','title','branch','dsr_b','nature_business'));        
                       
        
   
      
       
    }

    public function show($id_pra, $view=FALSE)
    {
        
        $user = Auth::user();
        if ($user->role==2 AND $user->role==3 ) {
                    return redirect('admin');   
        }
        else if ($user->role==0) {
                    return redirect('home');    
        }
        $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_result = Term::where('id_praapplication', $id_pra)->first()->verification_result;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $name = strtoupper($basic->name);
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
         $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
         $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
         $icnumber= $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;
        $statusx = 1;

   
          $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

         $credit = Credit::where('id_praapplication', $id_pra)->first(); 
           $pep = Pep::where('id_praapplication', $id_pra)->first(); 
           $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 

    $cal_bulan = $financial->first()->cal_bulan;
    if ($cal_bulan=='0000-00-00') {
     $cal_bulan = ''; 
    }
    else {
     $cal_bulan = date('F Y', strtotime($cal_bulan)); 
    }
    
    
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term

        $today = date("d/m/Y");
        //$view = "verify";
        $country = Country::get(); //term
        $country2 = Country::get(); //term
        $occupation = Occupations::get();
        $nature_business = Occupations::get();
        $position = Position::get();
        $relationship = Relationship::get();
        $relationship2 = Relationship::get();
        $title = Title::get();
        $branch = Branch::get();

          $dsr_b        = Dsr_b::where('id_praapplication',$id_pra

        )->first();
        
        $nationality = Country::get();
        $empstatus = CodeJobStatus::get();
         $foreigner      = Foreigner::where('id_praapplication',$id_pra)->first();
            $subs           = Subscription::where('id_praapplication',$id_pra)->first();
        return view('mo.moform', compact('view','name','cal_bulan','user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                     'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','today','credit','pep','commitments','country','occupation','position','relationship','relationship2','title','branch','dsr_b','nature_business','nationality','empstatus','foreigner','subs','country2'));        
                       
        
   
      
       
    }
           public function view($id_pra,$view=FALSE)
    {
        
        $user = Auth::user();
        if ($user->role==2 AND $user->role==3 ) {
                    return redirect('admin');   
        }
        else if ($user->role==0) {
                    return redirect('home');    
        }
        $email =  $user->email;
       
       $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
    //$id_pra = $pra->id;

        $status = Term::where('id_praapplication', $id_pra)->first()->status;
        $verification_result = Term::where('id_praapplication', $id_pra)->first()->verification_result;
        $verification_remark = Term::where('id_praapplication', $id_pra)->first()->verification_remark;
        $basic_v = Basic_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $contact_v = Contact_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $empinfo_v = Empinfo_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount_v = LoanAmmount_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $spouse_v = Spouse_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference_v = Reference_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $financial_v = Financial_v::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $basic = Basic::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->first(); 
        $name = strtoupper($basic->name);
        $employment = Employment::get();
        $document1 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '1' )->first();
        $document2 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '2' )->first();
        $document3 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '3' )->first();
        $document4 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '4' )->first();
        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
         $document10 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '10' )->first();
         $document11 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '11' )->first();
        //$customer = User::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();         
        $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        
        $id_type= $empinfo->first()->employment_id;
        $total_salary = $empinfo->first()->salary + $empinfo->first()->allowance ;
        
        $loan = Loan::where('id_type',$id_type)
                  ->where('min_salary','<=',$total_salary)
                  ->where('max_salary','>=',$total_salary)->limit('1')->get();
        
        $id_loan= $loan->first()->id;  
        
         $icnumber= $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
        $lahir = $tahun2.'-'.$bulan.'-'.$tanggal; 
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur = 61 - $oDateIntervall->y;
        $statusx = 1;

   
          $tenure = Tenure::where('id_loan',$id_loan)->where('years','<', $umur)->get();   
        
        $contact = Contact::latest('created_at')->where('id_praapplication', $id_pra)->limit('1')->get();
        $data = Basic::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

        $spouse = Spouse::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->get();
        $reference = Reference::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();
        $financial = Financial::latest('created_at')->where('id_praapplication',  $id_pra )->limit('1')->first();

         $credit = Credit::where('id_praapplication', $id_pra)->first(); 
           $pep = Pep::where('id_praapplication', $id_pra)->first(); 
           $commitments = Commitments::where('id_praapplication', $id_pra)->first(); 

    $cal_bulan = $financial->first()->cal_bulan;
    if ($cal_bulan=='0000-00-00') {
     $cal_bulan = ''; 
    }
    else {
     $cal_bulan = date('F Y', strtotime($cal_bulan)); 
    }
    
    
        $term = Term::where('id_praapplication',  $id_pra )->first(); //term

        $today = date("d/m/Y");
        $view = "verify";
        $country = Country::get(); //term
        $occupation = Occupations::get();
        $position = Position::get();
        $relationship = Relationship::get();
        $relationship2 = Relationship::get();
        $title = Title::get();
        $branch = Branch::get();

          $dsr_b        = Dsr_b::where('id_praapplication',$id_pra

        )->first();
      
        return view('mo.moform_view', compact('view','name','cal_bulan','user','data','basic_v','contact_v','empinfo_v','loanammount_v','spouse_v','reference_v','financial_v','spouse','contact','employment','empinfo','pra','tenure','loan','total_salary',
                                                     'loanammount', 'reference','financial','document1', 'document2','document3','document4','document5','document6', 'document7','document8','document9','term','statusx','document10','document11','today','credit','pep','commitments','country','occupation','position','relationship','relationship2','title','branch','dsr_b'));        
                       
        
   
      
       
    }
        

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        if($id=="stage")  {
                $term = Term::where('id_praapplication', $request->id_praapplication)
                ->update(['mo_stage' => '1', 'status'=>'1']);

                 $praEmail = PraApplication::where('id', $request->id_praapplication)
                ->update(['email' => Auth::User()->email]);
            }

         if($id == '1') {
            // Update Field
              $id_praapplication = $request->input('id_praapplication');

              $title = $request->input('title');
                $title_others = $request->input('title_others');
              $name = $request->input('name');
              $new_ic = $request->input('new_ic');
              $old_ic = $request->input('old_ic');
              $dob2 = $request->input('dob');
              $police_number = $request->input('police_number');
              $gender = $request->input('gender');
              $country = $request->input('country');
               $country_others = $request->input('country_others');
                $country_origin = $request->input('country_origin');

              $address = $request->input('address');
              $address2 = $request->input('address2');
              $address3 = $request->input('address3');

              $postcode = $request->input('postcode');
              $state = $request->input('state');
              $state_code = $request->input('state_code');
              $ownership = $request->input('ownership');
              $corres_address1 = $request->input('corres_address1');
               $corres_address2 = $request->input('corres_address2');
                $corres_address3 = $request->input('corres_address3');
              $corres_state = $request->input('corres_state');
               $corres_state2 = $request->input('corres_state2');
              $corres_postcode = $request->input('corres_postcode');
              $corres_homephone = $request->input('corres_homephone');
              $corres_mobilephone = $request->input('corres_mobilephone');
               $nationality = $request->input('nationality');
             // $corres_email = $request->input('corres_email');
              $citizen = $request->input('country');
              $race = $request->input('race');
               $race_others = $request->input('race_others');

              $bumiputera = $request->input('bumiputera');
              $religion = $request->input('religion');
              $religion_others = $request->input('religion_others');

              $marital = $request->input('marital');
              $dependents = $request->input('dependents');
              $education = $request->input('education');
              
               $country_dob = $request->input('country_dob');
                $corres_email = $request->input('corres_email');

            $address_correspondence = $request->input('checker');
            $resident = $request->input('resident');

            
             $basic = Basic::where('id_praapplication', $id_praapplication)
             ->update(['country' => $country, 
              'country_origin' => $country_origin, 
               'country_others' => $country_others, 
              'citizen' => $country, 
              'new_ic' => $new_ic, 
             'old_ic' => $old_ic, 
             'country_dob' => $country_dob,
             'name' => $name,
             'title' => $title, 
             'title_others' => $title_others,
             'gender' => $gender, 
             'marital' => $marital,
              'dependents' => $dependents,
             'race' => $race, 
             'race_others' => $race_others,
              'bumiputera' => $bumiputera, 
             'religion' => $religion , 
             'religion_others' => $religion_others,
             'marital' => $marital, 
             'police_number' => $police_number, 
             'address'=> $address,
              'address2'=> $address2,
               'address3'=> $address3,
              'postcode'=> $postcode,
               'state'=> $state,
               'state_code'=> $state_code, 
               'corres_address1'=> $corres_address1, 
               'corres_address2'=> $corres_address2, 
               'corres_address3'=> $corres_address3, 
               'corres_state'=> $corres_state, 
                'corres_state1'=> $corres_state2, 
               'corres_postcode'=> $corres_postcode, 
               'corres_homephone'=> $corres_homephone,
                'corres_mobilephone'=> $corres_mobilephone, 
                 'corres_email'=> $corres_email,
                'education'=> $education,
                 'ownership'=> $ownership,
                 'address_correspondence'=> $address_correspondence,
                 'nationality'=> $nationality,
                 'resident'=>$resident ]);

              $ter = Term::where('id_praapplication', $id_praapplication)
             ->update(['fullname' => $name, 
              'email' => $corres_email ]);
            

          
                

    

        }
      
      else if($id == '2') {

            $id_praapplication = $request->input('id_praapplication');

            $empname = $request->input('empname');
            $emptype = $request->input('emptype');
            $emptype_others = $request->input('emptype_others');
            $occupation = $request->input('occupation');
            $position = $request->input('position');
            $dept_name = $request->input('dept_name');
            $division = $request->input('division');
            $address = $request->input('address');
            $address2 = $request->input('address2');
            $address3 = $request->input('address3');
            $state =  $request->input('state');
            $state3 =  $request->input('state_code3');
            $postcode =  $request->input('postcode');
            $nature_business = $request->input('nature_business');
            $joined2 =  $request->input('joined');
            $joined2 = str_replace('/', '-', $joined2);
            $joined =  date('Y-m-d', strtotime($joined2));
            $office_phone = $request->input('office_phone');
            $office_fax = $request->input('office_fax');
            $working_exp = $request->input('working_exp');
            $empstatus = $request->input('empstatus');

            $mbsb_staff = $request->input('mbsb_staff');
            $staff_no = $request->input('staff_no');
            
                
            $empinfo = Empinfo::where('id_praapplication', $id_praapplication)
            ->update(['empname' => $empname,  'emptype' => $emptype, 'emptype_others' => $emptype_others,  'position' => $position, 'occupation' => $occupation, 'dept_name' => $dept_name, 'division' => $division,'address' => $address, 'address2' => $address2, 'address3' => $address3, 'state' => $state, 'postcode' => $postcode,  'nature_business' => $nature_business, 'joined' => $joined, 'office_phone' => $office_phone, 'office_fax' => $office_fax, 'working_exp' => $working_exp,  'empstatus' => $empstatus,'state_code'=>$state3, 'mbsb_staff' => $mbsb_staff, 'staff_no' => $staff_no]);



        }
        
        else if($id == '3') {

            $id_praapplication = $request->input('id_praapplication');

            
            $name = $request->input('name');
            $homephone =  $request->input('homephone');
            $mobilephone = $request->input('mobilephone');

            $tp = $request->input('emptype');
            $emptype = substr($tp, 0, 3);
            $emptype_others = $request->input('emptype_others');
           
        
            $spouse = Spouse::where('id_praapplication', $id_praapplication)
          ->update(['name' => $name, 'homephone' => $homephone, 'mobilephone' => $mobilephone, 'emptype' => $emptype, 'emptype_others' => $emptype_others ]);

            //reference
            $name1 = $request->input('name1');
            $mobilephone1 =  $request->input('mobilephone1');
            $home_phone1 =  $request->input('home_phone1');
            $relationship1 =  $request->input('relationship1');
            $name2 = $request->input('name2');
            $mobilephone2 =  $request->input('mobilephone2');
            $home_phone2 =  $request->input('home_phone2');
            $relationship2 =  $request->input('relationship2');


     

            $reference = Reference::where('id_praapplication', $id_praapplication)->update(['name1' => $name1, 'mobilephone1' => $mobilephone1, 'relationship1' => $relationship1, 'name2' => $name2, 'mobilephone2' => $mobilephone2, 'relationship2' => $relationship2, 'home_phone1' => $home_phone1,'home_phone2' => $home_phone2]);

             //v



        }
       else if($id == '4') {

            $id_praapplication = $request->input('id_praapplication');
            //$monthly_income = $request->input('monthly_income_hidden');
            //$result = str_replace(',', '', $monthly_income);
            
            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['monthly_income' => $request->input('monthly_income'), 
                    'other_income' => $request->input('other_income'), 
                    'total_income' => $request->input('total_income') ]);

            

          }
   
        else if($id == '5') {

                $id_praapplication = $request->input('id_praapplication');

            // Checking If Not Empty
            $check = Commitments::where('id_praapplication', $id_praapplication)->count();

            if($check>0) {
              $commitments = Commitments::where('id_praapplication', $id_praapplication)
              ->update(['name1' => $request->input('name1'), 
                    'monthly_payment1' => $request->input('monthly_payment1'), 
                    'financing1' => $request->input('financing1'),
                    'remaining1' => $request->input('remaining1'),

                    'name2' => $request->input('name2'), 
                    'monthly_payment2' => $request->input('monthly_payment2'), 
                    'financing2' => $request->input('financing2'),
                    'remaining2' => $request->input('remaining2'),
                    'remark' => $request->input('remark'),

                     ]);
            }
            else {
              $commitments = new Commitments;
              $commitments->id_praapplication = $request->input('id_praapplication');
              $commitments->name1 = $request->input('name1');
              $commitments->monthly_payment1 = $request->input('monthly_payment1');
              $commitments->financing1 = $request->input('financing1');
              $commitments->remaining1 = $request->input('remaining1');
              $commitments->name2 = $request->input('name2');
              $commitments->monthly_payment2 = $request->input('monthly_payment2');
              $commitments->financing2 = $request->input('financing2');
              $commitments->remaining2 = $request->input('remaining2');
              $commitments->remark = $request->input('remark');
              $commitments->save();
            }

            return response()->json(['check' => $check]);


    }

     
      


     else if($id == '6') {


        $id_praapplication = $request->input('id_praapplication');

            $package = $request->input('package');
            $product_bundling = $request->input('product_bundling');
            $product_bundling_specify = $request->input('product_bundling_specify');
            $cross_selling = $request->input('cross_selling');
            $cross_selling_specify = $request->input('cross_selling_specify');

            $takaful_coverage = $request->input('takaful_coverage');
            $takaful_coverage_specify = $request->input('takaful_coverage_specify');

            $financing_amount = $request->input('financing_amount');
            $tenure = $request->input('tenure');

            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['product_bundling' => $product_bundling, 'product_bundling_specify' => $product_bundling_specify,  'cross_selling' => $cross_selling,  'cross_selling_specify' => $cross_selling_specify, 'takaful_coverage' => $takaful_coverage ,'takaful_coverage_specify' => $takaful_coverage_specify]);

            $Loanamount = LoanAmmount::where('id_praapplication', $id_praapplication)
            ->update(['package' => $package, 'loanammount' => $financing_amount,  'new_tenure' => $tenure ]);

    }

    else if($id == '7') {

            $id_praapplication = $request->input('id_praapplication');

            $purpose_facility = $request->input('purpose_facility');
            $type_customer = $request->input('type_customer');
            $payment_mode = $request->input('payment_mode');
            $bank_name = $request->input('bank_name');
            $account_no = $request->input('account_no');

            $bank1 = $request->input('bank1');
            $bank2 = $request->input('bank2');

            $bank3 = $request->input('bank3');
            $bank4 = $request->input('bank4');

            $bank5 = $request->input('bank5');
            $bank6 = $request->input('bank6');

            $financial = Financial::where('id_praapplication', $id_praapplication)
            ->update(['purpose_facility' => $purpose_facility, 'type_customer' => $type_customer,  'payment_mode' => $payment_mode,  'bank_name' => $bank_name, 'account_no' => $account_no ,'bank1' => $bank1, 'bank2' => $bank2 ,'bank1' => $bank1, 'bank3' => $bank3 ,'bank4' => $bank4, 'bank5' => $bank5 ,'bank6' => $bank6]);

    
           

        }
        else if($id == '8') {
              $id_pra = $request->input('id_praapplication');

          $pra = PraApplication::latest('created_at')->where('id',$id_pra)->limit('1')->first();
        $basic = Basic::latest('created_at')->where('id_praapplication',$id_pra)->limit('1')->first();
          $dcmt = DB::table('document')->select(DB::raw(" max(id) as id"))->where('id_praapplication',$id_pra)->groupBy('type')->pluck('id');
        $files = Document::whereIn('id', $dcmt)->get(); 
        $filesd = Document::where('id_praapplication',$id_pra)->get(); 
        $prafullname =  str_replace(" ","%20",$pra->fullname);  
            

        $document5 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '5' )->first();
        $document6 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '6' )->first();
        $document7 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '7' )->first();
        $document8 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '8' )->first();
        $document9 = Document::latest('created_at')->where('id_praapplication',  $id_pra )->where('type',  '9' )->first();
        $url = url('')."/storage/uploads/file/".$id_pra."/"; 
       //$url = "https://www.mbsb.insko.my/public/storage/uploads/file/".$id_pra."/"; 
       
        if((!empty($document5->upload)) OR (!empty($document6->upload)) OR (!empty($document7->upload)) OR (!empty($document8->upload)) OR (!empty($document9->upload))) {
            $zip = new \ZipArchive();

 
            // store the public path
            $publicDir = public_path();
             $tmp_file = tempnam('.','');
            // Define the file name. Give it a unique name to avoid overriding.
            $zipFileName = $id_pra.'.zip';
              
            if ($zip->open($publicDir . '/storage/uploads/file/'.$id_pra.'/' . $zipFileName, \ZipArchive::CREATE) === true) {
                // Loop through each file
                foreach($files as $file){
                     $url2 = $url.$file->upload;
                     $url2 = str_replace(' ', '%20', $url2);

                        if (!function_exists('curl_init')){ 
                            die('CURL is not installed!');
                        }

                     $ch = curl_init();
                     curl_setopt($ch, CURLOPT_URL, $url2);
                     curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                     $output = curl_exec($ch);
                     curl_close($ch);
                     $download_file = $output;

                     $type = substr($url2, -5, 5); 
                     #add it to the zip
                     $zip->addFromString(basename($url.$file->upload.'.'.$type),$download_file);
                }
                // close zip
                $zip->close();
            }

             // Download the file using the Filesystem API
             $filePath = $publicDir . '/storage/uploads/file/' . $zipFileName;

             if (file_exists($filePath)) {
                 return Storage::download($filePath);
             }
        
              
                
            }
    
           

        }

    else if($id == '99') {


        $id_praapplication = $request->input('id_praapplication');
        $loanamount = $request->input('loanamount');
        $id_tenure = $request->input('tenure');
        $maxloan = $request->input('maxloan');
        $package = $request->input('package');
        $rate = $request->input('interests');
        $installment = $request->input('installment');
       
        $date = date('Y-m-d H:i:s');
        $lat = $request->input('lat');
        $lng = $request->input('lng');
        $location = $request->input('location');

        $loanammount = LoanAmmount::where('id_praapplication', $id_praapplication)
        ->update(['id_tenure' => $id_tenure, 'maxloan' => $maxloan, 
        'package' => $package]);

        $term = Term::where('id_praapplication', $id_praapplication)
        ->update(['mo_stage'=>'1','status'=>'1','lat'=> $lat,'lng' => $lng,'location' => $location]);

        $dsr = Dsr_b::where('id_praapplication', $id_praapplication)
        ->update(['financing_amount' => $loanamount, 'tenure' => $id_tenure, 
       'rate' => $rate,'monthly_repayment' => $installment]);

        /*OneSignal::sendNotificationToAll("New Application", $url = 'http://mbsb.insko.my/onesignal/public/admin/detail_pra/'.$termas->id_praapplication, $data = null);
        //$dsr_b = Dsr_b::where('id_praapplication', $id_praapplication)->update(['financing_amount'=>$loanamount,'tenure'=>$id_tenure,'tenure'=>$id_tenure,'tenure'=>$id_tenure]);*/

    }
}
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    

    public function uploads(Request $request,$id)
    {
                  $user = Auth::user();
                 $id_praapplication = $request->input('id_praapplication');

                   $pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
      
                   $nametest  = $pra->id;

                 
                  if ($request->hasFile('file'.$id)) {
                  $name = $request->input('document'.$id);
                 $file = $request->file('file'.$id);
                $tipe_file   = $_FILES['file'.$id]['type'];
                if($tipe_file == "application/pdf" || $tipe_file == "image/jpeg" || $tipe_file == "image/png" || $tipe_file == "image/bmp" || $tipe_file == "image/gif") {


                 $filename = str_random(25).'-'.$name.'-'.$file->getClientOriginalName();

                 if($tipe_file != "application/pdf") {

                    if(!(File::exists('storage/uploads/file/'.$nametest.'/'))) {
                        $create_folder =  File::makeDirectory('storage/uploads/file/'.$nametest.'/', 0777);
                    }

                      $img = Image::make($request->file('file'.$id));

                      $path = public_path('storage/uploads/ile/'.$nametest.'/'. $filename);
                 
                      $img->resize(800, null, function ($constraint) {
                          $constraint->aspectRatio();
                      })->encode('jpg', 25)->save($path);
                 }
                 else {
                    $destinationPath = 'storage/uploads/file/'.$nametest.'/';
                    $file->move($destinationPath, $filename);
                 }
 
        //Upload File to external server
       


                  
                $document = new Document;
                $document->name = $name;
                $document->type = $id;
                $document->upload = $filename; 
                $document->id_praapplication = $id_praapplication; 
                $document->save();
                 return response()->json(['file' => "$filename",'types' => "$type",'typess' => "$type"]);

               }
            

                }  
    }
    

    public function term(Request $request) {

        $password = $request->input('password');
        $user = User::find(auth()->user()->id);
        $id_praapplication = $request->input('id_praapplication');
        $verification_remark = $request->input('remark');
        $branch = $request->input('branch');
        $id = $id_praapplication;
        $declaration = $request->input('declaration');
        $user_aktif = Auth::user()->name;

        if(!Hash::check($password, $user->password)){
            return response()->json(['status' => "0",'message' => "Incorrect Password"]);
        }
        else {
            $pra_email = Basic::where('id_praapplication', $id_praapplication)->limit('1')->first();
            $emails= $pra_email->corres_email;
            $cek = Basic::where('corres_email',$emails)->count();
                if($cek>1) {
                    $praEmel = PraApplication::where('id', $id_praapplication)
                    ->update(['email_exists' => 1]);

                    return response()->json(['status' => "0",'message' => "Customer with this email is already exists, Try another email address",'email' => $request->email]);
                }
                else{


                    // cek kredit
                    $count_credit = Credit::where('id_praapplication', $id_praapplication)->count();
                    if($count_credit<1) {
                      $credit = new Credit ;
                      $credit->fullname = $request->input('fullname');
                      $credit->mykad =  $request->input('mykad');
                      $credit->passport =  $request->input('passport');
                      $credit->relationship =  $request->input('relationship');
                      $credit->id_praapplication = $id_praapplication;
                      $credit->save();
                    }
                    else {
                      $credit = Credit::where('id_praapplication', $id_praapplication)->update(['fullname' => $request->input('fullname'), 
                          'mykad' => $request->input('mykad'),
                          'passport' => $request->input('passport'),
                          'relationship' => $request->input('relationship')]);
                    }

                    // cek pep
                    $count_pep = Pep::where('id_praapplication', $id_praapplication)->count();
                    if($count_pep<1) {
                      $pep = new Pep ;
                      $pep->name1 = $request->input('name1');
                      $pep->relationship1 =  $request->input('relationship1');
                      $pep->status1 =  $request->input('status1');
                      $pep->prominent1 =  $request->input('prominent1');
                      //2
                      $pep->name2 = $request->input('name2');
                      $pep->relationship2 =  $request->input('relationship2');
                      $pep->status2 =  $request->input('status2');
                      $pep->prominent2 =  $request->input('prominent2');
                      //3
                      $pep->name3 = $request->input('name3');
                      $pep->relationship3 =  $request->input('relationship3');
                      $pep->status3 =  $request->input('status3');
                      $pep->prominent3 =  $request->input('prominent3');

                      $pep->id_praapplication = $id_praapplication;
                      $pep->save();
                    }
                    else {
                      $pep = Pep::where('id_praapplication', $id_praapplication)->update(['name1' => $request->input('name1'), 
                          'relationship1' => $request->input('relationship1'),
                          'status1' => $request->input('status1'),
                          'prominent1' => $request->input('prominent1'),
                          'name2' => $request->input('name2'), 
                          'relationship2' => $request->input('relationship2'),
                          'status2' => $request->input('status2'),
                          'prominent2' => $request->input('prominent2'),
                          'name3' => $request->input('name3'), 
                          'relationship3' => $request->input('relationship3'),
                          'status3' => $request->input('status3'),
                          'prominent3' => $request->input('prominent3'),
                          'id_praapplication' => $request->input('id_praapplication')

                          ]);
                    }

                    date_default_timezone_set("Asia/Kuala_Lumpur");
                    $file_created_time = date('Y-m-d H:i:s');

                    $praEmel = PraApplication::where('id', $id_praapplication)
                          ->update(['email_exists' => 0, 'acus_email' => $request->email]);

                        $subs = Subscription::where('id_praapplication', $id_praapplication)->update(['sub_wealth' => $request->input('sub_wealth'), 
              'gpa' => $request->input('gpa'),
               'hasanah' => $request->input('hasanah'),
              'annur' => $request->input('annur'),
              'wasiat' => $request->input('wasiat')]);

         $foreigner = Foreigner::where('id_praapplication', $id_praapplication)->update(['foreign1' => $request->input('foreigner1'), 
              'foreign2' => $request->input('foreigner1'),
              'foreign3' => $request->input('foreigner2'),
              'country' => $request->input('foreigner_country'),
              'foreign4' => $request->input('foreigner4')]);
         
                    $term = Term::where('id_praapplication', $id_praapplication)
                    ->update(['status' => '1', 
                      'verification_result' => '2',
                      'purchase_application' => $request->input('purchase_application'),
                      'appointment_mbsb' => $request->input('appointment_mbsb'),
                      'credit_transactions' => $request->input('credit_transactions'),
                      'consent_for' => $request->input('consent_for'),
                      'high_networth' => $request->input('high_networth'),
                      'politically' => $request->input('politically'),
                      'for_goods' => $request->input('for_goods'),
                      'product_disclosure' => $request->input('product_disclosure'),
                      'lat' => $request->input('lat'),
                      'lng' => $request->input('lng'),
                      'location' => $request->input('location'),
                      'file_created' => $file_created_time,
                      'mo_stage'=>'2'
                      ]);

                    // Set Time Log
                    

                    $log_download = new Log_download;
             
                    $log_download->id_praapplication   =  $id;
                    $log_download->id_user   =  Auth::User()->id;
                    $log_download->Activity   =  'Application Submitted by Marketing Officer to WAPS';
                    $log_download->downloaded_at   =  $file_created_time;
                    //$log_download->log_remark   =  $remark_admin;
                    // $log_download->log_reason  =  $reason;
                    $log_download->lat = $request->latitude;
                    $log_download->lng = $request->longitude;
                    $log_download->location = $request->location;
                    $log_download->save();

                    $main_sender = Mail_System::where('id',1)->first();
                    $main_sender_name= $main_sender->name;
                    $main_sender_email= $main_sender->email;


                    $noreply_sender = Mail_System::where('id',2)->first();
                    $noreply_sender_name = $noreply_sender->name;
                    $noreply_sender_email = $noreply_sender->email;

                    $term = Term::where('id_praapplication', $id_praapplication)->update(['verification_status' => '1', 'verification_result' => '2', 'verification_remark' => $verification_remark, 'verified_by' => $user_aktif, 'id_branch' => $branch  ]);


                         // Send to Admin Email
                      
                    Mail::send('mail.notification_application_submitted', compact('id'), function ($message) use ($noreply_sender_name, $noreply_sender_email,$main_sender_email ,$main_sender_name ) {
                    $message->from($noreply_sender_email, $noreply_sender_name);
                    $message->subject('New Application Submitted by Marketing Officer');
                    $message->to($main_sender_email, $main_sender_name);
                    });

                    // Create Customer Account

                    $password = 'mommbsb123';
                    $pra = PraApplication::where('id',$id_praapplication)->first();
                    $customer_email =$emails;
                    $customer_name = $pra->basic->name;
                    $loanamount = $pra->loanammount->loanammount;
                    //$mo_name = $user->MarOfficer->name;
                    //$mo_email = $user->email;
                    //$mo_phone = $user->MarOfficer->phoneno;

                    $active_code = str_random(100);
                    $user = new User;
                    $user->password = Hash::make($password);
                    $user->id   = str_random(32);  
                    $user->email =  $customer_email;
                    $user->activation_code = $active_code;
                    $user->name =  strtoupper($customer_name);
                    $user->active =  "0";
                    $user->mo_id =  Auth::User()->mo_id;
                    $user->role =  "7";
                    $user->first_login =  "1";
                    $user->save();

                // send to WAPS
              
                   
                $pra        = Basic::where('id_praapplication',$id_praapplication)->first();
                $empinfo    = Empinfo::where('id_praapplication',$id_praapplication)->first();
                $spouse     = Spouse::where('id_praapplication',$id_praapplication)->first();
                $ref        = Reference::where('id_praapplication',$id_praapplication)->first();
                $fin        = Financial::where('id_praapplication',$id_praapplication)->first();
                $com        = Commitments::where('id_praapplication',$id_praapplication)->first();
                $loan_a      = LoanAmmount::where('id_praapplication',$id_praapplication)->first();
                
                $dsr_a        = Dsr_a::where('id_praapplication',$id_praapplication)->first();
                $dsr_b        = Dsr_b::where('id_praapplication',$id_praapplication)->first();
                $dsr_c        = Dsr_c::where('id_praapplication',$id_praapplication)->first();
                $dsr_e        = Dsr_e::where('id_praapplication',$id_praapplication)->first();
                $dsr_d        = Dsr_d::where('id_praapplication',$id_praapplication)->first();
                $risk         = RiskRating::where('id_praapplication',$id_praapplication)->first();
                $term         = Term::where('id_praapplication',$id_praapplication)->first();
                $doc6         = Document::latest('created_at')->where('id_praapplication',$id_praapplication)->where('type','6')->limit('1')->first();
                 $doc7         = Document::latest('created_at')->where('id_praapplication',$id_praapplication)->where('type','7')->limit('1')->first();
                 $doc9         = Document::latest('created_at')->where('id_praapplication',$id_praapplication)->where('type','9')->limit('1')->first();

                Storage::disk('ftp')->makeDirectory($id_praapplication, $mode = 0777, true, true);

                 $zipFileName = $id_praapplication.'.zip';
                $file_local9 = public_path().'/storage/uploads/file/'. $id_praapplication.'/'.$zipFileName;
                  //Storage::disk('image')->put($name, file_get_contents($file));
               // $file_local9 = Storage::disk('public')->get($id_praapplication.'/'.$zipFileName);
                $file_ftp9 = Storage::disk('ftp')->put($id_praapplication.'/'.$zipFileName, file_get_contents($file_local9));

                if (!(file_exists($file_ftp9))) {
                    $move9 = Storage::move($file_local9, $file_ftp9);
                }  

                /* $this->soapWrapper->add('cds', function ($service) {
                  $service
                    ->wsdl('http://219.92.49.250/MOMWS/MOMCallWS.asmx?WSDL');
                });
               
                     $param=array(
                            'AppNo'   => $pra->id_praapplication,
                            'txnCode' => 'ST003',
                            'ActCode' => 'R',
                            'usr' => 'momwapsuat',
                            'pwd' => 'J@ctL71N$#'
                        );
                      $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));

                $smsMessages = is_array( $response->MOMCallStatResult)
            ? $response->MOMCallStatResult
            : array( $response->MOMCallStatResult );
             foreach ( $smsMessages as $smsMessage )
                {
             $result=$smsMessage->AppNo;
            if(!empty($result)){
                 return response()->json(['status' => "0",'message' => "Data already exist"]);
            }
            else{*/



                $this->soapWrapper->add('MOMBsc', function ($service) use($pra, $empinfo,$spouse,$ref,$fin,$com,$loan_a,$dsr_a,$dsr_b,$dsr_c,$dsr_d,$dsr_e,$risk,$term,$doc7,$doc9,$doc6){
                $service
                   // ->wsdl('http://219.92.49.250/MOMWS/MOMWS.asmx?WSDL')
                    ->wsdl('http://moaqs.com/MOMWS/MOMWS.asmx?WSDL')
                    ->trace(true)
                    ->cache(WSDL_CACHE_NONE)

                    //->header('http://www.w3.org/2001/XMLSchema-instance', 'Action', 'http://tempuri.org/MOMBsc')
                    ->options([
                        'login' => 'momwapsuat',
                        'password' => 'J@ctL71N$#'
                        ])
                    ->classmap([
                        MOMBsc::class,
                        MOMBscResponse::class,
                        MOMEmpInfResponse::class,
                        MOMSpResponse::class,
                        MOMRefResponse::class,
                        MOMFinResponse::class,
                        MOMCommitResponse::class
                    ]);
                });
                
                

                $changesymbol = $pra->name;
                $changesymbol =  str_replace("'","`",$changesymbol);

                $address = $pra->address;
                $address =  str_replace("'","`",$address);

                $address2 = $pra->address2;
                $address2 =  str_replace("'","`",$address2);

                $address3 = $pra->address3;
                $address3 =  str_replace("'","`",$address3);

                $corres_address1 = $pra->corres_address1;
                $corres_address1 =  str_replace("'","`",$corres_address1);

                $corres_address2 = $pra->corres_address2;
                $corres_address2 =  str_replace("'","`",$corres_address2);

                $corres_address3 = $pra->corres_address3;
                $corres_address3 =  str_replace("'","`",$corres_address3);

                $title_others = $pra->title_others;
                $title_others =  str_replace("'","`",$title_others);

                $country_others = $pra->country_others;
                $country_others =  str_replace("'","`",$country_others);

                $race_others = $pra->race_others;
                $race_others =  str_replace("'","`",$race_others);

                $religion_others = $pra->religion_others;
                $religion_others =  str_replace("'","`",$religion_others);

                $response = $this->soapWrapper->call('MOMBsc.MOMBsc',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'CI001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'ctry' => $pra->country_origin,
                    'ctry_ori' => $pra->country_origin,
                    'ctzn' => $pra->country,
                    'n_id' => $pra->new_ic,
                    'o_id' => $pra->old_ic,
                    'ctry_dob' => $pra->country_dob,
                    'p_id' => $pra->police_number,
                    'dob' => $pra->dob,
                    'nm' => $changesymbol,
                    'nm_pref' => $pra->name_prefed,
                    'ttl' => $pra->title,
                    'ttl_oth' => $title_others,
                    'gdr' => $pra->gender,
                    'marit' => $pra->marital,
                    'depend' => $pra->dependents,
                    'add1' => $address,
                    'add2' => $address2,
                    'add3' => $address3,
                    'add4' => $pra->postcode,
                    'add5' => $pra->state_code,
                    'cor_add1' => $corres_address1,
                    'cor_add2' => $corres_address2,
                    'cor_add3' => $corres_address3,
                    'cor_add4' => $pra->corres_postcode,
                     'cor_add5' => $pra->corres_state1,
                    'cor_add6' => $pra->corres_homephone,
                    'cor_add7' => $pra->corres_mobilephone,
                    'cor_add8' => $pra->corres_email,
                     'own' => $pra->ownership,
                    'rce' => $pra->race,
                    'bpr' => $pra->bumiputera,
                    'rel' => $pra->religion,
                    'edc' => $pra->education,
                    'ctry_oth' => $country_others,
                    'rce_oth' => $race_others,
                    'rel_oth' => $religion_others,
                    'ctg' => $pra->category,
                    ]
                ]);

                $empaddress = $empinfo->address;
                $empaddress =  str_replace("'","`",$empaddress);

                $empaddress2 = $empinfo->address2;
                $empaddress2 =  str_replace("'","`",$empaddress2);

                $empaddress3 = $empinfo->address3;
                $empaddress3 =  str_replace("'","`",$empaddress3);

                $division = $empinfo->division;
                $division =  str_replace("'","`",$division);

                $dept_name = $empinfo->dept_name;
                $dept_name =  str_replace("'","`",$dept_name);

                $emptype_others = $empinfo->emptype_others;
                $emptype_others =  str_replace("'","`",$emptype_others);
               
                $response_emp = $this->soapWrapper->call('MOMBsc.MOMEmpInf',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'EI001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'emp_nm' => $empinfo->empname,
                    'emp_ty' => $empinfo->emptype,
                    'emp_oth' => $emptype_others,
                    'emp_id' => $empinfo->employment_id,
                    'empr_id' => $empinfo->employer_id,
                    'post' => $empinfo->position,
                    'add1' => $empaddress,
                     'add2' => $empaddress2,
                    'add3' => $empaddress3,
                    'add4' => $empinfo->state_code,
                    'add5' => $empinfo->postcode,
                    'join1' => $empinfo->joined,
                    'slry' => $empinfo->salary,
                    'allw' => $empinfo->allowance,
                    'ddc' => $empinfo->deduction,
                     'occu' => $empinfo->occupation,
                    'occu_sec' => $empinfo->occupation_sector,
                    'emp_stat' => $empinfo->empstatus,
                    'dept_nm' => $dept_name,
                     'dvs' => $division,
                    'nob' => $empinfo->nature_business,
                    'of_phn' => $empinfo->office_phone,
                    'of_fx' => $empinfo->office_fax,
                    'wrk_ex' => $empinfo->working_exp,
                    ]
                ]);

                $changesymbol_sp = $spouse->name;
                $changesymbol_sp =  str_replace("'","`",$changesymbol_sp);

                $sp_emptype_others = $spouse->emptype_others;
                $sp_emptype_others =  str_replace("'","`",$sp_emptype_others);

                $response_sp = $this->soapWrapper->call('MOMBsc.MOMSp',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'CS001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    "nmSp"  => $changesymbol_sp,
                    "hmphSp"  => $spouse->homephone,
                    "mbphSp"  => $spouse->mobilephone,
                    "emtySp"  => $spouse->emptype,
                    "emtySp_ot"  => $sp_emptype_others,
                   ]
                 ]);

                $changesymbol_rf1 = $ref->name1;
                $changesymbol_rf1 =  str_replace("'","`",$changesymbol_rf1);

                $changesymbol_rf2 = $ref->name2;
                $changesymbol_rf2 =  str_replace("'","`",$changesymbol_rf2);

                $response_ref = $this->soapWrapper->call('MOMBsc.MOMRef', [
                    [
                    'AppNo' =>$pra->id_praapplication,
                    'txnCode' => 'CR001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'nm1' => $changesymbol_rf1,
                    'hm1' => $ref->home_phone1,
                    'hp1' => $ref->mobilephone1,
                    'rel1' => $ref->relationship1,
                    'nm2' => $changesymbol_rf2,
                    'hm2' => $ref->home_phone2,
                    'hp2' => $ref->mobilephone2,
                    'rel2' => $ref->relationship2,
                    ]
                ]);

                $bank_name = $fin->bank_name;
                $bank_name =  str_replace("'","`",$bank_name);

                $bank1 = $fin->bank1;
                $bank1 =  str_replace("'","`",$bank1);

                $bank2 = $fin->bank2;
                $bank2 =  str_replace("'","`",$bank2);

                $bank3 = $fin->bank3;
                $bank3 =  str_replace("'","`",$bank3);

                $bank4 = $fin->bank4;
                $bank4 =  str_replace("'","`",$bank4);

                $bank5 = $fin->bank5;
                $bank5 =  str_replace("'","`",$bank5);

                $bank6 = $fin->bank6;
                $bank6 =  str_replace("'","`",$bank6);

                $product_bundling_specify = $fin->product_bundling_specify;
                $product_bundling_specify =  str_replace("'","`",$product_bundling_specify);

                $cross_selling_specify = $fin->cross_selling_specify;
                $cross_selling_specify =  str_replace("'","`",$cross_selling_specify);

                $takaful_coverage_specify = $fin->takaful_coverage_specify;
                $takaful_coverage_specify =  str_replace("'","`",$takaful_coverage_specify);

                $response_fin = $this->soapWrapper->call('MOMBsc.MOMFin', [
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'FC001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'mthInc' => $fin->monthly_income,
                    'othInc' => $fin->other_income,
                    'totInc' => $fin->total_income,
                    'proBund' => $fin->product_bundling,
                    'proBundSpec' => $product_bundling_specify,
                     'cSell' => $fin->cross_selling,
                    'cSellSpec' => $cross_selling_specify,
                    'tkfCov' => $fin->takaful_coverage,
                    'tkfCovSpec' => $takaful_coverage_specify,
                    'purFac' => $fin->purpose_facility,
                    'tyCust' => $fin->type_customer,
                    'pyMod' => $fin->payment_mode,
                    'bNm' => $bank_name,
                    'accNo' => $fin->account_no,
                     'bnk1' => $bank1,
                    'bnk2' => $bank2,
                    'bnk3' => $bank3,
                    'bnk4' => $bank4,
                    'bnk5' => $bank5,
                    'bnk6' => $bank6,
                    ]
                ]);

                $comname1 = $com->name1;
                $comname1 =  str_replace("'","`",$comname1);

                $comfinancing1 = $com->financing1;
                $comfinancing1 =  str_replace("'","`",$comfinancing1);

                $comname2 = $com->name2;
                $comname2 =  str_replace("'","`",$comname2);

                $comfinancing2 = $com->financing2;
                $comfinancing2 =  str_replace("'","`",$comfinancing2);

                $response_com = $this->soapWrapper->call('MOMBsc.MOMCommit', [
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'CT001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'nm1' => $comname1,
                    'mth_py1' => $com->monthly_payment1,
                    'fin1' => $comfinancing1,
                    'rmn1' => $com->remaining1,
                    'nm2' => $comname2,
                    'fin2' => $comfinancing2,
                    'rmn2' => $com->remaining2,
                    'rmrk' => $com->remark,
                    ]
                ]);
                /*DSR A*/
            
                $response_da = $this->soapWrapper->call('MOMBsc.MOMDSRA',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DA001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'sec' => $dsr_a->sector,
                    'gdr' => $dsr_a->gender,
                    'cat' => $dsr_a->category,
                    'ex_mb' => $dsr_a->existing_mbsb,
                    'ag' => $dsr_a->age,
                    'bc_sal1' => $dsr_a->basic_salary1,
                    'bc_sal2' => $dsr_a->basic_salary2,
                    'bc_sal3' => $dsr_a->basic_salary3,
                    'fx_alw1' => $dsr_a->fixed_allowances1,
                    'fx_alw2' => $dsr_a->fixed_allowances2,
                    'fx_alw3' => $dsr_a->fixed_allowances3,
                    'tot_en1' => $dsr_a->total_earnings1,
                    'tot_en2' => $dsr_a->total_earnings2,
                    'tot_en3' => $dsr_a->total_earnings3,
                    'vr_in' => $dsr_a->variable_income,
                    'gmi' => $dsr_a->gmi,
                    'ep1' => $dsr_a->epf1,
                    'ep2' => $dsr_a->epf2,
                    'ep3' => $dsr_a->epf3,
                    'sc1' => $dsr_a->socso1,
                    'sc2' => $dsr_a->socso2,
                    'sc3' => $dsr_a->socso3,
                    'in_tx1' => $dsr_a->income_tax1,
                    'in_tx2' => $dsr_a->income_tax2,
                    'in_tx3' => $dsr_a->income_tax3,
                    'ot_cn1' => $dsr_a->other_contribution1,
                    'ot_cn2' => $dsr_a->other_contribution2,
                    'ot_cn3' => $dsr_a->other_contribution3,
                    'tot_dc1' => $dsr_a->total_deduction1,
                    'tot_dc2' => $dsr_a->total_deduction2,
                    'tot_dc3' => $dsr_a->total_deduction3,
                    'nmi' => $dsr_a->nmi,
                    'ad_in' => $dsr_a->additional_income,
                    'ite' => $dsr_a->ite,
                    'sc1_vby' => $dsr_a->sec1_verified_by,
                    'sc1_vt' => $dsr_a->sec1_verified_time,
                    'tot_gm' => $dsr_a->total_gross_income,
                    'tot_nin' => $dsr_a->total_net_income
                    ]
                ]);
                
                 $response_db = $this->soapWrapper->call('MOMBsc.MOMDSRB', [
                    [
                        'AppNo' => $pra->id_praapplication,
                        'txnCode' => 'DB001',
                        'ActCode' => 'A',
                        'usr' => 'momwapsuat',
                        'pwd' => 'J@ctL71N$#',
                        'fnam' => $dsr_b->financing_amount,
                        'ten' => $dsr_b->tenure,
                        'rt' => $dsr_b->rate,
                        'mt_rp' => $dsr_b->monthly_repayment,
                        'ex_ln' => $dsr_b->existing_loan,
                        'tot_gpx' =>$dsr_b->total_group_exposure
                    ]
                ]);

                 $response_dc = $this->soapWrapper->call('MOMBsc.MOMDSRC', [
                    [
                        'AppNo' => $pra->id_praapplication,
                        'txnCode' => 'DC001',
                        'ActCode' => 'A',
                        'usr' => 'momwapsuat',
                        'pwd' => 'J@ctL71N$#',
                        'mb_ho' => $dsr_c->mbsb_housing,
                        'mb_hr' =>  $dsr_c->mbsb_hire,
                        'mb_p' => $dsr_c->mbsb_personal,
                        'd_ag' => $dsr_c->de_angkasa,
                        'd_gvh' => $dsr_c->de_govhousing,
                        'd_kp' => $dsr_c->de_koperasi,
                        'd_nb' => $dsr_c->de_nonbiro,
                        'd_ol' => $dsr_c->de_otherloan,
                        'd_ot' => $dsr_c->de_other,
                        'c_ho' => $dsr_c->ccris_housing,
                        'c_cd' => $dsr_c->ccris_creditcard,
                        'c_hr' => $dsr_c->ccris_hire,
                        'c_od' => $dsr_c->ccris_otherdraft,
                        'c_ot' => $dsr_c->ccris_other,
                        'tot_cm' => $dsr_c->total_commitment
                    ]
                ]);

                $response_de = $this->soapWrapper->call('MOMBsc.MOMDSRE', [
                    [
                        'AppNo' => $pra->id_praapplication,
                        'txnCode' => 'DE001',
                        'ActCode' => 'A',
                        'usr' => 'momwapsuat',
                        'pwd' => 'J@ctL71N$#',
                        'tot_db' => $dsr_e->total_debt,
                        'n_in' => $dsr_e->net_income,
                        'ds' => $dsr_e->dsr,
                        'ds_bn' => $dsr_e->dsr_bnm,
                        'ndi' => $dsr_e->ndi,
                        'dec' => $dsr_e->decision,
                        'd_sv' => $dsr_e->data_save,
                        'sc2_vby' => $dsr_d->sec2_verified_by,
                        'sc2_vt' => $dsr_d->sec2_verified_time,
                    ]
                ]);

                $response_rk = $this->soapWrapper->call('MOMBsc.MOMRsk', [
                    [
                        'AppNo' => $pra->id_praapplication,
                        'txnCode' => 'DE001',
                        'ActCode' => 'A',
                        'usr' => 'momwapsuat',
                        'pwd' => 'J@ctL71N$#',
                        'ds' => $dsr_e->dsr,
                        'ndi' => $dsr_e->ndi,
                        'sl_dn' => $risk->salary_deduction,
                        'ag' => $risk->age,
                        'edlv' => $risk->education_level,
                        'emp' => $risk->employment,
                        'pos' => $risk->position,
                        'mart' => $risk->marital_status,
                        'sp_ep' => $risk->spouse_employment,
                        'pr_ln' => $risk->adverse_ccris,
                        'ad_c' => $risk->adverse_ccris,
                        'tot_sc' => $risk->total_score,
                        'grd' => $risk->grading,
                        'dcs' => $risk->decision,
                        'sc_by' => $risk->scoring_by,
                        'd_sc' => $risk->date_scoring,
                        'rv_by' => $risk->reviewed_by,
                        'rv_d' => $risk->date_reviewed,
                    ]
                ]);

                $response_ln = $this->soapWrapper->call('MOMBsc.MOMLn', [
                    [
                        'AppNo' => $pra->id_praapplication,
                        'txnCode' => 'LN001',
                        'ActCode' => 'A',
                        'usr' => 'momwapsuat',
                        'pwd' => 'J@ctL71N$#',
                        'pack' => $loan_a->package,
                        'lnamt' => $loan_a->loanammount,
                        'nwten' => $loan_a->new_tenure,
                        'mxln' => $loan_a->maxloan,
                        'inst' => $loan_a->installment,
                        'rt' => $loan_a->rate,
                    ]
                ]);

                 $response_tm= $this->soapWrapper->call('MOMBsc.MOMTrm', [
                    [
                        'AppNo' => $pra->id_praapplication,
                        'txnCode' => 'TR001',
                        'ActCode' => 'A',
                        'usr' => 'momwapsuat',
                        'pwd' => 'J@ctL71N$#',
                        'b_offid' => $term->mo_id,
                        'v_offid' => $term->verified_by,
                        'v_rmk' => $term->verification_remark,
                        'brid' => $term->id_branch,
                    ]
                ]);

                $changesymbol_st = $pra->name;
                $changesymbol_st =  str_replace("'","`",$changesymbol_st);

                 $response_stat = $this->soapWrapper->call('MOMBsc.MOMStat',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'ST001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'ApNm' => $changesymbol_st,
                    'ApID' => $pra->new_ic,
                    'ApStat' => 'GLAN',
                    ]
                ]);
              
                  $response_doc7 = $this->soapWrapper->call('MOMBsc.MOMDoc',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DC001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'nm' => $doc7->name,
                    'dwld' => $doc7->download,
                    'upl' => $doc7->upload,
                    'ty' => $doc7->type,
                    'vrf_flg' => $doc7->verification,
                    'vfr' => $doc7->verified_by
                    ]
                ]);

                  $response_doc6 = $this->soapWrapper->call('MOMBsc.MOMDoc',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DC001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'nm' => $doc6->name,
                    'dwld' => $doc6->download,
                    'upl' => $doc6->upload,
                    'ty' => $doc6->type,
                    'vrf_flg' => $doc6->verification,
                    'vfr' => $doc6->verified_by
                    ]
                ]);

                  $response_doc9 = $this->soapWrapper->call('MOMBsc.MOMDoc',[
                    [
                    'AppNo' => $pra->id_praapplication,
                    'txnCode' => 'DC001',
                    'ActCode' => 'A',
                    'usr' => 'momwapsuat',
                    'pwd' => 'J@ctL71N$#',
                    'nm' => $doc9->name,
                    'dwld' => $doc9->download,
                    'upl' => $doc9->upload,
                    'ty' => $doc9->type,
                    'vrf_flg' => $doc9->verification,
                    'vfr' => $doc9->verified_by
                    ]
                ]);
                    

                //var_dump($response_emp);
                /*dd($response_emp);
                dd($response);
                dd($response_sp);
                dd($response_fin);
                dd($response_ref);
                dd($response_com);*/

                var_dump($response_com);
                dd($response_com);


                var_dump($response_emp);
                dd($response_emp);

                var_dump($response_sp);
                dd($response_sp);


                var_dump($response_fin);
                dd($response_fin);



                    // $result = var_dump($response);
                  //   echo '<pre>' . var_export($response->ResponseDesc, true) . '</pre>';

              
                   
                Mail::send('mail.mo.appsubmited_customer', compact('customer_email','customer_name','mo_name','mo_email','mo_phone','loanamount','password'), function ($message) use ($customer_email, $customer_name, $main_sender_email, $main_sender_name) {
                    $message->from($main_sender_email, $main_sender_name);
                    $message->subject('Your Application Successfully Submitted to MBSB');
                    $message->to($customer_email, $customer_name);
                    });   
                $message->attach($file_ftp9);
                    

                     return response()->json(['status' => "1",'message' => "Application Successfully Routed to Branch"]);
                    
               // }
            //}
        }

        }
         
  }


    public function requestedit($id) {
         
          $user = Auth::user();
          $term = Term::where('id_praapplication', $id)->update(['edit' => '1' ]);
          $email = $user->email;
          $basic = Basic::latest('created_at')->where('id_praapplication', $id)->limit('1')->first(); 
          $fullname  = $basic->name;
          $new_ic  = $basic->new_ic;
      
       // Set Time Log
          date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Request Edit/Route Back';
        $log_download->downloaded_at   =  $time_log;
        //$log_download->log_remark   =  $remark_admin;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
          
         /*  Mail::send('mail.requestedit', compact('fullname'), function ($message) use ($email, $fullname) {
          $message->from('loan@ezlestari.com.my', 'Personal loan Co-Opbank Persatuan');
          $message->subject('Request Edit Application Form');
          $message->to($email, $fullname);
          });
          
          // Kasih notif ke admin juga
          $email_admin = "loan@ezlestari.com.my";
          $fullname_admin ="Personal loan Co-Opbank Persatuan";
          Mail::send('mail.requestedit_admin', compact('email','fullname','new_ic'), function ($message) use ($email_admin, $fullname_admin) {
          $message->from('no-reply@ezlestari.com.my', 'no-reply');
          $message->subject('New Request for Edit Application Form');
          $message->to($email_admin, $fullname_admin);
          }); */
          return redirect('home')->with('message', 'Request for edit have been sent, please wait for admin approval');
    }

     public function approvetedit($id) {
    $user = Auth::user();
          $term = Term::where('id_praapplication', $id)->update(['edit' => '1' ]);
          $basic = Basic::latest('created_at')->where('id_praapplication', $id)->limit('1')->first();
          $pra = PraApplication::latest('created_at')->where('id',$id)->limit('1')->first();
          $fullname  = $basic->name;
          $email  = $pra->email;
    
          $term = Term::where('id_praapplication', $id)->update(['edit' => '0',  'status' => '0', 'verification_result' => '4']);
      // Set Time Log
          date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Approve Request Route Back from User';
        $log_download->downloaded_at   =  $time_log;
        //$log_download->log_remark   =  $remark_admin;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
        
        /*  Mail::send('mail.requestedit_approve', compact('fullname'), function ($message) use ($email, $fullname) {
          $message->from('loan@ezlestari.com.my', 'Personal loan Co-Opbank Persatuan');
          $message->subject('Request Edit Application Form has Approved');
          $message->to($email, $fullname);
          }); */

          return redirect('admin')->with('message', 'request for edit have been approved');
    }
  
    public function routeback(Request $request) {
        $user = Auth::user();
        $id_praapplication = $request->input('id_praapplication');

        $cus_name = $request->input('cus_name');
        $cus_ic = $request->input('cus_ic');

        $reason = $request->input('reason');
        $cus_email = $request->input('cus_email');

        // Set Time Activity
    
            date_default_timezone_set("Asia/Kuala_Lumpur");
            $time_name = date('Ymd');
            $time_download = date('Y-m-d H:i:s');
            $log_download = new Log_download;
            
            $user = Auth::user();
            $id_user =  $user->id;
            $log_download->id_praapplication   =  $id_praapplication;
            $log_download->id_user   =  $id_user;
            $log_download->Activity   =  'Route Back to User';
            $log_download->downloaded_at   =  $time_download;
            $log_download->log_reason  =  $reason;
            $log_download->save();
          
          
            // Send to Customer Email
            /*
            Mail::send('mail.notifikasi_routeback_customer', compact('cus_name','cus_ic','reason'), function ($message) use ($cus_email, $cus_name) {
            $message->from('loan@ezlestari.com.my', 'loan@ezlestari.com.my');
            $message->subject('[EZLESTARI.COM.MY] Route Back Application');
            $message->to($cus_email, $cus_name);
            
        
            }); */
            
        //$name  = $request->input('Name');
        $term = Term::where('id_praapplication', $id_praapplication)->update(['edit' => '0',  'status' => '0', 'verification_result' => '4']);
        return redirect('routeback_status')->with('success', 'Application has Routed Back to User');
        

    }


  public function uploaddoc(Request $request)
    {
        
        $user = Auth::user();
        $id_praapplication = $request->input('id_praapplication');
        $icnumber = $request->input('icnumber');

        $debt_consolidation = $request->input('debt_consolidation');
       
          $financial = Financial::where('id_praapplication', $id_praapplication)
        ->update(['debt_consolidation' => $debt_consolidation ]);

        
        if($debt_consolidation>0) {
        
          // settlement
          $setgroup = array_map(null, $request->set_institution, $request->set_installment, $request->set_amount); 
            foreach ($setgroup as $del) {
              $delete = Settlement::where('id_praapplication', $id_praapplication)->delete();
            }
            
            foreach ($setgroup as $val) {
             /* $val[2] = str_replace('/', '-', $val[2]);
              $valx =  date('Y-m-d', strtotime($val[2])); */
              $settlement = new Settlement;
              $settlement->institution = $val[0];
              $settlement->installment = $val[1]; 
              $settlement->amount = $val[2];
              $settlement->id_praapplication = $id_praapplication;
              $settlement->save();
            } 

        }
        
        $pra = PraApplication::latest('created_at')->where('id',$id_praapplication)->limit('1')->first();
        if ($icnumber<>$pra->icnumber) {
          return redirect('upload')->with('error', 'Wrong IC Number. Please try again ');
        }
        $email  = $pra->email;

        date_default_timezone_set("Asia/Kuala_Lumpur");
        $time_name = date('Ymd');
        $time_log = date('Y-m-d H:i:s');
        
        $doc_status = Term::where('id_praapplication', $id_praapplication)->update(['time_upload' => $time_log, 'status' => '99', 'file_created' => $time_log]);
        // $term = Term::where('id_praapplication', $id_praapplication)->update(['status' => '1' ]);
                
        $log_download = new Log_download;
        
        $user = Auth::user();
        $id_user =  $user->id;
        $log_download->id_praapplication   =  $id_praapplication;
        $log_download->id_user   =  $id_user;
        $log_download->Activity   =  'Customer Upload Document';
        $log_download->downloaded_at   =  $time_log;
        // $log_download->log_reason  =  $reason;
        $log_download->save();
        
        // Send to Customer Email
        
        $cus_name = $pra->fullname;
        $to = $pra->email;

        $main_sender = Mail_System::where('id',1)->first();
        $main_sender_name= $main_sender->name;
        $main_sender_email= $main_sender->email;


        $noreply_sender = Mail_System::where('id',2)->first();
        $noreply_sender_name = $noreply_sender->name;
        $noreply_sender_email = $noreply_sender->email;
        
        Mail::send('mail.cus_submitdokumen', compact('cus_name'), function ($message) use ($main_sender_email, $main_sender_name, $cus_name, $to) {
        $message->from($main_sender_email, $main_sender_name);
        $message->subject('Documents Sucessfully Uploaded');
        $message->to($to,$cus_name);
        
    
        });

          // Email ke bos
          $loanammount = loanAmmount::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first();
          $jumlahpinjam = $loanammount->loanammount;
          $empinfo = Empinfo::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first();
          $majikan = $empinfo->empname;
          $basic = Basic::latest('created_at')->where('id_praapplication',  $id_praapplication )->limit('1')->first();
          $nama = $basic->name;
          $ic = $basic->new_ic;

          Mail::send('mail.notification_upload_document_boss', compact('id','nama','ic','majikan','jumlahpinjam'), function ($message) use ($main_sender_email, $main_sender_name) {
          $message->from($main_sender_email, $main_sender_name);
          $message->subject('[DCA MBSB] Stage 1 - Application Request');
          $message->to('idsusilo@gmail.com', 'Che Onn Ismail');
        
          }); 

         ?>
          <script>alert("Dokumen anda telah berjaya dihantar");location.href="../home";</script>
         <?php
        
    }


    
}
//
