<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\GeoLoc_Error;
use Auth;
use App\Model\PraApplication;

class GeoLocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
          
    }

    public function map($id) {
         $location =  PraApplication::latest('id')->where('id','=',$id)->first();
        return view('location', compact('id','location'));
    }

    public function error($id)
    {
        $geoloc_error = GeoLoc_Error::Where('id',$id)->first();
        $user = Auth::user();
        return view('geolocation.error', compact('geoloc_error','user')); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function getLocation(Request $request) {


       if(!empty($request->latitude) && !empty($request->longitude)){
            //Send request and receive json data by latitude and longitude
            $url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($request->latitude).','.trim($request->longitude).'&sensor=false';
            $json = @file_get_contents($url);
            $data = json_decode($json);
            $status = $data->status;
            if($status=="OK"){
                //Get address from json data
                $location = $data->results[0]->formatted_address;
            }else{
                $location =  '';
            }
            //Print address 
            return response()->json([   'status' => "Detect Request", 
                                            'latitude' => $request->latitude, 'longitude' => $request->longitude, 'location' => $location ]);
        }
        else {
            return response()->json([   'status' => "Cannot Detect Request"]);
        }
        

    }
}
