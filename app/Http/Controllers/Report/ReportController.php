<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Input;
use App\Model\PraApplication;
use App\Model\User;
use App\Model\Term;
use DateTime;
use App;
use DB;
use App\Model\Employment;


class ReportController extends Controller
{
		private $testing;
		private $base_url;
		private $subject_t;
        public function __construct() {
				 $this->middleware('auth'); 
				 $this->middleware('admin');
				 
				

				//$this->middleware('branch');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
		
    }
	

    public function calculated()
    {
         $user = Auth::user();
		 $employment = Employment::get();


      return view('report.calculated', [ 'user' => $user, 'employment' => $employment    ]);
        
    }
    
	
	 public function calculated_view(Request $request)
    {

          $user = Auth::user();
		  $employment = Employment::get();
            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');
			$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
			
			$viewdate1 = date("d-m-Y", strtotime($tanggal1));
			$viewdate2 = date("d-m-Y", strtotime($tanggal2));
       
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 

			$empname = Employment::latest('created_at')->where('id',$jenis_pekerjaan)->first();
            $email = User::pluck('email');
			$sumloan = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)->orderBy('created_at', 'asc')->get()->sum('loanamount');
			 
             $pra = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)->whereNull('referral_id')->whereNull('email')->orderBy('created_at', 'asc')->get();
        
				return view('report.calculated_view', compact('pra','user','date1','date2','viewdate1','viewdate2','sumloan','employment','empname','jenis_pekerjaan')); 
              // return view('report.calculated_excel', [ 'pra' => $pra ]);

        
    }

       public function calculated_excel(Request $request)
    {

          
            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');
			$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
       
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 


            $email = User::pluck('email');
        
             $pra = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)
             ->whereNotIn('email', $email)->groupBy('icnumber')->orderBy('created_at', 'asc')->get();
        

              return view('report.calculated_excel', [ 'pra' => $pra, 'jenis_pekerjaan' => $jenis_pekerjaan ]);

        
    }

     public function registered()
    {
         $user = Auth::user();
		$employment = Employment::get();

      return view('report.registered', [ 'user' => $user, 'employment' => $employment    ]);
        
    }
    

    public function registered_view(Request $request)
    {
			$user = Auth::user();
			$employment = Employment::get();

            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');
			$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
			
			
			$viewdate1 = date("d-m-Y", strtotime($tanggal1));
			$viewdate2 = date("d-m-Y", strtotime($tanggal2));
       
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 

			$empname = Employment::latest('created_at')->where('id',$jenis_pekerjaan)->first();
            $email = User::pluck('email');
			$sumloan = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)->orderBy('created_at', 'asc')->get()->sum('loanamount');
             $pra = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)->whereNull('referral_id')->whereNotNull('email')->orderBy('created_at', 'asc')->get();

             /*
             $sumloan = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)
             ->whereIn('email', $email)->groupBy('icnumber')->orderBy('created_at', 'asc')->get()->sum('loanamount');
             $pra = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)
             ->whereIn('email', $email)->groupBy('icnumber')->orderBy('created_at', 'asc')->get();
             */
        

		
			return view('report.registered_view', compact('pra','user','date1','date2','viewdate1','viewdate2','sumloan','jenis_pekerjaan','employment','empname')); 
           //    return view('report.registered_excel', [ 'pra' => $pra ]);

        
    }
	
	 public function registered_excel(Request $request)
    {
			$user = Auth::user();
          
            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');
			$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
			
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 


            $email = User::pluck('email');
        
             $pra = PraApplication::latest('created_at')->where('created_at','>=',$date1)->where('created_at','<=',$date2)
             ->whereIn('email', $email)->groupBy('icnumber')->orderBy('created_at', 'asc')->get();
        

		
			// return view('report.registered_view', compact('pra','user')); 
            return view('report.registered_excel', [ 'pra' => $pra, 'jenis_pekerjaan' => $jenis_pekerjaan ]);

        
    }




     public function submitted()
    {
         $user = Auth::user();
		 $employment = Employment::get();

      return view('report.submitted', [ 'user' => $user, 'employment' => $employment     ]);
        
    }


    public function submitted_view(Request $request)
    {
            $user = Auth::user();
			$employment = Employment::get();
			
            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');
			$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
			
			
			$viewdate1 = date("d-m-Y", strtotime($tanggal1));
			$viewdate2 = date("d-m-Y", strtotime($tanggal2));
       
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 

			$empname = Employment::latest('created_at')->where('id',$jenis_pekerjaan)->first();

			//$pra = Term::latest('file_created')->where('file_created','>=',$date1)->where('file_created','<=',$date2) ->where('status', 1)->where('verification_status','0')->where('referral_id','0')->get();
            $pra = Term::latest('file_created')->where('file_created','>=',$date1)->where('file_created','<=',$date2)
            ->where('status', 1)->where('verification_result','2')->where('referral_id','0')->get();
			 
			$sumloan = Term::latest('file_created')->where('file_created','>=',$date1)->where('file_created','<=',$date2)
            ->where('status', 1)->where('verification_result','2')->join('loanammount', 'term.id_praapplication', '=', 'loanammount.id_praapplication')
			->get()->sum('loanammount.loanammount');
			
			// $sumloan = $pra->loanAmmount->sum('loanammount');
			//print $sumloan;
		$sales = term::join('loanammount', 'loanammount.id_praapplication', '=', 'term.id_praapplication')
        ->select(DB::raw('sum(loanammount) as total'))
        ->where('term.file_created','>=',$date1)->where('term.file_created','<=',$date2)
        ->where('term.status', 1)->where('term.verification_result','2')
        ->first(); 
        

        
             return view('report.submitted_view', compact('pra','user','date1','date2','viewdate1','viewdate2','sales','jenis_pekerjaan','employment','empname'));
           //    return view('report.registered_excel', [ 'pra' => $pra ]);

        
    }



    public function submitted_excel(Request $request)
    {

          
            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');
			$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
			
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 
			

            
              $pra = Term::latest('file_created')->where('file_created','>=',$date1)->where('file_created','<=',$date2)
             ->where('status', 1)->get();
        

        

              return view('report.submitted_excel', [ 'pra' => $pra, 'jenis_pekerjaan' => $jenis_pekerjaan ]);

        
    }
    
	public function bystatus()
    {
         $user = Auth::user();
		 $employment = Employment::get();

      return view('report.bystatus', [ 'user' => $user, 'employment' => $employment     ]);
        
    }
	
	
    public function bystatus_view(Request $request)
    {
            $user = Auth::user();
			$employment = Employment::get();
			
            $tanggal1  = $request->input('tanggal1');
            $tanggal2  = $request->input('tanggal2');
			$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
			$status  = $request->input('status');
			if ($status==1) {
				$s_string = "Approved";		
			}
			elseif ($status==2) {
				$s_string = "Rejected";
			}
			elseif ($status==3) {
				$s_string = "Pending Approval";
			}
			elseif ($status==0) {
				$s_string = "In Process";
			}
			elseif ($status==-99) {
				$s_string = "All";
			}
			
			$viewdate1 = date("d-m-Y", strtotime($tanggal1));
			$viewdate2 = date("d-m-Y", strtotime($tanggal2));
       
            $date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
            $date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2)); 

			$empname = Employment::latest('created_at')->where('id',$jenis_pekerjaan)->first();
			
			if (($status<>'2') AND ($status<>'-99')) {
				$pra = Term::latest('file_created')
				->where('verification_result_by_bank',$status)->where('file_created','>=',$date1)->where('file_created','<=',$date2)
				->where('status', 1)->where('referral_id','0')->get();
			}
			elseif($status=='-99') {
				$pra = Term::latest('file_created')
				 ->orWhere(function ($query) {
						$query->wherein('verification_result_by_bank',['0','1','2','3'])
						->Wherein('verification_result',['0','1','2','3']);
					})
				->where('file_created','>=',$date1)->where('file_created','<=',$date2)
				->where('status', 1)->get();
			}
			else {
				$pra = Term::latest('file_created')
				->where('verification_result_by_bank','2')->orWhere('verification_result','3')
				->where('file_created','>=',$date1)->where('file_created','<=',$date2)
				->where('status', 1)->get();
			}
			
			 

			
			// $sumloan = $pra->loanAmmount->sum('loanammount');
			//print $sumloan;
		$sales = term::join('loanammount', 'loanammount.id_praapplication', '=', 'term.id_praapplication')
        ->select(DB::raw('sum(loanammount) as total'))
        ->where('term.file_created','>=',$date1)->where('term.file_created','<=',$date2)
        ->where('term.status', 1)
        ->first(); 
        

        
             return view('report.bystatus_view', compact('pra','user','date1','date2','viewdate1','viewdate2','sales','jenis_pekerjaan','employment','empname','status','s_string'));
           //    return view('report.registered_excel', [ 'pra' => $pra ]);

        
    }
	
	public function bystatus_excel(Request $request)
    {
     
		$tanggal1  = $request->input('tanggal1');
		$tanggal2  = $request->input('tanggal2');
		$jenis_pekerjaan  = $request->input('jenis_pekerjaan');
		$status  = $request->input('status');
		
		$date1 =  date('Y-m-d 00:00:01', strtotime($tanggal1)); 
		$date2 =  date('Y-m-d 23:59:59', strtotime($tanggal2));
		
	
		if (($status<>'2') AND ($status<>'-99')) {
				$pra = Term::latest('file_created')
				->where('verification_result_by_bank',$status)->where('file_created','>=',$date1)->where('file_created','<=',$date2)
				->where('status', 1)->get();
			}
			elseif($status=='-99') {
				$pra = Term::latest('file_created')
				 ->orWhere(function ($query) {
						$query->wherein('verification_result_by_bank',['0','1','2','3'])
						->Wherein('verification_result',['0','1','2','3']);
					})
				->where('file_created','>=',$date1)->where('file_created','<=',$date2)
				->where('status', 1)->get();
			}
			else {
				$pra = Term::latest('file_created')
				->where('verification_result_by_bank','2')->orWhere('verification_result','3')
				->where('file_created','>=',$date1)->where('file_created','<=',$date2)
				->where('status', 1)->get();
			}


		  return view('report.bystatus_excel', [ 'pra' => $pra, 'jenis_pekerjaan' => $jenis_pekerjaan ]);
     
    }
    
	
		
		 

}
