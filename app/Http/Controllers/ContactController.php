<?php

namespace App\Http\Controllers;

use App\Http\Requests;

use Illuminate\Http\Request;
use Auth;
use Input;
use Mail;


class ContactController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{

	}

	/**
	 * Show the application dashboard.
	 *
	 * @return Response
	 */
	public function agent(Request $request)
	{	$faq=1;
		$user = Auth::user();
		
$device = $request->header('User-Agent');
       	 return view('home.agent', compact('faq','user','device')); 	
         
    }

    public function index(Request $request)
	{	$faq=1;
		$user = Auth::user();
		
$device = $request->header('User-Agent');
       	 return view('home.contact', compact('faq','user','device')); 	
         
    }
	
	public function contact_save(Request $request) {
				$named = $request->input('named');
				$emaild = $request->input('emaild');
				$subject = $request->input('subject');
				$pesan = htmlentities($request->input('pesan'));
				$copy = $request->input('copy');
				
	 
			
						
						// Send to Admin Email
						
		
						Mail::send('mail.contact', compact('named','emaild','subject','pesan'), function ($message) use ($emaild, $named, $subject) {
						$message->from($emaild, $named);
						$message->subject($subject);
						$message->to('loan@ezlestari.com.my', 'loan@ezlestari.com.my');
						$message->to('idsusilo@gmail.com', 'Susilo Giono');
	 
						
						});
						
						// Send to Customer Email
						if ($copy==1) {
						 	Mail::send('mail.contact_copy', compact('named','emaild','subject','pesan'), function ($message) use ($emaild, $named) {
							  $message->from('loan@ezlestari.com.my', 'loan@ezlestari.com.my');
							  $message->subject('subject');
							  $message->to($emaild, $named);
	  
					  
							  });
							
						}
			   return redirect('contact')->with('message', 'Your message has been successfully sent. We will contact you very soon! ');
					
						
	}
}