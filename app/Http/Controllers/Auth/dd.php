<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Auth;
use App\Model\Loglogin;
use Alert;

class MBSBAuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLogin() {
        return view('admin_mbsb.login');
    }

     public function login(Request $request)
    {

           
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password ])) {
            // Authentication passed...

         $userx = Auth::user();

         $loglogin = new Loglogin;

         $loglogin->name = $userx->name;
         $loglogin->email = $userx->email;
         $loglogin->lat = $request->latitude;
         $loglogin->lng = $request->longitude;
         $loglogin->location = $request->location;
         $loglogin->save();
        
         if($userx->role==8) {
            return redirect('admin');
         }else {
            Alert::error("Sorry, you don't have access to this page");
              return redirect('/mbsb_login');
         }
            
        }
        else {
              Alert::error('Your account and/or password is incorrect, please try again');
              return redirect('/mbsb_login');
        }
    }

    public function logout(Request $request) {
        $userx = Auth::user();
         if($userx->role==8) {
            Auth::logout();
            return redirect('/mbsb_login');
         }
         else{
             Auth::logout();
            return redirect('/');
         }
     
    }

}
