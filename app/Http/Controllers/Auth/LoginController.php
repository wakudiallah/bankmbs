<?php

namespace App\Http\Controllers\Auth;

use App\Model\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Auth;
use App\Model\Loglogin;
use Alert;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    protected $maxAttempts = 3;
protected $decayMinutes = 1;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

     public function adm()
    {
        return view('auth.login_adm');    
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }



   public function username()
    {
        return 'email';
    }
    

  

    public function login(Request $request)
    {
        if ($this->hasTooManyLoginAttempts($request)) {
            Alert::error('Too many login attempts. Please try again in 60 seconds');
            return redirect('/');
        }
   
        /** Validation is done, now login user */
        //else to user profile
        $count = User::where('email',$request->email)->count();
        $check_email = User::where('email',$request->email)->first();
        
        if($count=='1'){

            $role= $check_email->role;
            if($role=='0'){

                $check = Auth::attempt(['email' => $request['email'],'password' => $request['password']]);

                if($check){
                    $user = Auth::user();
                    /** Since Authentication is done, Use it here */
                    $this->clearLoginAttempts($request);
                    if ($user->role == 0){
                        $userx = Auth::user();

                        $loglogin = new Loglogin;

                        $loglogin->name = $userx->name;
                        $loglogin->email = $userx->email;
                        $loglogin->lat = $request->latitude;
                        $loglogin->lng = $request->longitude;
                        $loglogin->location = $request->location;
                        $loglogin->save();
                        return redirect('home');
                    }
                }
                else{
                /** Authentication Failed */
                    $this->incrementLoginAttempts($request);
                    Alert::error('Your account and/or password is incorrect, please try again');
                    return redirect('');
                }
            }
            else if($role!='0'){
                Alert::error("Sorry, you don't have access to this page");
                return redirect('/');
            }
        }
         else{
            Alert::error("Couldn't find your account");
                  return redirect('/');
        }
    }


}
