<?php

namespace App\Http\Middleware;

use Closure;

class IpMiddleware
{

    public function handle($request, Closure $next)
    {
        if ($request->ip() != "::1") {
        // here insted checking single ip address we can do collection of ip 
        //address in constant file and check with in_array function
            return redirect('http://localhost:8080/dataentry/dataentry/public/assets/public/403.html');
        }

        return $next($request);
    }

}