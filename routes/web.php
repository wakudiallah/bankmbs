<?php

Route::group(['middleware' => 'web'], function () {

Auth::routes();
Route::get('/home', 'HomeController@index');
Route::get('/upload', 'HomeController@upload');
Route::post('form/uploaddoc', 'LoanForm\LoanFormController@uploaddoc');
Route::post('verifiedDoc/upload/{id}', 'Admin\VerifiedDocController@upload');
Route::get('/admin/downloaddocpdf/{ic}/{file}', 'HomeController@downloaddocpdf');
Route::get('/agreement/{pra}', 'HomeController@agreement');
Route::get('/terma/{pra}', 'HomeController@terma');
Route::get('/downloadpdf/{id_pra}', 'User\UserController@downloadpdf');
Route::post('form/savesettlement', 'LoanForm\LoanFormController@savesettlement');
Route::get('/postcode/{id}', 'HomeController@postcode');
Route::get('/review/{id}', 'HomeController@review');
Route::get('/faq', 'FaqController@index');
Route::get('/contact', 'ContactController@index');
Route::get('/agent', 'ContactController@agent');
Route::post('/contact/contact_save', 'ContactController@contact_save');

Route::view('/excel', 'excel', ['name' => 'excel']);
Route::post('/move', 'Admin\AdminController@move');

Route::resource('admin/master/package', 'Master\PackageController'); 
Route::resource('admin/master/job-sector', 'Master\EmploymentController');
Route::get('/admin/step1/{id_pra}/{view}', 'Admin\AdminController@step1_verification');
Route::post('/admin/scoring', 'Admin\AdminController@scoring');
Route::post('/admin/sector', 'Admin\AdminController@sector');
Route::get('grading/{id}', 'Admin\AdminController@grading');

Route::get('admin/master/scorerating', 'Master\PackageController@scorerating'); 
Route::post('/admin/master/save/scorerating', 'Master\PackageController@savescorerating'); 
Route::post('/admin/master/update/scorerating/{id}', 'Master\PackageController@update_scorerating'); 

Route::get('admin/master/scorecard', 'Master\PackageController@scorecard'); 
Route::post('/admin/master/save/scorecard', 'Master\PackageController@savescorecard'); 
Route::post('/admin/master/update/scorecard/{id}', 'Master\PackageController@update_scorecard'); 
//DETAIL
Route::get('/admin/detail_pra/{id_pra}', 'Admin\AdminController@detail_pra');
Route::get('/moform/{id_pra}/{verify}', 'LoanForm\MoFormController@show'); //new
Route::get('/admin/get_document/{id_pra}/{name}', 'Admin\AdminController@get_document');//new
Route::get('/get_document/{id_pra}/{name}', 'HomeController@get_document');//new
//add doc
Route::get('/add_document/{id_pra}/{verify}', 'LoanForm\MoFormController@add_document'); //new

Route::get('/', 'PraApplication\PraApplicationController@index');
Route::get('applynow', 'PraApplication\PraApplicationController@applynow');
Route::resource('praapplication', 'PraApplication\PraApplicationController'); 
Route::resource('form', 'LoanForm\LoanFormController'); 
Route::resource('employer', 'Master\EmployerController'); 
Route::post('employer/update', 'Master\EmployerController@update2');
Route::post('employer/delete', 'Master\EmployerController@delete'); 
Route::resource('user', 'User\UserController'); 
Route::get('auth/login', 'Auth\LoginController@getLogin');
Route::get('admdca/login', 'Auth\LoginController@adm');
Route::post('auth/login', 'Auth\LoginController@login');
Route::post('praapplication/{id}', 'PraApplication\PraApplicationController@update2');
Route::post('mopraapplication/{id}', 'PraApplication\PraApplicationController@updatemo');
Route::resource('admin', 'Admin\AdminController'); 
Route::get('/admin/user_detail/{id_pra}/{view}', 'Admin\AdminController@detail');
Route::get('/admin/downloadzip/{id_pra}', 'HomeController@downloadzip');
Route::get('/admin/downloadpdf/{id_pra}', 'Admin\AdminController@downloadpdf');
Route::get('/admin/form/view/{id}', 'Admin\AdminController@view_pdf'); 

Route::resource('verifiedForm', 'Admin\VerifiedController'); 
Route::resource('verifiedMoForm', 'Admin\VerifiedMoController'); 
//Route::post('verifiedMoForm/term', 'Admin\VerifiedMoController@term'); 
Route::post('fom/{id}', 'PraApplication\PraApplicationController@update2');
Route::post('form/upload/{id}', 'LoanForm\LoanFormController@upload');
Route::get('/admin/allhq_user/{id}', 'Admin\AdminController@allhq_user');
Route::get('/admin/allbranch_user/{id}', 'Admin\AdminController@allbranch_user');
Route::post('/admin/addhq_user', 'Admin\AdminController@addhq_user');
Route::post('/admin/addbranch_user', 'Admin\AdminController@addbranch_user');
Route::post('/admin/editbranch_user', 'Admin\AdminController@editbranch_user');
Route::post('/admin/deletebranch_user', 'Admin\AdminController@deletebranch_user');
Route::get('/admin/allbranch/{id}', 'Admin\AdminController@allbranch');
Route::post('/admin/addbranch', 'Admin\AdminController@addbranch');
Route::post('/admin/editbranch', 'Admin\AdminController@editbranch');
Route::post('/admin/deletebranch', 'Admin\AdminController@deletebranch');
Route::post('/admin/changed_bybank', 'Admin\AdminController@changed_bybank');
Route::post('/admin/confirm_approved', 'Admin\AdminController@confirm_approved');
Route::post('form/term', 'LoanForm\LoanFormController@term');
Route::get('form/requestedit/{id}', 'LoanForm\LoanFormController@requestedit');
Route::get('form/approveedit/{id}', 'LoanForm\LoanFormController@approvetedit');
Route::post('form/routeback', 'LoanForm\LoanFormController@routeback');
Route::post('verifiedForm/term', 'Admin\VerifiedController@term');
Route::resource('verifiedDoc', 'Admin\VerifiedDocController');
Route::post('verifiedDoc/term', 'Admin\VerifiedDocController@term');
Route::get('/admin/report/{id}', 'Admin\AdminController@report_first');
Route::get('/admin/report/{startdate}/{finisdate}/{status}/{branch}', 'Admin\AdminController@report');
Route::get('/admin/report_result/{startdate}/{finisdate}/{status}/{branch}', 'Admin\AdminController@report_result');
Route::resource('testemail', 'Admin\Testemail'); 
Route::get('test/{id}', 'Admin\AdminController@test');
Route::post('update_password', 'User\UserController@update_password'); 
Route::get('incomplated_doc', 'Admin\AdminController@incomplated_doc');
Route::post('/admin/deletepackage', 'Master\PackageController@destroy');

//report
Route::get('report/calculated', 'Report\ReportController@calculated');
Route::post('report/calculated', 'Report\ReportController@calculated_view');
Route::post('report/calculated_excel', 'Report\ReportController@calculated_excel');

Route::get('report/registered', 'Report\ReportController@registered');
Route::post('report/registered', 'Report\ReportController@registered_view');
Route::post('report/registered_excel', 'Report\ReportController@registered_excel');

Route::get('report/submitted', 'Report\ReportController@submitted');
Route::post('report/submitted', 'Report\ReportController@submitted_view');
Route::post('report/submitted_excel', 'Report\ReportController@submitted_excel');

Route::get('report/bystatus', 'Report\ReportController@bystatus');
Route::post('report/bystatus', 'Report\ReportController@bystatus_view');
Route::post('report/bystatus_excel', 'Report\ReportController@bystatus_excel');



//reset
/*Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/emails', 'Auth\PasswordController@postEmails');*/
Route::get('password/email', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.email');;
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

// aplikasi yang statusnya lagi di route back aja
Route::get('routeback_status', 'Admin\AdminController@routeback_status');

Route::resource('moapplication', 'PraApplication\MoApplicationController'); 
Route::resource('moinfo', 'MoInfoController'); 
Route::get('loglogin', 'MoInfoController@loglogin'); 
Route::get('viewmap/{id}/{lat}/{lng}', 'MoInfoController@loglogin_map'); 

// mo register
Route::resource('auth/moregister', 'User\MoController');
Route::post('getLocation', 'User\MoController@getlocation');

// List aplikasi yang di reject
Route::get('reject', 'Admin\AdminController@reject');
Route::get('docsreject', 'Admin\AdminController@docsreject');
Route::get('approved', 'Admin\AdminController@approved');

Route::get('stats', 'Admin\AdminController@stats');
Route::get('activities/{id}', 'Admin\AdminController@activities');
Route::get('activities_user/{id}', 'HomeController@activities_user');

// tes query 
Route::get('tesquery', 'Admin\AdminController@tesquery');

// mo reg geo location
Route::resource('geolocation', 'GeoLocController');
Route::get('geolocation/error/{id}', 'GeoLocController@error');
//Route::get('geolocation/map/{lat}/{lng}/{location?}', 'GeoLocController@map');
Route::get('geolocation/map/{id}', 'GeoLocController@map');
Route::resource('marketing', 'MOM\MarketingController');
Route::post('activate_agent', 'MOM\MarketingController@activate_agent');
Route::get('marketing_list', 'Admin\AdminController@marketing_list');

Route::resource('manager', 'MOM\ManagerController');
Route::post('save_manager', 'MOM\ManagerController@save_manager');
Route::resource('moform', 'LoanForm\MoFormController'); 
Route::post('moform/term', 'LoanForm\MoFormController@term');
Route::get('/email_check/{emel}/{id_pra}', 'LoanForm\MoFormController@email_check');
Route::get('/email_save/{emel}/{id_pra}', 'LoanForm\MoFormController@email_save');


Route::get('mbsb_login', 'Auth\MBSBAuthController@getLogin');
Route::post('mbsb_login', 'Auth\MBSBAuthController@login');
Route::get('mbsb_logout', 'Auth\MBSBAuthController@logout');

Route::get('direct_application', 'Admin\AdminController@direct_application');
Route::get('/admin/download_form/{id_pra}', 'Admin\AdminController@download_form');

//WEB SERVICES
Route::get('/list_of_agent/{id}', 'WebServices\ListMOController@agent');
Route::get('/mo', 'MoInfoController@mo');
//Route::post('/send_to_mom', 'MoInfoController@send_to_mom');
//anti attrition
Route::get('/agent/anti-attrition', 'CLRT\AntiAttritionController@index');
Route::get('/agent/anti-attrition-multi', 'CLRT\AntiAttritionController@multi');

Route::get('/anti-attrition-multi', 'CLRT\AntiAttritionController@multi');

Route::get('myform/ajax/{id}',array('as'=>'myform.ajax','uses'=>'CLRT\AntiAttritionController@myformAjax'));
Route::post('/send_to_mom', 'CLRT\AntiAttritionController@assign_to_mo');

Route::get('/clrt/new-loan-offer', 'CLRT\NewLoanOfferController@newloanoffer');
Route::post('/clrt/new_loan_offer_view', 'CLRT\NewLoanOfferController@newloanoffer_view');
Route::get('/clrt/loan-offer-detail/{id}', 'CLRT\NewLoanOfferController@detail_loan_offer');
Route::get('/clrt/loan-offer-detail-sms/{id}', 'CLRT\NewLoanOfferController@detail_loan_offer_sms');
Route::get('/clrt/loan-offer-detail-assigned/{id}', 'CLRT\NewLoanOfferController@detail_loan_offer_assigned');
Route::get('/clrt/otp-timeout/{id}', 'CLRT\NewLoanOfferController@otp_timeout');
Route::get('/clrt/request-ramcy/{id}', 'CLRT\NewLoanOfferController@request_ramcy');
Route::post('/clrt/send_to_mo', 'CLRT\AntiAttritionController@send_to_mo');
Route::get('/clrt/select-tenure/{id}', 'CLRT\NewLoanOfferController@select_tenure');
Route::get('/clrt/anti-attrition', 'CLRT\AntiAttritionController@antiattrition');

Route::post('/send-sms', 'CLRT\AntiAttritionController@send_sms');
Route::post('/send-sms-next', 'CLRT\AntiAttritionController@send_sms_next');
Route::post('/verify-otp', 'CLRT\AntiAttritionController@verify_otp');
Route::post('/send-sms-timeout', 'CLRT\AntiAttritionController@send_sms_timeout');

Route::get('/insert','InsertController@index');
Route::post('/submitForm','InsertController@submitForm');
Route::post('/submitotp','InsertController@submitOtp');
Route::get('/showresult','InsertController@show');
});