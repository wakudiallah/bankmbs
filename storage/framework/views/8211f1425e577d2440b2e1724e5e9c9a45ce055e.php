<!-- ==========================CONTENT ENDS HERE ========================== -->

<script type="text/javascript">
    $(document).ready(function() {
        $("#smart-form-register2").hide();
        $("#smart-form-register").validate({

          // Rules for form validation
            rules : {
                FullName: {
                    required : true,
                    maxlength:100
                },
                ICNumber : {
                    required : true,
                    maxlength:12,
                    minlength:12
                },
                PhoneNumber: {
                    required: true,
                    maxlength:16,
                    minlength:9
                },
                Deduction: {
                    required: true
                },
                Allowance: {
                    required: true
                },
                Package: {
                    required: true
                },
                Employment: {
                    required: true
                },
                Employer: {
                    required: true,
                    maxlength:20
                },
                BasicSalary: {
                    required: true   
                },
                LoanAmount: {
                    required: true
                },
                majikan: {
                    required: true,
                    maxlength: 60
                }
            },

          // Messages for form validation
            messages : {
                FullName: {
                    required : 'Please enter your full name'
                },
                ICNumber: {
                    required: 'Please enter your ic number'
                },
                PhoneNumber: {
                    required: 'Please enter your phone number'
                },
                Allowance: {
                    required: 'Please enter  yor allowance'
                },
                Deduction: {
                    required: 'Please enter your total deduction'
                },
                Package: {
                    required: 'Please select package'
                },
                Employment: {
                    required: 'Please select employement type'
                },
                Employer: {
                    required: 'Please select employer'
                },
                 majikan: {
                    required: 'Please enter employer'
                },
                BasicSalary: {
                    required: 'Please enter your basic salary'
                },
                LoanAmount: {
                    required: 'Please select your loan amount'
                }
            }
        });
    });
</script>

<script type="text/javascript">
    $("#FullNames").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
     $("#name_card").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
     $("#mother_name").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
</script>
<!--only 1 space-->
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
  
  var input = document.getElementById('FullNamse');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
   var input = document.getElementById('customer_name');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
   var input = document.getElementById('mother_name');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
});
</script>


<?php 
  //include required scripts
  include("asset/inc/scripts.php"); 
?>


<script type="text/javascript">
    <?php if(Session::has('employer')): ?> 
        $("#majikan2").hide();
        $("#majikan").show();
    <?php else: ?>
        $("#majikan").hide();
        $("#majikan2").show();
    <?php endif; ?>

    $( "#Employment" ).change(function() {
        var Employment = $('#Employment').val();

    if( Employment == '1') {
        $("#Employer").html(" ");
        $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
        $("#majikan").hide();
        $("#Package").val("Mumtaz-i");
    }

    else if( Employment == '2') {
        $("#Employer").html(" ");
        $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
        $("#majikan").hide();
        $("#Package").val("Afdhal-i");

    }

    else if( Employment == '3') {
        $("#Employer").html(" ");
        $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
        $("#majikan").hide();
        $("#Package").val("Afdhal-i");
    }

    else if( Employment == '4') {
        $("#Employer").html(" ");
        $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
        $("#majikan").hide();
        $("#Package").val("Private Sector PF-i");
    }

    else if( Employment == '5') {
        $("#Employer").html(" ");
        $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
        $("#majikan").hide();
        $("#Package").val("Afdhal-i");
        //  $("#majikan").simulate('click');
    }

    $.ajax({
        url: "<?php  print url('/'); ?>/employer/"+Employment,
        dataType: 'json',
            data: {

            },

            success: function (data, status) {
                jQuery.each(data, function (k) {
                    $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });
                }
            });
    });
</script>


<script type="text/javascript">
  $( "#BasicSalary" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script type="text/javascript">
  $( "#Allowance" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script type="text/javascript">
  $( "#Deduction" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script type="text/javascript">
  $( "#LoanAmount" ).blur(function() {
    this.value = parseFloat(this.value).toFixed(2);
});
</script>
<script>
    function isNumberKey(evt) {
        var theEvent = evt || window.event;
        var key = theEvent.keyCode || theEvent.which;
        key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}

 
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 2; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>

<script type="text/javascript">
    $(document).ready(function() {
        var found = [];
        $("#Employment option").each(function() {
            if($.inArray(this.value, found) != -1) $(this).remove();
            found.push(this.value);
        });
     });
</script>


<script language="javascript">
    function getkey(e)
    {
        if (window.event)
            return window.event.keyCode;
        else if (e)
            return e.which;
        else
            return null;
    }

    function goodchars(e, goods, field)
    {
        var key, keychar;
        key = getkey(e);
        if (key == null) return true;

        keychar = String.fromCharCode(key);
        keychar = keychar.toLowerCase();
        goods = goods.toLowerCase();

        // check goodkeys
        if (goods.indexOf(keychar) != -1)
            return true;
        // control keys
        if ( key==null || key==0 || key==8 || key==9 || key==27 )
            return true;

        if (key == 13) 
            {
                var i;
                for (i = 0; i < field.form.elements.length; i++)
                if (field == field.form.elements[i])
                break;
                i = (i + 1) % field.form.elements.length;
                field.form.elements[i].focus();
                return false;
            };
        // else return false
            return false;
    }
</script>


<script type="text/javascript">
    $(document).ready(function(){
    $("#inputTextBox").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0 ) && (inputValue != 44 && inputValue != 47 )){
            event.preventDefault();
        }
    });
});
</script>

<script type="text/javascript">
    $(document).ready(function(){
    $("#FullName").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
});
</script>
<script>
    function yesno(that) {
        if (that.value == "IP") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
        else if (that.value == "IO") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "PN") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "AN") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "PP") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "IN"){
          document.getElementById("ifOther").style.display = "none";
            $('.other').hide().find(':input').attr('required', false);

            document.getElementById("ifDOB").style.display = "none";
            $('.dob').hide().find(':input').attr('required', false);

             document.getElementById("ifgender").style.display = "none";
            $('.gender').hide().find(':input').attr('required', false);

            document.getElementById("ifNewIc").style.display = "block";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
        else{
          document.getElementById("ifOther").style.display = "none";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "none";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "none";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
    }
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
        _gaq.push(['_trackPageview']);
    
    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();


        $('.startdate').datepicker({
        dateFormat : 'dd/mm/yy',
         changeYear: true,
         changeMonth: true,
         yearRange: '1950:2017',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });

         $('.date').datepicker({
        dateFormat : 'dd/mm/yy',
         changeYear: true,
         changeMonth: true,
         yearRange: '1950:2017',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });
</script>