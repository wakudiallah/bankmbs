<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Administrator";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        <?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>
      
      <section id="widget-grid" class="">
                
                    <!-- row -->
                  
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-9 col-lg-9">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
            
                            <style>
#loadImg{position:absolute;z-index:999;}
#loadImg div{display:table-cell;width:50px;height:50px;background:#fff;text-align:center;vertical-align:middle;}
</style>
<div id="loadImg" ><div><img src="<?php echo e(url('/')); ?>/img/soon.gif" /></div></div>
                        <iframe onload="document.getElementById('loadImg').style.display='none';" width="190%" height="500" src="<?php echo e(url('/')); ?>/stats"></iframe>
                        <div id=""></div>


                  
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                       
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
                        
                        
        <section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-1" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Application List</h2>

                        </header>

                        <!-- widget div-->
                        <div>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
                                <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                            <thead>                         
                                                <tr>
                                                    <th>No.</th>
                                                    <th class="hidden-xs">IC Number</th>
                                                    <th>Name</th>
                                                    <th class="hidden-xs">Phone</th>
                                                    <th>Submit Date</th>
                                                    <th>Status</th>
                                                    <th>Branch Status</th>
                                                    <th class="hidden-xs">Activity</th>
                                                    <th>Action</th>
                                                 <th class="hidden-xs">Download</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $i=1; ?>
                                                 <?php $__currentLoopData = $terma; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $term): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                                                <tr>
                                                    <td><?php echo e($i); ?></td>
                                                    <td class="hidden-xs"><?php echo e($term->Basic->new_ic); ?>

                                                        <input type='hidden' id='ic99<?php echo e($i); ?>' name='ic' value='<?php echo e($term->Basic->new_ic); ?>'/>
                                                            <div id='block<?php echo e($i); ?>'></div>
                                                                <script type="text/javascript">
                                                                    $(document).ready(function() {
                                                                        var old_icx = $('#ic99<?php echo e($i); ?>').val();
                                                                        if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
                                                                            {
                                                                                $('#block<?php echo e($i); ?>').html("<span class='label label-danger'>IC Blocked </span>");
                                                                            }
                                                                    });
                                                                </script>
                                                    </td>
                                                    <td><?php $name = strtoupper($term->Basic->name ); ?><?php echo e($name); ?></td>
                                                    <td class="hidden-xs"><?php echo e($term->PraApplication->phone); ?></td>
                                                    <td><?php echo e($term->file_created); ?></td>
                                                    <td><div align='center'>
                                               
                                                    <?php if($term->verification_result ==0): ?>
                                                        <?php if($term->status==1): ?>
                                                            <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-warning'><b>Pending Documents Verification</b></span>
                                                        <?php elseif($term->status==77): ?>
                                                            <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-default'><b>Customer Ready Fill Up Form</b></span>
                                                        <?php elseif($term->status==88): ?>
                                                            <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-danger'><b>Documents Rejected</b></span>
                                                        <?php else: ?>
                                                        <?php if($term->referral_id!=0): ?>
                                                            <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-warning'>Submitted to WAPS</span>
                                                            <?php elseif($term->referral_id==0): ?>
                                                            <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>" class='label label-info'>Pending Verification</span>
                                                            <?php endif; ?>
                                                        <?php endif; ?>
                                                        
                                                    <?php elseif($term->verification_result ==1): ?>                                                
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-success'>Ready for 2nd Verification</span>
                                                    <?php elseif($term->verification_result ==2): ?>
                                                      
                                                        
                                                        <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-primary'>Submitted to Processor/WAPS</span>
                                                                                    <br>
                                                                                    <?php if($term->id_branch=='0'): ?>
                                                             <div align='center'> - </div> 
                                                                                    <?php else: ?>                                            
                                                            <i><?php echo e($term->Branch->branchname); ?></i>
                                                                                    <?php endif; ?> 
                                                        <br>
                                                         <?php if($term->financial->l3_jumlah > 0): ?>
                                                          <font color="red"><i>Overlapping Case</i></font>
                                                        <?php endif; ?>
                                                     
                                                      
                                                
                                                    <?php elseif($term->verification_result ==3): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-danger'>Rejected</span>
                                                 
                                                    <?php elseif($term->verification_result ==4): ?>
                                                      <span data-toggle="tooltip" title="<?php echo e($term->verification_remark); ?>"  class='label label-default'>Route Back to User</span>
                                                 
                                                    <?php endif; ?>
                                                    
                        
                                                       </div>
                                                </td>
                                         
                                                <td><div align='center'>
                                                   




                                                <?php if($term->verification_result_by_bank >=0 AND $term->verification_result ==2): ?>
                                                                                                     <?php
                                                 $this->soapWrapper = $soap_Wrapper;

                                                $this->soapWrapper->add('cds', function ($service) {
                                                                  $service
                                                                    ->wsdl('http://moaqs.com/MOMWS/MOMCallWS.asmx?WSDL');
                                                                    });
                                                                  
                                                 $param=array(
                                                        'AppNo'   => $term->id_praapplication,
                                                        'txnCode' => 'ST003',
                                                        'ActCode' => 'R',
                                                        'usr' => 'momwapsuat',
                                                        'pwd' => 'J@ctL71N$#'
                                                    );
                                                $response = $this->soapWrapper->call('cds.MOMCallStat',array($param));
                                                $smsMessages = is_array( $response->MOMCallStatResult)
                                                ? $response->MOMCallStatResult
                                                : array( $response->MOMCallStatResult );

                                                foreach ( $smsMessages as $smsMessage )
                                                {
                                                  $result=$smsMessage->ApvStat;
                                                  //echo $result;
                                                }


                                                    ?>
  
                
                                                 <?php if($result=='GLAN'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-success'>Pending WAPS</span>
                                                 
                                                 <?php elseif(($result=='DECL')): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-warning'>Decline</span>
                                                 <script type="text/javascript">
   setTimeout(function(){
       location.reload();
   },15000000);
</script>
                                                 <!--<script type="text/javascript">
                                                    Push.create("New Process Waps");
                                                  </script>-->
                                                 <?php elseif($result=='APVA'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-info'>Approved with </span>
                                                  <?php elseif($result=='REJM'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-danger'>Rejected</span>
                                                  <?php elseif($result=='ACCP'): ?>
                                                 <span data-toggle="tooltip" title=""  class='label label-primary'>Acepted</span>
                                                 <?php endif; ?>
                                            
                                                  <?php endif; ?>
                                                  
                                                   
                                                    </div>  
                                                </td>
                                                </td>
                                               
                                                  <td class="hidden-xs">
                                                  <a href="JavaScript:newPopup('<?php echo e(url('/')); ?>/activities/<?php echo e($term->id_praapplication); ?>');"   class='btn btn-sm btn-default'><i class="fa fa-search"></i></a>
                                                  </td>
                                                <td align='center'>
                                                    <?php if(($term->status==1) AND ($term->verification_result==0) ): ?>
                                                            <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-warning' ><i class='fa fa-file'></i> Verify Docs</a>
                                                    <?php elseif($term->status==77): ?>
                                                            <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a>

                                                            
                                                          <?php elseif(($term->status==1) AND ($term->verification_result==2) ): ?>
                                                           <a href="<?php echo e(url('/admin/step1/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default' ><i class='fa fa-file'></i>  View Docs</a><br>

                                                             <a href="<?php echo e(url('/admin/user_detail/'.$term->id_praapplication.'/view')); ?>" class='btn btn-sm btn-default fa fa-book' >View Form</a>
                                                    
                                                    <?php else: ?>
                                                       

                                                        
                                                        <?php if($term->status=='99'): ?>
                                                            <a href="<?php echo e(url('/admin/user_detail/'.$term->id_praapplication.'/verify')); ?>" class='btn btn-sm btn-success fa fa-book' >Verify Form</a>
       
                                                        <?php endif; ?> 


                                                        <?php if($term->edit=='1'): ?>
                                                            <?php if($term->verification_result !=3): ?>
                                                                <?php if($term->verification_result !=2): ?>
                                                                    <a href="<?php echo e(url('/')); ?>/form/approveedit/<?php echo e($term->id_praapplication); ?>" 
                                                                    onclick="return confirm('Are you sure ?')"  class='btn btn-sm btn-warning'>Approve RB</a>
                                                                <?php endif; ?> 
                                                            <?php endif; ?> 
                                                        <?php endif; ?> 

                                                        <?php if($term->status=='1'): ?>
                                                            <?php if($term->verification_result !=3): ?>
                                                              

                                                  <!-- Modal -->
                                                    <div class="modal fade" id="amyModal<?php echo e($i); ?>" tabindex="-1" role="dialog" aria-labelledby="amyModal<?php echo e($i); ?>" aria-hidden="true">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                        &times;
                                                                    </button>
                                                                    <h4 class="modal-title" id="addBranch">Route Back Application to User</h4>
                                                                </div>
                                                                <div class="modal-body">
                                                                 <?php echo Form::open(['url' => 'form/routeback','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                                                                  <fieldset>
                                                                            <section >
                                                                                  <label class="label">Customer Name</label>
                                                                                    <label class="input">
                                                                                    <input type='text' value='<?php echo e($term->Basic->name); ?>' readonly disabled/>
                                                                                    <b class="tooltip tooltip-bottom-right">Customer Name</b>
                                                                                    </label><br>
                                                                            </section>
                                                                            <section >
                                                                                  <label class="label">IC Number</label>
                                                                                    <label class="input">
                                                                                        <input type='text' value='<?php echo e($term->Basic->new_ic); ?>' readonly disabled/>
                                                                                    <b class="tooltip tooltip-bottom-right">IC Number</b>
                                                                                    </label><br>
                                                                            </section>
                                                                            <section >
                                                                                  <label class="label">Reason</label>
                                                                                    <label class="input">
                                                                                        <textarea id='reason' name='reason' class='form-control' rows="4" cols="73"></textarea>
                                                                                    <b class="tooltip tooltip-bottom-right">Reason</b>
                                                                                    </label><br>
                                                                            </section>
                                                                
                                                                         <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                                          <input type="hidden" name="id_praapplication" value="<?php echo e($term->id_praapplication); ?>">
                                                                          <input type="hidden" name="cus_ic" value="<?php echo e($term->Basic->new_ic); ?>">
                                                                          <input type="hidden" name="cus_name" value="<?php echo e($term->Basic->name); ?>">
                                                                          <input type="hidden" name="cus_email" value="<?php echo e($term->PraApplication->email); ?>">
                                                                     </fieldset>
                                                                </div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                        Cancel
                                                                    </button>
                                                                    <button type="submit" name="submit" class="btn btn-lg txt-color-darken">
                                                                                   Submit
                                                                                </button>
                                                                               
                                                                       <?php echo Form::close(); ?>   
                                                                </div>
                                                            </div><!-- /.modal-content -->
                                                        </div><!-- /.modal-dialog -->
                                                    </div><!-- /.modal -->
                                                            
                                                            
                                                        <?php endif; ?> 
                                                    <?php endif; ?>  
                                                <?php endif; ?> 
                                                </td>
                                              
                                                <td class="hidden-xs"> <a href="<?php echo e(url('/')); ?>/admin/downloadzip/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                 <?php if($term->status!=1 AND $term->status!=88 AND $term->status!=77 AND $term->status!=99): ?>
                                                  <a href="<?php echo e(url('/')); ?>/admin/downloadpdf/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img></a>
                                                <?php endif; ?>
                                                <?php if($term->verification_result_by_bank >=0 AND $term->verification_result ==2): ?>
                                                  <a href="<?php echo e(url('/')); ?>/admin/download_form/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img></a>

                                                <a href="<?php echo e(url('/')); ?>/admin/downloadpdf/<?php echo e($term->id_praapplication); ?>"
                                                ><img width='24' height='24' src="<?php echo e(url('/')); ?>/asset/img/pdf.png"></img></a>
                                                 <?php endif; ?>
                                                 </td>
                                            </tr>
                                            
                                              <?php
                                              $i++; ?>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            </tbody>
                                        </table>
                                          
                                          <br>
                                       

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>

<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
                var responsiveHelper_dt_basic = undefined;
                var responsiveHelper_datatable_fixed_column = undefined;
                var responsiveHelper_datatable_col_reorder = undefined;
                var responsiveHelper_datatable_tabletools = undefined;
                
                var breakpointDefinition = {
                    tablet : 1024,
                    phone : 480
                };
                $('#dt_basic').dataTable({
                      
                        "scrollX": true,
                    "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
                        "t"+
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                    "autoWidth" : true,
                    "preDrawCallback" : function() {
                        // Initialize the responsive datatables helper once.
                        if (!responsiveHelper_dt_basic) {
                            responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                        }
                    },
                    "rowCallback" : function(nRow) {
                        responsiveHelper_dt_basic.createExpandIcon(nRow);
                    },
                    "drawCallback" : function(oSettings) {
                        responsiveHelper_dt_basic.respond();
                    }
                });
                
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>

<script type="text/javascript">
// Popup window code
function newPopup(url) {
    popupWindow = window.open(
        url,'popUpWindow','height=400,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=no,menubar=no,location=no,directories=no,status=yes')
}
</script>

<script type="text/javascript">
   setTimeout(function(){
       location.reload();
   },50000);
</script>


          
