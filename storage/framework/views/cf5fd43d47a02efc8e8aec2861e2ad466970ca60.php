<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Add New Applicant";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        <?php if(Session::has('message')): ?>
          <div class="alert adjusted alert-info fade in">
          <button class="close" data-dismiss="alert">
              ×
          </button>
          <i class="fa-fw fa-lg fa fa-exclamation"></i>
            <strong><?php echo e(Session::get('message')); ?></strong> 
        </div>
      <?php endif; ?>             
        
        <?php if(count($errors) > 0): ?>
        <div class="alert alert-danger">
            <button class="close" data-dismiss="alert">
              ×
          </button>  
            <ul>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <li><?php echo e($error); ?></li>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </ul>
        </div>
    <?php endif; ?>
        <input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">

<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCz-U3AbNWcMJ8Ous00VgIR0RsQXypsOlY", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng, location: success.location.location}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         window.location.href = "<?php echo e(url('/')); ?>/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
   var location = position.coords.location;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "<?php echo e(url('/')); ?>/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude, location:location },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>  
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="well no-padding">
          <?php echo Form::open(['url' => 'moapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]); ?>

            <span id="latitude"></span>
                <span id="longitude"></span>
                <span id="location"></span>
                <header>
                    <p class="txt-color-white"><b>  Kalkulator Pembiayaan  </b> </p>
                </header>

                <fieldset>
                    <div class="row">
                        <div class="col-xs-12 col-12 col-md-12">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Nama Penuh  <sup>*</sup></b></label>
                                <label class="input">
                                    <i class="icon-append fa fa-user"></i>
                                    <input type="text" id="FullName" name="FullName" minlength="2" maxlength="100" onkeyup="this.value = this.value.toUpperCase()" onkeypress="return FullName(event);" placeholder="Full Name" <?php if(Session::has('fullname')): ?>  value="<?php echo e(Session::get('fullname')); ?>" <?php endif; ?> size="55">
                                    <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                             <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Jenis Pengenalan  <sup>*</sup></b></label>
                                <label class="select">
                                    <select name="type" id="type" class="form-control" onchange="yesno(this);">
                                         <option>--Select--</option>
                                    <?php $__currentLoopData = $idtype; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $idtype): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($idtype->idtypecode); ?>"><?php echo e($idtype->idtypedesc); ?></option>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select> <i></i>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifNewIc" style="display: none">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> No Kad Pengenalan  <sup>*</sup></b></label>
                                <label class="input <?php if(Session::has('icnumber_error')): ?> state-error  <?php endif; ?>">
                                    <i class="icon-append fa fa-user"></i>
                                    <input  type="number" pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  <?php if(Session::has('icnumber')): ?>  value="<?php echo e(Session::get('icnumber')); ?>" <?php endif; ?> >
                                    <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifOther" style="display: none">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> No Kad Pengenalan Lain  <sup>*</sup></b></label>
                                <label class="input <?php if(Session::has('icnumber_error')): ?> state-error  <?php endif; ?>">
                                    <i class="icon-append fa fa-user"></i>
                                    <input  type="text" id="other"  name="other"  placeholder="Other"  <?php if(Session::has('other')): ?>  value="<?php echo e(Session::get('other')); ?>" <?php endif; ?> minlength="6" maxlength="7" required="">
                                    <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12" id="ifDOB" style="display: none">
                             <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b>DOB (dd/mm/yyyy)  <sup>*</sup></b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                    <input type="text" data-mask="99/99/9999" data-mask-placeholder= "-" placeholder="dd/mm/yyyy"  maxlength="14" name="dob" value=""  class="form-control startdate" id="dob" required="" />
                                
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12" id="ifgender" style="display: none">
                             <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Jantina  <sup>*</sup></b></label>
                                <label class="select">
                                    <select name="gender" id="gender" class="form-control" required="">
                                        <option>--Select--</option>
                                        <option value="11">Male</option>
                                        <option value="22">Female</option>
                                    </select> <i></i>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                             <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> No. Tel Bimbit  <sup>*</sup></b> </label>
                                <label class="input">
                                    <i class="icon-append fa fa-mobile-phone"></i>
                                   <input  type="number" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number" i pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" minlength="7" maxlength="14"    <?php if(Session::has('phone')): ?>  value="<?php echo e(Session::get('phone')); ?>" <?php endif; ?>>
                                    <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                             <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Jenis Pekerjaan  <sup>*</sup></b></label>
                                <label class="select">
                                    <select  name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                            <?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                <option value="<?php echo e($employment->id); ?>"><?php echo e($employment->name); ?></option>
                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        </select> <i></i>
                                        <input type="hidden" name="Employment2" id="Employment2" value="" />
                                 </label>
                             </section>
                         </div>
                    </div>
                </fieldset>
                <fieldset>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                             <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Majikan  <sup>*</sup></b></label>
                                <label class="select" id="majikan">
                                    <select name="Employer" id="Employer" class="form-control" onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >
                                        <?php if(Session::has('employer')): ?> 
                                            <option  value="<?php echo e(Session::get('employer')); ?>"><?php echo e(Session::get('employer2')); ?></option>
                                        <?php endif; ?>
                                    </select> <i></i>
                                    <input type="hidden" name="Employer2" id="Employer2" value="" />
                                </label>

                                <label id="majikan2" class="input">
                                    <i class="icon-append fa fa-briefcase"></i>
                                    <input type="text" id="majikantext" onkeyup="this.value = this.value.toUpperCase()" name="majikan"  placeholder="Majikan" minlength="2" maxlength="60"  <?php if(Session::has('majikan')): ?>  value="<?php echo e(Session::get('majikan')); ?>" <?php endif; ?> >
                                    <b class="tooltip tooltip-bottom-right"> Majikan</b>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Gaji Asas (RM)  <sup>*</sup></b>  </label>
                                <label class="input <?php if(Session::has('hadpotongan')): ?> state-error  <?php endif; ?> ">
                                    <i class="icon-append fa fa-credit-card"></i>
                                    <input type="tel" value="0.00" inc="1" onkeypress="return isNumberKey(event)" id="BasicSalary_x" placeholder="Basic Salary (RM)"   <?php if(Session::has('basicsalary')): ?>  value="<?php echo e(Session::get('basicsalary')); ?>" <?php endif; ?>  >
                                    <input type="number" id="display_BasicSalary_x" name="BasicSalary" class="hidden">
                                    <b class="tooltip tooltip-bottom-right">Gaji Asas (RM) </b>
                                </label>
                            </section>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Elaun (RM)  <sup>*</sup></b></label>
                                <label class="input <?php if(Session::has('hadpotongan')): ?> state-error  <?php endif; ?>">
                                    <i class="icon-append fa fa-credit-card"></i>
                                    <input type="tel" value="0.00" onkeypress="return isNumberKey(event)" id="Allowance_x" placeholder="Allowance (RM)"   <?php if(Session::has('allowance')): ?>  value="<?php echo e(Session::get('allowance')); ?>" <?php endif; ?> >
                                    <input type="number" id="display_Allowance_x" name="Allowance" class="hidden">
                                        <b class="tooltip tooltip-bottom-right">Elaun (RM)</b>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"><b> Potongan Bulanan  <sup>*</sup></b> </label>
                                <label class="input <?php if(Session::has('hadpotongan')): ?> state-error  <?php endif; ?>">
                                    <i class="icon-append fa fa-credit-card"></i>
                                    <input type="tel"  onkeypress="return isNumberKey(event)" id="Deduction_x" placeholder="Existing Total Deduction"  <?php if(Session::has('deduction')): ?>  value="<?php echo e(Session::get('deduction')); ?>" <?php endif; ?>  value="0.00">
                                    <input type="number" id="display_Deduction_x" name="Deduction" class="hidden">
                                    <b class="tooltip tooltip-bottom-right"> Jumlah Potongan Bulanan Semasa (RM)  </b>
                                </label>
                            </section>
                        </div>
                    </div>
                </fieldset>
                
                <fieldset>
                    <div class="row">
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Pakej  <sup>*</sup></b><div class="visible-xs"></div> </label>
                                <label class="input">
                                    <input type="text" name="Package" id="Package" <?php if(Session::has('package_name')): ?>  value="<?php echo e(Session::get('package_name')); ?>" <?php else: ?> value="Mumtaz-i" <?php endif; ?> class="form-control" readonly>
                                </label>
                            </section>
                        </div>
                        <div class="col-xs-6 col-12">
                            <section class="col col-12 col-lg-12 col-md-12">
                                <label class="label"> <b> Jumlah Pembiayaan  <sup>*</sup></b></label>
                                <label class="input <?php if(Session::has('hadpotongan')): ?> state-error  <?php endif; ?>"">
                                    <i class="icon-append fa fa-credit-card"></i>
                                    <input value="0.00" type="tel"  onkeypress="return isNumberKey(event)" name="" id="LoanAmount_x" placeholder="Loan Amount (RM)"  <?php if(Session::has('loanAmount')): ?>  value="<?php echo e(Session::get('loanAmount')); ?>" <?php endif; ?>>
                                    <input type="number" id="display_LoanAmount_x" name="LoanAmount" class="hidden">
                                     <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan (RM)</b>
                                </label>
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            </section>
                        </div>
                    </div>
                </fieldset>
                <footer>
                  <button type="submit" class="btn btn-danger">
                        <b> Kira Kelayakan  </b>
                    </button>
                     <br><br><br><br>
                 </footer>
            </form>
          </div>
        </article>
    </div>
</div>
<!-- PAGE FOOTER -->
<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

    $(document).ready(function() {
        
          $("#smart-form-register2").hide();
        $("#smart-form-register").validate({

          // Rules for form validation
           rules : {
              FullName: {
              required : true,
              maxlength:100
            },
            ICNumber : {
              required : true,
              maxlength:12,
              minlength:12
            },
            other : {
              required : true,
              minlength:6,
              maxlength:7,
            },

            
            PhoneNumber: {
                required: true,
                 maxlength:16,
                minlength:9,

            },
            Deduction: {
                required: true
            },
            
            Allowance: {
                required: true
            },
            Package: {
                required: true
            },
            Employment: {
                required: true
            },
            Employer: {
                required: true,
              maxlength:60,
            },
            
            
            BasicSalary: {
                required: true
                            
            },
            LoanAmount: {
                required: true
            }
          },
          // Messages for form validation
          messages : {

              FullName: {
              required : 'Please enter your full name'
            },
            
            ICNumber: {
                required: 'Please enter your ic number'
            },
            PhoneNumber: {
                required: 'Please enter your phone number'
            },
            Allowance: {
                required: 'Please enter  yor allowance'
            },
            Deduction: {
                required: 'Please enter your total deduction'
            },
            Package: {
                required: 'Please select package'
            },
            Employment: {
                required: 'Please select employement type'
            },

            Employer: {
                required: 'Please select employer'
            },


            BasicSalary: {
                required: 'Please enter your basic salary'
            },
            LoanAmount: {
                required: 'Please select your loan amount'
            }
          }
        });

      });
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                ICNumber : {
                    required : true,
                    minlength : 12,
                    maxlength : 13
                },
                other : {
                    required : true,
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email2 : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>
<?php 
  //include required scripts
  include("asset/inc/scripts.php"); 
?>
<?php 
  //include required scripts
  include("asset/inc/footer.php"); 
?>
<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    

<script type="text/javascript">

 <?php if(Session::has('employer')): ?> 
 
   $("#majikan2").hide();
    $("#majikan").show();
  <?php else: ?>
   $("#majikan").hide();
   $("#majikan2").show();
  <?php endif; ?>

$( "#Employment" ).change(function() {
    var Employment = $('#Employment').val();

    if( Employment == '1') {

      $("#Employer").html(" ");
      $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
       $("#Package").val("Mumtaz-i");
    }
      else if( Employment == '2') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Afdhal-i");
    }

    else if( Employment == '3') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Afdhal-i");
    }
      else if( Employment == '4') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Private Sector PF-i");
    }
    else if( Employment == '5') {
         
         $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();

          $("#Package").val("Afdhal-i");
            
        //  $("#majikan").simulate('click');
         


    }
  $.ajax({
                url: "<?php  print url('/'); ?>/employer/"+Employment,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });

                }
            });

   
});
</script>

<!--<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>-->
<script type="text/javascript">
    function isNumberKey(evt) {
    var charCode = ((evt.which) ? evt.which : event.keyCode);
    return !(charCode > 31 && (charCode != 46 && charCode != 44 && (charCode < 48 || charCode > 57)));

}

</script>

<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("#Employment option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });



</script>

<script type="text/javascript">

function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}


         var Employment = getSelectedText('Employment');
         var Employer = getSelectedText('Employer');
          $("#Employment2").val(Employment);
           $("#Employer2").val(Employer); 

</script>
<script type="text/javascript">
    
      document.getElementById("BasicSalary_x").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    document.getElementById("display_BasicSalary_x").value = this.value.replace(/,/g, "");   
    
}
</script>
<!--<script type="text/javascript">
    $("input[type=tel]").keyup(function () {
        var number = parseFloat($(this).val());
        var inc = parseFloat($(this).attr("inc"));
        var newValue = number / inc;
        $("input[type=tel]").each(function () {
              if(isNaN(newValue * parseFloat($(this).attr("inc"))))
      $(this).val(0);
    else
      $(this).val(newValue * parseFloat($(this).attr("inc")));
                });
        });
</script>-->
<script type="text/javascript">
    function Telefon(event) {
        var regex = new RegExp("^[0-9]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    } 
    function NamaPenuh(event) {
        var regex = new RegExp("^[ABCDEFGHIJKLMNOPQRSTUVWXYZ-/'?]");
        var key = String.fromCharCode(event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    } 
</script>

<script type="text/javascript">
   // if (document.getElementById('LoanAmount_x').value != '') { 
    document.getElementById("LoanAmount_x").onblur =function (){    
      this.value = parseFloat(this.value.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ","); 

    document.getElementById("display_LoanAmount_x").value = this.value.replace(/,/g, "");  
  }
 // }
//else{
    // document.getElementById("LoanAmount_x").value = '0.00';
//}
</script>

<script type="text/javascript">
    document.getElementById("Allowance_x").onblur =function (){    
      this.value = parseFloat(this.value.replace(/,/g, ""))
                      .toFixed(2)
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");   
  
  document.getElementById("display_Allowance_x").value = this.value.replace(/,/g, ""); 

  }

</script>

<script type="text/javascript">
    document.getElementById("Deduction_x").onblur =function (){    
    this.value = parseFloat(this.value.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    
    document.getElementById("display_Deduction_x").value = this.value.replace(/,/g, "");   
    
}
  
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $("#FullName").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 90)  && (inputValue != 97 && inputValue != 98 && inputValue != 99 && inputValue != 100 && inputValue != 101 && inputValue != 102 && inputValue != 103 && inputValue != 104 && inputValue != 105 && inputValue != 106 && inputValue != 107 && inputValue != 108 && inputValue != 109 && inputValue != 110 && inputValue != 111 && inputValue != 112 && inputValue != 113&& inputValue != 114 && inputValue != 115 && inputValue != 116 && inputValue != 117 && inputValue != 118 && inputValue != 119 && inputValue != 120 && inputValue != 121 && inputValue != 122 && inputValue != 92 && inputValue != 64 && inputValue != 46 ) && (inputValue != 32) && (inputValue != 44 && inputValue != 47 && inputValue != 39 )){
            event.preventDefault();
        }
    });
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
    $("#ICNumber").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 48 && inputValue <= 57)){
            event.preventDefault();
        }
    });
    $("#PhoneNumber").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 48 && inputValue <= 57)){
            event.preventDefault();
        }
    });
});
</script>
<!--<script type="text/javascript">
    
    $("#FullName").keypress(function(e) {
       if (e.which === 32 && !this.value.length) {
           e.preventDefault();
       }
       var inputValue = event.charCode;
       if(!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && (inputValue != 191)){
           event.preventDefault();
       }          
   });
</script>-->
<!--only 1 space-->
<script type="text/javascript">
    document.addEventListener('DOMContentLoaded', function() {
  
  var input = document.getElementById('FullName');
  input.addEventListener('keydown', function(e){      
       var input = e.target;
       var val = input.value;
       var end = input.selectionEnd;
       if(e.keyCode == 32 && (val[end - 1] == " " || val[end] == " ")) {
         e.preventDefault();
         return false;
      }      
    });
});
</script>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript">
    $(function() {
        $('#txtNumeric').keyup(function() {
            if (this.value.match(/[^0-9]/g)) {
                this.value = this.value.replace(/[^0-9]/g, '');
            }
        });
        $('#txtAlphabet').keyup(function() {
            if (this.value.match(/[^a-zA-Z]/g)) {
                this.value = this.value.replace(/[^a-zA-Z]/g, '');
            }
        });
        $('#ICNumbers').keyup(function() {
            if (this.value.match(/[^a-zA-Z0-9]/g)) {
                this.value = this.value.replace(/[^a-zA-Z0-9]/g, '');
            }
        });
         $('#otherx').keyup(function() {
            if (this.value.match(/[^a-zA-Z0-9]/g)) {
                this.value = this.value.replace(/[^a-zA-Z0-9]/g, '');
            }
        });
    });
</script>

<script type="text/javascript">
   $(document).ready(function(){
  /***phone number format***/
  $("#ICNumbers").keypress(function (e) {
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
    var curchr = this.value.length;
    var curval = $(this).val();
    if (curchr == 6) {
      $(this).val(curval + "/");
    }
    else if (curchr ==11) {
      $(this).val(curval + "-");
    }
    else if (curchr ==14) {
      $(this).val(curval + "/");
       $(this).attr('maxlength', '18');
    }
  });
});
</script>
<script>
    function yesno(that) {
        if (that.value == "IP") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
        else if (that.value == "IO") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "PN") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "AN") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "PP") {
          document.getElementById("ifOther").style.display = "block";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "block";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "block";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        } 
        else if (that.value == "IN"){
          document.getElementById("ifOther").style.display = "none";
            $('.other').hide().find(':input').attr('required', false);

            document.getElementById("ifDOB").style.display = "none";
            $('.dob').hide().find(':input').attr('required', false);

             document.getElementById("ifgender").style.display = "none";
            $('.gender').hide().find(':input').attr('required', false);

            document.getElementById("ifNewIc").style.display = "block";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
        else{
          document.getElementById("ifOther").style.display = "none";
            $('.other').hide().find(':input').attr('required', false);
            document.getElementById("ifDOB").style.display = "none";
            $('.dob').hide().find(':input').attr('required', false);
            document.getElementById("ifgender").style.display = "none";
            $('.gender').hide().find(':input').attr('required', false);
            document.getElementById("ifNewIc").style.display = "none";
            $('.ICNumber').hide().find(':input').attr('required', true);
        }
    }
</script>

<script type="text/javascript">
    $('#other').on("keyup", function() {
  let val = $(this).val();
  let reg = /^[a-z0-9]\d*$/
  let newval = val.replace(reg, '');
  if (!val.match(reg)) {
    $(this).val(''); // clear the field if pattern don't match. 
  }

});
</script>
<script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: '1950:2017',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

                 $('.date').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: '1950:2017',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });


        </script>
        