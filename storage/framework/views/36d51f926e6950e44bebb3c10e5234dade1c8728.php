 <html>
 <head>
 <style>
body {
            /* styles */
    
            -webkit-print-color-adjust:exact;
            font-family: "Arial";

        }

        td.border {

        	font-color:white;
        	  padding: 10px; /* cellpadding */
        	   line-height: 150%;
 
        }

        td.border1 {
        	 border-collapse: collapse;
    		border: 1px solid black;
        }

  
    

 </style>
 </head>
 <body>


<script>

function Fungsiprint() {
    window.print();
}
window.onload=Fungsiprint;
</script>

 <table >
                                                                <tr>
                                                                    <td class="border" align="left" bgcolor="#0055a5">  
                                                                        <font color="white"> <b>7.1) PURCHASE APPLICATION AND PROMISE TO BUY</b>/ <i>PERMOHONAN BELIAN DAN AKUJANJI UNTUK MEMBELI </i></font>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="border" align="justify">
                                                                          <b><br>Upon approval of financing and my acceptance to the Terms and Conditions, I hereby order and request MBSB to purchase commodity with the purchase price that will be approved by MBSB as per SMS 'Aqad and promise to buy the same commodity at cost plus profit, depending on the financing facility and I will be held responsible for any violation to the agremeent.</b><br> 
                                                                        <i>Setelah mendapat kelulusan pembiayaan dan penerimaan saya kepada Terma dan Syarat, saya dengan ini membuat pesanan dan memohon MBSB untuk membeli komoditi dengan harga belian yang akan diluluskan oleh MBSB seperti tertera di dalam 'Aqad SMS dan berjanji untuk membeli komoditi tersebut pada kos tambah keuntungan, bergantung kepada kelulusan pembiayaan, di mana saya bertanggungjawab sepenuhnya akibat mengingkari perjanjian ini.</i><br><br>
                                                                    <table width="90%" border="0">
                                                                         <tr>
                                                                             <td valign="top" width="40%">
                                                                                 <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Name</b><br>
                                                                                            <i>Nama</i>
                                                                                        </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="<?php echo e($data->name); ?>" name="" disabled/>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>MyKad No.</b><br>
                                                                                            <i>No. MyKad</i>
                                                                                         </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="<?php echo e($data->new_ic); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>Date.</b><br>
                                                                                            <i>Tarikh</i>
                                                                                         </td>
                                                                                          <td>

                                                                                            <input type="text" class="form-control" value="<?php echo e($today); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                             </td>
                                                                             <td valign="top">
                                                                                    <table  width="500" height="100">
                                                                                        <tr>
                                                                                            <td class="border1" align="center"><h2>
                                                                                                <input type="checkbox" value="1" name="purchase_application" <?php if($term->purchase_application>0): ?> checked <?php endif; ?> ><b> I Agree</b> / Saya Bersetuju</h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                             </td>
                                                                         </tr>
                                                                    </table>
                                                                       <br>


                                                                       
                                                                    
                                                                                
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                          <!-- APPOINTMENT --><br>

                                                            <table >
                                                          <tr>
                                                          <td class="border" align="left" bgcolor="#0055a5">  <font color="white"> <b>7.2) APPOINTMENT OF MBSB AS A SALES AGENT</b> / <i>PERLANTIKAN MBSB SEBAGAI EJEN JUALAN</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td class="border" align="justify">

                                                        <b><br>Subject to MBSB's acceptance, I hereby irrevocably and unconditionally appoint MBSB as my agent under the shariah contract of Wakalah to sell the commodities  to any third party purchaser/commodity trader as MBSB may deem fit and in accordance with such terms acceptable to MBSB. I shall be bound by any contract or agremeent that MBSB may enter into with the said third party purchaser/trader for the purpose of the sale of the commodities on my behalf. I hereby agree to pay MBSB a sum of RM36.04 (inclusive of Goods and Services Tax) as Wakalah Fee. I hereby  undertake to indemnify MBSB to make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB may suffer or incur as a result of fulfilling MBSB's agency function as set out above.</b><br> 
                                                          <i>Tertakluk kepada penerimaan MBSB, saya dengan ini secara tidak boleh batal dan tanpa syarat melantik MBSB sebagai ejen saya di bawah kontrak syariah Wakalah untuk menjual komoditi kepada mana-mana pembeli/peniaga komoditi pihak ketiga sebagaimana yang difikirkan patut oleh MBSB dan mengikut apa-apa terma yang diterima oleh MBSB. Saya akan terikat dengan mana-mana kontrak atau perjanjian yang dibuat oleh MBSB dengan pembeli/peniaga komoditi pihak ketiga tersebut bagi tujuan penjualan komoditi bagi pihak saya. Saya dengan ini bersetuju untuk membayar MBSB sebanyak <b>RM36.04</b> (termasuk Cukai Barangan dan Perkhidmatan) sebagai fi Wakalah. Saya dengan ini mengaku janji untuk menanggung segala kerugian MBSB dengan membayar sepenuhnya semua kerugian, kos, dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB akibat memenuhi fungsi agensi seperti yang dinyatakan di atas.</i><br><br>

                                                        <table width="90%" border="0">
                                                             <tr>
                                                                 <td valign="top" width="40%">

                                                                   <table>
                                                                        <tr>
                                                                            <td>
                                                                                <b>Name</b><br>
                                                                                <i>Nama</i>
                                                                            </td>
                                                                             <td>
                                                                                <input type="text" class="form-control" value="<?php echo e($data->name); ?>" name="" disabled/>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>MyKad No.</b><br>
                                                                                <i>No. MyKad</i>
                                                                             </td>
                                                                             <td>
                                                                                <input type="text" class="form-control" value="<?php echo e($data->new_ic); ?>" name="" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Date.</b><br>
                                                                                <i>Tarikh</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" class="form-control" value="<?php echo e($today); ?>" name="" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    </td>
                                                                    <td valign="top">
                                                                        <table class="border1"  width="500" height="100">
                                                                            <tr>
                                                                                <td align="center" class="border1" ><h2><input type="checkbox" name="appointment_mbsb" value="1" <?php if($term->appointment_mbsb>0): ?> checked <?php endif; ?> ><b> I Agree</b> / Saya Bersetuju</h2>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                  </td>
                                                              </tr>
                                                          </table>

                                                            </td>

                                                        
                                                          </tr>
                                                          </table>

                                                        <br>
                                                          
                                                      <table >
                                                          <tr>
                                                          <td class="border" align="left" bgcolor="#0055a5">  <font color="white"> <b>7.3) DECLARATION/DISCLOSURE BY APPLICANT/CO-APPLICANT/GUARANTOR </b> / <i>PERAKUAN/PENDEDAHAN OLEH PEMOHON/PEMOHON BERSAMA/PENJAMIN</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td class="border" align="justify">
                                                        <ol type='1'>
                                                          <li> <b>I/We hereby declare that I/we am/are NOT a bankrupt.<br></b>
                                                                <i> Saya/Kami mengesahkan bahawa saya/kami TIDAK muflis. 
                                                                <br></i><br>
                                                          </li>
                                                          <li>
                                                                  <b> I/We declare that the information furnished in this form are completely true and accurate and I/we have not withheld any information which may prejudice my/our financing application or have a bearing on your financing decision.</b> <br><i>Saya/Kami mengesahkan bahawa maklumat yang disediakan di dalam borang ini adalah benar, tepat, dan lengkap dan saya/kami tidak menyembunyikan sebarang maklumat yang mungkin prejudis terhadap 
                                                                 permohonan pembiayaan saya/kami atau mempunyai kesan ke atas keputusan 
                                                                 pembiayaan anda.<br></i><br></li>
                                                          <li> <b> I/We hereby authorize you/your representative to obtain the relevant information relating to this application from any relevant source as deemed suitable by MBSB but not limited to any bureaus or agencies established by Bank Negara Malaysia ("BNM") or other parties. The authorization to obtain the relevant information is also extended to prospective guarantors, security providers, authorized depository agent and other party relating to this application as deemed necessary by MBSB.</b><br><i>Saya/Kami memberi kebenaran kepada MBSB untuk mendapatkan maklumat yang relevan terhadap permohonan ini dari sumber-sumber yang relevan dan yang dianggap sesuai oleh MBSB termasuk dan tidak terhad kepada mana-mana biro atau agensi yang ditubuhkan oleh Bank Negara Malaysia ("BNM") atau pihak yang lain. Kebenaran untuk mendapatkan maklumat yang relevan juga merangkumi bakal penjamin dan/atau pemberi sekuriti dan mana-mana wakil penyimpan yang dibenarkan dan pihak yang berkenaan dengan permohonan ini yang dianggap sesuai oleh MBSB. <br></i><br> </li>

                                                          <li> <b>I/We understand that MBSB reserves the absolute right to approve or decline this application as MBSB deems fit without assigning any reason.</b><br>
                                                          <i>Saya/Kami faham bahawa pihak MBSB mempunyai hak mutlak untuk meluluskan atau menolak permohonan tanpa menyatakan sebarang alasan. <br></i><br></li>

                                                          <li> <b>I/We expressly irrevocably consent and authorize MBSB to furnish all relevant information relating to or arising from or in connection with financing facilities to any subsidiary companies of MBSB, its agents and/or such person or BNM, Cagamas Berhad and debt collection agents or such other authority or body established by BNM, or such other authority having jurisdiction over MBSB as MBSB may absolutely deem fit and such other authority as may be authorized by law. </b><br> <i>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB untuk mendedahkan sebarang maklumat yang diperlukan berkaitan dengan atau berbangkit daripada apa-apa hubungan dengan kemudahan pembiayaan kepada mana-mana anak syarikat MBSB, ejennya, dan/atau mana-mana individu atau BNM, Cagamas Berhad dan ejen pemungut hutang atau mana-mana pihak berkuasa lain yang mempunyai bidang kuasa ke atas MBSB di mana MBSB secara mutlak berhak memberi kata putus dan mana-mana pihak berkuasa yang dibenarkan selaras dengan undang-undang. <br></i><br></li>

                                                          <li><b>I/We hereby further expressly irrevocably consent and authorize MBSB to seek any information concerning me/us with or from any credit reference/reporting agencies, including but not limited to CCRIS, CTOS, RAMCI, FIS and/or Inland Revenue Authorities or any authority as MBSB may from time to time deem fit.</b> <br> <i>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB untuk mendapatkan sebarang maklumat berkaitan saya/kami daripada mana-mana agensi rujukan kredit, termasuk dan tidak terhad kepada CCRIS, CTOS, RAMCI, FIS dan/atau Lembaga Hasil Dalam Negeri atau mana-mana pihak berkuasa yang  diputuskan oleh MBSB dari masa ke semasa.<br></i><br></li>

                                                          <li><b>I/We also acknowledge that it is a requirement that all information relating to this application must be transmitted and/or updated to the Central Credit Reference Information System ("CCRIS"), a database maintained by BNM as and when necessary.</b> <br> <i>Saya/Kami juga mengesahkan bahawa semua maklumat berkaitan permohonan ini mestilah dimaklumkan dan/atau dikemaskini kepada Sistem Maklumat Rujukan Kredit Pusat ("CCRIS"), pengkalan data yang diuruskan oleh BNM apabila perlu.<br></i><br></li>

                                                          <li><b>I/We shall comply with MBSB's requirements in respect of my/our application and I/we understand that MBSB's offer of the financing shall be subject to MBSB performing the necessary verification.</b> <br> <i>Saya/Kami akan mematuhi segala keperluan MBSB untuk permohonan saya/kami dan saya/kami memahami bahawa tawaran pembiayaan oleh MBSB adalah tertakluk kepada pengesahan yang diperlukan oleh MBSB<br></i><br></li>

                                                          <li><b>I/We hereby undertake to notify and/or inform the emergency contact person and my/our spouse that their personal data has been provided to MBSB and undertake to indemnify and hold MBSB harmless in the event of any legal repercussions arising from my/our failure and/or to notify the said emergency contact person/spouse. </b><br> <i>Saya/Kami dengan ini bersetuju untuk memberitahu dan/atau menghubungi penama rujukan kecemasan dan suami isteri bahawa data peribadi mereka telah diberi kepada MBSB dan berjanji tidak akan mengambil sebarang tindakan ke atas MBSB sekiranya timbul sebarang tindakan undang-undang daripada kegagalan saya/kami untuk memberitahu dan/atau menghubungi penama rujukan kecemasan/suami isteri.<br></i><br></li>

                                                          <li><b>This application form and all supporting documents that were submitted to MBSB shall be the sole property of MBSB and MBSB is entitled to retain the same irrespective of whether my/our application is approved or rejected by MBSB. </b><br> <i>Borang permohonan ini dan semua dokumen sokongan yang telah diserahkan kepada MBSB adalah hak milik mutlak MBSB dan MBSB berhak untuk mengekalkan semua dokumen tanpa mengira samaada permohonan saya/kami diluluskan atau ditolak oleh MBSB.<br></i><br></li>

                                                          <li><b>I/We hereby irrevocably agree to waive my/our rights to a refund where the amount is not exceeding RM5.00 arising from but not limited  to early settlement or closure of my/our financing account. I/We further consent and authorize MBSB to donate the said amount to charitable organisations deemed appropriate by MBSB.</b><br> <i>Saya/Kami bersetuju tanpa hak menarik balik bagi bayaran balik di mana jumlahnya tidak melebihi RM5.00, hasil daripada tetapi tidak terhad kepada penyelesaian awal atau penutupan akaun pembiayaan saya/kami. Saya/Kami seterusnya membenar dan memberi kuasa kepada MBSB untuk menderma jumlah tersebut kepada badan-badan kebajikan yang difikirkan wajar oleh pihak MBSB.<br></i><br></li>

                                                        </ol>
                                                         
                                                         
                                                         
                                                         
                                                         
                                                          
                                                          
                                                          
                                                          
                                                         
                                                          </td>
                                                            </tr>
                                                          </table>


                                                          <!-- PERMISSION TO DEDUCT --><br>

                                                           <table >
                                                          <tr>
                                                            <td class="border" align="left" bgcolor="#0055a5">  <font color="white"><b>7.4) PERMISSION TO DEDUCT FROM PERSONAL FINANCING-i FACILITY</b> / <i>KEBENARAN PEMOTONGAN DARI KEMUDAHAN PEMBIAYAAN PERIBADI-i</i></font></td>
                                                          </tr>
                                                          <tr>
                                                        
                                                          <td align="justify">
                                                            <ol type="1">

                                                               <li>
                                                                     <b>I hereby authorize and allow MBSB to deduct and pay directly from the financing amount approved by MBSB, the following :</b><br>
                                                                     <i>Saya dengan ini memberi kuasa dan membenarkan MBSB melakukan pemotongan / pembayaran secara terus daripada jumlah pembiayaan yang diluluskan oleh MBSB, seperti berikut:</i></li>
                                                                      <ol type="i">
                                                                        <li><b>Security Deposit</b>/<i> Deposit Sekuriti</i>.</li>
                                                                        <li><b> GCTT Takaful Contribution (if applicable)</b>/<i> Sumbangan Takaful GCTT (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Wakalah Fee</b>/<i> Fi Wakalah</i>.</li>
                                                                        <li><b>Redemption for other financing (if applicable)</b>/<i> Penyelesaian pembiayaan lain (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Banca Takaful Product (if applicable)</b>/<i> Produk Banca Takaful (sekiranya berkenaan)</i>.</li>
                                                                        <li><b>Interbank Giro (IBG) Fee</b>/<i> Fi Giro Antara Bank (IBG)</i>.</li>
                                                                        <li><b>Other charges (if applicable)</b>/<i> Lain-lain caj (sekiranya berkenaan)</i>.</li>
                                                                    </ol><br>
                                                                </li>

                                                                <li> 
                                                                    <b>The deductions to be made are subject to the package taken by me and based on the terms imprinted in the SMS 'aqad.</b>
                                                                    <br><i>Potongan yang akan dibuat adalah tertakluk kepada pakej yang diambil oleh saya dan sebagaimana yang tertera  di dalam 'aqad SMS.</i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I also agree that the net financing amount (after deductions) will be paid by MBSB to me by crediting my account details of which I have provided or by any other mode deemed suitable by MBSB.</b>
                                                                    <br> <i>Saya juga bersetuju bahawa baki amaun pembiayaan di atas (selepas potongan) akan dibayar oleh MBSB kepada saya dengan mengkreditkan akaun saya mengikut butiran yang telah disertakan atau melalui apa-apa cara yang difikirkan sesuai oleh MBSB.</i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I also confirm and undertake that this authorization is irrevocable in the event the amount to settle other debts has been paid to any third party on my behalf by MBSB  and I further agree and undertake to pay all indebtedness owing to MBSB in the event I cancel the facility.</b>
                                                                    <br> <i>Saya juga dengan ini akujanji bahawa kebenaran ini adalah kebenaran tidak boleh batal sekiranya kesemua amaun tebus hutang telah pun dibayar kepada mana-mana pihak ketiga bagi pihak saya oleh MBSB dan saya juga bersetuju serta berakujanji akan membayar kesemua amaun tebus hutang tersebut kepada MBSB jika saya membatalkan kemudahan tersebut.  </i><br><br>
                                                                </li>

                                                                <li>
                                                                    <b>I shall be responsible to pay in full any cost incurred. I hereby further undertake to keep MBSB fully indemnified and make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB may suffer or incur as a result of fulfilling its function as set out above.</b>
                                                                    <br><i>Saya akan bertanggungjawab untuk membayar sepenuhnya semua kos yang ditanggung. Saya juga mengaku janji akan menanggung segala kerugian MBSB dengan membayar sepenuhnya  semua kerugian, kos dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB akibat memenuhi fungsi seperti yang dinyatakan di atas.</i>
                                                                </li>
                                                            </ol>
                                                        
                                                         </td>
                                                        </tr>
                                                      </table>

                                                       <!-- Credit Transactions --><br>

                                                        <table  >
                                                          <tr>
                                                                <td class="border" align="left" bgcolor="#0055a5">  <font color="white"><b>7.5) CREDIT TRANSACTIONS AND EXPOSURES WITH CONNECTED PARTIES/ </b>/ <i>TRANSAKSI KREDIT DAN PENDEDAHAN DENGAN PIHAK BERKAITAN </i></font> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="border" align="justify">
                                                                    <b><br> Do you have any immediate family or close relatives (including parents, brother/sister and their spouses, dependent's spouse and own/step/adopted child) that are employees of MBSB?</b><br> 
                                                                    <i>Adakah anda mempunyai ahli keluarga atau saudara terdekat (termasuk ibubapa, abang/kakak/adik dan pasangan, pasangan dibawah tanggungan, anak dan saudara tiri/angkat) yang sedang bekerja dengan MBSB? </i><br><br>
                                                                     <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" id="credit_yes" name="credit_transactions" value="1" <?php if($term->credit_transactions>0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" id="credit_no" name="credit_transactions" value="0" <?php if($term->credit_transactions==0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                    <br>
                                                                    <b>If Yes, please complete the information below:/</b><i> Jika Ya, sila lengkapkan maklumat dibawah:</i><br><br>
                                                                    <table width="40%">
                                                                        <tr>
                                                                            <td>
                                                                                <b>Full Name</b><br>
                                                                                <i>Nama Penuh</i>
                                                                            </td>
                                                                             <td>
                                                                                <input type="text" class="form-control" name="fullname" <?php if(!empty($credit->fullname)): ?>  value="<?php echo e($credit->fullname); ?>" <?php endif; ?> disabled/>
                                                                            </td>

                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>MyKad No.</b><br>
                                                                                <i>No. MyKad</i>
                                                                             </td>
                                                                             <td>
                                                                                <input type="text" class="form-control" <?php if(!empty($credit->mykad)): ?>   value="<?php echo e($credit->mykad); ?>" <?php endif; ?>  name="mykad" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Passport No.</b><br>
                                                                                <i>No. Passport</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" class="form-control" <?php if(!empty($credit->passport)): ?>  value="<?php echo e($credit->passport); ?>" <?php endif; ?> name="passport" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                             <td>
                                                                                 <b>Relationship</b><br>
                                                                                <i>Hubungan</i>
                                                                             </td>
                                                                              <td>

                                                                                <input type="text" class="form-control" <?php if(!empty($credit->relationship)): ?>   value="<?php echo e($credit->relationship); ?>" <?php endif; ?>  name="relationship" disabled/>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>


                                                          <!-- Consent --><br>

                                                        <table  >
                                                          <tr>
                                                                <td align="left" class="border" bgcolor="#0055a5">  <font color="white"><b>7.6) CONSENT FOR CROSS-SELLING, MARKETING, PROMOTIONS, ETC</b>/ <i>PERSETUJUAN UNTUK JUALAN SILANG, PEMASARAN, PROMOSI DAN LAIN-LAIN </i></font> 
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                
                                                                <td class="border" align="justify">
                                                                     <b><br> I/We expressly consent and authorize MBSB to process any information that I/we have provided to MBSB for the purposes of cross-selling, marketing and promotions including disclosure to its strategic partners or such persons or third parties as MBSB deem fit.</b><br>
                                                                    <i>Saya/Kami mengesahkan bahawa saya/kami memberi kebenaran dan kuasa kepada MBSB yang tidak boleh dibatal tanpa kebenaran untuk mendedahkan sebarang maklumat yang telah saya/kami kemukakan kepada MBSB bagi tujuan jualan silang, pemasaran dan promosi termasuk pendedahan kepada rakan strategik atau mana-mana individu atau pihak ketiga yang difikirkan wajar oleh MBSB.</i><br><br>
                                                                     <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="consent_for" id="consent_for_yes" value="1" <?php if($term->consent_for>0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="consent_for" id="consent_for_no" value="0" <?php if($term->consent_for==0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                              
                                                            </tr>
                                                        </table>

                                                        <!-- High Networth --><br>

                                                        <table > 
                                                            <tr>
                                                                 <td align="left" class="border" bgcolor="#0055a5">  <font color="white">  <b>7.7) HIGH NETWORTH INDIVIDUAL CUSTOMER ("HNWI")</b>/ <i>INDIVIDU YANG BERPENDAPATAN TINGGI ("HNWI") </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border" align="justify">
                                                                    <br> <b> HNWI means an individual whose total net personal assets, or total net joint assets with his or her spouse, exceeds RM3 million or its equivalent in foreign currencies, excluding the value of the individual's primary residence. Calculation of HNWI is total asset less total liabilities.</b><br> 
                                                                   <i>HNWI bermaksud seseorang individu di mana jumlah bersih aset-aset peribadi, atau jumlah bersih aset-aset bersama dengan pasangan, melebihi RM3 juta atau yang setaraf dengannya dalam mata awang asing, tidak termasuk nilai kediaman utama individu tersebut. Pengiraan HNWI adalah berdasarkan jumlah keseluruhan aset tolak jumlah keseluruhan liabiliti.</i><br>

                                                                    <br> <b>Does your total net personal assets or total net joint assets with your spouse exceeds RM3 million?</b></br>
                                                                    <i>Adakah jumlah bersih aset-aset peribadi anda atau jumlah bersih aset bersama dengan pasangan anda melebihi RM3 juta?. </i><br><br>
                                                                    <table>
                                                                        <tr>
                                                                            <td>
                                                                                <input type="radio" name="high_networth" id="high_networth_yes" value="1" <?php if($term->high_networth>0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>Yes</b>/Ya</td>
                                                                            <td>&nbsp;&nbsp;&nbsp;</td>
                                                                            <td>
                                                                                <input type="radio" name="high_networth" id="high_networth_no" value="0" <?php if($term->high_networth==0): ?> checked <?php endif; ?> >
                                                                            </td>
                                                                            <td><b>No</b>/Tidak</td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>

                                                          <!-- POLITICALY EXPOSED PERSON --><br>

                                                        <table > 
                                                            <tr>
                                                                 <td align="left" class="border" bgcolor="#0055a5">  <font color="white">  <b>7.8) POLITICALLY EXPOSED PERSON ("PEP")</b>/ <i> INDIVIDU BERKAITAN POLITIK ("PEP") </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border" align="justify">
                                                                    <br> <b> PEP - Individuals who are or who have been entrusted with prominent public functions domestically or internationally. Family members of PEPs are defined as those who may be expected to influence or be influenced by that PEP, as well as dependents of the PEP. This includes the PEP’s:</b><br> 
                                                                    <i>PEP – seseorang Individu yang diamanahkan dengan “Fungsi Awam Yang Penting” samaada domestik atau antarabangsa. Ahli keluarga PEP adalah ditakrifkan sebagai mereka yang dijangka boleh mempengaruhi atau dipengaruhi oleh PEP tersebut dan juga tanggungan PEP. Ianya termasuk:</i><br>
                                                                    <ol type="i">
                                                                        <li>
                                                                            <b>Spouse and dependents of the spouse;</b><br><i>Pasangan suami atau isteri berserta tanggungannya;</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Child (including step children and adopted children) and spouse of the child;</b><br>
                                                                            <i>Anak (termasuk anak tiri atau anak angkat yang sah) berserta pasangan suami atau isteri kepada anak-anak tersebut;</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Parent; and</b><br>
                                                                            <i>Ibu bapa; dan</i>
                                                                        </li>
                                                                        <li>
                                                                            <b>Brother or sister and their spouses.</b><br>
                                                                            <i>Adik beradik berserta pasangan suami atau isteri mereka. </i>
                                                                        </li>
                                                                    </ol>

                                                                    <b>Definition of Related Closed Associates of PEPs:/</b> Definisi Kenalan-Kenalan yang Berkait Rapat dengan PEPs:
                                                                    <ul>
                                                                        <li>
                                                                            <b>Related close associate to PEP is defined as individual who is closely connected to a PEP, either socially or professionally.</b><br>
                                                                            <i>Kenalan-kenalan yang berkait rapat dengan PEP ditakrifkan sebagai individu yang saling berhubung dan berkait rapat dengan PEP, samada secara sosial atau profesional. </i>
                                                                        </li>
                                                                    </ul><br>
                                                                     <b>For Individual</b> / <i>Untuk Individu:</i>
                                                                     <ol type="1">
                                                                        <li>
                                                                            <b>Are you a PEP or a Family Member(s) of PEP or a Related Close Associate(s) of PEP?</b>
                                                                            <br>
                                                                            <i>Adakah anda seorang PEP atau ahli keluarga PEP atau kenalan-kenalan berkait rapat dengan PEP?</i>
                                                                             <table>
                                                                                <tr>
                                                                                    <td>
                                                                                        <input type="radio" name="politically" value="1" id="politically_yes" <?php if($term->politically>0): ?> checked <?php endif; ?> >
                                                                                    </td>
                                                                                    <td><b>Yes</b>/Ya</td>
                                                                                    <td>&nbsp;&nbsp;&nbsp;</td>
                                                                                    <td>
                                                                                        <input type="radio" name="politically" id="politically_no" value="0" <?php if($term->politically==0): ?> checked <?php endif; ?> >
                                                                                    </td>
                                                                                    <td><b>No</b>/Tidak</td>
                                                                                </tr>
                                                                            </table><br>
                                                                            <b>If Yes, please complete the information below:/</b><i> Jika Ya, sila lengkapkan maklumat di bawah:</i><br>
                                                                            <table border="1" width="100%">
                                                                                <tr align="center">
                                                                                    <td width="5%">No.</td>
                                                                                    <td><b>Name</b><br><i>Nama</i></td>
                                                                                    <td><b>Relationship with customer</b><br>
                                                                                    <i>Hubungan dengan Pelanggan</i></td>
                                                                                    <td><b>Status**</b><br><i>Status**</i></td>
                                                                                    <td><b>Prominent Public Position</b><br><i>Kedudukan Awam yang Penting</i></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        1
                                                                                    </td>
                                                                                    <td>
                                                                                         <?php if(!empty($pep->name1)): ?> <?php echo e($pep->name1); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->relationship1)): ?> <?php echo e($pep->relationship1); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->status1)): ?> <?php echo e($pep->status1); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->prominent1)): ?> <?php echo e($pep->prominent1); ?> <?php endif; ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        2
                                                                                    </td>
                                                                                    <td>
                                                                                         <?php if(!empty($pep->name2)): ?>  <?php echo e($pep->name2); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->relationship2)): ?> <?php echo e($pep->relationship2); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->status2)): ?> <?php echo e($pep->status2); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->prominent2)): ?> <?php echo e($pep->prominent2); ?> <?php endif; ?>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        3
                                                                                    </td>
                                                                                    <td>
                                                                                         <?php if(!empty($pep->name3)): ?> <?php echo e($pep->name3); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->relationship3)): ?> <?php echo e($pep->relationship3); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                        <?php if(!empty($pep->status3)): ?> <?php echo e($pep->status3); ?> <?php endif; ?>
                                                                                    </td>
                                                                                    <td>
                                                                                         <?php if(!empty($pep->prominent3)): ?> <?php echo e($pep->prominent3); ?> <?php endif; ?>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                            <b>** Currently holding/ is actively seeking/ is being considered/ Previously holding</b><br><i>** Memegang jawatan buat masa ini/ masih aktif mencari/ dalam pertimbangan/ memegang jawatan sebelum ini</i>

                                                                        </li>
                                                                     </ol>
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>

                                                          <!-- FOR GOODS AND SERVICES--><br>

                                                        <table > 
                                                            <tr>
                                                                 <td align="left"  class="border" bgcolor="#0055a5">  <font color="white">  <b>7.9) FOR GOODS AND SERVICES TAX ("GST") EFFECTIVE 1 APRIL 2015</b>/ <i>UNTUK CUKAI BARANGAN DAN PERKHIDMATAN (“CBP”) BERKUATKUASA 1 APRIL 2015 </i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border" align="justify">
                                                                    <br> <b>I/We hereby agree that I/We shall be liable for all Goods and Services Tax (GST) payable in connection with this application or any account or any service in connection therein and MBSB shall be authorized to debit my/our account for the same.</b><br> 
                                                                    <i>Saya/Kami bersetuju bahawa saya/kami akan bertanggungjawab ke atas Cukai Barangan dan Perkhidmatan (“CBP”) yang berkaitan dengan permohonan ini atau mana-mana akaun atau apa-apa perkhidmatan yang berkaitan dan MBSB dibenarkan untuk mendebit akaun saya/kami bagi tujuan yang sama.</i><br><br>
                                                                   
                                                                   
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>


                                                          <!-- FOR GOODS AND SERVICES--><br>

                                                        <table > 
                                                            <tr>
                                                                 <td class="border" align="left" bgcolor="#0055a5">  <font color="white">  <b>7.10) PRODUCT DISCLOSURE SHEET</b>/ <i>LEMBARAN PENJELASAN PRODUK</i></font>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="border" align="justify">
                                                                    <br> <b>I/We hereby declare that I/ we have been briefed on the information contained in the Product Disclosure Sheet that has been given to me/us for the product applied herein</b><br> 
                                                                    <i>Saya/Kami mengesahkan bahawa saya/kami telah diberitahu mengenai maklumat yang terkandung di dalam Lembaran Penjelasan Produk yang telah diberikan kepada saya/kami berkaitan dengan produk yang dipohon di sini.</i><br><br>
                                                                    <table width="90%" border="0">
                                                                         <tr>
                                                                             <td valign="top" width="40%">
                                                                                <table>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <b>Name</b><br>
                                                                                            <i>Nama</i>
                                                                                        </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="<?php echo e($data->name); ?>" name="" disabled/>
                                                                                        </td>

                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>MyKad No.</b><br>
                                                                                            <i>No. MyKad</i>
                                                                                         </td>
                                                                                         <td>
                                                                                            <input type="text" class="form-control" value="<?php echo e($data->new_ic); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                         <td>
                                                                                             <b>Date.</b><br>
                                                                                            <i>Tarikh</i>
                                                                                         </td>
                                                                                          <td>

                                                                                            <input type="text" class="form-control" value="<?php echo e($today); ?>" name="" disabled/>
                                                                                        </td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                             <td valign="top">
                                                                                    <table  width="500" height="100">
                                                                                        <tr>
                                                                                            <td align="center" class="border1" ><h2>
                                                                                                 <input type="checkbox" value="1" name="product_disclosure" <?php if($term->product_disclosure>0): ?> checked <?php endif; ?> > <b>I Agree</b> / Saya Bersetuju</h2>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>

                                                                   

                                                                   
                                                                </td>
                                                            </tr>

                                                           
                                                          </table>
                                                          </body>
                                                          </html>

