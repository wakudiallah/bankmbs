<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Get Egibity Result";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");

include("asset/inc/header.php");

?>


<style>
        .tos {
            /* styles */
    
            -webkit-print-color-adjust:exact;
            font-family: "Arial";
            font-size:12px;
            color: #0055a5;

        }

        td.border {

          font-color:white;
            padding: 10px; /* cellpadding */
             line-height: 150%;
 
        }

        td.border1 {
           border-collapse: collapse;
        border: 1px solid black;
        }

  
    

 </style>
<!-- ==========================CONTENT STARTS HERE ========================== -->
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>

        <div id="main" role="main">
<br><br><br><br>

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
                     
 
     <?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>    
           
          
				<div class="row">
					<div class="col-md-2 col-lg-2">
   
</div>
                    
	
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
		<div class="well no-padding">
            <div class='smart-form client-form' id='smart-form-register2'>
                <header> <p class="txt-color-white"><b>   Upload Documents </b> </p> </header>
                    <fieldset>
                        <form id="wizard-1" >
                            <table class=" table-hover" width="50%">
                                <tr>
                                    <td width="50%">Name</td>
                                    <td width="5%">: </td>
                                    <td width="45%"> <?php echo e($pra->fullname); ?></td>
                                </tr>
                                <tr>
                                    <td>IC Number</td>
                                    <td>: </td>
                                    <td> <?php echo e($pra->icnumber); ?></td>
                                </tr>
                                <tr>
                                    <td>Financing Amount</td>
                                    <td>: </td>
                                    <td> RM <?php echo e(number_format($loanamount->loanammount)); ?></td>
                                </tr>
                                <tr>
                                    <td>Tenures</td>
                                    <td>: </td>
                                    <td> <?php echo e($loanamount->Tenure->years); ?> tahun</td>
                                </tr>
                                <tr>
                                    <td>Package</td>
                                    <td>: </td>
                                    <td>
                                        <?php if($pra->package->id=="1"): ?>
                                            Mumtaz-<i><font size="2">i</font></i>
                                        <?php elseif($pra->package->id=="2"): ?>
                                             Afdhal-<i><font size="2">i</font></i>
                                        <?php else: ?>
                                           Private Sector PF-<i><font size="2">i</font></i>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                            </table>
                            <br>
                            <br>
                           
                            <section>
                                <div class="col-lg-7">
                                    <label class=""> Latest Payslip</b> </label>
                                </div>

                                <div class="col-lg-5">      
                                    <label class="input ">
                                        <input type="hidden" name="_token" id="token" value="<?php echo e(csrf_token()); ?>">   
                                         <input type="hidden" id="id_praapplication" name="id_praapplication" value="<?php echo e($pra->id); ?>"/>    
                                         <input id="fileupload7"  <?php if(empty($document7->name)): ?> required <?php endif; ?>   class="btn btn-default" type="file" name="file7"/>
                                        <input type="hidden" name="document7"  id="documentx7"  value="Payslip">
                                        &nbsp; <span id="document7"> </span>
                                        <input type='hidden' value='<?php if(!empty($document7->name)): ?><?php echo e($document7->upload); ?><?php endif; ?>' id='a7' name='a7'/>                        
                                        <?php if(!empty($document7->name)): ?>

                                        <!--<img src="<?php echo e(asset('/')); ?>/storage/uploads/file/<?php echo e($pra->id); ?>/<?php echo e($document7->upload); ?>" width="110"/>-->

                                            <span id="document7a"><a target='_blank' href="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->icnumber)); ?>/<?php echo e($document7->upload); ?>"> <?php echo e($document7->name); ?></a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                                            <!--<img src="<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->icnumber)); ?>/<?php echo e($document7->upload); ?>"/> -->
                                        <?php else: ?>
                                        <?php endif; ?>
                                    </label>
                                </div>
                            </section>

                            
                            <i>*Documents in PDF / JPG / PNG format (please make sure the document is clear)</i>
                        </form><br><br>
                               <?php echo Form::open(['url' => 'form/savesettlement' ]); ?>

                               <div class="form-group"><BR><BR>
                        <label><b>
Need to complete financing with other banks / cooperatives (overlapping) ?</b> :</label>
                        <br>
                   
                      <?php if($financial->debt_consolidation=='1'): ?>
                        <input type='radio' onchange="show2()" id='debt_consolidation'  name='debt_consolidation' checked value='1' required>Yes<br>
                        <input type='radio' onchange="show(this.value)" id='debt_consolidation'   name='debt_consolidation'  value='0' required>No<br><br>
                     
                      <?php else: ?>
                        <input type='radio' onchange="show2()" id='debt_consolidation'   name='debt_consolidation' value='1' required>Ye<br>
                        <input type='radio' checked onchange="show(this.value)" id='debt_consolidation'  name='debt_consolidation'  value='0' required>No<br><br>
                      <?php endif; ?>

              </div>
              
                      
                                  <div class="form-group">
                                  <?php if($financial->debt_consolidation>0): ?> 
                                           <?php $style_settlement = "style='display:block'"; ?>
                                  <?php else: ?>
                                      <?php $style_settlement = "style='display:none'"; ?>
                                  <?php endif; ?>
            <?php if($financial->debt_consolidation>0): ?> 
              <div id="add_redemption_button" style='display:block' >
                
                  <div class="form-group">
                     
                    
                        <table id="setData" border='1' class='table table-hover table-borderd col-md-11 col-lg-11'> 
                          <tbody>
                          <tr bgcolor="#e6f3ff"> 
                              
                              <td>Name of Entity</td> 
                              <td>Monthly Payment</td>
                              <td>Remaining Financing Term </td>
                          
                            </tr> 
                         
                          <?php $z=1;?>
                    
                     
                           
                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                            <input type="hidden" id="id_praapplication" name="id_praapplication" value="<?php echo e($pra->id); ?>"/>
                            <tr>
                              
                             
                              <td width='55%'>
                                sds
                               </td>
                               <td width='55%'>
                                sds
                               </td>
                               <td width='55%'>
                                sds
                               </td></tr>
                            <?php $__currentLoopData = $settlement; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $set): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                              
                             
                              <td width='55%'>
                                sds
                               </td>
                             
                               <td width='25%'><input type='text' id='set_installment<?php echo e($z); ?>'  class="form-control settlement"  name='set_installment[]' value='<?php echo e($set->installment); ?>' /></td>

                                 <td width='25%'><input type='text' id='set_amount<?php echo e($z); ?>'  value='<?php echo e($set->amount); ?>' class="form-control settlement"  name='set_amount[]' value='' /></td>
                           
                         
                            </tr>
                            <?php $z++; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           
                       
                          
                       
                          
                         
                       
                          </tbody>
                        </table><br>
                        

                          
                         
                 
                </div>
              </div>
               
               
          
            </div><br><br>
             <?php echo Form::close(); ?> 
             <?php else: ?>
              <div id="add_redemption_button" style='display:none' >
                
                  <div class="form-group">
                        <table id="setData" border='1' class='table table-hover table-borderd col-md-11 col-lg-11'> 
                          <tbody> 
                            <tr bgcolor="#e6f3ff"> 
                              
                              <td>Name of Entity</td> 
                              <td>Monthly Payment</td>
                              <td>Remaining Financing Term </td>
                          
                            </tr>
                         
                           
                    
                     
                           
                           
                            <?php if(empty($financial->debt_consolidation)): ?>

                            <?php for($z=1;$z<=3;$z++): ?>
                            <tr>
                              
                             
                              <td width='55%'>
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                <input type="hidden" id="id_praapplication" name="id_praapplication" value="<?php echo e($pra->id); ?>"/>
                                <input type="hidden" id="type" name="type" value="<?php echo e($z); ?>"/>
                              <input type='text' class="form-control settlement"  name='set_institution[]' id='set_institution<?php echo e($z); ?>'/>
                               </td>
                               <td width='25%'><input type='text' class="form-control settlement"  name='set_installment[]' id='set_installment<?php echo e($z); ?>' /></td>
                               
                                 <td width='25%'><input type='text' value='' class="form-control settlement"  name='set_amount[]' value='' id='set_amount<?php echo e($z); ?>'/></td>
                             
                            </tr>
                             <?php endfor; ?>
                            <?php else: ?>
                           
                            <?php $jumlah =0; ?>
                            <?php $__currentLoopData = $settlement; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $set): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                              
                             
                              <td width='55%'>
                                <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                               <input type="hidden" id="id_praapplication" name="id_praapplication" value="<?php echo e($pra->id); ?>"/>
                              <input type='text' class="form-control"  name='set_institution[]' value='<?php echo e($set->institution); ?>' />

                               </td>
                               <td width='25%'><input type='text' class="form-control"  name='set_installment[]' value='<?php echo e($set->installment); ?>' /></td>
                               
                                 <td width='25%'><input type='text' value='<?php echo e($set->amount); ?>' class="form-control"  name='set_amount[]' value='' /></td>
                             
                            </tr>
                             <?php $jumlah++; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                          


                            <?php endif; ?>
                       
                          
                       
                          
                         
                       
                          </tbody>
                        </table><p>&nbsp;</p>
                
                         
                 
                </div>
              </div>
               
               
          
            </div>
             <?php echo Form::close(); ?> 
             <?php endif; ?>

                  <div class="form-group">
                   
                        
                        <input type='checkbox' id='terms'  name='terms' value='1' required="required" ><b> I agree with <a href="" data-toggle="modal" data-target="#termModal">Term and condition</a></b><br>
                      

              </div><br>

              <div align='right'>
                                        <a align='right' id="submit_upload" class="btn btn-lg btn-primary"><b><i class="fa fa-send"></i> Submit</b></a>
                                    </div>

                                </fieldset>
                             
                                
                                     
                                                        
                             
                                <footer>
								                  
                                   
                                </footer>


                                  

                    <div>
                    </form>


                                      

						</div>
					
					</div>


				</div>


			</div>
		

		</div>
		
		</div>
       <div class="page-footer">
        <div class="row">
            <div class="col-xs-4 col-sm-4 text-left ">
                <div class="txt-color-black inline-block">
                    <span id="logo"> <a href="<?php print url('/') ?>"> <img src="<?php echo ASSETS_URL; ?>/img/islam.png" alt="SmartAdmin"  style="margin-top: -17px !important; width: 150px"> </a></span>
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 text-right " style="font-size: 11px">
                <div class="txt-color-black inline-block">
                    <span class="txt-color-black">Copyright © <?php echo date('Y'); ?> MBSB Bank Berhad</span><br>
                    <span class="txt-color-black">Registration No: 200501033981 (716122-P). All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Account Verification</h4>
					
                    </div>
                    <div class="modal-body">
       <?php echo Form::open(['url' => 'form/uploaddoc', 'enctype' => 'multipart/form-data']); ?>

                      <fieldset>
					
						  <div class="form-body">	
								<div class="form-group">
									<font color='black'>IC Number</font>
										<input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
				    
									<input type="icnumber" id="icnumber" maxlength='12' minlength='6' class="form-control" name="icnumber" value="" placeholder="IC Number" required>
									<input type="hidden" id="fullname" name="fullname" value="<?php echo e($pra->fullname); ?>" placeholder="Full Name" readonly >
									<input type="hidden" id="id_praapplication" name="id_praapplication" value="<?php echo e($pra->id); ?>" readonly >
                   <input type='hidden' id='status_penyelesaian' name="debt_consolidation" value='0'/>
                    
                  <?php if($financial->debt_consolidation==0): ?>
                   <?php for($m=1;$m<=3;$m++): ?>
                 
                  <input type="hidden" name="set_institution[]" id="institusi<?php echo e($m); ?>"/>
                
                  <input type="hidden" name="set_installment[]" id="ansuran<?php echo e($m); ?>"/>
                
                  <input type="hidden" name="set_amount[]" id="jumlah<?php echo e($m); ?>"/>
                
                  <?php endfor; ?>
                  <?php else: ?>
                 <?php  $m=1; ?>
                   <?php $__currentLoopData = $settlement; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $set): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                           
                                <input type='hidden' class="form-control settlement" id="institusi<?php echo e($m); ?>"  name='set_institution[]' value='<?php echo e($set->institution); ?>' />
                             <input type='hidden' id="ansuran<?php echo e($m); ?>"  class="form-control settlement"  name='set_installment[]' value='<?php echo e($set->installment); ?>' />

                                <input type='hidden' id="jumlah<?php echo e($m); ?>"  value='<?php echo e($set->amount); ?>' class="form-control settlement"  name='set_amount[]' value='' />
                           
                         
                           
                            <?php $m++; ?>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
               

               
							
								</div>
							
						  </div>
					   </div>
					  
                               
                                     
                                     
                                </fieldset>
                     
						<div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">
						   Submit
						</button>
                                   
                           <?php echo Form::close(); ?>   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->









        <!-- Modal -->
    <div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              &times;
            </button>
            <h4 class="modal-title" id="myModalLabel">TERMA DAN SYARAT</h4>
          </div>
          <div class="modal-body custom-scroll terms-body">
            
 <div id="left" class="tos">

<table width='95%' border='0'>
  <tr>
    <td width="10%"> <img src="<?php echo e(url('/')); ?>/asset/img/mbsb_small.png"></td>
    <td width="90%"> <div align="center"><h3><b>TERMA DAN SYARAT UNTUK KEMUDAHAN PEMBIAYAAN PERIBADI-i MBSB<br>
DALAM VERSI BAHASA MALAYSIA SAHAJA</h3></b></div></td>
  </tr>
</table>

           <hr>
<b>

               <table >
                <tr>
                    <td class="border" align="justify">
                          <b>Terma & Syarat (“Terma-Terma”) Untuk Pembiayaan Peribadi-i MBSB Berdasarkan Konsep Syariah Tawarruq (“Kemudahan”)</b><br><br>
                          Tertakluk kepada penerimaan Malaysia Building Society Berhad (No. Syarikat: 9417-K) (“MBSB”) terhadap permohonan dari pemohon yang dinamakan (“Pemohon”) di dalam borang permohonan untuk kemudahan Pembiayaan Peribadi-i MBSB (“Borang ini”), Pemohon dengan ini bersetuju bahawa terma-terma dan syarat-syarat yang tercatat di sini (“Terma-Terma”) adalah terpakai bagi Kemudahan ini dan Pemohon bersetuju untuk terikat dengan Terma-Terma tersebut. 
 
                       <br>


                       
                    
                                
                    </td>
                </tr>
            </table>
             <table >
                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>1. DEFINISI-DEFINISI</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                          “Ansuran” bermaksud ansuran bulanan yang perlu dibayar oleh Pemohon kepada MBSB samada sebelum atau selepas ibra` (rebat) diberikan oleh MBSB bagi pembayaran Harga Jualan Komoditi MBSB secara bayaran tertangguh. “Harga Belian Komoditi MBSB” bermaksud harga belian bagi Komoditi yang dibeli oleh MBSB daripada pembekal Komoditi pada harga belian yang diperincikan di dalam tawaran Kemudahan yang bersamaan dengan jumlah Kemudahan yang ditawarkan kepada Pemohon.<br> <br> 
                          “Harga Jualan Komoditi MBSB” bermaksud harga jualan Komoditi yang dijual oleh MBSB kepada Pemohon pada harga jualan berdasarkan pada kadar keuntungan sebanyak _______________% setahun yang kena dibayar oleh Pemohon seperti yang diperincikan didalam tawaran Kemudahan. “Notis Pengeluaran Penuh dan Notis” bermaksud notis bertulis yang dikeluarkan oleh MBSB kepada Pemohon selepas amaun bersih Kemudahan dibayar kepada Pemohon yang menyatakan butiran Ansuran bagi Harga Jualan Komoditi MBSB dan juga merujuk kepada apa jua notis bertulis yang dikeluarkan oleh MBSB dari masa ke semasa selepas itu termasuk sebarang notis pindaan atau pembetulan. “Komoditi” bermaksud apa-apa komoditi yang halal dan mematuhi prinsip Syariah yang akan ditentukan pada masa transaksi oleh MBSB. “Sekuriti” bermaksud apa jua sekuriti yang dianggap diperlukan oleh MBSB dari masa ke semasa dan yang perlu disediakan oleh Pemohon atau pihak-pihak lain yang bersetuju memberi sekuriti berikutan permintaan Pemohon dan disetujui oleh MBSB bagi kesemua amaun yang perlu dibayar oleh Pemohon kepada MBSB. <br><br>
                        
                          &nbsp; 1A. TUJUAN KEMUDAHAN<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Kemudahan ini diberikan kepada Pemohon untuk kegunaan peribadi yang halal dan mematuhi Syariah.<br>


                       
                    
                                
                    </td>
                </tr>
                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>2. AMAUN YANG KENA DIBAYAR/PEMOTONGAN DARIPADA JUMLAH KEMUDAHAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                          Semua amaun yang dinyatakan di bawah (sekiranya berkenaan) akan ditolak daripada jumlah Kemudahan yang diluluskan oleh MBSB:<br>
                          Deposit Sekuriti (hanya akan diguna pakai pada akhir tempoh pembiayaan);
                          <ol type="i">
                            <li>Sumbangan Takaful GCTT;</li>
                            <li>Fi Wakalah;</li>
                            <li>Penyelesaian Pembiayaan lain;</li>
                            <li>Produk Banca Takaful;</li>
                            <li>Fi Giro Antara Bank (IBG);</li>
                            <li>Lain-lain caj dan amaun yang perlu dibayar oleh Pemohon.</li>
                            </ol>
                            Semua yang dinyatakan di atas adalah tertakluk kepada pakej yang diambil oleh Pemohon<br>
                                
                    </td>
                </tr>

                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>3. TRANSAKSI KEMUDAHAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                          Selaras dengan prosedur pembiayaan MBSB, MBSB akan menawarkan Kemudahan kepada Pemohon dengan menghantar satu khidmat pesanan ringkas ("SMS") kepada Pemohon atau dalam bentuk medium yang lain yang difikirkan sesuai oleh MBSB. Penerimaan Pemohon terhadap tawaran Kemudahan MBSB seperti yang terkandung dalam SMS dengan menjawab YA, atau dalam bentuk medium yang lain yang difikirkan perlu oleh MBSB, tertakluk kepada apa-apa pindaan yang akan dimaklumkan melalui notis oleh MBSB, akan membentuk obligasi dan komitmen Pemohon terhadap Terma-Terma dalam tawaran Kemudahan.<br><br>
                          Jawapan “TIDAK" terhadap SMS bermakna bahawa Pemohon menolak dan tidak mahu meneruskan Kemudahan.<br>Jawapan "TUKAR" terhadap SMS bermakna Pemohon ingin menukar jumlah atau tempoh Kemudahan.
                          <br><br>
                          Sekiranya Pemohon menjawab YA dan selaras dengan prinsip Syariah Tawarruq dan berdasarkan permohonan belian yang telah ditandatangani oleh Pemohon dalam Borang ini, MBSB akan membeli Komoditi pada Harga Belian Komoditi MBSB.<br><br>
                          Setelah MBSB membeli Komoditi tersebut daripada pembekal Komoditi, MBSB akan menjual Komoditi tersebut kepada Pemohon selaras dengan akujanji Pemohon untuk membeli Komoditi
daripada MBSB yang terkandung di dalam Borang ini pada Harga Jualan Komoditi MBSB.<br><br>

‘Aqad jual-beli di antara MBSB dan Pemohon akan dibuat dalam bentuk SMS yang akan dihantar oleh MBSB kepada Pemohon. Pemohon mempunyai pilihan untuk menjawab YA untuk menerima
tunai atau TIDAK untuk mengambil penghantaran<br><br>

Sekiranya Pemohon menjawab YA bermakna Pemohon bersetuju untuk membeli Komoditi dan berdasarkan perlantikan MBSB sebagai wakil Pemohon yang terkandung di dalam Borang ini,
MBSB akan menjual Komoditi tersebut sebagai wakil Pemohon kepada mana-mana pihak ketiga pada harga yang bersamaan dengan jumlah Kemudahan yang ditawarkan kepada Pemohon.<br><br>
Hasil jualan (iaitu jumlah Kemudahan) akan dimasukkan ke dalam akaun bank Pemohon setelah ditolak segala jumlah pemotongan yang dinyatakan dalam tawaran Kemudahan dan Terma-Terma.<br><br>
Sekiranya Pemohon menjawab TIDAK bermakna Pemohon akan mengambil penghantaran Komoditi tersebut dan kos penghantaran akan ditanggung sepenuhnya oleh Pemohon. Pemohon
hendaklah membayar Harga Jualan Komoditi MBSB kepada MBSB secara tangguh mengikut jadual bayaran/ansuran yang dinyatakan dalam tawaran Kemudahan.
                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>4. SYARAT TERDAHULU PENGGUNAAN KEMUDAHAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                    Sebelum MBSB membuat pengeluaran penuh amaun Kemudahan kepada Pemohon dan sebelum Pemohon boleh menggunakan Kemudahan tersebut, syarat-syarat berikut perlulah dipenuhi
terlebih dahulu oleh kesemua pihak yang terlibat:-<br>
    <ol type="i">
      <li>Semua dokumen Sekuriti dan dokumen sokongan berkaitan Kemudahan ini dan yang diperlukan oleh MBSB telah dikemukakan, diterima dan disempurnakan;</li>
      <li>Pemohon telah mematuhi semua terma dan syarat yang telah ditetapkan oleh MBSB seperti yang dinyatakan di dalam Terma-Terma disini;</li>
      <li>Tiada Kejadian Ingkar (Event of Default) seperti yang dinyatakan di perenggan (12) sedang atau telah berlaku;</li>
      <li>Pemohon telah lulus penilaian kredit dalaman (internal credit evaluation) MBSB seperti yang ditetapkan oleh MBSB;</li>
      <li>Pemohon telah membayar segala yuran dan kos yang perlu dibayar dibawah Kemudahan ini; dan</li>
      <li>Apa-apa syarat terdahulu yang MBSB fikirkan perlu dipatuhi oleh Pemohon dari masa ke semasa.</li>
</ol>
                         
                                
                    </td>
                </tr>

                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>5. REPRESENTASI & JAMINAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                 Pemohon memberi representasi dan jaminan kepada MBSB bahawa:<br>
                  <ol type="i">
                    <li>Pemohon mempunyai hak, kuasa dan keupayaan untuk menerima tawaran Kemudahan ini dan untuk melaksanakan tanggungjawabnya berdasarkan Terma-Terma ini.</li>
                     <li>Pemohon yang dinamakan di dalam Borang ini bukan seorang bankrap yang belum dilepaskan, dan tiada apa-apa prosiding kebankrapan, tindakan undang-undang atau apa-apa tindakan
yang lain samada semasa atau belum diputuskan terhadapnya yang akan mempengaruhi keupayaan Pemohon untuk memenuhi obligasinya dibawah Terma-Terma ini.</li>
                      <li>Tiada Kejadian Ingkar (Event of Default) seperti yang dinyatakan di perenggan (12) di bawah telah berlaku atau masih berlaku disamping berjanji bahawa Pemohon akan memaklumkan
kepada MBSB secara bertulis sekiranya Pemohon bukan lagi kakitangan kerajaan atau mana-mana agensi kerajaan sebelum Pemohon berhenti daripada perkhidmatan tersebut.</li>
                       <li>Terma-Terma ini menjadi kewajipan undang-undang yang sah, mengikat dan boleh dikuatkuasakan terhadap Pemohon berdasarkan Terma-Terma yang termaktub;</li>
                       <li>Segala maklumat yang diberikan oleh Pemohon kepada MBSB berkaitan Kemudahan ini adalah sahih dan benar dan tiada sebarang pengabaian oleh Pemohon yang mengakibatkan
maklumat tersebut tidak benar atau mengelirukan.</li>





                </ol>

                Representasi dan Jaminan yang diberikan disini akan berterusan selagi mana Harga Jualan Komoditi MBSB atau tangunggan di bawah Kemudahan ini masih tertunggak dan terhutang oleh
Pemohon. Kebenaran dan ketepatan representasi yang dinyatakan di dalam representasi-representasi dan jaminan-jaminan yang dinyatakan oleh Pemohon disini adalah menjadi asas komitmen
MBSB untuk menyediakan Kemudahan tersebut kepada Pemohon. Jika sebarang representasi-representasi dan/atau jaminan-jaminan yang telah dibuat selepas ini dan pada bila-bila masa
didapati tidak benar dalam mana-mana aspek yang metarial, di dalam keadaan tersebut dan tanpa mengambil kira apa-apa yang bertentangan di sini, MBSB akan mempunyai hak mutlak untuk
mengkaji semula, menggantungkan, menuntut semula atau membatalkan Kemudahan tersebut atau sebahagian daripadanya.<br><br>
Dimana bersesuaian dan apabila dikehendaki oleh MBSB, Pemohon akan melaksanakan atau menyebabkan pelaksanaan Sekuriti lain atau yang selanjutnya sepertimana diperlukan oleh MBSB.<br><br>

&nbsp; &nbsp; 5A. AKUJANJI (COVENANTS)<br>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Sepanjang tempoh Kemudahan ini, Pemohon berakujanji akan:
    <ol type="i">
      <li>membayar segala Ansuran dan jumlah keberhutangan (indebtedness) tepat pada masanya dan melaksanakan segala tanggungjawabnya;</li>
        <li>mengambil segala langkah yang perlu bagi memastikan bahawa tiada perubahan material ke atas kedudukan kewangannya;</li>
          <li>memberi kepada MBSB segala maklumat dan dokumen yang diperlukan oleh MBSB berkaitan dengan Kemudahan;</li>
            <li>memaklumkan kepada MBSB sekiranya berlaku Kejadian Ingkar di bawah Kemudahan ini atau sebarang Kejadian Ingkar yang berkaitan dengan mana-mana keberhutangan
(indebtedness) Pemohon yang lain;</li>
              <li>memberi kebenaran kepada MBSB untuk melantik mana-mana pihak dan/atau ejen untuk menguruskan segala urusan pembelian dan penjualan Komoditi bagi tujuan Kemudahan
ini selaras dengan prosedur pembiayaan MBSB.</li>
<li>Memastikan bayaran sumbangan bagi produk Banca Takaful yang dilanggan melalui Pakej Gabungan Produk dikekalkan selagi kemudahan Pembiayaan Peribadi-i aktif dengan
MBSB.</li>
    </ol>

                         
                                
                    </td>
                </tr>


                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>6. PEMBAYARAN HARGA JUALAN KOMODITI MBSB</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                         Apabila Kemudahan diluluskan, ia akan dikreditkan ke dalam akaun Pemohon oleh MBSB atau melalui apa-apa cara yang dianggap sesuai oleh MBSB atau budi bicaranya. Pemohon mengesahkan
bahawa penerimaan permohonan Kemudahan oleh MBSB adalah dianggap lengkap sebaik sahaja transaksi-transaksi yang dinyatakan di dalam (3) di atas telah dilaksanakan dan disempurnakan
atau Kemudahan yang diluluskan oleh MBSB telah dikreditkan ke dalam akaun Pemohon meskipun Kemudahan tersebut tidak diguna pakai di dalam akaun Pemohon atau amaun tebus hutang
(jika berkaitan) telahpun dibayar kepada mana-mana pihak ketiga oleh pihak MBSB bagi pihak Pemohon (yang mana Pemohon bersetuju dan berakujanji akan membayar kesemua amaun tebus
hutang tersebut kepada MBSB jika Kemudahan tersebut dibatalkan oleh Pemohon).<br><br>
Apa-apa perbezaan di dalam amaun Kemudahan yang dipohon, dan apabila diluluskan tidak akan memberi kesan kepada perihal sah Terma-Terma ini dan Pemohon akan dianggap bersetuju
untuk mendapatkan Kemudahan dalam amaun yang diluluskan oleh MBSB. Pembayaran Jualan Komoditi MBSB perlulah dibuat oleh Pemohon dalam amaun dan mengikut bilangan ansuran
seperti yang dinyatakan di dalam tawaran Kemudahan dan/atau seperti yang terkandung di dalam Notis Pengeluaran Penuh yang dikeluarkan oleh MBSB.
                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>7. BAYARAN ANSURAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                        Bayaran balik ansuran bulanan bagi Kemudahan ini boleh dibuat melalui potongan gaji, bayaran secara tunai / cek di mana-mana cawangan MBSB, melalui arahan tetap pembayaran atau melalui
bayaran atas talian dengan bank-bank tertentu. Kaedah, tarikh bayaran Ansuran pertama dan Ansuran berikutnya yang perlu dibayar oleh Pemohon adalah seperti yang ditetapkan di dalam
tawaran Kemudahan dan/atau Notis Pengeluaran Penuh.<br><br>Bagi akaun yang terikat dengan deposit sekuriti, deposit sekuriti tersebut tidak akan dipulangkan kepada Pemohon sebaliknya ianya akan digunakan untuk menyelesaikan baki akhir ansuran
bulanan Kemudahan. Dengan demikian, tempoh pembiayaan anda secara tidak langsung mungkin akan berkurangan dan akan tamat sebelum tempoh matang.<br><br>
<u>Bayaran Ansuran melalui Potongan Gaji:</u><br>
Bagi akaun yang melibatkan bayaran melalui potongan ANGKASA dan/atau Majikan dan/atau badan-badan dan organisasi-organisasi yang diluluskan oleh MBSB, sekiranya pihak ANGKASA
dan/atau Majikan dan/atau badan-badan dan organisasi-organisasi yang diluluskan oleh MBSB telah membuat pemotongan gaji dalam tempoh tiga (3) bulan atau tempoh lebih awal dari tarikh
pengeluaran Kemudahan / tarikh Ansuran pertama dan / atau Ansuran berikutnya sebagaimana yang dinyatakan didalam Notis Pengeluaran Penuh, maka tempoh Kemudahan akan dikurangkan
sewajarnya dan tiada pemulangan bayaran Ansuran akan dibuat oleh MBSB.<br><br>
Walau bagaimanapun, bayaran Ansuran oleh Pemohon hanya akan dianggap dibayar oleh Pemohon apabila MBSB menerima bayaran dan bukannya semasa pemotongan gaji dilakukan oleh
pihak Koperasi Biro Angkasa (“ANGKASA”) / Akauntan General (AG) Negara / Negeri dan/atau pihak majikan Pemohon (“Majikan”). Tanpa menjejaskan peruntukan di atas, Pemohon mestilah
membuat bayaran Ansuran secara terus kepada MBSB:-
  <ol type="i">
    <li>Sehingga potongan gaji oleh ANGKASA dan/atau oleh pihak Majikan berkuatkuasa; dan/atau</li>
     <li>Sekiranya potongan gaji tersebut tidak diterima oleh MBSB atas apa-apa alasan sekalipun; dan/atau</li>
      <li>Sekiranya amaun yang diterima melalui potongan gaji kurang daripada bayaran Ansuran atas apa-apa alasan sekalipun</li>
     
  </ol>

Pemohon dengan ini juga membenarkan (kebenaran yang tidak boleh dibatalkan) MBSB untuk mendebitkan mana-mana akaun deposit Pemohon untuk Ansuran bulanan pada atau sebelum
tarikh Ansuran bulanan mesti dibayar dan mana-mana caj lain yang berkaitan dengan Kemudahan. Untuk tujuan debit yang dinyatakan di sini:- 
   <ol type="i">
    <li>Pemohon berakujanji untuk memastikan simpanan yang secukupnya telah disimpan di dalam akaun untuk memenuhi bayaran ansuran. MBSB boleh mengenakan caj levi ke atas semua
arahan tetap (standing instructions) pada tarikh matang, yang mana tidak akan dipulangkan walaupun arahan tersebut tidak digunapakai oleh kerana kekurangan simpanan.</li>
     <li>MBSB tidak akan menanggung apa-apa liabiliti terhadap mana-mana kesilapan, keengganan atau pengabaian untuk membuatkan semua atau mana-mana pembayaran atau berdasarkan
alasan bayar lewat atau pengabaian untuk mematuhi arahan tersebut.</li>
      <li>MBSB akan menentukan pembayaran wang daripada akaun Pemohon berdasarkan arahan ini atau mana-mana perintah diberikan kepada MBSB.</li>
       <li>Sekiranya bayaran tidak boleh dibuat kerana baki akaun simpanan tidak mencukupi, MBSB tidak akan membuat bayaran pada tarikh bayaran berkenaan. Pembayaran selanjutnya hanya
akan dibuat pada tarikh pembayaran yang berikutnya. Pemohon dikehendaki membuat pelan alternatif untuk memberi kesan kepada bayaran yang berkenaan.</li>
<li>Sebagai balasan untuk MBSB menyusunkan arahan ini, Pemohon mengakujanji kepada MBSB untuk menanggung rugi semua atau mana-mana dakwaan, tuntutan, kehilangan, kerugian,
kos, caj dan/atau perbelanjaan yang MBSB mungkin atau akan alami atau tanggung.</li>
  </ol>
                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>8. TA`WIDH (PAMPASAN)</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                   Tanpa mengambil kira apa-apa yang terkandung di sini, Pemohon bersetuju dan berjanji untuk membayar kepada MBSB ta`widh (pampasan) di atas Ansuran yang tertunggak dan bayaran untuk
keberhutangan (Indebtedness) atau Harga Jualan Komoditi MBSB, seperti mana yang bersesuaian, secara berikutnya:-<br>
    <ol type="i">
      <li>Semasa tempoh Kemudahan: Pampasan bayaran lewat (Ta’widh) akan dikenakan pada kadar 1% setahun ke atas jumlah tunggakkan atau sebahagian daripadanya atau mengikut apa-apa
kaedah atau kadar lain yang ditetapkan oleh Jawatankuasa Penasihat Syariah MBSB dari semasa ke semasa; dan</li>
      <li>Selepas tempoh matang Kemudahan: Pampasan bayaran lewat (Ta’widh) akan dikenakan pada kadar yang tidak melebihi kadar semasa Pasaran Wang Antara Bank Islam (IIMM) ke atas
baki tertunggak; dan</li>
      <li>Jumlah bagi Ta`widh (pampasan) ke atas keberhutangan termasuk Harga Jualan Komoditi MBSB, seperti mana yang bersesuaian tidak akan dikompaunkan (compounded).</li>
</ol>
                         
                                
                    </td>
                </tr>

                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>9. PENYELESAIAN AWAL HARGA JUALAN KOMODITI MBSB</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                   Dalam kes-kes penyelesaian awal untuk keseluruhan atau sebahagian daripada Harga Jualan Komoditi, pelarasan ibra' (rebat) akan dilakukan oleh MBSB berdasarkan peraturan-peraturan yang
ditetapkan oleh Jawatankuasa Penasihat Syariah MBSB. MBSB akan memberikan ibra' (rebat) kepada Pemohon di mana terdapat:<br>
    <ol type="i">
      <li>Penyelesaian awal atau penebusan awal Kemudahan tersebut; atau</li>
      <li>Penyelesaian kontrak pembiayaan asal kerana penyusunan semula pembiayaan; atau</li>
      <li>Penyelesaian oleh Pemohon dalam kes lalai; atau</li>
      <li>Penyelesaian oleh Pemohon sekiranya berlaku penamatan atau pembatalan pembiayaan sebelum tarikh matang; atau</li>
      <li>Sekiranya Kadar Keuntungan Efektif lebih rendah daripada Kadar Keuntungan Siling (yang berkenaan).</li>
</ol>
<u>Bagi Pembiayaan Peribadi dengan Kadar Tetap:-</u><br><br>
Pengiraan ibra' (rebat) =<br>
<img src="<?php echo e(url('/')); ?>/img/formula.jpg"/><br>
<u>Bagi Pembiayaan Peribadi dengan Kadar Berubah:-</u><br>
Pengiraan ibra' (rebat) = Untung Tertangguh - Caj Penyelesaian Awal<br><br>
Jumlah Penyelesaian = Baki Harga Jualan + Bayaran Ansuran Tertunggak + Caj bayaran lewat - Penyelarasan keatas Ibra' (rebat) disebabkan perubahan pada Kadar Keuntungan Efektif - Ibra'
semasa penyelesaian
                         
                                
                    </td>
                </tr>

                  <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>10. PENOLAKAN (SET-OFF), PENCANTUMAN DAN PENGGABUNGAN AKAUN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                  MBSB berhak pada bila-bila masa dalam budi bicara mutlaknya dan tanpa notis kepada Pemohon menggabungkan atau mencantumkan semua atau mana-mana akaun Pemohon (apa-apa jenis
sama ada tertakluk kepada notis ataupun tidak) tidak kira di mana ia berada dan menggunakan kesemuanya, termasuk sebarang wang di dalam simpanan MBSB, melalui potongan gaji ANGKASA
dan/atau oleh pihak Majikan untuk menyelesaikan sebarang keberhutangan dan tanggungjawab Pemohon terhadap MBSB.<br><br>
MBSB berhak untuk memindahkan atau menolak apa-apa jumlah kredit dalam mana-mana akaun Pemohon untuk tujuan penyelesaian mana-mana liabiliti Pemohon ke atas MBSB dan MBSB
dengan ini diberi kuasa penuh oleh Pemohon untuk melakukan pemindahan atau penolakan tersebut.

                         
                                
                    </td>
                </tr>

                   <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>11. TAKAFUL, YURAN DAN KOS</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                 Pemohon digalakkan mengambil pelan takaful persendirian daripada syarikat takaful yang diluluskan oleh MBSB apabila menerima tawaran Kemudahan ini. Bayaran takaful akan ditolak
daripada jumlah Kemudahan yang diluluskan kepada Pemohon.<br><br>
Semua yuran dan perbelanjaan, yuran guaman (mengikut asas peguam dan Pemohon) dan kos yang ditanggung atau dibiayai oleh MBSB berkenaan atau berkaitan dengan peruntukan dan
penyediaan dokumen bagi Kemudahan ini, dan/atau mana-mana Sekuriti dan/atau penguatkuasaan MBSB bagi haknya dibawah Kemudahan, dan/atau Sekuriti hendaklah dibayar oleh Pemohon,
dan atas pertimbangan MBSB, boleh didebitkan ke akaun Pemohon yang dinyatakan atau mana-mana akaun Pemohon dengan MBSB.<br><br>
Sebagai tambahan kepada peruntukan di sini, pihak Pemohon bersetuju untuk menanggung rugi MBSB dan memastikan MBSB tidak menanggung sebarang pembayaran, kerugian, kos, caj atau
perbelanjaan, secara sah atau sebaliknya, yang pihak MBSB mungkin alami atau menanggung akibat memberi atau melanjutkan Kemudahan kepada Pemohon atau akibat daripada keingkaran
Pemohon. MBSB berhak untuk mengenakan caj dan pihak Pemohon mengaku janji untuk menanggung segala bayaran dan caj yang dikenakan oleh MBSB yang berkaitan dengan Kemudahan
atau mana-mana Sekuriti dan penyata tebus balik (redemption statement(s)) yang dikeluarkan atau yang akan dikeluarkan.

                         
                                
                    </td>
                </tr>



                   <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>12. KEJADIAN INGKAR</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
               Jumlah keseluruhan bagi Harga Jualan Komoditi MBSB dan apa-apa wang yang perlu dibayar dan tertunggak di bawah Kemudahan ini termasuklah ganti rugi (Ta`widh) jika ada akan serta-merta
perlu dibayar oleh Pemohon apabila berlakunya mana-mana Kejadian Ingkar seperti berikut:-<br>
                <ol type="i">
                  <li>Pemohon gagal atau ingkar dalam pembayaran Harga Jualan Komoditi MBSB dan apa-apa wang yang perlu dibayar dibawah peruntukan Terma-Terma ini atau Kemudahan ini, sama ada
dituntut secara rasmi atau tidak;</li>
                  <li>Pemohon dan/atau mana-mana pihak yang menyediakan Sekuriti ingkar atau mengancam untuk ingkar atau mengabaikan mana-mana janji, stipulasi, terma atau syarat yang terkandung
di dalam Terma-Terma ini, di dalam Borang ini dan/atau mana-mana dokumen-dokumen Sekuriti;</li>
                  <li>Apa-apa pernyataan, representasi atau jaminan yang dibuat oleh Pemohon di sini dibuktikan sebagai tidak benar atau adalah salah pada masa pernyataan, representasi atau jaminan
tersebut dibuat atau dianggap seolah-olah dibuat;</li>
                  <li>Terma-Terma ini dan/atau dokumen-dokumen Sekuriti adalah tidak atau akan menjadi tidak sah atau tidak boleh dikuatkuasakan;</li>
                  <li>Pemohon tidak membayar mana-mana keberhutangan yang terhutang kepada mana-mana pihak (termasuk keberhutangan yang terhutang kepada MBSB) atau keberhutangan (selain
daripada keberhutangan Pemohon di dalam Borang ini) Pemohon atau mana-mana pihak yang menyediakan Sekuriti kepada MBSB atau mana-mana bank atau institusi kewangan yang
lain di atas sebab Kejadian Ingkar oleh Pemohon atau mana-mana pihak yang menyediakan Sekuriti menjadi tertunggak atau mampu diisytiharkan tertunggak sebelum tarikh
matangnya;</li>
                  <li>Pemohon atau mana-mana pihak yang menyediakan Sekuriti telah membekalkan maklumat atau data yang palsu kepada MBSB; </li>
                  <li>Di dalam pendapat mutlak MBSB, akaun Pemohon dengan mana-mana bank, tidak diuruskan secara memuaskan;</li>
                  <li>Kejadian atau peristiwa atau situasi telah berlaku di mana akan atau boleh di dalam pendapat mutlak MBSB mempengaruhi kemampuan Pemohon atau mana-mana pihak yang menyediakan Sekuriti untuk melaksanakan obligasi-obligasinya dalam Kemudahan ini dan dalam jaminan atau mana-mana Sekuriti; </li>
                  <li>Pemohon atau mana-mana pihak yang menyediakan Sekuriti menjadi bankrap, melakukan perbuatan-perbuatan kebankrapan, tidak waras atau meninggal dunia;</li>
                  <li>Satu distres atau perlaksanaan (execution) dikenakan terhadap mana-mana hartanah-hartanah Pemohon atau pemegang amanah, atau pegawai rasmi yang telah dilantik terhadap
keseluruhan atau mana-mana bahagian daripada aset-asetnya;</li>
                  <li>Dimana ia boleh digunakan, Pemohon berhenti atau mengugut untuk berhenti daripada menjalankan perniagaannya atau memindahkan atau menjual atau berniat untuk memindahkan
atau menjual sebahagian besar daripada aset-asetnya;</li>
                  <li>Pemohon mendakwa bahawa seluruh atau sebahagian daripada Terma-Terma ini tidak mempunyai kuasa atau kesan sepenuhnya;</li>
                  <li>mana-mana akaun semasa Pemohon telah ditutup oleh mana-mana bank atas perintah dan /atau arahan agensi yang mempunyai kuasa atas cek-cek tendang (dishonoured cheques)
dan/atau polisi-polisi bank tersebut dalam mengekalkan akaun-akaun semasa tersebut, tanpa mengambil kira akaun (akaun-akaun) semasanya dengan MBSB, sama ada dimiliki secara
perseorangan ataupun milikan bersama dengan orang lain, telah dikendalikan dengan sebaiknya;</li>
                  <li>Pemohon bukan lagi kakitangan kerajaan atau mana-mana agensi kerajaan atas apa jua sebab sekali pun; </li>
                  <li>Penerima atau pemegang amanah dilantik bagi mengambil alih pemilikan harta benda Pemohon atau apa-apa pelaksanaan yang akan dan/atau sedang atau telah diambil terhadap harta
benda Pemohon;</li>
                  <li>Terjadi kemungkiran pada mana-mana perjanjian yang melibatkan pinjaman wang atau pendahuluan kredit kepada Pemohon yang memberikan hak kepada kreditor berkenaan
(termasuk MBSB) untuk menarik balik Kemudahan atau mempercepatkan bayaran atau menarik balik pinjaman wang atau pendahuluan kredit tersebut; </li>
                  <li>Perubahan di dalam mana-mana undang-undang atau peraturan yang menyebabkan ianya menjadi satu kesalahan kepada MBSB untuk terus menyediakan Kemudahan ini kepada
Pemohon.</li>
                 

                </ol>
                        Sekiranya berlaku mana-mana Kejadian Ingkar yang dinyatakan di atas, Kemudahan atau mana-mana bahagiannya akan ditangguhkan, ditarik balik atau ditamatkan dan/atau Harga Jualan
Komoditi MBSB dan semua wang yang perlu dibayar kepada MBSB di bawah Kemudahan dan Borang ini akan menjadi perlu dibayar. Selanjutnya, MBSB boleh, atas budi bicaranya
menguatkuasakan mana-mana Sekuriti yang telah disediakan untuk membayar semua jumlah yang tertunggak dan tidak dibayar di bawah Kemudahan. 
                                
                    </td>
                </tr>

     <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>13. NOTIS</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
       Sebarang notis atau tuntutan hendaklah dalam bentuk bertulis dan perlu dihantar kepada MBSB melalui kiriman pos berdaftar kepada alamat berdaftar MBSB yang terkini dan perlu dihantar
kepada Pemohon melalui kiriman pos biasa kepada alamat Pemohon seperti yang dinyatakan di dalam Borang ini. Penyerahan untuk sebarang proses undang-undang ke atas Pemohon boleh
dihantar melalui pos berdaftar kepada alamat pihak Pemohon seperti yang dinyatakan di dalam Borang ini dan penghantaran notis, tuntutan dan proses undang-undang tersebut akan dianggap
berkesan dan sempurna dihantar setelah dihantar dua (2) hari selepas tarikh ia dihantar tanpa mengambil kira notis atau tuntutan tersebut dikembalikan semula oleh pihak pos jika tidak
dihantar atau Pemohon mungkin meninggal dunia (kecuali dan sehingga pihak MBSB menerima notis bertulis daripada wakil Pemohon berkenaan geran probate atau surat kuasa mentadbir
harta pusaka Pemohon yang meninggal dunia). Tiada perubahan di dalam alamat Pemohon yang dinyatakan di dalam Borang ini dianggap berkuatkuasa atau mengikat MBSB kecuali MBSB
menerima notis yang nyata berkenaan perubahan alamat tersebut.

                                
                    </td>
                </tr>


             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>14. KEUTAMAAN PEMBAHAGIAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
MBSB berhak mengutamakan pembayaran Ansuran berbanding pembayaran amaun-amaun lain yang tertunggak daripada mana-mana pembayaran yang diterima oleh MBSB. MBSB atas budi
bicaranya berhak menilai semula keutamaan (priority) ini sebagaimana yang difikirkan patut. Jika mana-mana amaun yang telah diterima dalam bentuk bayaran atau dimiliki semula melalui
penguatkuasaan adalah kurang daripada amaun yang tertunggak; MBSB akan menggunakan amaun tersebut dalam kadar (proportion) dan turutan keutamaan (priority) dan secara
keseluruhannya mengikut cara yang akan ditentukan oleh MBSB. 

                                
                    </td>
                </tr>


             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>15. PERAKUAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Mana-mana sijil atau perakuan (certificate), notis atau tuntutan yang telah ditandatangani bagi pihak MBSB oleh mana-mana pegawai MBSB atau mana-mana peguamcara atau firma
peguamcara yang mewakili MBSB akan dianggap sebagai bukti kukuh terhadap Pemohon atas jumlah yang terhutang daripada Pemohon kepada MBSB dan semua perkara yang dinyatakan di
dalamnya bagi apa-apa tujuan sekalipun termasuk bagi tujuan prosiding undang-undang dan tidak akan dipersoalkan atas apa-apa sebab sekalipun.

                                
                    </td>
                </tr>

                             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>16. PENYERAHAN HAK (ASSIGNMENT)</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
MBSB mempunyai hak untuk menyerah hak dan liabilitinya di bawah Kemudahan ini. Liabiliti-liabiliti dan obligasi-obliagasi di bawah Terma-Terma di sini, atau mana-mana dokumen-dokumen
Sekuriti akan dianggap sah dan mengikat untuk semua tujuan tanpa mengambil kira mana-mana perubahan melalui pencantuman, pengubahan semula atau apa-apa sekalipun yang akan dibuat
di dalam memorandum dan tata urusan syarikat MBSB. Masa yang diperuntukkan oleh undang-undang untuk menuntut kembali semua jumlah tertunggak kepada MBSB tidak akan diambil kira
sebagai bertentangan dengan MBSB sehingga tuntutan bertulis bagi bayaran jumlah yang tertunggak dikeluarkan terhadap Pemohon.

                                
                    </td>
                </tr>


                             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>17. KEBERASINGAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Mana-mana peruntukan di sini yang bertentangan di sisi undang-undang, tidak sah, dilarang atau tidak boleh dikuatkuasakan di dalam mana-mana bidang kuasa akan terhadap bidang kuasa
tersebut menjadi tidak efektif sehingga sejauh mana ia adalah bertentangan di sisi undang-undang, tidak sah, dilarang atau tidak boleh dikuatkuasakan tanpa memudaratkan peruntukan yang
lainnya.<br><br>
Tiada kegagalan, pengabaian atau kelewatan oleh MBSB dalam melaksanakan apa-apa hak, kuasa, keistimewaan atau remedi yang terakru kepada MBSB dalam Terma-Terma ini menjejaskan
apa-apa hak, kuasa, keistimewaan atau remedi atau dianggap sebagai pengecualian atau persetujuan terhadap keingkaran tersebut, dan tiada tindakan oleh MBSB berkaitan apa-apa
keingkaran atau persetujuan terhadap apa-apa keingkaran tersebut akan menjejaskan mana-mana hak, kuasa, keistimewaan atau remedi MBSB berkaitan mana-mana keingkaran yang lain atau
yang seterusnya. Hak-hak dan remedi yang diperuntukan di sini adalah terkumpul dan termasuk kuasa atau remedi yang diperuntukkan di bawah undang-undang. Terma-Terma ini
menggunapakai undang-undang Malaysia

                                
                    </td>
                </tr>
             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>18. PINDAAN PADA TERMA-TERMA</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Adalah dengan ini dipersetujui oleh semua pihak bahawa walau apapun peruntukan dan terma yang terkandung, ia adalah tertakluk kepada perubahan oleh pihak MBSB dengan memberi notis
bertulis kepada Pemohon atau melalui apa-apa cara yang difikirkan sesuai oleh MBSB sekurang-kurangnya dua puluh satu (21) hari sebelum sebarang perubahan pada peruntukan menjadi
efektif dan merupakan sebahagian daripada Terma-Terma pada masa pelaksanaan.

                                
                    </td>
                </tr>

               <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>19. TAKRIF</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Kepala surat, logo, tajuk-tajuk yang digunakan di sini hanyalah bertujuan untuk memudahkan sahaja dan tidak digunapakai dalam penterjemahan maksud Terma-Terma ini

                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>20. MASA</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Masa apabila dinyatakan, adalah intipati dalam semua transaksi bagi Kemudahan tersebut.

                                
                    </td>
                </tr>

                  <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>21. PEMBAYARAN PENUH TANPA PENOLAKAN OLEH PEMOHON</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Semua bayaran yang dilakukan oleh Pemohon adalah dengan pembayaran penuh tanpa sebarang penolakan atau tuntutan balas.

                                
                    </td>
                </tr>

                         <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>22. PENGIKAT WARIS</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Terma-Terma ini adalah mengikat Pemohon, warisnya, wakil diri dan pengganti (jika perlu) dan hendaklah dikuatkuasakan oleh dan untuk kepentingan MBSB, waris dan penggantinya.

                                
                    </td>
                </tr>

                   <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>23. KESAN KEINGKARAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Tanpa mengambil kira Terma-Terma ini, sekiranya terdapat sebarang keingkaran terhadap mana-mana akaun dan/atau kemudahan bersama MBSB atau mana-mana pihak yang pada pendapat
mutlak MBSB mampu menjejaskan kemampuan Pemohon untuk memenuhi tanggungjawab Pemohon terhadap Kemudahan ini dan juga Terma-Terma di sini, MBSB berhak untuk menuntut
pembayaran penuh terhadap mana-mana jumlah wang yang terhutang yang belum dibayar oleh Pemohon kepada MBSB dan/atau melaksanakan apa-apa langkah yang terkandung di sini dengan
sewajarnya.

                                
                    </td>
                </tr>

                  <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>24. PENEPIAN SYARAT-SYARAT TERDAHULU</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Dengan ini diakui dan dimaklumkan bahawa apa-apa syarat terdahulu di dalam Terma-Terma ini adalah semata-mata untuk faedah MBSB yang mana boleh diketepikan sama ada sepenuhnya
atau sebahagian oleh MBSB mengikut budi bicara mutlaknya tanpa menjejaskan haknya dan penepian tersebut tidak menjejaskan hak MBSB untuk memastikan bahawa Pemohon mematuhi
mana-mana syarat terdahulu yang diketepikan pada masa akan datang.

                                
                    </td>
                </tr>


                <tr>
                    <td class="border" align="justify">
 Nama/ Name :  <?php echo e($pra->fullname); ?><br>
            MyKad No. / No. MyKad :  <?php echo e($pra->icnumber); ?><br>
            Date/ Tarikh :  <?php echo date('d M Y'); ?><br></div>

            </b>

                    </td>
                </tr>

            </table>
           
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">
              Cancel
            </button>
            <button type="button" class="btn btn-primary" id="i-agree">
              <i class="fa fa-check"></i> I Agree
            </button>
            
            <a target="_blank" href="<?php echo e(url('/')); ?>/terma/<?php echo e($pra->id); ?>" class="btn btn-danger pull-left" id="print">
              <i class="fa fa-print"></i> Print
            </a>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

		$(document).ready(function() {
				
				
				$("#smart-form-register").validate({

					// Rules for form validation
					rules : {
					    FullName: {
							required : true
						},
						ICNumber : {
							required : true
						},
						
						PhoneNumber: {
						    required: true
						},
						Deduction: {
						    required: true
						},
						
						Allowance: {
						    required: true
						},
						Package: {
						    required: true
						},
						Employment: {
						    required: true
						},
						Employer: {
						    required: true
						},
						
						
						BasicSalary: {
						    required: true,
                            min : 2999
						},
						LoanAmount: {
						    required: true
						}
					},

					// Messages for form validation
					messages : {

					    FullName: {
							required : 'Please enter your full name'
						},
						
						ICNumber: {
						    required: 'Please enter your ic number'
						},
						PhoneNumber: {
						    required: 'Please enter your phone number'
						},
						Allowance: {
						    required: 'Please enter yor allowance'
						},
						Deduction: {
						    required: 'Please enter your total deduction'
						},
						Package: {
						    required: 'Please select package'
						},
						Employment: {
						    required: 'Please select employement type'
						},

						Employer: {
						    required: 'Please select employer'
						},


						BasicSalary: {
						    required: 'Please enter your basic salary',
                            min: 'minimum salary is 3000 my'
						},
						LoanAmount: {
						    required: 'Please enter your loan amount'
						}
					}
				});

			});
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();

         $("#i-agree").click(function(){
          $this=$("#terms");
          if($this.checked) {
            $('#termModal').modal('toggle');
          } else {
            $this.prop('checked', true);
            $('#termModal').modal('toggle');
          }
        });

        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                password : {
                    required : true,
                    minlength : 8,
                    maxlength : 36
                },
                password_confirmation : {
                    required : true,
                   
                    maxlength : 36,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    Email2 : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                password : {
                    required : 'Please enter your password'
                },
                password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>



<?php 
	//include required scripts
	include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
		


<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>


<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<?php for ($x = 1; $x <= 15; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'form/upload/<?php echo e($x); ?>';
    $('#fileupload<?php echo e($x); ?>').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx<?php echo e($x); ?>').val();
            $("#document<?php echo e($x); ?>").html("<a target='_blank' href='"+"<?php echo e(url('/')); ?>/admin/downloaddocpdf/<?php echo e(str_replace('/', '', $pra->id)); ?>/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document<?php echo e($x); ?>a").hide();
             $("#a<?php echo e($x); ?>").val(data.file);
            $("#a<?php echo e($x); ?>").val(data.file);
			$("#a<?php echo e($x); ?>").val(data.file);

             document.getElementById("fileupload<?php echo e($x); ?>").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>
<script>
$(document).ready(function(){
    $("#submit_upload").click(function(){


      var status_penyelesaian = $('#status_penyelesaian').val();
    
      var terms = $('input[name="terms"]:checked').val();
	   var status_penyelesaian = $('#status_penyelesaian').val();
     var set_institution1 = $('#set_institution1').val();
     var set_installment1 = $('#set_installment1').val();
     var set_amount1 = $('#set_amount1').val();
     
   
	 if ($("#a7").val()=='') {
			bootbox.alert('Sila Muat Naik Salinan Penyata Gaji Terkini');
		}
 
    
    else if (status_penyelesaian>0) {
        if((set_institution1!='') &&  (set_installment1!='0') && (set_amount1!='0')) {

        //lewat bae karna berisi 1 baris settlementnya
            if(terms > 0) {
                $('#myModal').modal('show');
            }
       

            else{
              bootbox.alert('You must agree to the Terms and Conditions');  
            }

        }
        else {
          bootbox.alert("jika penyelesaian 'yes' sila isi penyelesaian anda");
        }
      
    }
    else if(terms > 0   ) {
        $('#myModal').modal('show');
    }
   

		else{
			bootbox.alert('You must agree to the Terms and Conditions');  
		}
    });
});


</script>




<script type="text/javascript">
            function show(str){
            
        document.getElementById('add_redemption_button').style.display = 'none';
        document.getElementById('status_penyelesaian').value = '0';
        
            }
            function show2(sign){
               
        document.getElementById('add_redemption_button').style.display = 'block';
        document.getElementById('status_penyelesaian').value = '1';
             
            }


    
</script>

<script>
$('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });
</script>

<script>
$(document).ready(function() {
  
  $('.waktu').datepicker({
      dateFormat : 'dd/mm/yy',
      prevText : '<i class="fa fa-chevron-left"></i>',
      nextText : '<i class="fa fa-chevron-right"></i>',
      onSelect : function(selectedDate) {
        $('#finishdate').datepicker('option', 'minDate', selectedDate);
      }
    });
    
  
   
});

</script>

<script type="text/javascript">
$('input[name="debt_csadasdonsolidation"]').change(function() {
  var debt_consolidation = $('input[name="debt_csfdonsolidation"]:checked').val();
  if (debt_consolidation==1) {
    var status_penyelesaian = 1;
  }
  else {
     var status_penyelesaian = 0;
  }
  $('#status_penyelssesaian').val(status_penysselesaian); 
});
</script>

<script type="text/javascript">
$(".settlement").keyup(function() {
  <?php for($n=1;$n<=3;$n++): ?>
  var set_institution<?php echo e($n); ?> =  $('#set_institution<?php echo e($n); ?>').val();
  var set_installment<?php echo e($n); ?> =  $('#set_installment<?php echo e($n); ?>').val();
  var set_amount<?php echo e($n); ?> =  $('#set_amount<?php echo e($n); ?>').val();
  <?php if($n==1): ?>
  var debt_consolidation = $('input[name="debt_consolidation"]:checked').val();
  $('#debt').val(debt_consolidation); 

  <?php endif; ?>

  $('#institusi<?php echo e($n); ?>').val(set_institution<?php echo e($n); ?>); 
  $('#ansuran<?php echo e($n); ?>').val(set_installment<?php echo e($n); ?>); 
  $('#jumlah<?php echo e($n); ?>').val(set_amount<?php echo e($n); ?>); 

  <?php endfor; ?>
});
</script>


<script type="text/javascript">
      var uploadField = document.getElementById("changePicture1");
      
    uploadField.onchange = function() {


            if(this.files[0].size > 1200000){
             alert("Please upload file below 1MB");
             this.value = "";
          }
          else{
            var fileExtension = ['jpeg','jpg','png'];
          if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
          alert("Only '.pdf' formats are allowed.");
          }
          else{

              var form = document.getElementById('uploadfrontic');
                  form.submit();

          }
          }       
    };
    </script>


  <script type="text/javascript">
      var uploadField2 = document.getElementById("changePicture2");

    uploadField2.onchange = function() {
        if(this.files[0].size > 1200000){
           alert("Please upload file below 1MB");
           this.value = "";
        }
        else{
          var fileExtension = ['jpeg','jpg','png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only '.jpg' formats are allowed.");
        }
        else{

            var form = document.getElementById('uploadbackic');
                form.submit();

        }
        }
    };
    </script>

    <script type="text/javascript">
      var uploadField2 = document.getElementById("changePicture3");

    uploadField2.onchange = function() {
        if(this.files[0].size > 1200000){
           alert("Please upload file below 1MB");
           this.value = "";
        }
        else{
          var fileExtension = ['jpeg','jpg','png'];
        if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
        alert("Only '.jpg' formats are allowed.");
        }
        else{

            var form = document.getElementById('uploadselfie');
                form.submit();

        }
        }
    };
    </script>
