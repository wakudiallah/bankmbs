<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");





?>

<html lang="en-us" <?php echo implode(' ', array_map(function($prop, $value) {
            return $prop.'="'.$value.'"';
        }, array_keys($page_html_prop), $page_html_prop)) ;?>>
    <head>
        <meta charset="utf-8">
        <!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

        <title> <?php echo $page_title != "" ? $page_title." - " : ""; ?>MBSB</title>
    
        <meta charset="UTF-8">


        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Basic Styles -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/font-awesome.min.css">

        <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-production-plugins.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-production.min.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-skins.min.css">
        <link rel="stylesheet" href="<?php echo ASSETS_URL; ?>/css/jquery.fileupload.css">
        
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/normalize.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/main.css">
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/jquery.steps.css">
        <!-- SmartAdmin RTL Support is under construction-->
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-rtl.min.css">

        <!-- We recommend you use "your_style.css" to override SmartAdmin
             specific styles this will also ensure you retrain your customization with each SmartAdmin update.
        <link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/your_style.css"> -->

        <?php

            if ($page_css) {
                foreach ($page_css as $css) {
                    echo '<link rel="stylesheet" type="text/css" media="screen" href="'.ASSETS_URL.'/css/'.$css.'">';
                }
            }
        ?>


    

        <!-- FAVICONS -->
        <link rel="shortcut icon" href="<?php echo ASSETS_URL; ?>/img/favicon/favicon.ico" type="image/x-icon">
        <link rel="icon" href="<?php echo ASSETS_URL; ?>/img/favicon/favicon.ico" type="image/x-icon">

        <!-- GOOGLE FONT -->
        <link rel="stylesheet" href="<?php echo ASSETS_URL; ?>/css/css.css">

        <!-- Specifying a Webpage Icon for Web Clip
             Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
        <link rel="apple-touch-icon" href="<?php echo ASSETS_URL; ?>/img/splash/sptouch-icon-iphone.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?php echo ASSETS_URL; ?>/img/splash/touch-icon-ipad.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?php echo ASSETS_URL; ?>/img/splash/touch-icon-iphone-retina.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?php echo ASSETS_URL; ?>/img/splash/touch-icon-ipad-retina.png">

        <!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">

        <!-- Startup image for web apps -->
        <link rel="apple-touch-startup-image" href="<?php echo ASSETS_URL; ?>/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
        <link rel="apple-touch-startup-image" href="<?php echo ASSETS_URL; ?>/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
        <link rel="apple-touch-startup-image" href="<?php echo ASSETS_URL; ?>/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

        <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
        <script>
            if (!window.jQuery) {
                document.write('<script src="<?php echo ASSETS_URL; ?>/js/libs/jquery-2.1.1.min.js"><\/script>');
            }
        </script>

        <script>
            if (!window.jQuery.ui) {
                document.write('<script src="<?php echo ASSETS_URL; ?>/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
            }
        </script>


    </head>
    <body class="smart-style-1 fixed-header">

<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->


 
<div id="main" role="main">


                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                   

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget " id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                <?php 
$geolocation = $location->lat.','.$location->lng;
$request = 
'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=true_or_false&key=AIzaSyCz-U3AbNWcMJ8Ous00VgIR0RsQXypsOlY'; 
$file_contents = file_get_contents($request);
$json_decode = json_decode($file_contents);
if(isset($json_decode->results[0])) {
    $response = array();
    foreach($json_decode->results[0]->address_components as $addressComponet) {
        if(in_array('political', $addressComponet->types)) {
                $response[] = $addressComponet->long_name; 
        }
    }

    if(isset($response[0])){ $first  =  $response[0];  } else { $first  = 'null'; }
    if(isset($response[1])){ $second =  $response[1];  } else { $second = 'null'; } 
    if(isset($response[2])){ $third  =  $response[2];  } else { $third  = 'null'; }
    if(isset($response[3])){ $fourth =  $response[3];  } else { $fourth = 'null'; }
    if(isset($response[4])){ $fifth  =  $response[4];  } else { $fifth  = 'null'; }
    

    if( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth != 'null' ) {
        echo "<br/>Address:: ".$first;
        echo "<br/>City:: ".$second;
        echo "<br/>State:: ".$fourth;
        echo "<br/>Country:: ".$fifth;
    }
    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth != 'null' && $fifth == 'null'  ) {
        echo "<br/>Address:: ".$first;
        echo "<br/>City:: ".$second;
        echo "<br/>State:: ".$third;
        echo "<br/>Country:: ".$fourth;
    }
    else if ( $first != 'null' && $second != 'null' && $third != 'null' && $fourth == 'null' && $fifth == 'null' ) {
        echo "<br/>City:: ".$first;
        echo "<br/>State:: ".$second;
        echo "<br/>Country:: ".$third;
    }
    else if ( $first != 'null' && $second != 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
        echo "<br/>State:: ".$first;
        echo "<br/>Country:: ".$second;

    }
    else if ( $first != 'null' && $second == 'null' && $third == 'null' && $fourth == 'null' && $fifth == 'null'  ) {
        echo "<br/>Country:: ".$first;
    }
  }
?>

     <?php
        $lat= $location->lat; 
        $lng= $location->lng;
        $geolocation = $location->lat.','.$location->lng;

        function getaddress($lat,$lng){

        $geolocation = $lat.','.$lng;

       $url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng='.$geolocation.'&sensor=true_or_false&key=AIzaSyCz-U3AbNWcMJ8Ous00VgIR0RsQXypsOlY';
         $json = @file_get_contents($url);
         $data=json_decode($json);
         $status = $data->status;
         if($status=="OK")
         {
           return $data->results[0]->formatted_address;
         }
         else
         {
           return false;
         }
    }

        $address= getaddress($lat,$lng);
    //echo "Your payment will be &pound;" . round($amount2,0) . " a month, for " . $months . " months";
    if($address)
    {
        echo"<br>";
        echo $address;
    }
    else
    {
        echo "Not found";
    }
?>
     
 
  

                                
                                <header>
                                     &nbsp;<font size='2'><?php echo e($location->location); ?> </font>
                                      
                                </header>

                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                                        
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                                    <style>
     
                                          #map {
                                            height: 60%;
                                          }
                                    </style>

                                                    
                                                         <div id="map"></div>
                                        <script>

                                    function initMap() {
                                      var myLatLng = {lat: <?php echo e($location->lat); ?>, lng: <?php echo e($location->lng); ?> };

                                      var map = new google.maps.Map(document.getElementById('map'), {
                                        zoom: 20,
                                        center: myLatLng
                                      });

                                      var marker = new google.maps.Marker({
                                        position: myLatLng,
                                        map: map,
                                        title: 'Log Location'
                                      });
                                    }

                                        </script>
                                        <!--<script async defer
                                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBY9ugTUfpkYiutrw2a873Louo_1wZ5yqE&callback=initMap"></script>
                                         <script async defer
                                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATzsafy5WHjV4871j-qZmJYfZXXlrxSv0&callback=initMap"></script>-->
                                            <script async defer
                                            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATzsafy5WHjV4871j-qZmJYfZXXlrxSv0&callback=initMap"></script>
          
                                                  
                                                    

                                    </div>
                                
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

<buton onclick="goBack()" class="btn btn-primary"><i class="fa fa-arrow-left"></i> Back</button>
                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->


<script>
function goBack() {
    window.history.back();
}
</script>

          
