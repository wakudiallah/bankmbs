<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Submitted Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	<?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>


						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            
                         

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
									 <?php echo Form::open(['url' => 'report/bystatus','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                                              <fieldset>
													<section>
                                                          <label class="label">Status </label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                <select name='status'>
																	<?php if($status=='1'): ?>
																		<option selected value="1">Approved</option>
																		<option value="2">Rejected</option>
																		<option value="3">Pending Approval</option>
																		<option value="0">In Process</option>
																		<option value="-99">All</option>
																	<?php elseif($status=='2'): ?>
																		<option value="1">Approved</option>
																		<option selected value="2">Rejected</option>
																		<option value="3">Pending Approval</option>
																		<option value="0">In Process</option>
																		<option value="-99">All</option>
																	<?php elseif($status=='3'): ?>
																		<option value="1">Approved</option>
																		<option value="2">Rejected</option>
																		<option selected value="3">Pending Approval</option>
																		<option value="0">In Process</option>
																		<option value="-99">All</option>
																	<?php elseif($status=='0'): ?>
																		<option value="1">Approved</option>
																		<option value="2">Rejected</option>
																		<option value="3">Pending Approval</option>
																		<option selected value="0">In Process</option>
																		<option value="-99">All</option>
																	<?php elseif($status=='-99'): ?>
																		<option value="1">Approved</option>
																		<option value="2">Rejected</option>
																		<option value="3">Pending Approval</option>
																		<option value="0">In Process</option>
																		<option selected value="-99">All</option>
																	<?php endif; ?>
																	
																
																</select>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
													<section >
                                                          <label class="label">Jenis Pekerjaan </label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                <select name='jenis_pekerjaan'>
																	<?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																		<?php if($jenis_pekerjaan<>'-99'): ?>
																			<?php $selected99 = ""; ?>
																			<?php if($jenis_pekerjaan==$employment->id): ?>
																				<?php $selected = "selected"; ?>
																			<?php else: ?> 
																				<?php $selected = ""; ?> 
																			<?php endif; ?>
																		<?php else: ?> 
																			<?php $selected99 = "selected"; ?>
																			<?php $selected = ""; ?> 
																		<?php endif; ?>
																	<option <?php echo e($selected); ?> value="<?php echo e($employment->id); ?>"><?php echo e($employment->name); ?></option>
																	  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																	  <option <?php echo e($selected99); ?> value="-99"> All </option>
																</select>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                    <section >
                                                          <label class="label">From Date </label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text" name="tanggal1" placeholder="From Date "  class="form-control startdate"  required>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                         <section >
                                                          <label class="label">Date To</label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text"  name="tanggal2" placeholder="From Date "  class="form-control startdate"  required>
                                                                <b class="tooltip tooltip-bottom-right"> Date To</b>
                                                            </label>
                                                        </section>
                                                        
                                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                 </fieldset>
                                       
                                            <div class="modal-footer">
                                              
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               &nbsp; Generate &nbsp;
                                                            </button>
                                                           
                                                   <?php echo Form::close(); ?> 
												  <br><br><br>
											</div>
										</div>
									</div>
										</div>
									</article>
									
										
								
						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div align='center'><b><?php echo e($s_string); ?> Application Report
													<?php if($jenis_pekerjaan=='-99'): ?>
														All Employment
													<?php else: ?>
														<?php echo e($empname->name); ?>

													<?php endif; ?>
													from <?php echo e($viewdate1); ?> to <?php echo e($viewdate2); ?></b></div>
                                                         
							<?php echo Form::open(['url' => 'report/bystatus_excel','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

									<button type="submit" name="submit" class="btn btn-primary">
										<input type='hidden' name='tanggal1' value='<?php echo e($date1); ?>'>
										<input type='hidden' name='status' value='<?php echo e($status); ?>'>
										<input type='hidden' name='tanggal2' value='<?php echo e($date2); ?>'>
										<input type='hidden' name='jenis_pekerjaan' value='<?php echo e($jenis_pekerjaan); ?>'>
									   &nbsp; &nbsp; Export Excel &nbsp; &nbsp;
									</button>

									<?php echo Form::close(); ?> 
									<div align='right'>Total Loan Amount Request :
											<?php $sumloan=0; ?>
							                <?php $__currentLoopData = $pra; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $praz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
												<?php if($praz->PraApp->id_employer > 0 ): ?>
													<?php if($jenis_pekerjaan<>'-99'): ?>
														<?php if($praz->PraApp->employer->employment->id  == $jenis_pekerjaan ): ?>	
															<?php $sumloan = $sumloan + $praz->PraApp->loanamount; ?>
															
														<?php endif; ?>
													<?php else: ?>
														<?php $sumloan = $sumloan + $praz->PraApp->loanamount; ?>		
													<?php endif; ?>													
												<?php endif; ?>
												
											<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
											RM <?php echo e($sumloan); ?>

	
	</div>
                         <br>

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
									
										<table id="example" class="table table-striped table-bordered table-hover" width="100%">
											<thead>			                
												<tr>
													<th>No.</th>
                                                    <th>IC Number</th>
                                                    <th>Name</th>
													<th>Phone</th>
													<th>Email</th>
                                                    <th>Employment</th>
													<th>Employer</th>
                                                    <th>Basic Sallary</th>
                                                    <th>Laon Amount</th>
                                               
                                                      

												</tr>
											</thead>
											<tbody>
												<?php $i=1; ?>
                                                <?php $__currentLoopData = $pra; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pra): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

<?php if($pra->PraApp->id_employer > 0 ): ?>
<?php  $empname = $pra->PraApp->employer->employment->name;   ?>
<?php endif; ?>

<?php if($pra->PraApp->id_employer > 0 ): ?>
    <?php if($pra->PraApp->employer->employment->id  != '2' ): ?>
		<?php  $majikan = $pra->PraApp->majikan; ?>
    <?php else: ?>
		<?php $majikan = $pra->PraApp->employer->name; ?>
    <?php endif; ?>
	  
	<?php  $fullname = strtoupper($pra->PraApp->fullname) ; ?>
	<?php if($jenis_pekerjaan<>'-99'): ?>
		<?php if($pra->PraApp->employer->employment->id  == $jenis_pekerjaan ): ?>			  
				<tr>
					<td>
					   <?php echo e($i); ?>                              
					</td>
				  
					<td>
						<?php echo e($pra->PraApp->icnumber); ?>

						  <input type='hidden' id='ic99<?php echo e($i); ?>' name='ic' value='<?php echo e($pra->icnumber); ?>'/>
							<div id='block<?php echo e($i); ?>'></div>
							<script type="text/javascript">

								$(document).ready(function() {
								
								var old_icx = $('#ic99<?php echo e($i); ?>').val();
								
								   
								if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
								{
								$('#block<?php echo e($i); ?>').html("<span class='label label-danger'>IC Blocked </span>");
								
								}
								});
							 </script>
					</td>
					<td><?php echo e($fullname); ?></td>
					<td><?php echo e($pra->PraApp->phone); ?></td>
					<td><?php echo e($pra->PraApp->email); ?></td>
					<td><?php echo e($empname); ?></td>
					<td><?php echo e($majikan); ?></td>
					<td>RM <?php echo e($pra->PraApp->basicsalary); ?></td>
					<td>RM <?php echo e($pra->PraApp->loanAmmount->loanammount); ?></td>
				</tr>
			<?php endif; ?>
		<?php else: ?>  
		<tr>
			<td>
			   <?php echo e($i); ?>                              
			</td>
		  
			<td>
				<?php echo e($pra->PraApp->icnumber); ?>

				  <input type='hidden' id='ic99<?php echo e($i); ?>' name='ic' value='<?php echo e($pra->icnumber); ?>'/>
					<div id='block<?php echo e($i); ?>'></div>
					<script type="text/javascript">

						$(document).ready(function() {
						
						var old_icx = $('#ic99<?php echo e($i); ?>').val();
						
						   
						if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
						{
						$('#block<?php echo e($i); ?>').html("<span class='label label-danger'>IC Blocked </span>");
						
						}
						});
					 </script>
			</td>
			<td><?php echo e($fullname); ?></td>
			<td><?php echo e($pra->PraApp->phone); ?></td>
			<td><?php echo e($pra->PraApp->email); ?></td>
			<td><?php echo e($empname); ?></td>
			<td><?php echo e($majikan); ?></td>
			<td>RM <?php echo e($pra->PraApp->basicsalary); ?></td>
			<td>RM <?php echo e($pra->PraApp->loanAmmount->loanammount); ?></td>
		</tr>
	<?php endif; ?>
	 
<?php endif; ?>

	  
	<?php $i++; ?>
	<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	</tbody>
</table>
                                       
													
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */

	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
				$(document).ready(function() {
					var t = $('#example').DataTable( {
						"columnDefs": [ {
							"searchable": false,
							"orderable": false,
							"targets": 0
						} ],
						"order": [[ 1, 'asc' ]]
					} );
				 
					t.on( 'order.dt search.dt', function () {
						t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
							cell.innerHTML = i+1;
						} );
					} ).draw();
				} );
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
 <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

        </script>




          
