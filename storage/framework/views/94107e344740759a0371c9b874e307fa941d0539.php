<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "Branch Master";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["MO Info"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        <?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>

                        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                   

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body no-padding">
                
                                    <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                                         
                                        <tbody>
                                            <tr>
                                                <td>Full Name</td>
                                                <td><?php echo e($user->name); ?></td>
                                           </tr>
                                           <tr>
                                                <td>Email Address</td>
                                                <td><?php echo e($user->email); ?></td>
                                           </tr>
                                           <tr>
                                            <?php if($user->role=='5'): ?>
                                                <td>Manager</td>
                                                <td><?php echo e($user->Officer->Manager->name); ?></td>
                                                <?php endif; ?>
                                           </tr>
                                           <!--
                                           <tr>
                                                <td>Registered At</td>
                                                <td>
                                                    <?php if(empty($user->location)): ?>
                                                        <i>User disallow Location Permission</i>
                                                    <?php else: ?>
                                                        <?php echo e($user->location); ?>

                                                    <?php endif; ?>
                                                </td>
                                           </tr>-->
                                        </tbody>
                                    </table>
                                                                                       

          
                                                    
                                                    

                                    </div>
                                
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->



<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>



          
