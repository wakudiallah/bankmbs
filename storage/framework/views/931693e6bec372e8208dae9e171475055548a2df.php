<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "FAQ - Pinjaman Peribadi i-Lestari ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>

        <div id="main" role="main">
<br><br><br><br>
			<!-- MAIN CONTENT -->
			<div id="content" class="container">
                     <?php if(Session::has('message')): ?>
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong><?php echo e(Session::get('message')); ?></strong> 
    </div>
            
            <?php endif; ?>             
            <?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
      <!-- NEW WIDGET START -->
						<article class="col-sm-12 col-md-12 col-lg-12">
				
							<!-- Widget ID (each widget will need unique ID)-->
							<div class="jarviswidget jarviswidget-color-blueLight" id="wid-id-10" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-sortable="false">
								<!-- widget options:
								usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
				
								data-widget-colorbutton="false"
								data-widget-editbutton="false"
								data-widget-togglebutton="false"
								data-widget-deletebutton="false"
								data-widget-fullscreenbutton="false"
								data-widget-custombutton="false"
								data-widget-collapsed="true"
								data-widget-sortable="false"
				
								-->
								<!--<header>
									<span class="widget-icon"> <i class="fa fa-list-alt"></i> </span>
									<h2>Frequently Asked Question </h2>
				
								
				
								</header>
				 -->
								<!-- widget div-->
								<!--<div> -->
				
									<!-- widget edit box -->
									<!--<div class="jarviswidget-editbox"> -->
										<!-- This area used as dropdown edit box -->
				
									<!--</div> -->
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<!--<div class="widget-body no-padding">
				
											
										<div class="panel-group smart-accordion-default" id="accordion-2">
										

										<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseOne-1"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Apakah syarat-syarat untuk memohon pembiayaan peribadi?</a></h4>
												</div>
												<div id="collapseOne-1" class="panel-collapse collapse">
													<div class="panel-body">
													1) Warga negara Malaysia dan berumur 18 tahun ke atas;<br>
													2) Pemohon mestilah seorang pekerja kepada mana-mana sektor kerajaan atau sektor swasta

(berkelayakan) yang stabil dengan pendapatan tetap setiap bulan.</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTwo-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Bagaimana untuk membuat semakan kelayakan ?</a></h4>
												</div>
												<div id="collapseTwo-1" class="panel-collapse collapse">
													<div class="panel-body">
													Semakan boleh dibuat secara online. Sila <a target='blank' href='document/Panduan-Kira-Kelayakan-dan-Isi-Borang-Baru.pdf'>klik di sini</a> untuk maklumat lanjut.</div>
												</div>
											</div>
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseThree-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Bagaimana untuk membuat permohonan pembiayaan di Co-opbank Persatuan? </a></h4>
												</div>
												<div id="collapseThree-1" class="panel-collapse collapse">
													<div class="panel-body">
													Permohonan boleh dibuat secara online. Sila <a target='blank' href='document/Panduan-Kira-Kelayakan-dan-Isi-Borang-Baru.pdf'>klik di sini</a> untuk maklumat lanjut.</div>
												</div>
											</div>
											 <div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFour-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Apakah dokumen yang diperlukan?</a></h4>
												</div>
												<div id="collapseFour-1" class="panel-collapse collapse">
													<div class="panel-body">
													<p>Dokumen yang diperlukan adalah seperti berikut:</p>

a) Salinan Kad Pengenalan pemohon (disahkan oleh pihak majikan)<br>

b) Salinan slip gaji 3 bulan yang terkini (disahkan oleh pihak majikan)<br>

c) Surat pengesahan jawatan oleh majikan<br>

d) Penyata bank untuk 3 bulan gaji yang terkini<br>

e) Borang Biro (BPA) – untuk pembiayaan yang telah diluluskan<br>

f) Bil utiliti<br>

g) Lain-lain dokumen sokongan (jika diperlukan)<br></div>
												</div>
											</div>
											  <div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseFive-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Adakah saya layak memohon sekiranya saya disenaraihitam di dalam CTOS?</a></h4>
												</div>
												<div id="collapseFive-1" class="panel-collapse collapse">
													<div class="panel-body">
													Anda layak membuat permohonan dengan syarat anda tidak berstatus bankrap. Pemohon perlu

memberikan sebarang surat pelepasan dari institusi yang berkenaan dan dokumen-dokumen

yang berkaitan untuk memudahkan proses permohonan.</div>
												</div>
											</div>
											 <div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSix-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Siapakah yang boleh dihubungi bagi tujuan pertanyaan tentang pembiayaan?</a></h4>
												</div>
												<div id="collapseSix-1" class="panel-collapse collapse">
													<div class="panel-body">
													Pemohon boleh menghubungi no talian 03-4144 4075 atau emel di loan@ezlestari.com.my

untuk maklumat lanjut mengenai pembiayaan.
													</div>
												</div>
											</div>
											  <div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseSeven-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Berapa lamakah tempoh proses permohonan pembiayaan peribadi-i?</a></h4>
												</div>
												<div id="collapseSeven-1" class="panel-collapse collapse">
													<div class="panel-body">
													  Permohonan akan diproses dalam tempoh 3 hari bekerja selepas penerimaan lengkap borang permohonan
													</div>
												</div>
											</div>
											  <div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseEight-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Adakah penjamin diperlukan?</a></h4>
												</div>
												<div id="collapseEight-1" class="panel-collapse collapse">
													<div class="panel-body">
													Penjamin hanya diperlukan sekiranya pelanggan tidak memenuhi syarat pembiayaan.</div>
												</div>
											</div>
									<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseNine-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Adakah yuran proses akan dikenakan?</a></h4>
												</div>
												<div id="collapseNine-1" class="panel-collapse collapse">
													<div class="panel-body">
													Tiada yuran proses akan dikenakan kepada pemohon sektor kerajaan, badan berkanun dan GLC.

Yuran proses sebanyak 0.5% akan dikenakan untuk kategori majikan swasta.</div>
												</div>
											</div>
									<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapseTen-1" class="collapsed"> <i class="fa fa-fw fa-plus-circle txt-color-green"></i> <i class="fa fa-fw fa-minus-circle txt-color-red"></i>Adakah akan dikenakan bayaran awal / pendahuluan?</a></h4>
												</div>
												<div id="collapseTen-1" class="panel-collapse collapse">
													<div class="panel-body">
													Tiada bayaran awal / pendahuluan untuk pemohon sektor kerajaan, badan berkanun dan GLC

dengan cara bayaran balik melalui BPA.

Untuk lain-lain cara bayaran balik dan kategori majikan, bayaran awal / pendahuluan 1 bulan

akan ditolak dari jumlah pembiayaan dan dikreditksn ke dalam akaun pembiayaan yang

berkenaan.</div>
												</div>
											</div>
										</div>
				
									</div> -->
									<!-- end widget content -->
				
								<!--</div>-->
								<!-- end widget div -->
				
							</div>
							<!-- end widget -->
				
						</article>
				  
				

				 
          



</div>
        </div>
      
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-black inline-block">
                        <span class="txt-color-black">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>
	

		
        

<!-- ==========================CONTENT ENDS HERE ========================== -->
<?php 
	//include required scripts
	include("asset/inc/scripts.php"); 
?>


<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>