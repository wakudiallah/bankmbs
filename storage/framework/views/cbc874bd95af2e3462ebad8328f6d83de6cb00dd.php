<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Submitted Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!------ Include the above in your HEAD tag 

  
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
         <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>---------->
<style type="text/css">
    .card-default {
    color: #333;
    background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
    font-weight: 600;
    border-radius: 6px;
}

</style>
<script type="text/javascript"> $(function () {
        $(".date").datepicker({
            autoclose: true,
            todayHighlight: true
        });
    });</script>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  	<?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	<?php if(Session::has('error')): ?>
	   		<div class="alert adjusted alert-danger fade in">
		        <button class="close" data-dismiss="alert">
		            ×
		        </button>
        		<i class="fa-fw fa-lg fa fa-exclamation"></i>
          		<strong><?php echo e(Session::get('error')); ?></strong> 
        	</div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
		        <button class="close" data-dismiss="alert">
		            ×
		        </button>
         		<i class="fa-fw fa-lg fa fa-exclamation"></i>
          		<strong><?php echo e(Session::get('success')); ?></strong> 
        	</div>
      	<?php endif; ?>
      	
      
     	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
     		
     			<br>
     			<div class="jarviswidget well" id="wid-id-0">
     				<header>
     				 	<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
     				 	<h2>Widget Title </h2>   
     				 </header>
     				<div>
     				 	<div class="jarviswidget-editbox"></div>
     				 	<div class="widget-body no-padding">
     				 		<div class="container">
<div id="accordion">
    <div class="row">
        <div class="col-lg-12">
            <div class="text-center">
                <h3>Customer Detail View</h3>
            </div>
        </div>
    </div>

    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                    <i class="glyphicon glyphicon-search text-gold"></i>
                    <b>New Loan Offer</b>
                </a>
            </h4>
        </div>
        <form method="POST" class="form-horizontal" id="popup-validation" action="<?php echo e(url('/clrt/send_to_mo')); ?>" >
        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
        <input type="hidden" name="acid" value="<?php echo e($id); ?>">
        <input type="hidden" class="form-control" name="employment" value="<?php echo e($detail->CustWrkSec); ?>" readonly=""/>
        <div id="collapse1" class="collapse show">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Cust Name</label>
                            <input type="text" class="form-control" name="fullname" value="<?php echo e($detail->CustName); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-1 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Previous Loan</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->CustInfo->AppvAmt); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-1 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Previous Tenor</label>
                            <input class="form-control" type="text" value="<?php echo e($detail->CustInfo->AppvTenure); ?>" readonly="" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Cust Employer</label>
                            <input type="text" class="form-control" name="employer" value="<?php echo e($detail->CustInfo->CustEmployer); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Previous Int</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->CustInfo->AppvInst); ?>" readonly=""/>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Previous Installment</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->CustInfo->AppvInst); ?>" readonly=""/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">IC No</label>
                            <input type="text" class="form-control" name="ic"  value="<?php echo e($detail->CustInfo->CustNewId); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">DOB</label>
                            <input type="text" class="form-control" name="dob" value="<?php echo e($detail->CustDob); ?>" readonly=""/>
                        </div>
                    </div>

                    <div class="col-md-4 col-lg-4">
                        <div class="form-group">
                            <label class="control-label">Previous DSR</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->SettInfo->DSRUtilise); ?>" readonly=""/>
                        </div>
                    </div>
                </div>
                <?php
                    $tanggal    = substr($detail->CustInfo->CustNewId,4, 2);
                    $bulan      = substr($detail->CustInfo->CustNewId,2, 2);
                    $tahun      = substr($detail->CustInfo->CustNewId,0, 2); 

                    if($tahun > 30) {
                        $tahun2 = "19".$tahun;
                    }
                    else {
                        $tahun2 = "20".$tahun;
                    }

                    $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
                    $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month')); 

                    $oDateNow       = new DateTime();
                    $oDateBirth     = new DateTime($lahir);
                    $oDateIntervall = $oDateNow->diff($oDateBirth);

                    $age =  $oDateIntervall->y;
                ?>
                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Age</label>
                            <input type="text" class="form-control" name="age" value="<?php echo e($age); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">State</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->State); ?>" readonly=""/>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Handphone No</label>
                            <input type="text" class="form-control" name="phone" value="<?php echo e($detail->Phone->AddTel1); ?>" readonly=""/>
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="text" class="form-control" name="email" value="<?php echo e($detail->CustInfo->Custemail); ?>" readonly=""/>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <table id="example" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>                         
                                <tr>
                                    <th>No.</th>
                                    <th>Max Loan</th>
                                    <th>Net Proceed</th>
                                    <th>Installment</th>
                                    <th>Tenor</th>
                                    <th>DSR %</th>
                                    <th>Increment</th>
                                    <th>Sallary</th>
                                    <th>Max Deduction</th>
                                    <th>Interest</th>
                                    <th>Variance Rate</th>
                                    <th>Profit Earn</th>
                                    <th>Payout Percent</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                <?php $__currentLoopData = $loan_offer; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>       
                                <tr>
                                    <td><?php echo e($i); ?></td>
                                    <td><?php echo e($new->NewLoanOffer); ?></td>
                                    <td><?php echo e($new->NetProceed); ?></td>
                                    <td><?php echo e($new->MonthlyInst); ?></td>
                                    <td><?php echo e($new->Tenor); ?></td>
                                    <td><?php echo e($new->NewDSR); ?></td>
                                    <td><?php echo e($new->NewLoanOffer); ?></td>
                                    <td><?php echo e($new->NewSalary); ?></td>
                                    <td><?php echo e($new->NewLoanOffer); ?></td>
                                    <td><?php echo e($new->IntRate); ?></td>
                                    <td><?php echo e($new->VarianceRate); ?></td>
                                    <td><?php echo e($new->ProfitToEarn); ?></td>
                                    <td><?php echo e($new->NewLoanOffer); ?></td>
                                    <td><a href="<?php echo e(url('/clrt/loan-offer-detail/'.$new->CustIDNo)); ?>" class='btn btn-sm btn-success'>Detail</a></td>
                                </tr>
                                <?php $i++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="card card-default">
        <div class="card-header">
            <h4 class="card-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">
                    <i class="glyphicon glyphicon-lock text-gold"></i>
                    <b>Customer Detail</b>
                </a>
            </h4>
        </div>
        <div id="collapse2" class="collapse show">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <!--<p>As a part of the admissions process at the University of University Name School of the Arts, all applicants are asked to submit this recommendation form. We appreciate any comments and evaluations that you would like to offer about this candidate. Feel free to attach comments or descriptions. A prompt reply is requested.</p>-->
                    </div>
                </div>
              

                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Increment</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->SalIncrementPercent); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Profit Earned</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->SettInfo->ProfitEarned); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Due Date</label>
                            <input class="form-control" type="text" value="<?php echo e($detail->SettInfo->DueDate); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Disbursed Date</label>
                            <input class="form-control" type="text" value="<?php echo e($detail->SettInfo->AppvTenure); ?>" readonly="" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">New Salary</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->CustName); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Prev Salary</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->CustInfo->AppvAmt); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Paid Installment</label>
                            <input class="form-control" type="text" value="<?php echo e($detail->SettInfo->PaidInstallment); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Unpaid Installment</label>
                            <input class="form-control" type="text" value="<?php echo e($detail->SettInfo->RemInstallment); ?>" readonly="" />
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Full Settlement</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->SettInfo->FullSettlement); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Bal Outstanding</label>
                            <input type="text" class="form-control" value="<?php echo e($detail->SettInfo->BalOutStanding); ?>" readonly="" />
                        </div>
                    </div>
                    <div class="col-md-3 col-lg-3">
                        <div class="form-group">
                            <label class="control-label">Rebate  <?php echo e($state); ?></label>
                            <input class="form-control" type="text" value="<?php echo e($detail->SettInfo->Rebate); ?>" readonly="" />
                        </div>
                    </div>
                </div>

                 <input class="form-control" type="hidden" value="<?php echo e($state); ?>" name="wilayah" id="wilayah"/>
                 <div class="row">
                    <div class="col-lg-12">
                        <table id="example" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>                         
                                <tr>
                                    <th>No.</th>
                                    <th>Address Type</th>
                                    <th>Address 1</th>
                                    <th>Address 2</th>
                                    <th>Address 3</th>
                                    <th>City</th>
                                    <th>State</th>
                                    <th>Postcode</th>
                                    <th>Telephone 1</th>
                                    <th>Telephone 2</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i=1;?>
                                <?php $__currentLoopData = $address; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $address): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>       
                                <tr>
                                    <td><?php echo e($i); ?></td>
                                    <td><?php echo e($address->AddTypes->AddTyDesc); ?></td>
                                    <td><?php echo e($address->Add1); ?></td>
                                    <td><?php echo e($address->Add2); ?></td>
                                    <td><?php echo e($address->Add3); ?></td>
                                    <td><?php echo e($address->AddCity); ?></td>
                                    <td><?php echo e($address->AddState); ?></td>
                                    <td><?php echo e($address->AddPCode); ?></td>
                                    <td><?php echo e($address->AddTel1); ?></td>
                                    <td><?php echo e($address->AddTel2); ?></td>
                                </tr>
                                <?php $i++; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="control-label"><b style="font-size: 13px!important">Manager</b></label>
                                <select name="state" class="form-control" style="width:350px">
                                         <option>--- Select Manager ---</option>
                                        <?php $__currentLoopData = $manager; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $manager): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                          <?php 
                                          if(!empty($detail->MO->id_manager)==$manager->id) {
                                              $selected = "selected";

                                          }
                                          else {
                                            $selected = "";
                                          } 
                                          ?>
                                          <option <?php echo e($selected); ?> value="<?php echo e($manager->id); ?>"><?php echo e($manager->name); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </select>
                        </div>
                    </div>
                   <div class="col-md-6 col-lg-6">
                        <div class="form-group">
                            <label class="control-label"><b style="font-size: 13px!important">Marketing Officer </b></label>
                             <select name="city" class="form-control" style="width:350px">
                            <?php if(!empty($detail->MO->id_manager)): ?>
                                <option>--- Select MO ---</option>
                                <?php $__currentLoopData = $mo; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $mo): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                  <?php 
                                  if(!empty($detail->mo_id)==$mo->id_mo) {
                                      $selected = "selected";

                                  }
                                  else {
                                    $selected = "";
                                  } 
                                  ?>
                                  <option <?php echo e($selected); ?> value="<?php echo e($mo->id_mo); ?>"><?php echo e($mo->desc_mo); ?></option>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                            <?php else: ?>
                             <select name="city" class="form-control" style="width:350px"></select>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <!--<label class="control-label">Please mark the appropriate box that best describes this candidate:</label>

                        
                        <label class="control-label">Comments (please feel free toattach a letter or other documentation):</label>-->
                        <textarea rows="6" class="form-control"></textarea>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-lg-12">
                        <!--<label class="control-label">I recommend this candidate:</label>

                        <label class="control-label">
                            <input type="checkbox" >
                            With Reservation
                        </label>
                        <label class="control-label">
                            <input type="checkbox" >
                            Failry Strongly
                        </label>

                        <label class="control-label">
                            <input type="checkbox">
                            Strongly
                        </label>
                        <label class="control-label">
                            <input type="checkbox">
                            Enthusiastically
                        </label>-->
                    </div>
                    <hr/>
                </div>
            </div>
        </div>
    </div>
    <br />
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="pull-right">
            <button class="btn btn-success btn-lg"  type="submit" ><i class="fa fa-save"></i>Assign</button>
            <a class="btn btn-warning btn-lg" href="#" id="btnToTop"><i class="fa fa-arrow-up"></i> Top</a>
        </div>
    </div>
</div>
 </form>
</div>
                                       
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>										
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
		pageSetUp();
	
		/* BASIC ;*/
			var responsiveHelper_dt_basic = undefined;
			var responsiveHelper_datatable_fixed_column = undefined;
			var responsiveHelper_datatable_col_reorder = undefined;
			var responsiveHelper_datatable_tabletools = undefined;
			
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	})
</script>
							</div><br><br><br><br>
						</div>
					</div>
				</article>
			</div>
		</div>
<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
	var responsiveHelper_dt_basic = undefined;
	var responsiveHelper_datatable_fixed_column = undefined;
	var responsiveHelper_datatable_col_reorder = undefined;
	var responsiveHelper_datatable_tabletools = undefined;
	
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};
	$('#dt_basic').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_dt_basic) {
				responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_dt_basic.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_dt_basic.respond();
		}
	});
	
	$(document).ready(function() {
		var t = $('#example').DataTable( {
			"columnDefs": [ {
				"searchable": false,
				"orderable": false,
				"targets": 0
			} ],
			"order": [[ 1, 'asc' ]]
		} );
	 
		t.on( 'order.dt search.dt', function () {
			t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				cell.innerHTML = i+1;
			} );
		} ).draw();
	} );		
</script>
<script>
	  $(document).ready(function() {
	    // show the alert
	    window.setTimeout(function() {
	    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
	        $(this).remove(); 
	    });
	}, 2800);
	});
</script>
<script type="text/javascript">
    var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
        _gaq.push(['_trackPageview']);
    
    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();


        $('.startdate').datepicker({
        dateFormat : 'yy-mm-dd',
        prevText : '<i class="fa fa-chevron-left"></i>',
        nextText : '<i class="fa fa-chevron-right"></i>',
        onSelect : function(selectedDate) {
            $('#finishdate').datepicker('option', 'minDate', selectedDate);
        }
    });
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('select[name="state"]').on('change', function() {
          
            var stateID = $(this).val();
             var wilayah    = $('#wilayah').val();
            if(stateID) {
                $.ajax({
                    url:  "<?php  print url('/'); ?>/myform/ajax/"+stateID,

                    type: "GET",
                    dataType: "json",
                    success:function(data) {

                        
                        $('select[name="city"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="city"]').append('<option value="'+ key +'">'+ value +'</option>');
                        });


                    }
                });
            }else{
                $('select[name="city"]').empty();
            }
        });
    });
</script>



          
