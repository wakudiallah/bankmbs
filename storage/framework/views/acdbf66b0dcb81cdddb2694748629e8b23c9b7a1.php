<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Submitted Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
         <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
         <style type="text/css">
             #wrapper {
  font-family: Lato;
  font-size: 1.5rem;
  text-align: center;
  box-sizing: border-box;
  color: #333;
  
  #dialog {
    border: solid 1px #ccc;
    margin: 10px auto;
    padding: 20px 30px;
    display: inline-block;
    box-shadow: 0 0 4px #ccc;
    background-color: #FAF8F8;
    overflow: hidden;
    position: relative;
    max-width: 850px;
    
    h3 {
      margin: 0 0 10px;
      padding: 0;
      line-height: 1.25;
    }
    
    span {
      font-size: 90%;
    }
    
    #form {
      max-width: 240px;
      margin: 25px auto 0;
      
      input {
        margin: 0 5px;
        text-align: center;
        line-height: 80px;
        font-size: 50px;
        border: solid 1px #ccc;
        box-shadow: 0 0 5px #ccc inset;
        outline: none;
        width: 20%;
        transition: all .2s ease-in-out;
        border-radius: 3px;
        
        &:focus {
          border-color: purple;
          box-shadow: 0 0 5px purple inset;
        }
        
        &::selection {
          background: transparent;
        }
      }
      
      button {
        margin:  30px 0 50px;
        width: 100%;
        padding: 6px;
        background-color: #B85FC6;
        border: none;
        text-transform: uppercase;
      }
    }
    
    button {
      &.close {
        border: solid 2px;
        border-radius: 30px;
        line-height: 19px;
        font-size: 120%;
        width: 22px;
        position: absolute;
        right: 5px;
        top: 5px;
      }           
    }
    
    div {
      position: relative;
      z-index: 1;
    }
    
    img {
      position: absolute;
      bottom: -70px;
      right: -63px;
    }
  }
}
         </style>
<style type="text/css">
    .card-default {
    color: #333;
    background: linear-gradient(#fff,#ebebeb) repeat scroll 0 0 transparent;
    font-weight: 600;
    border-radius: 6px;
}
body {
    font-size: 0.9em;
    color: #212121;
    font-family: Arial;
}
.container{
    width: 350px;
    margin: 50px auto;
    box-sizing: border-box;
}

#frm-mobile-verification {
    border: #E0E0E0 1px solid;
    border-radius: 3px;
    padding: 30px;
    text-align: center;
}

.form-heading {
    font-size: 1.5em;
    margin-bottom: 30px;
}

.form-row {
    margin-bottom: 30px;
}

.form-input {
    padding: 10px 20px;
    width: 100%;
    box-sizing: border-box;
    border-radius: 3px;
    border: #E0E0E0 1px solid;
}

.btnSubmit {
    background: #4cb7ff;
    padding: 8px 20px;
    border: #47abef 1px solid;
    border-radius: 3px;
    width: 100%;
    color: #FFF;
}


.error {
    color: #483333;
    padding: 10px;
    background: #ffbcbc;
    border: #efb0b0 1px solid;
    border-radius: 3px;
    margin: 0 auto;
    margin-bottom: 20px;
    width: 350px;
    display:none;
    box-sizing: border-box;
}

.success {
    color: #483333;
    padding: 10px 20px;
    background: #cff9b5;
    border: #bce4a3 1px solid;
    border-radius: 3px;
    margin: 0 auto;
    margin-bottom: 20px;
    width: 350px;
    display:none;
    box-sizing: border-box;
}

.btnVerify {
    background: #4CAF50;
    padding: 8px 20px;
    border: #449e48 1px solid;
    border-radius: 3px;
    width: 100%;
    color: #FFF;
}
</style>
<script type="text/javascript"> $(function () {
        $(".date").datepicker({
            autoclose: true,
            todayHighlight: true
        });
    });</script>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  	<?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	 <?php if(Session::has('message')): ?>
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong><?php echo e(Session::get('message')); ?></strong> 
    </div>
            
            <?php endif; ?>             
            <?php if(count($errors) > 0): ?>
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <li><?php echo e($error); ?></li>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
    </div>
<?php endif; ?>
      	
      
     	<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
 			<br>
 			<div class="jarviswidget well" id="wid-id-0">
 				<header>
 				 	<span class="widget-icon"> <i class="fa fa-comments"></i> </span>
 				 	<h2>Widget Title </h2>   
 				 </header>
 				<div>
 				 	<div class="jarviswidget-editbox"></div>
 				 	<div class="widget-body no-padding">
 				 		<div class="container">
                            <div id="wrapper">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <h5>Please enter the 4-digit verification code we sent via SMS:</h5>
                                <!--<span>(we want to make sure it's you before we contact our movers)</span>-->
                                <div id="form">
                                    <form method="POST" class="form-horizontal" id="popup-validation" action="<?php echo e(url('/verify-otp')); ?>">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                        <input type="text" name="otp1" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                        <input type="text" name="otp2" maxLength="1" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                        <input type="text" maxLength="1" name="otp3" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                        <input type="text" maxLength="1" name="otp4" size="1" min="0" max="9" pattern="[0-9]{1}" />
                                        <input readonly  name="mobile" value="60142449179" type="hidden" required class="form-control fn" id="mobile" >
                                        <input readonly name="acid" value="<?php echo e($id); ?>" type="hidden" required class="form-control fn" id="acid" >
                                    <button class="btn btn-primary btn-embossed"><h5> Verify</h5></button>
                                  </form>
                                </div><br/><br/>
                                <div>
                                 <b style="font-size: : 6px !important">Didn't receive the code?</b><br />
                                  <form method="POST" class="form-horizontal" id="" action="<?php echo e(url('/send-sms-next')); ?>" >
                                            <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                            <input readonly  name="mobile" value="60142449179" type="hidden" required class="form-control fn" id="mobile" >
                                            <input readonly name="acid2" value="<?php echo e($id); ?>" type="hidden" required class="form-control fn" id="acid" >
                                            <button class="btn btn-danger " type="submit"><h5> Send code again</h5></button>
                                            </form>
                                 
                                </div>
                                <div>Time left = <span id="timer"></span></div>
                            </div>
                        </div>
                                    <!--<form id="frm-mobile-verification">
                                    <form method="POST" class="form-horizontal" id="popup-validation" action="<?php echo e(url('/verify-otp')); ?>">
                                        <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>" class="form-control fn">
                                        <div class="form-heading">OTP Verification</div>
                                            <div class="form-row">
                                                <input type="number" id="otp" class="form-input" placeholder="Enter the 6 digit otp" value="" name="otp">
                                                 <input readonly  name="mobile" value="60142449179" type="hidden" required class="form-control fn" id="mobile" >
                                    <input readonly name="acid" value="<?php echo e($id); ?>" type="hidden" required class="form-control fn" id="acid" >
                                            </div>
                                        
                                            <button class="btn btn-success btn-lg"  class="btnSubmit" type="submit" >sms</button>-->
                                            <!--<a id="btn_otp" class="btn btn-lg btn-success">Send OTP</a>
                                    </form>-->
                                    <!--<input type="text" name="otp" placeholer="Enter your otp"/>-->

                                    <!--<input readonly  name="mobile" value="60142449179" type="hidden" required class="form-control fn" id="mobile" >
                                    <input readonly name="acid" value="<?php echo e($id); ?>" type="hidden" required class="form-control fn" id="acid" >
                                    <a id="btn_scoring" class="btn btn-lg btn-success">Scoring</a>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </article>
    </div>
</div>
<!--<script src="<?php  print url('/'); ?>/js/jquery-3.2.1.min.js"></script>-->
                                       
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.15/jquery.mask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>										
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!--<script src="<?php  print url('/'); ?>/js/verification.js"></script>-->

<script type="text/javascript">
	$(document).ready(function() {
		pageSetUp();
	
		/* BASIC ;*/
			var responsiveHelper_dt_basic = undefined;
			var responsiveHelper_datatable_fixed_column = undefined;
			var responsiveHelper_datatable_col_reorder = undefined;
			var responsiveHelper_datatable_tabletools = undefined;
			
			var breakpointDefinition = {
				tablet : 1024,
				phone : 480
			};
	})
</script>
							</div><br><br><br><br>
						</div>
					</div>
				</article>
			</div>
		</div>
<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
	var responsiveHelper_dt_basic = undefined;
	var responsiveHelper_datatable_fixed_column = undefined;
	var responsiveHelper_datatable_col_reorder = undefined;
	var responsiveHelper_datatable_tabletools = undefined;
	
	var breakpointDefinition = {
		tablet : 1024,
		phone : 480
	};
	$('#dt_basic').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
			"t"+
			"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_dt_basic) {
				responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_dt_basic.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_dt_basic.respond();
		}
	});
	
	$(document).ready(function() {
		var t = $('#example').DataTable( {
			"columnDefs": [ {
				"searchable": false,
				"orderable": false,
				"targets": 0
			} ],
			"order": [[ 1, 'asc' ]]
		} );
	 
		t.on( 'order.dt search.dt', function () {
			t.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
				cell.innerHTML = i+1;
			} );
		} ).draw();
	} );		
</script>

<script type="text/javascript">

    $( "#btn_scoring" ).click(function() {
    
    var mobile  = $('#mobile').val();
    var acid    = $('#acid').val();
    var _token = $('#_token').val();
    //var ndi     = $('#rr_ndi').val();

    $.ajax({
        type: "POST",
        url: "<?php echo e(url('/send-sms')); ?>",
        data: { mobile: mobile, acid: acid, _token : _token},
                 cache: false,

                 beforeSend: function () {
                 },

                success: function (data) {

                },

                 error: function () {
                 }

            });              

});

</script>

<script type="text/javascript">

    $( "#btn_otp" ).click(function() {
    
    var mobile  = $('#mobile').val();
    var acid    = $('#acid').val();
     var otp    = $('#otp').val();
    var _token = $('#_token').val();
    //var ndi     = $('#rr_ndi').val();

    $.ajax({
         headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
        type: "POST",
        url: "<?php echo e(url('/verify-otp')); ?>",
        data: { mobile: mobile, acid: acid, _token : _token,otp:otp},
                 cache: false,

                 beforeSend: function () {
                 },

                success: function (data) {
                      alert("Send Success !");
                },

                 error: function () {
                      $(".error").html('Please enter a valid number!')
                 }

            });              

});

</script>
<?php $ac= $id; 
echo $id; ?>
<script type="text/javascript">
    let timerOn = true;

function timer(remaining) {
  var m = Math.floor(remaining / 60);
  var s = remaining % 60;
  var acid =$('#acid').val();
  m = m < 10 ? '0' + m : m;
  s = s < 10 ? '0' + s : s;
  document.getElementById('timer').innerHTML = m + ':' + s;
  remaining -= 1;
  
  if(remaining >= 0 && timerOn) {
    setTimeout(function() {
        timer(remaining);
    }, 1000);
    return;
  }

  if(!timerOn) {
    // Do validate stuff here
    return;
  }
   
  // Do timeout stuff here
  alert('Timeout for otp');
    //window.location = '/clrt/otp-timeout/'.$id;
     //window.location="<?php echo e(url('/')); ?>/clrt/otp-timeout/".$id;
    window.location = "<?php echo e(url('/')); ?>/clrt/otp-timeout/<?php echo e($id); ?>";
      
}

timer(120);
</script>
<script type="text/javascript">$(function() {
  'use strict';

  var body = $('body');

  function goToNextInput(e) {
    var key = e.which,
      t = $(e.target),
      sib = t.next('input');

    if (key != 9 && (key < 48 || key > 57)) {
      e.preventDefault();
      return false;
    }

    if (key === 9) {
      return true;
    }

    if (!sib || !sib.length) {
      sib = body.find('input').eq(0);
    }
    sib.select().focus();
  }

  function onKeyDown(e) {
    var key = e.which;

    if (key === 9 || (key >= 48 && key <= 57)) {
      return true;
    }

    e.preventDefault();
    return false;
  }
  
  function onFocus(e) {
    $(e.target).select();
  }

  body.on('keyup', 'input', goToNextInput);
  body.on('keydown', 'input', onKeyDown);
  body.on('click', 'input', onFocus);

})</script>