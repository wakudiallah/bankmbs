<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Pinjaman Peribadi i-Lestari ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
     
     
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">

	  
	  <section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-9 col-lg-9">
                    <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                       

                        <!-- widget div-->
                       <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>Current Statistics</h2>

                        </header>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                              
						<?php 	
								function ringgit($nilai, $pecahan = 0) 
									{ 
										return number_format($nilai, $pecahan, '.', ','); 
									}
								
								$sumloan= 0;
								$sumapp= 0;						
						
								$sumapproved= 0;
								$sumloan_approved= 0; 
								$sumloan_approved_bank= 0;
								
								$sumrejected= 0; 
								$sumloan_rejected= 0; 

								$sumrejected_admin= 0; 
								$sumloan_rejected_admin= 0; 
								
								$sumprocess= 0;
								$sumloan_process= 0; 
								
								$sumpendingapproval= 0; 
								$sumloan_pendingapproval= 0;

								$sumpending2ndadmin= 0; 
								$sumloan_pending2ndadmin= 0; 

								$sumpendingadmin= 0; 
								$sumloan_pendingadmin= 0; 

								$sumpendingdoc= 0; 
								$sumloan_pendingdoc= 0;

								$sumapproveddoc= 0; 
								$sumloan_approveddoc= 0;

								$sumrejecteddoc= 0; 
								$sumloan_rejecteddoc= 0;

								?>
								
                        <?php $__currentLoopData = $terma; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $termb): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
							<?php $sumloan = $sumloan + $termb->PraApp->loanamount;
									$sumapp = $sumapp + 1; ?>
							<?php if($termb->verification_result_by_bank ==1): ?>
									<?php 	$sumapproved = $sumapproved + 1; 
											$sumloan_approved = $sumloan_approved + $termb->PraApp->loanamount; 
											$sumloan_approved_bank = $sumloan_approved_bank + $termb->aproved_loan; 
									?>
							<?php endif; ?>
							<?php if($termb->verification_result_by_bank ==2): ?>
									<?php 	$sumrejected = $sumrejected + 1; 
											$sumloan_rejected = $sumloan_rejected + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							<?php if($termb->verification_result ==3): ?>
									<?php 	$sumrejected_admin = $sumrejected_admin + 1; 
											$sumloan_rejected_admin = $sumloan_rejected_admin + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>

							<?php if($termb->verification_result_by_bank ==2): ?>
									<?php 	$sumpendingapproval = $sumpendingapproval + 1;
											$sumloan_pendingapproval = $sumloan_pendingapproval + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							 <?php if($termb->verification_result_by_bank ==0 AND $termb->verification_result ==2): ?>
									<?php 	$sumprocess = $sumprocess + 1; 
											$sumloan_process = $sumloan_process + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							<?php if($termb->verification_result ==1): ?>
									<?php 	$sumpending2ndadmin = $sumpending2ndadmin + 1; 
											$sumloan_pending2ndadmin = $sumloan_pending2ndadmin + $termb->PraApp->loanamount; 
									?>
							<?php endif; ?>
							<?php if($termb->verification_result ==0): ?>
								<?php if($termb->status==88): ?>
									<?php 	$sumrejecteddoc = $sumrejecteddoc + 1; 
											$sumloan_rejecteddoc = $sumloan_rejecteddoc + $termb->PraApp->loanamount; 
									?>

								<?php elseif($termb->status==77): ?>
									<?php 	$sumapproveddoc = $sumapproveddoc + 1; 
											$sumloan_approveddoc = $sumloan_approveddoc + $termb->PraApp->loanamount; 
									?>

								<?php elseif($termb->status==99): ?>
									<?php 	$sumpendingdoc = $sumpendingdoc + 1; 
											$sumloan_pendingdoc = $sumloan_pendingdoc + $termb->PraApp->loanamount; 
									?>

								<?php else: ?>
									<?php 	$sumpendingadmin = $sumpendingadmin + 1; 
											$sumloan_pendingadmin = $sumloan_pendingadmin + $termb->PraApp->loanamount; 
									?>
								<?php endif; ?>
									
							<?php endif; ?>
							
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
											<th>Status</th>
											<th>Total Application</th>
											<th>Total Loan Amount Request</th>
											<th>Total Loan Amount Approved</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<tr>
											<td width="50%">Total Approved by MBSB Branch </td>
											<td><?php echo e($sumapproved); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_approved)); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_approved_bank)); ?></td>
										</tr>
										<tr>
											<td>Total Application Rejected by MBSB Branch </td>
											<td><?php echo e($sumrejected); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_rejected)); ?></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Total Application Rejected by Netxpert </td>
											<td><?php echo e($sumrejected_admin); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_rejected_admin)); ?></td>
											<td>&nbsp;</td>
										</tr>

										<tr>
											<td>Total Pending Approval MBSB Branch</td>
											<td><?php echo e($sumpendingapproval); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_pendingapproval)); ?></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Total In Process MBSB Branch</td>
											<td><?php echo e($sumprocess); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_process)); ?></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Total Pending Documents Verification  </td>
											<td><?php echo e($sumpendingdoc); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_pendingdoc)); ?></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Total Documents Approved  </td>
											<td><?php echo e($sumapproveddoc); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_approveddoc)); ?></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Total Documents Rejected  </td>
											<td><?php echo e($sumrejecteddoc); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_rejecteddoc)); ?></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td>Total Pending Form Verification  </td>
											<td><?php echo e($sumpendingadmin); ?></td>
											<td>RM <?php echo e(ringgit($sumloan_pendingadmin)); ?></td>
											<td>&nbsp;</td>
										</tr>
										<tr>
											<td><b>Allss</b></td>
											<td><b><?php echo e($sumapp); ?></b></td>
											<td><b>RM <?php echo e(ringgit($sumloan)); ?></b></td>
											<td><b>RM <?php echo e(ringgit($sumloan_approved_bank)); ?></b></td>
										</tr>                                     
                                    </tbody>
                                </table>

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
						
						
		
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                    
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					  
						"scrollX": true,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>





          
