<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "By Status Report";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["report"]["sub"]["calculated"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    <?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
                            
                           

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                              
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
									 <?php echo Form::open(['url' => 'report/bystatus','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]); ?>

                                              <fieldset>
													<section>
                                                          <label class="label">Status </label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                <select name='status'>
																
																	<option value="1">Approved</option>
																	<option value="2">Rejected</option>
																	<option value="3">Pending Approval</option>
																	<option value="0">In Process</option>
																	<option value="-99">All</option>
																
																</select>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
													<section>
                                                          <label class="label">Jenis Pekerjaan </label>
                                                             <label class="select">
                                                                <i class="icon-append"></i>
                                                                <select name='jenis_pekerjaan'>
																	<option value="-99"> All </option>
																	<?php $__currentLoopData = $employment; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $employment): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
																	<option value="<?php echo e($employment->id); ?>"><?php echo e($employment->name); ?></option>
																	  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
																	 
																</select>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                    <section >
                                                          <label class="label">From Date </label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text" name="tanggal1" placeholder="From Date "  class="form-control startdate"  required>
                                                                <b class="tooltip tooltip-bottom-right">From Date</b>
                                                            </label>
                                                        </section>
                                                         <section >
                                                          <label class="label">Date To</label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-calendar"></i>
                                                                <input type="text"  name="tanggal2" placeholder="From Date "  class="form-control startdate"  required>
                                                                <b class="tooltip tooltip-bottom-right"> Date To</b>
                                                            </label>
                                                        </section>
                                                        
                                                     <input type="hidden" name="_token" value="<?php echo e(csrf_token()); ?>">
                                                 </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                              
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               Generate
                                                            </button>
                                                           
                                                   <?php echo Form::close(); ?> 
																					   
										
													
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->


<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
    
    <script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Name: {
                    required : true,
                    minlength : 1,
                    maxlength : 50
                },
                TelephoneNumber: {
                    required : true,
                    minlength : 11,
                    maxlength : 11
                }
            },

            // Messages for form validation
             messages : {

                    Name: {
                    required : 'Please enter Branch Name',
                    email : 'Please enter a VALID email address'
                }
                    }
        });

    });
</script>
 <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'yy-mm-dd',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

        </script>



          
