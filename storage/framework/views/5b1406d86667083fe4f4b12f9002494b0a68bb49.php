<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Reset Page";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
        <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
        <div id="main" role="main">
<br><br><br><br>
            <!-- MAIN CONTENT -->
            <div id="content" class="container">
               
           
          
                <div class="row">
                    <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3">

                 
             

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
                         <div class="well no-padding">
                


   <?php echo Form::open(['url' => 'password/email','class' => 'smart-form client-form','method' => 'POST' ]); ?>

   
                
                        <header>
                                    <p class="txt-color-white"><b>     Reset Password </b> </p>
                        </header>

                        <fieldset>
                        
                   <?php if($errors->any()): ?>
                    <div class="row">
                        <div class="alert alert-mini alert-danger margin-bottom-30 margin-left-20 margin-right-20"><!-- SUCCESS -->
                            <button type="button" class="close" data-dismiss="alert">
                                <span aria-hidden="true">×</span>
                                <span class="sr-only">Close</span>
                            </button>
                             <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                            <?php echo e($error); ?>

                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           
                            
                        </div>
                    </div>
                    <?php endif; ?> 
                        <!-- ALERT -->
                                            <?php if(session('status')): ?>
                        
                         <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>           <?php echo e(session('status')); ?></strong> 
    </div>

                    <?php endif; ?>
                                  <?php if(Session::has('message')): ?>
    

   
    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong><?php echo e(Session::get('message')); ?></strong> 
    </div>
            
            <?php endif; ?>   

    <?php echo csrf_field(); ?>

                         
                            
                                          
                            <section>
                                <label class="label">Email</label>
                                <label class="input"> <i class="icon-append fa fa-envelope"></i>
                                    <input type="email" name="email" id="email" value="<?php echo e(old('email')); ?>">
                                    <b class="tooltip tooltip-top-right"><i class="fa fa-envelope txt-color-teal"></i> Please enter email </b></label>
                            </section>

                           

                            
                        </fieldset>
                        <footer>
                            <button type="submit" class="btn btn-primary">
                               Send Password Reset Link
                            </button>
                        </footer>
                  <?php echo Form::close(); ?>  

                </div>
                    
                    </div>
                </div>
              
            </div>

        </div>
             
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

     
<!-- ==========================CONTENT ENDS HERE ========================== -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>
<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
        
