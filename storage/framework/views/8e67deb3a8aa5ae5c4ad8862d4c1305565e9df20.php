<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Please Active Your Location!";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<input type="hidden" name="_token" id="_token" value="<?php echo e(csrf_token()); ?>">
<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         //window.location.href = "<?php echo e(url('/')); ?>/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "<?php echo e(url('/')); ?>/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").html("<input type='hidden' name='latitude' id='latitude' value='"+data.latitude+"'>");
              $("#longitude").html("<input type='hidden' name='longitude' id='longitude' value='"+data.longitude+"'>");
               $("#location").html("<input type='hidden' name='location' id='location' value='"+data.location+"'>");
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">

    <div class='col-md-6'>

    </div>
    <section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
     <h3><b><font color='#a90329!important'><u>Please Active Your Location!</u></font></b></h3>
    </article>
    <article class="col-sm-12 col-md-12 col-lg-6">
 <h2 class="title topictitle2 doc-c-headers doc-c-headers_mod_h2" id="ariaid-title2">Desktop & Tablet (Chrome)</h2>
               
               <div class="body conbody" data-help-text="1">
                  
                  <p class="p">A warning appears at the top of the page when a site asks for your location: </p>
                  
                  <figure class="fig fignone">
                     <img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/3Qb8YL9vqwb2mV2aTL7HuBa-4mY.png">
                     
                  </figure>
                  
                  <p class="p">Configure geolocation settings: </p>
                  
                  <ul class="ul doc-c-list doc-c-list_type_ul" id="separate-sites__ul-g-1">
                     
                     <li class="li doc-c-list doc-c-list__li">To let the site determine your location, click <strong class="ph b">Allow</strong>.
                     </li>
                     
                     <li class="li doc-c-list doc-c-list__li">If you don't want to let the site determine your location until you relaunch the browser, click <img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/BR3PXpXVGzwAbKX96ozeTnKZn8o.png">.
                     </li>
                     
                     <li class="li doc-c-list doc-c-list__li">To block requests from this site to determine your location: click <strong class="ph b">Block</strong>.
                     </li>
                     
                  </ul>
                  
               </div>
               
         
               
               <h2 class="title topictitle2 doc-c-headers doc-c-headers_mod_h2" id="ariaid-title3">Reset access settings for this site</h2>
               
               <div class="body conbody" data-help-text="1">
                  
                  <p class="p">Use one of the following methods to reset access settings:</p>
                  
                  <dl class="dl doc-c-list doc-c-list_type_dl">
                     
                     
                     <dt class="dt dlterm doc-c-list doc-c-list__dt">In the address bar</dt>
                     
                     <dd class="dd doc-c-list doc-c-list__dd">
                        
                        <p class="p">Click <img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/mh7pk-0EZEAbgIodwyomSVpTyEs.png"> → <strong class="ph b">Clear these settings for future visits</strong>. 
                        </p>
                        
                        <p class="p">Settings will be reset when you refresh the page</p>
                        
                        <p class="p"><img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/mmam2zNkvGPx8GdilEvXeLueavE.png"></p>
                        
                     </dd>
                     
                     
                     
                     <dt class="dt dlterm doc-c-list doc-c-list__dt">In browser settings</dt>
                     
                     <dd class="dd doc-c-list doc-c-list__dd">
                        
                        <ol class="ol doc-c-list doc-c-list_type_ol">
                           
                           <li class="li doc-c-list doc-c-list__li" id="reset__li-chr-1">In the upper right corner of the browser, click&nbsp;<img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/W-zjphvWjP8PSr3bWbdRYDI4crs.png">&nbsp;→&nbsp;<span class="ph uicontrol doc-c-uicontrol">Settings</span>.
                           </li>
                           
                           <li class="li doc-c-list doc-c-list__li" id="reset__li-chr-2">Click <span class="ph uicontrol doc-c-uicontrol">Show advanced settings</span> (at the bottom of the page).
                           </li>
                           
                           <li class="li doc-c-list doc-c-list__li" id="reset__li-chr-3">Click the <span class="ph uicontrol doc-c-uicontrol">Content settings</span> button in the <span class="ph uicontrol doc-c-uicontrol">Privacy</span> block.
                           </li>
                           
                           <li class="li doc-c-list doc-c-list__li">Click <strong class="ph b">Manage exceptions</strong> in the <strong class="ph b">Location</strong> block.
                           </li>
                           
                           <li class="li doc-c-list doc-c-list__li">Check the line containing the name of the site and click <img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/BR3PXpXVGzwAbKX96ozeTnKZn8o.png">.
                           </li>
                           
                           <li class="li doc-c-list doc-c-list__li" id="reset__li-chr-4">Click <strong class="ph b">Done</strong>.
                           </li>
                           
                        </ol>
                        
                     </dd>
                     
                     
                  </dl>
                  
                  
               </div>
               
       
               
               <h2 class="title topictitle2 doc-c-headers doc-c-headers_mod_h2" id="ariaid-title4">Configure access settings for all sites</h2>
               
               <div class="body conbody" data-help-text="1">
                  
                  <ol class="ol doc-c-list doc-c-list_type_ol">
                     
                     <li class="li doc-c-list doc-c-list__li">In the upper right corner of the browser, click&nbsp;<img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/W-zjphvWjP8PSr3bWbdRYDI4crs.png">&nbsp;→&nbsp;<span class="ph uicontrol doc-c-uicontrol">Settings</span>.
                     </li>
                     
                     <li class="li doc-c-list doc-c-list__li">Click <span class="ph uicontrol doc-c-uicontrol">Show advanced settings</span> (at the bottom of the page).
                     </li>
                     
                     <li class="li doc-c-list doc-c-list__li">Click the <span class="ph uicontrol doc-c-uicontrol">Content settings</span> button in the <span class="ph uicontrol doc-c-uicontrol">Privacy</span> block.
                     </li>
                     
                     <li class="li doc-c-list doc-c-list__li">Flip the switch to the appropriate position in the <strong class="ph b">Location</strong> block:
                        <p class="p"><img class="image doc-c-image" src="//yastatic.net/doccenter/images/support.yandex.com/en/common/freeze/apaX6_LZxZ8yuVJHIVQT6pO7tEc.png"></p>
                     </li>
                     
                     <li class="li doc-c-list doc-c-list__li">Click <strong class="ph b">Done</strong>.
                     </li>
                     
                  </ol>
                  
               </div>
    </article>

    <article class="col-sm-12 col-md-12 col-lg-6">
 <h2 class="title topictitle2 doc-c-headers doc-c-headers_mod_h2" id="ariaid-title2">iPhone (Safari)</h2>

<p><span class="wysiwyg-font-size-medium">If you&#8217;re clocking in from an iPhone, location services must be enabled for Safari in order for us to track your location. To enable location tracking, go to your iPhone Settings &gt; Privacy &gt; Location Services, and make sure to allow &#8220;Safari Websites&#8221; to access your location &#8220;While Using the App&#8221;. More info here:</span></p>
<p><span class="wysiwyg-font-size-medium">https://support.apple.com/en-us/HT201357</span></p>
 <h2 class="title topictitle2 doc-c-headers doc-c-headers_mod_h2" id="ariaid-title2">iPhone (Chrome)</h2>
<p>https://support.google.com/chrome/answer/142065?hl=en</p>
<ol>
<li>Open your device&#8217;s settings app.</li>
<li>From the list, touch <strong>Chrome</strong> .</li>
<li>Touch <strong>Location.</strong></li>
<li>Choose to either have Chrome never access your location, or allow Chrome access to your location only while using the app.</li>
</ol>
 <h2 class="title topictitle2 doc-c-headers doc-c-headers_mod_h2" id="ariaid-title2">Android (Chrome)</h2>
<p>https://support.google.com/chrome/answer/142065?hl=en</p>
<ol>
<li>Open the Google Chrome app.</li>
<li>Touch the Chrome menu <img src="https://lh3.googleusercontent.com/nDJypNJU_xy3P4JqU03dQIznfoCxX6mgbWu2eFZKDBBx8cujr0RM1lupJKnlR5oEZpo=h18" alt="" width="auto" height="18" />.</li>
<li>Touch <strong>Settings</strong> &gt; <strong>Site settings</strong> &gt; <strong>Location</strong>.</li>
<li>Use the switch to either have Chrome ask before accessing your location, or to block all sites from accessing your location.</li>
<li>Touch the specific blocked or allowed sites to manage exceptions.</li>
</ol>
</article>
  
                
                    </div>
                
                    <!-- end row -->
                
        </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
        var responsiveHelper_dt_basic = undefined;
        var responsiveHelper_datatable_fixed_column = undefined;
        var responsiveHelper_datatable_col_reorder = undefined;
        var responsiveHelper_datatable_tabletools = undefined;
        
        var breakpointDefinition = {
          tablet : 1024,
          phone : 480
        };
        $('#dt_basic').dataTable({
          "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
          "autoWidth" : true,
          "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
              responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
          },
          "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
          },
          "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
          }
        });
        
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>




          
