<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Pinjaman Peribadi MBSB ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>

<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
     
     
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	<?php if(Session::has('error')): ?>
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('error')); ?></strong> 
        </div>
        <?php elseif(Session::has('success')): ?>
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong><?php echo e(Session::get('success')); ?></strong> 
        </div>
            
      <?php endif; ?>
	  
	  <section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                    <div class="jarviswidget jarviswidget-color-oceanblue" id="wid-id-0" data-widget-editbutton="false">
                        <!-- widget options:
                        usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

                        data-widget-colorbutton="false"
                        data-widget-editbutton="false"
                        data-widget-togglebutton="false"
                        data-widget-deletebutton="false"
                        data-widget-fullscreenbutton="false"
                        data-widget-custombutton="false"
                        data-widget-collapsed="true"
                        data-widget-sortable="false"

                        -->
                        <?php $j=1; foreach($terma as $termxz) { 

                                                   { ?>      
				

                        <!-- widget div-->
                     <header>
                            <span class="widget-icon"> <i class="fa fa-table"></i> </span>
                            <h2>		<b><?php echo e($termxz->Basic->name); ?> - <?php echo e($termxz->Basic->new_ic); ?> 
													<?php if($termxz->id_branch=='0'): ?>
													- 
                                                    <?php else: ?>                                            
                                                       of  <b><?php echo e($termxz->Branch->branchname); ?></b> Branch
                                                    <?php endif; ?>  </b></h2>

                        </header>

                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->

                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body">
                        
                       <table id="dt_basic" class='table table-striped table-bordered table-hover'>
							<thead>
							  <tr>
								<th>Date</th>
							  <th>User</th>
	                            <th>Activity</th>
                                <th>Location</th>
	                              <th>Remark</th>
								  <th>Reason</th>
							  </tr>
							
							</thead>
							  	<tbody>
                                      <tr>
                                            <td> <?php echo e($termxz->PraApp->created_at); ?></td>
                              
                                                <td><?php echo e($termxz->PraApp->fullname); ?>


                                        </td>

                                        <?php if($termxz->referral_id!='0'): ?>
                                            <td> Registered By Marketing Officer</td>
                                            <td>
                                                <a href="<?php echo e(url('/geolocation/map/'.$termxz->PraApp->id)); ?>">
                                                <?php echo e($termxz->location); ?> <b>(Click to View Location)</b> 
                                                </a>
                                            </td>
                                            <td>MO Email : <?php echo e($termxz->PraApp->email); ?><br> Cust. Email : <?php echo e($termxz->PraApp->acus_email); ?></td>
                                        <?php else: ?>
                                            <td> Customer Registered</td>
                                            <td> &nbsp; </td>
                                            <td>Email : <?php echo e($termxz->PraApp->email); ?> </td>
                                        <?php endif; ?>
                                  
                                            <td> &nbsp; </td>


                                         
                                      </tr>
                              
							  <?php if(!empty($termxz->Log_download->first()->id)): ?>

								<?php $__currentLoopData = $termxz->Log_download; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $logfull): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								

										
											  <tr>
													<td> <?php echo e($logfull->downloaded_at); ?></td>
												<td> <?php echo e($logfull->user->name); ?>

												</td>
	                                              <td> <?php echo e($logfull->activity); ?></td>
                                                  <td> 
                                                    <a href="<?php echo e(url('/geolocation/map/'.$logfull->lat.'/'.$logfull->lng.'/'.$logfull->location)); ?>">
                                                        <?php echo e($logfull->location); ?> 

                                                        <?php if( $logfull->location!=""): ?>

                                                        <b>(Click to View Location)</b>

                                                        <?php endif; ?>
                                                    </a>
                                                    </td>

	                                                <td> <?php echo e($logfull->log_remark); ?></td>
											  	<?php if($logfull->log_reason==""): ?>
													<td> &nbsp; </td>
													<?php else: ?>
													<td> <?php echo e($logfull->log_reason); ?></td>
													<?php endif; ?>
											  </tr>
											
											
									
								 <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
							</tbody>
								
							 
							  <?php endif; ?>
							  
												  
						</table>							  
<?php  } } ?>
                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
						
						
		
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		

                            </div>
                            <!-- end widget content -->

                        </div>
                        <!-- end widget div -->

                    </div>
                      
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->

<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					  
						"scrollX": true,
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>



