<html>
  <head>
      <style>
        .tos {
            /* styles */
    
            -webkit-print-color-adjust:exact;
            font-family: "Arial";
            font-size:12px;
            color: #0055a5;

        }

        td.border {

          color:#0055a5;
            padding: 10px; /* cellpadding */
             line-height: 150%;
             font-size:9px;
             font-weight: bold;
 
        }
        td.header {
          color:#0055a5;
          font-size:11px;
          font-weight: bold;
        }

        td.border1 {
           border-collapse: collapse;
        border: 1px solid black;
        }


        td.assign {
           border-collapse: collapse;
        border: 1px solid black;
          color:#0055a5;
            padding: 10px; /* cellpadding */
             line-height: 150%;
             font-size:9px;
             font-weight: bold;
        }


  
    

 </style>

  </head>
  <body onload="printpage()">



 <div id="left" class="tos">
<table width='95%' border='0'>
  <tr>
    <td width="10%"> <img src="<?php echo e(url('/')); ?>/asset/img/mbsb_small.png"></td>
    <td class="header" width="90%"> <div align="center"><h3><b>TERMA DAN SYARAT UNTUK KEMUDAHAN PEMBIAYAAN PERIBADI-i MBSB<br>
DALAM VERSI BAHASA MALAYSIA SAHAJA</h3></b></div></td>
  </tr>
</table>

           <hr>
<b>

               <table >
                <tr>
                    <td class="border" align="justify">
                          <b>Terma & Syarat (“Terma-Terma”) Untuk Pembiayaan Peribadi-i MBSB Berdasarkan Konsep Syariah Tawarruq (“Kemudahan”)</b><br><br>
                          Tertakluk kepada penerimaan Malaysia Building Society Berhad (No. Syarikat: 9417-K) (“MBSB”) terhadap permohonan dari pemohon yang dinamakan (“Pemohon”) di dalam borang permohonan untuk kemudahan Pembiayaan Peribadi-i MBSB (“Borang ini”), Pemohon dengan ini bersetuju bahawa terma-terma dan syarat-syarat yang tercatat di sini (“Terma-Terma”) adalah terpakai bagi Kemudahan ini dan Pemohon bersetuju untuk terikat dengan Terma-Terma tersebut. 
 
                       <br>


                       
                    
                                
                    </td>
                </tr>
            </table>
             <table >
                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>1. DEFINISI-DEFINISI</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                          “Ansuran” bermaksud ansuran bulanan yang perlu dibayar oleh Pemohon kepada MBSB samada sebelum atau selepas ibra` (rebat) diberikan oleh MBSB bagi pembayaran Harga Jualan Komoditi MBSB secara bayaran tertangguh. “Harga Belian Komoditi MBSB” bermaksud harga belian bagi Komoditi yang dibeli oleh MBSB daripada pembekal Komoditi pada harga belian yang diperincikan di dalam tawaran Kemudahan yang bersamaan dengan jumlah Kemudahan yang ditawarkan kepada Pemohon.<br> <br> 
                          “Harga Jualan Komoditi MBSB” bermaksud harga jualan Komoditi yang dijual oleh MBSB kepada Pemohon pada harga jualan berdasarkan pada kadar keuntungan sebanyak _______________% setahun yang kena dibayar oleh Pemohon seperti yang diperincikan didalam tawaran Kemudahan. “Notis Pengeluaran Penuh dan Notis” bermaksud notis bertulis yang dikeluarkan oleh MBSB kepada Pemohon selepas amaun bersih Kemudahan dibayar kepada Pemohon yang menyatakan butiran Ansuran bagi Harga Jualan Komoditi MBSB dan juga merujuk kepada apa jua notis bertulis yang dikeluarkan oleh MBSB dari masa ke semasa selepas itu termasuk sebarang notis pindaan atau pembetulan. “Komoditi” bermaksud apa-apa komoditi yang halal dan mematuhi prinsip Syariah yang akan ditentukan pada masa transaksi oleh MBSB. “Sekuriti” bermaksud apa jua sekuriti yang dianggap diperlukan oleh MBSB dari masa ke semasa dan yang perlu disediakan oleh Pemohon atau pihak-pihak lain yang bersetuju memberi sekuriti berikutan permintaan Pemohon dan disetujui oleh MBSB bagi kesemua amaun yang perlu dibayar oleh Pemohon kepada MBSB. <br><br>
                        
                          &nbsp; 1A. TUJUAN KEMUDAHAN<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Kemudahan ini diberikan kepada Pemohon untuk kegunaan peribadi yang halal dan mematuhi Syariah.<br>


                       
                    
                                
                    </td>
                </tr>
                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>2. AMAUN YANG KENA DIBAYAR/PEMOTONGAN DARIPADA JUMLAH KEMUDAHAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                          Semua amaun yang dinyatakan di bawah (sekiranya berkenaan) akan ditolak daripada jumlah Kemudahan yang diluluskan oleh MBSB:<br>
                          Deposit Sekuriti (hanya akan diguna pakai pada akhir tempoh pembiayaan);
                          <ol type="i">
                            <li>Sumbangan Takaful GCTT;</li>
                            <li>Fi Wakalah;</li>
                            <li>Penyelesaian Pembiayaan lain;</li>
                            <li>Produk Banca Takaful;</li>
                            <li>Fi Giro Antara Bank (IBG);</li>
                            <li>Lain-lain caj dan amaun yang perlu dibayar oleh Pemohon.</li>
                            </ol>
                            Semua yang dinyatakan di atas adalah tertakluk kepada pakej yang diambil oleh Pemohon<br>
                                
                    </td>
                </tr>

                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>3. TRANSAKSI KEMUDAHAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                          Selaras dengan prosedur pembiayaan MBSB, MBSB akan menawarkan Kemudahan kepada Pemohon dengan menghantar satu khidmat pesanan ringkas ("SMS") kepada Pemohon atau dalam bentuk medium yang lain yang difikirkan sesuai oleh MBSB. Penerimaan Pemohon terhadap tawaran Kemudahan MBSB seperti yang terkandung dalam SMS dengan menjawab YA, atau dalam bentuk medium yang lain yang difikirkan perlu oleh MBSB, tertakluk kepada apa-apa pindaan yang akan dimaklumkan melalui notis oleh MBSB, akan membentuk obligasi dan komitmen Pemohon terhadap Terma-Terma dalam tawaran Kemudahan.<br><br>
                          Jawapan “TIDAK" terhadap SMS bermakna bahawa Pemohon menolak dan tidak mahu meneruskan Kemudahan.<br>Jawapan "TUKAR" terhadap SMS bermakna Pemohon ingin menukar jumlah atau tempoh Kemudahan.
                          <br><br>
                          Sekiranya Pemohon menjawab YA dan selaras dengan prinsip Syariah Tawarruq dan berdasarkan permohonan belian yang telah ditandatangani oleh Pemohon dalam Borang ini, MBSB akan membeli Komoditi pada Harga Belian Komoditi MBSB.<br><br>
                          Setelah MBSB membeli Komoditi tersebut daripada pembekal Komoditi, MBSB akan menjual Komoditi tersebut kepada Pemohon selaras dengan akujanji Pemohon untuk membeli Komoditi
daripada MBSB yang terkandung di dalam Borang ini pada Harga Jualan Komoditi MBSB.<br><br>

‘Aqad jual-beli di antara MBSB dan Pemohon akan dibuat dalam bentuk SMS yang akan dihantar oleh MBSB kepada Pemohon. Pemohon mempunyai pilihan untuk menjawab YA untuk menerima
tunai atau TIDAK untuk mengambil penghantaran<br><br>

Sekiranya Pemohon menjawab YA bermakna Pemohon bersetuju untuk membeli Komoditi dan berdasarkan perlantikan MBSB sebagai wakil Pemohon yang terkandung di dalam Borang ini,
MBSB akan menjual Komoditi tersebut sebagai wakil Pemohon kepada mana-mana pihak ketiga pada harga yang bersamaan dengan jumlah Kemudahan yang ditawarkan kepada Pemohon.<br><br>
Hasil jualan (iaitu jumlah Kemudahan) akan dimasukkan ke dalam akaun bank Pemohon setelah ditolak segala jumlah pemotongan yang dinyatakan dalam tawaran Kemudahan dan Terma-Terma.<br><br>
Sekiranya Pemohon menjawab TIDAK bermakna Pemohon akan mengambil penghantaran Komoditi tersebut dan kos penghantaran akan ditanggung sepenuhnya oleh Pemohon. Pemohon
hendaklah membayar Harga Jualan Komoditi MBSB kepada MBSB secara tangguh mengikut jadual bayaran/ansuran yang dinyatakan dalam tawaran Kemudahan.
                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>4. SYARAT TERDAHULU PENGGUNAAN KEMUDAHAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                    Sebelum MBSB membuat pengeluaran penuh amaun Kemudahan kepada Pemohon dan sebelum Pemohon boleh menggunakan Kemudahan tersebut, syarat-syarat berikut perlulah dipenuhi
terlebih dahulu oleh kesemua pihak yang terlibat:-<br>
    <ol type="i">
      <li>Semua dokumen Sekuriti dan dokumen sokongan berkaitan Kemudahan ini dan yang diperlukan oleh MBSB telah dikemukakan, diterima dan disempurnakan;</li>
      <li>Pemohon telah mematuhi semua terma dan syarat yang telah ditetapkan oleh MBSB seperti yang dinyatakan di dalam Terma-Terma disini;</li>
      <li>Tiada Kejadian Ingkar (Event of Default) seperti yang dinyatakan di perenggan (12) sedang atau telah berlaku;</li>
      <li>Pemohon telah lulus penilaian kredit dalaman (internal credit evaluation) MBSB seperti yang ditetapkan oleh MBSB;</li>
      <li>Pemohon telah membayar segala yuran dan kos yang perlu dibayar dibawah Kemudahan ini; dan</li>
      <li>Apa-apa syarat terdahulu yang MBSB fikirkan perlu dipatuhi oleh Pemohon dari masa ke semasa.</li>
</ol>
                         
                                
                    </td>
                </tr>

                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>5. REPRESENTASI & JAMINAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                 Pemohon memberi representasi dan jaminan kepada MBSB bahawa:<br>
                  <ol type="i">
                    <li>Pemohon mempunyai hak, kuasa dan keupayaan untuk menerima tawaran Kemudahan ini dan untuk melaksanakan tanggungjawabnya berdasarkan Terma-Terma ini.</li>
                     <li>Pemohon yang dinamakan di dalam Borang ini bukan seorang bankrap yang belum dilepaskan, dan tiada apa-apa prosiding kebankrapan, tindakan undang-undang atau apa-apa tindakan
yang lain samada semasa atau belum diputuskan terhadapnya yang akan mempengaruhi keupayaan Pemohon untuk memenuhi obligasinya dibawah Terma-Terma ini.</li>
                      <li>Tiada Kejadian Ingkar (Event of Default) seperti yang dinyatakan di perenggan (12) di bawah telah berlaku atau masih berlaku disamping berjanji bahawa Pemohon akan memaklumkan
kepada MBSB secara bertulis sekiranya Pemohon bukan lagi kakitangan kerajaan atau mana-mana agensi kerajaan sebelum Pemohon berhenti daripada perkhidmatan tersebut.</li>
                       <li>Terma-Terma ini menjadi kewajipan undang-undang yang sah, mengikat dan boleh dikuatkuasakan terhadap Pemohon berdasarkan Terma-Terma yang termaktub;</li>
                       <li>Segala maklumat yang diberikan oleh Pemohon kepada MBSB berkaitan Kemudahan ini adalah sahih dan benar dan tiada sebarang pengabaian oleh Pemohon yang mengakibatkan
maklumat tersebut tidak benar atau mengelirukan.</li>





                </ol>

                Representasi dan Jaminan yang diberikan disini akan berterusan selagi mana Harga Jualan Komoditi MBSB atau tangunggan di bawah Kemudahan ini masih tertunggak dan terhutang oleh
Pemohon. Kebenaran dan ketepatan representasi yang dinyatakan di dalam representasi-representasi dan jaminan-jaminan yang dinyatakan oleh Pemohon disini adalah menjadi asas komitmen
MBSB untuk menyediakan Kemudahan tersebut kepada Pemohon. Jika sebarang representasi-representasi dan/atau jaminan-jaminan yang telah dibuat selepas ini dan pada bila-bila masa
didapati tidak benar dalam mana-mana aspek yang metarial, di dalam keadaan tersebut dan tanpa mengambil kira apa-apa yang bertentangan di sini, MBSB akan mempunyai hak mutlak untuk
mengkaji semula, menggantungkan, menuntut semula atau membatalkan Kemudahan tersebut atau sebahagian daripadanya.<br><br>
Dimana bersesuaian dan apabila dikehendaki oleh MBSB, Pemohon akan melaksanakan atau menyebabkan pelaksanaan Sekuriti lain atau yang selanjutnya sepertimana diperlukan oleh MBSB.<br><br>

&nbsp; &nbsp; 5A. AKUJANJI (COVENANTS)<br>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; Sepanjang tempoh Kemudahan ini, Pemohon berakujanji akan:
    <ol type="i">
      <li>membayar segala Ansuran dan jumlah keberhutangan (indebtedness) tepat pada masanya dan melaksanakan segala tanggungjawabnya;</li>
        <li>mengambil segala langkah yang perlu bagi memastikan bahawa tiada perubahan material ke atas kedudukan kewangannya;</li>
          <li>memberi kepada MBSB segala maklumat dan dokumen yang diperlukan oleh MBSB berkaitan dengan Kemudahan;</li>
            <li>memaklumkan kepada MBSB sekiranya berlaku Kejadian Ingkar di bawah Kemudahan ini atau sebarang Kejadian Ingkar yang berkaitan dengan mana-mana keberhutangan
(indebtedness) Pemohon yang lain;</li>
              <li>memberi kebenaran kepada MBSB untuk melantik mana-mana pihak dan/atau ejen untuk menguruskan segala urusan pembelian dan penjualan Komoditi bagi tujuan Kemudahan
ini selaras dengan prosedur pembiayaan MBSB.</li>
<li>Memastikan bayaran sumbangan bagi produk Banca Takaful yang dilanggan melalui Pakej Gabungan Produk dikekalkan selagi kemudahan Pembiayaan Peribadi-i aktif dengan
MBSB.</li>
    </ol>

                         
                                
                    </td>
                </tr>


                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>6. PEMBAYARAN HARGA JUALAN KOMODITI MBSB</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                         Apabila Kemudahan diluluskan, ia akan dikreditkan ke dalam akaun Pemohon oleh MBSB atau melalui apa-apa cara yang dianggap sesuai oleh MBSB atau budi bicaranya. Pemohon mengesahkan
bahawa penerimaan permohonan Kemudahan oleh MBSB adalah dianggap lengkap sebaik sahaja transaksi-transaksi yang dinyatakan di dalam (3) di atas telah dilaksanakan dan disempurnakan
atau Kemudahan yang diluluskan oleh MBSB telah dikreditkan ke dalam akaun Pemohon meskipun Kemudahan tersebut tidak diguna pakai di dalam akaun Pemohon atau amaun tebus hutang
(jika berkaitan) telahpun dibayar kepada mana-mana pihak ketiga oleh pihak MBSB bagi pihak Pemohon (yang mana Pemohon bersetuju dan berakujanji akan membayar kesemua amaun tebus
hutang tersebut kepada MBSB jika Kemudahan tersebut dibatalkan oleh Pemohon).<br><br>
Apa-apa perbezaan di dalam amaun Kemudahan yang dipohon, dan apabila diluluskan tidak akan memberi kesan kepada perihal sah Terma-Terma ini dan Pemohon akan dianggap bersetuju
untuk mendapatkan Kemudahan dalam amaun yang diluluskan oleh MBSB. Pembayaran Jualan Komoditi MBSB perlulah dibuat oleh Pemohon dalam amaun dan mengikut bilangan ansuran
seperti yang dinyatakan di dalam tawaran Kemudahan dan/atau seperti yang terkandung di dalam Notis Pengeluaran Penuh yang dikeluarkan oleh MBSB.
                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>7. BAYARAN ANSURAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                        Bayaran balik ansuran bulanan bagi Kemudahan ini boleh dibuat melalui potongan gaji, bayaran secara tunai / cek di mana-mana cawangan MBSB, melalui arahan tetap pembayaran atau melalui
bayaran atas talian dengan bank-bank tertentu. Kaedah, tarikh bayaran Ansuran pertama dan Ansuran berikutnya yang perlu dibayar oleh Pemohon adalah seperti yang ditetapkan di dalam
tawaran Kemudahan dan/atau Notis Pengeluaran Penuh.<br><br>Bagi akaun yang terikat dengan deposit sekuriti, deposit sekuriti tersebut tidak akan dipulangkan kepada Pemohon sebaliknya ianya akan digunakan untuk menyelesaikan baki akhir ansuran
bulanan Kemudahan. Dengan demikian, tempoh pembiayaan anda secara tidak langsung mungkin akan berkurangan dan akan tamat sebelum tempoh matang.<br><br>
<u>Bayaran Ansuran melalui Potongan Gaji:</u><br>
Bagi akaun yang melibatkan bayaran melalui potongan ANGKASA dan/atau Majikan dan/atau badan-badan dan organisasi-organisasi yang diluluskan oleh MBSB, sekiranya pihak ANGKASA
dan/atau Majikan dan/atau badan-badan dan organisasi-organisasi yang diluluskan oleh MBSB telah membuat pemotongan gaji dalam tempoh tiga (3) bulan atau tempoh lebih awal dari tarikh
pengeluaran Kemudahan / tarikh Ansuran pertama dan / atau Ansuran berikutnya sebagaimana yang dinyatakan didalam Notis Pengeluaran Penuh, maka tempoh Kemudahan akan dikurangkan
sewajarnya dan tiada pemulangan bayaran Ansuran akan dibuat oleh MBSB.<br><br>
Walau bagaimanapun, bayaran Ansuran oleh Pemohon hanya akan dianggap dibayar oleh Pemohon apabila MBSB menerima bayaran dan bukannya semasa pemotongan gaji dilakukan oleh
pihak Koperasi Biro Angkasa (“ANGKASA”) / Akauntan General (AG) Negara / Negeri dan/atau pihak majikan Pemohon (“Majikan”). Tanpa menjejaskan peruntukan di atas, Pemohon mestilah
membuat bayaran Ansuran secara terus kepada MBSB:-
  <ol type="i">
    <li>Sehingga potongan gaji oleh ANGKASA dan/atau oleh pihak Majikan berkuatkuasa; dan/atau</li>
     <li>Sekiranya potongan gaji tersebut tidak diterima oleh MBSB atas apa-apa alasan sekalipun; dan/atau</li>
      <li>Sekiranya amaun yang diterima melalui potongan gaji kurang daripada bayaran Ansuran atas apa-apa alasan sekalipun</li>
     
  </ol>

Pemohon dengan ini juga membenarkan (kebenaran yang tidak boleh dibatalkan) MBSB untuk mendebitkan mana-mana akaun deposit Pemohon untuk Ansuran bulanan pada atau sebelum
tarikh Ansuran bulanan mesti dibayar dan mana-mana caj lain yang berkaitan dengan Kemudahan. Untuk tujuan debit yang dinyatakan di sini:- 
   <ol type="i">
    <li>Pemohon berakujanji untuk memastikan simpanan yang secukupnya telah disimpan di dalam akaun untuk memenuhi bayaran ansuran. MBSB boleh mengenakan caj levi ke atas semua
arahan tetap (standing instructions) pada tarikh matang, yang mana tidak akan dipulangkan walaupun arahan tersebut tidak digunapakai oleh kerana kekurangan simpanan.</li>
     <li>MBSB tidak akan menanggung apa-apa liabiliti terhadap mana-mana kesilapan, keengganan atau pengabaian untuk membuatkan semua atau mana-mana pembayaran atau berdasarkan
alasan bayar lewat atau pengabaian untuk mematuhi arahan tersebut.</li>
      <li>MBSB akan menentukan pembayaran wang daripada akaun Pemohon berdasarkan arahan ini atau mana-mana perintah diberikan kepada MBSB.</li>
       <li>Sekiranya bayaran tidak boleh dibuat kerana baki akaun simpanan tidak mencukupi, MBSB tidak akan membuat bayaran pada tarikh bayaran berkenaan. Pembayaran selanjutnya hanya
akan dibuat pada tarikh pembayaran yang berikutnya. Pemohon dikehendaki membuat pelan alternatif untuk memberi kesan kepada bayaran yang berkenaan.</li>
<li>Sebagai balasan untuk MBSB menyusunkan arahan ini, Pemohon mengakujanji kepada MBSB untuk menanggung rugi semua atau mana-mana dakwaan, tuntutan, kehilangan, kerugian,
kos, caj dan/atau perbelanjaan yang MBSB mungkin atau akan alami atau tanggung.</li>
  </ol>
                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>8. TA`WIDH (PAMPASAN)</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                   Tanpa mengambil kira apa-apa yang terkandung di sini, Pemohon bersetuju dan berjanji untuk membayar kepada MBSB ta`widh (pampasan) di atas Ansuran yang tertunggak dan bayaran untuk
keberhutangan (Indebtedness) atau Harga Jualan Komoditi MBSB, seperti mana yang bersesuaian, secara berikutnya:-<br>
    <ol type="i">
      <li>Semasa tempoh Kemudahan: Pampasan bayaran lewat (Ta’widh) akan dikenakan pada kadar 1% setahun ke atas jumlah tunggakkan atau sebahagian daripadanya atau mengikut apa-apa
kaedah atau kadar lain yang ditetapkan oleh Jawatankuasa Penasihat Syariah MBSB dari semasa ke semasa; dan</li>
      <li>Selepas tempoh matang Kemudahan: Pampasan bayaran lewat (Ta’widh) akan dikenakan pada kadar yang tidak melebihi kadar semasa Pasaran Wang Antara Bank Islam (IIMM) ke atas
baki tertunggak; dan</li>
      <li>Jumlah bagi Ta`widh (pampasan) ke atas keberhutangan termasuk Harga Jualan Komoditi MBSB, seperti mana yang bersesuaian tidak akan dikompaunkan (compounded).</li>
</ol>
                         
                                
                    </td>
                </tr>

                <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>9. PENYELESAIAN AWAL HARGA JUALAN KOMODITI MBSB</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                   Dalam kes-kes penyelesaian awal untuk keseluruhan atau sebahagian daripada Harga Jualan Komoditi, pelarasan ibra' (rebat) akan dilakukan oleh MBSB berdasarkan peraturan-peraturan yang
ditetapkan oleh Jawatankuasa Penasihat Syariah MBSB. MBSB akan memberikan ibra' (rebat) kepada Pemohon di mana terdapat:<br>
    <ol type="i">
      <li>Penyelesaian awal atau penebusan awal Kemudahan tersebut; atau</li>
      <li>Penyelesaian kontrak pembiayaan asal kerana penyusunan semula pembiayaan; atau</li>
      <li>Penyelesaian oleh Pemohon dalam kes lalai; atau</li>
      <li>Penyelesaian oleh Pemohon sekiranya berlaku penamatan atau pembatalan pembiayaan sebelum tarikh matang; atau</li>
      <li>Sekiranya Kadar Keuntungan Efektif lebih rendah daripada Kadar Keuntungan Siling (yang berkenaan).</li>
</ol>
<u>Bagi Pembiayaan Peribadi dengan Kadar Tetap:-</u><br><br>
Pengiraan ibra' (rebat) =<br>
<img src="<?php echo e(url('/')); ?>/img/formula.jpg"/><br>
<u>Bagi Pembiayaan Peribadi dengan Kadar Berubah:-</u><br>
Pengiraan ibra' (rebat) = Untung Tertangguh - Caj Penyelesaian Awal<br><br>
Jumlah Penyelesaian = Baki Harga Jualan + Bayaran Ansuran Tertunggak + Caj bayaran lewat - Penyelarasan keatas Ibra' (rebat) disebabkan perubahan pada Kadar Keuntungan Efektif - Ibra'
semasa penyelesaian
                         
                                
                    </td>
                </tr>

                  <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>10. PENOLAKAN (SET-OFF), PENCANTUMAN DAN PENGGABUNGAN AKAUN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                  MBSB berhak pada bila-bila masa dalam budi bicara mutlaknya dan tanpa notis kepada Pemohon menggabungkan atau mencantumkan semua atau mana-mana akaun Pemohon (apa-apa jenis
sama ada tertakluk kepada notis ataupun tidak) tidak kira di mana ia berada dan menggunakan kesemuanya, termasuk sebarang wang di dalam simpanan MBSB, melalui potongan gaji ANGKASA
dan/atau oleh pihak Majikan untuk menyelesaikan sebarang keberhutangan dan tanggungjawab Pemohon terhadap MBSB.<br><br>
MBSB berhak untuk memindahkan atau menolak apa-apa jumlah kredit dalam mana-mana akaun Pemohon untuk tujuan penyelesaian mana-mana liabiliti Pemohon ke atas MBSB dan MBSB
dengan ini diberi kuasa penuh oleh Pemohon untuk melakukan pemindahan atau penolakan tersebut.

                         
                                
                    </td>
                </tr>

                   <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>11. TAKAFUL, YURAN DAN KOS</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
                 Pemohon digalakkan mengambil pelan takaful persendirian daripada syarikat takaful yang diluluskan oleh MBSB apabila menerima tawaran Kemudahan ini. Bayaran takaful akan ditolak
daripada jumlah Kemudahan yang diluluskan kepada Pemohon.<br><br>
Semua yuran dan perbelanjaan, yuran guaman (mengikut asas peguam dan Pemohon) dan kos yang ditanggung atau dibiayai oleh MBSB berkenaan atau berkaitan dengan peruntukan dan
penyediaan dokumen bagi Kemudahan ini, dan/atau mana-mana Sekuriti dan/atau penguatkuasaan MBSB bagi haknya dibawah Kemudahan, dan/atau Sekuriti hendaklah dibayar oleh Pemohon,
dan atas pertimbangan MBSB, boleh didebitkan ke akaun Pemohon yang dinyatakan atau mana-mana akaun Pemohon dengan MBSB.<br><br>
Sebagai tambahan kepada peruntukan di sini, pihak Pemohon bersetuju untuk menanggung rugi MBSB dan memastikan MBSB tidak menanggung sebarang pembayaran, kerugian, kos, caj atau
perbelanjaan, secara sah atau sebaliknya, yang pihak MBSB mungkin alami atau menanggung akibat memberi atau melanjutkan Kemudahan kepada Pemohon atau akibat daripada keingkaran
Pemohon. MBSB berhak untuk mengenakan caj dan pihak Pemohon mengaku janji untuk menanggung segala bayaran dan caj yang dikenakan oleh MBSB yang berkaitan dengan Kemudahan
atau mana-mana Sekuriti dan penyata tebus balik (redemption statement(s)) yang dikeluarkan atau yang akan dikeluarkan.

                         
                                
                    </td>
                </tr>



                   <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>12. KEJADIAN INGKAR</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
               Jumlah keseluruhan bagi Harga Jualan Komoditi MBSB dan apa-apa wang yang perlu dibayar dan tertunggak di bawah Kemudahan ini termasuklah ganti rugi (Ta`widh) jika ada akan serta-merta
perlu dibayar oleh Pemohon apabila berlakunya mana-mana Kejadian Ingkar seperti berikut:-<br>
                <ol type="i">
                  <li>Pemohon gagal atau ingkar dalam pembayaran Harga Jualan Komoditi MBSB dan apa-apa wang yang perlu dibayar dibawah peruntukan Terma-Terma ini atau Kemudahan ini, sama ada
dituntut secara rasmi atau tidak;</li>
                  <li>Pemohon dan/atau mana-mana pihak yang menyediakan Sekuriti ingkar atau mengancam untuk ingkar atau mengabaikan mana-mana janji, stipulasi, terma atau syarat yang terkandung
di dalam Terma-Terma ini, di dalam Borang ini dan/atau mana-mana dokumen-dokumen Sekuriti;</li>
                  <li>Apa-apa pernyataan, representasi atau jaminan yang dibuat oleh Pemohon di sini dibuktikan sebagai tidak benar atau adalah salah pada masa pernyataan, representasi atau jaminan
tersebut dibuat atau dianggap seolah-olah dibuat;</li>
                  <li>Terma-Terma ini dan/atau dokumen-dokumen Sekuriti adalah tidak atau akan menjadi tidak sah atau tidak boleh dikuatkuasakan;</li>
                  <li>Pemohon tidak membayar mana-mana keberhutangan yang terhutang kepada mana-mana pihak (termasuk keberhutangan yang terhutang kepada MBSB) atau keberhutangan (selain
daripada keberhutangan Pemohon di dalam Borang ini) Pemohon atau mana-mana pihak yang menyediakan Sekuriti kepada MBSB atau mana-mana bank atau institusi kewangan yang
lain di atas sebab Kejadian Ingkar oleh Pemohon atau mana-mana pihak yang menyediakan Sekuriti menjadi tertunggak atau mampu diisytiharkan tertunggak sebelum tarikh
matangnya;</li>
                  <li>Pemohon atau mana-mana pihak yang menyediakan Sekuriti telah membekalkan maklumat atau data yang palsu kepada MBSB; </li>
                  <li>Di dalam pendapat mutlak MBSB, akaun Pemohon dengan mana-mana bank, tidak diuruskan secara memuaskan;</li>
                  <li>Kejadian atau peristiwa atau situasi telah berlaku di mana akan atau boleh di dalam pendapat mutlak MBSB mempengaruhi kemampuan Pemohon atau mana-mana pihak yang menyediakan Sekuriti untuk melaksanakan obligasi-obligasinya dalam Kemudahan ini dan dalam jaminan atau mana-mana Sekuriti; </li>
                  <li>Pemohon atau mana-mana pihak yang menyediakan Sekuriti menjadi bankrap, melakukan perbuatan-perbuatan kebankrapan, tidak waras atau meninggal dunia;</li>
                  <li>Satu distres atau perlaksanaan (execution) dikenakan terhadap mana-mana hartanah-hartanah Pemohon atau pemegang amanah, atau pegawai rasmi yang telah dilantik terhadap
keseluruhan atau mana-mana bahagian daripada aset-asetnya;</li>
                  <li>Dimana ia boleh digunakan, Pemohon berhenti atau mengugut untuk berhenti daripada menjalankan perniagaannya atau memindahkan atau menjual atau berniat untuk memindahkan
atau menjual sebahagian besar daripada aset-asetnya;</li>
                  <li>Pemohon mendakwa bahawa seluruh atau sebahagian daripada Terma-Terma ini tidak mempunyai kuasa atau kesan sepenuhnya;</li>
                  <li>mana-mana akaun semasa Pemohon telah ditutup oleh mana-mana bank atas perintah dan /atau arahan agensi yang mempunyai kuasa atas cek-cek tendang (dishonoured cheques)
dan/atau polisi-polisi bank tersebut dalam mengekalkan akaun-akaun semasa tersebut, tanpa mengambil kira akaun (akaun-akaun) semasanya dengan MBSB, sama ada dimiliki secara
perseorangan ataupun milikan bersama dengan orang lain, telah dikendalikan dengan sebaiknya;</li>
                  <li>Pemohon bukan lagi kakitangan kerajaan atau mana-mana agensi kerajaan atas apa jua sebab sekali pun; </li>
                  <li>Penerima atau pemegang amanah dilantik bagi mengambil alih pemilikan harta benda Pemohon atau apa-apa pelaksanaan yang akan dan/atau sedang atau telah diambil terhadap harta
benda Pemohon;</li>
                  <li>Terjadi kemungkiran pada mana-mana perjanjian yang melibatkan pinjaman wang atau pendahuluan kredit kepada Pemohon yang memberikan hak kepada kreditor berkenaan
(termasuk MBSB) untuk menarik balik Kemudahan atau mempercepatkan bayaran atau menarik balik pinjaman wang atau pendahuluan kredit tersebut; </li>
                  <li>Perubahan di dalam mana-mana undang-undang atau peraturan yang menyebabkan ianya menjadi satu kesalahan kepada MBSB untuk terus menyediakan Kemudahan ini kepada
Pemohon.</li>
                 

                </ol>
                        Sekiranya berlaku mana-mana Kejadian Ingkar yang dinyatakan di atas, Kemudahan atau mana-mana bahagiannya akan ditangguhkan, ditarik balik atau ditamatkan dan/atau Harga Jualan
Komoditi MBSB dan semua wang yang perlu dibayar kepada MBSB di bawah Kemudahan dan Borang ini akan menjadi perlu dibayar. Selanjutnya, MBSB boleh, atas budi bicaranya
menguatkuasakan mana-mana Sekuriti yang telah disediakan untuk membayar semua jumlah yang tertunggak dan tidak dibayar di bawah Kemudahan. 
                                
                    </td>
                </tr>

     <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>13. NOTIS</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
       Sebarang notis atau tuntutan hendaklah dalam bentuk bertulis dan perlu dihantar kepada MBSB melalui kiriman pos berdaftar kepada alamat berdaftar MBSB yang terkini dan perlu dihantar
kepada Pemohon melalui kiriman pos biasa kepada alamat Pemohon seperti yang dinyatakan di dalam Borang ini. Penyerahan untuk sebarang proses undang-undang ke atas Pemohon boleh
dihantar melalui pos berdaftar kepada alamat pihak Pemohon seperti yang dinyatakan di dalam Borang ini dan penghantaran notis, tuntutan dan proses undang-undang tersebut akan dianggap
berkesan dan sempurna dihantar setelah dihantar dua (2) hari selepas tarikh ia dihantar tanpa mengambil kira notis atau tuntutan tersebut dikembalikan semula oleh pihak pos jika tidak
dihantar atau Pemohon mungkin meninggal dunia (kecuali dan sehingga pihak MBSB menerima notis bertulis daripada wakil Pemohon berkenaan geran probate atau surat kuasa mentadbir
harta pusaka Pemohon yang meninggal dunia). Tiada perubahan di dalam alamat Pemohon yang dinyatakan di dalam Borang ini dianggap berkuatkuasa atau mengikat MBSB kecuali MBSB
menerima notis yang nyata berkenaan perubahan alamat tersebut.

                                
                    </td>
                </tr>


             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>14. KEUTAMAAN PEMBAHAGIAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
MBSB berhak mengutamakan pembayaran Ansuran berbanding pembayaran amaun-amaun lain yang tertunggak daripada mana-mana pembayaran yang diterima oleh MBSB. MBSB atas budi
bicaranya berhak menilai semula keutamaan (priority) ini sebagaimana yang difikirkan patut. Jika mana-mana amaun yang telah diterima dalam bentuk bayaran atau dimiliki semula melalui
penguatkuasaan adalah kurang daripada amaun yang tertunggak; MBSB akan menggunakan amaun tersebut dalam kadar (proportion) dan turutan keutamaan (priority) dan secara
keseluruhannya mengikut cara yang akan ditentukan oleh MBSB. 

                                
                    </td>
                </tr>


             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>15. PERAKUAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Mana-mana sijil atau perakuan (certificate), notis atau tuntutan yang telah ditandatangani bagi pihak MBSB oleh mana-mana pegawai MBSB atau mana-mana peguamcara atau firma
peguamcara yang mewakili MBSB akan dianggap sebagai bukti kukuh terhadap Pemohon atas jumlah yang terhutang daripada Pemohon kepada MBSB dan semua perkara yang dinyatakan di
dalamnya bagi apa-apa tujuan sekalipun termasuk bagi tujuan prosiding undang-undang dan tidak akan dipersoalkan atas apa-apa sebab sekalipun.

                                
                    </td>
                </tr>

                             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>16. PENYERAHAN HAK (ASSIGNMENT)</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
MBSB mempunyai hak untuk menyerah hak dan liabilitinya di bawah Kemudahan ini. Liabiliti-liabiliti dan obligasi-obliagasi di bawah Terma-Terma di sini, atau mana-mana dokumen-dokumen
Sekuriti akan dianggap sah dan mengikat untuk semua tujuan tanpa mengambil kira mana-mana perubahan melalui pencantuman, pengubahan semula atau apa-apa sekalipun yang akan dibuat
di dalam memorandum dan tata urusan syarikat MBSB. Masa yang diperuntukkan oleh undang-undang untuk menuntut kembali semua jumlah tertunggak kepada MBSB tidak akan diambil kira
sebagai bertentangan dengan MBSB sehingga tuntutan bertulis bagi bayaran jumlah yang tertunggak dikeluarkan terhadap Pemohon.

                                
                    </td>
                </tr>


                             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>17. KEBERASINGAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Mana-mana peruntukan di sini yang bertentangan di sisi undang-undang, tidak sah, dilarang atau tidak boleh dikuatkuasakan di dalam mana-mana bidang kuasa akan terhadap bidang kuasa
tersebut menjadi tidak efektif sehingga sejauh mana ia adalah bertentangan di sisi undang-undang, tidak sah, dilarang atau tidak boleh dikuatkuasakan tanpa memudaratkan peruntukan yang
lainnya.<br><br>
Tiada kegagalan, pengabaian atau kelewatan oleh MBSB dalam melaksanakan apa-apa hak, kuasa, keistimewaan atau remedi yang terakru kepada MBSB dalam Terma-Terma ini menjejaskan
apa-apa hak, kuasa, keistimewaan atau remedi atau dianggap sebagai pengecualian atau persetujuan terhadap keingkaran tersebut, dan tiada tindakan oleh MBSB berkaitan apa-apa
keingkaran atau persetujuan terhadap apa-apa keingkaran tersebut akan menjejaskan mana-mana hak, kuasa, keistimewaan atau remedi MBSB berkaitan mana-mana keingkaran yang lain atau
yang seterusnya. Hak-hak dan remedi yang diperuntukan di sini adalah terkumpul dan termasuk kuasa atau remedi yang diperuntukkan di bawah undang-undang. Terma-Terma ini
menggunapakai undang-undang Malaysia

                                
                    </td>
                </tr>
             <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>18. PINDAAN PADA TERMA-TERMA</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Adalah dengan ini dipersetujui oleh semua pihak bahawa walau apapun peruntukan dan terma yang terkandung, ia adalah tertakluk kepada perubahan oleh pihak MBSB dengan memberi notis
bertulis kepada Pemohon atau melalui apa-apa cara yang difikirkan sesuai oleh MBSB sekurang-kurangnya dua puluh satu (21) hari sebelum sebarang perubahan pada peruntukan menjadi
efektif dan merupakan sebahagian daripada Terma-Terma pada masa pelaksanaan.

                                
                    </td>
                </tr>

               <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>19. TAKRIF</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Kepala surat, logo, tajuk-tajuk yang digunakan di sini hanyalah bertujuan untuk memudahkan sahaja dan tidak digunapakai dalam penterjemahan maksud Terma-Terma ini

                                
                    </td>
                </tr>

                 <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>20. MASA</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Masa apabila dinyatakan, adalah intipati dalam semua transaksi bagi Kemudahan tersebut.

                                
                    </td>
                </tr>

                  <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>21. PEMBAYARAN PENUH TANPA PENOLAKAN OLEH PEMOHON</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Semua bayaran yang dilakukan oleh Pemohon adalah dengan pembayaran penuh tanpa sebarang penolakan atau tuntutan balas.

                                
                    </td>
                </tr>

                         <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>22. PENGIKAT WARIS</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Terma-Terma ini adalah mengikat Pemohon, warisnya, wakil diri dan pengganti (jika perlu) dan hendaklah dikuatkuasakan oleh dan untuk kepentingan MBSB, waris dan penggantinya.

                                
                    </td>
                </tr>

                   <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>23. KESAN KEINGKARAN</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Tanpa mengambil kira Terma-Terma ini, sekiranya terdapat sebarang keingkaran terhadap mana-mana akaun dan/atau kemudahan bersama MBSB atau mana-mana pihak yang pada pendapat
mutlak MBSB mampu menjejaskan kemampuan Pemohon untuk memenuhi tanggungjawab Pemohon terhadap Kemudahan ini dan juga Terma-Terma di sini, MBSB berhak untuk menuntut
pembayaran penuh terhadap mana-mana jumlah wang yang terhutang yang belum dibayar oleh Pemohon kepada MBSB dan/atau melaksanakan apa-apa langkah yang terkandung di sini dengan
sewajarnya.

                                
                    </td>
                </tr>

                  <tr>
                    <td class="border" align="left" bgcolor="#0055a5">  
                        <font color="white"> <b>24. PENEPIAN SYARAT-SYARAT TERDAHULU</b></font>
                    </td>
                </tr>
                <tr>
                    <td class="border" align="justify">
Dengan ini diakui dan dimaklumkan bahawa apa-apa syarat terdahulu di dalam Terma-Terma ini adalah semata-mata untuk faedah MBSB yang mana boleh diketepikan sama ada sepenuhnya
atau sebahagian oleh MBSB mengikut budi bicara mutlaknya tanpa menjejaskan haknya dan penepian tersebut tidak menjejaskan hak MBSB untuk memastikan bahawa Pemohon mematuhi
mana-mana syarat terdahulu yang diketepikan pada masa akan datang.

                                
                    </td>
                </tr>


                <tr>
                    <td class="border" align="justify">
                    <table class="" width="50%">
                      <tr>
                        <td class="assign">
                              &nbsp; Nama/ Name :  <?php echo e($pra->fullname); ?><br>
                                &nbsp; MyKad No. / No. MyKad :  <?php echo e($pra->icnumber); ?><br>
                                &nbsp; Date/ Tarikh :  <?php echo date('d M Y'); ?><br></div>
                                  <br>
                               <u><input type="checkbox" checked disabled="disabled" readonly> Saya Bersetuju dengan Terma dan Syarat</u>

                     </td>
                      </tr>
                      </table>


                    </td>
                </tr>



            </table>

               </b>
</div>
<script>
function printpage() {
window.print();  


}
</script>
  </body>
</html>
