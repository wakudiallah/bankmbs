<html>
<style type="text/css">

body {
    font-size: 9px;

  font-family: "Arial", Arial, Sans-serif;

}
  
th {
    font-size: 14px;
  

}

br.border2  {
   display: block;
   margin: 20px 0;
}

td {
    font-size: 8px;
    color: #2751a6;

  font-family: "Arial", Arial, Sans-serif;

}
p {
    
    line-height: 130%;

}
.column {
    
    width: 100%;
    padding: 0px;
    height: 300px; /* Should be removed. Only for demonstration */
}
.column2 {
    float: left;
    width: 50%;
    padding: 0px;
    height: 300px; /* Should be removed. Only for demonstration */
}
/* Clear floats after the columns */
.row:after {
    content: "";
    display: table;
    clear: both;
}
div.sansserif {
    font-family: Arial, Helvetica, sans-serif;
  color :#00005A;
  font-size :14px;
}

div.small {
  font-size :9px;
}

div.agakbesak {
  font-size :14px;
}

ul
{
    list-style-type: none;
}

 table.border {
  border-collapse: collapse;
    border: 1px solid black;
    
} 

 table.bordera {
  border-collapse: collapse;
    border: 0px solid black;
    
} 
table.border2 {
  border-collapse: collapse;
    border: 1px solid black;
    
} 

td.border {
  border-collapse: collapse;
    border: 1px solid black;
    width: 10px;
    border-color: blue;
    font-weight: bold;
}

td.bordera {
  border-collapse: collapse;
    border: 0px solid black;
    width: 10px;
    border-color: blue;
    font-weight: bold;
}
td.border1 {
   border-collapse: collapse;
    border: 1px solid black;
    /*width: 10px;*/
    border-color: blue;
}
td.border4 {
  border-collapse: collapse;
    border: 4px solid black;
}


td.header {
 background-color:black;
 color:white;
 font-weight: bold;

}

tr.border {
  border-collapse: collapse;
    border: 1px solid black;
    border-color: blue;
}

td.border2 {
  border-collapse: collapse;
     border-bottom:1pt solid black;
}


/*li:before {
    content: '✔';   
    margin-left: -1em; margin-right: .100em;
}

ul{
    
   padding-left:20px;
   text-indent:2px;
   list-style: none;
   list-style-position:outside; 
  
}*/
td.clas {
   border: 1px solid black;
    width: 500px;
    height:15px;
}
td.signature {
   border: 1px solid black;
    width: 250px;
    height:15px;
}
td.classbank {
   border: 1px solid blue;
    width: 325px;
    height:12px;
}
td.classborder {
   border: 1px solid blue;
    width: 600px;
    height:15px;
}
td.classborder50 {
   border: 1px solid blue;
    width: 255px;
    height:15px;
}
td.classbordercontact {
   border: 1px solid blue;
    width: 600px;
    height:15px;
}
td.classborderrelation {
   border: 1px solid blue;
    width: 300px;
    height:15px;
}
td.classbordernature {
   border: 1px solid blue;
    width: 300px;
    height:15px;
}
.no-border-top {
  border-top: solid 1px #FFF!important;
  border-bottom: solid 1px #FFF!important;
  color: red;
  width: 10px;
}
td.classborderspouse {
   border: 1px solid blue;
    width: 580px;
    height:15px;
}
  u.underlinea {
font-size: 9px;
display: inline-block;
position: relative;
  }
u.underlinea:before {
content: "";
position: absolute;
width: 450px !important;
height: 1px;
bottom: 0;
left: 0;
border-bottom: 1px solid #333;
}
#footer {
  position: fixed;
  width: 100%;
  bottom: 0;
  left: 0;
  right: 0;
}
#footer p {
  border-top: 0px solid #555555;
  margin-top:10px;
}

div.a {
  line-height: normal;
}

div.b {
  line-height: 1.6;
}

div.c {
  line-height: 80%;
}

div.d {
  line-height: 200%;
}
</style>
<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:4px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:4px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
.tg .tg-baqh{text-align:center;vertical-align:top}
.tg .tg-wxgh{text-decoration:underline;vertical-align:top}
.tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
.tg .tg-9hbo{font-weight:bold;vertical-align:top}
.tg .tg-yw4l{vertical-align:top}
 input.largerCheckbox { 
        width: 40px; 
        height: 40px; 
    } 
     tr.hide_all > td, td.hide_all{
        border-style:hidden;
      }
</style>
<body>
    <?php 
        $ic     = $data->new_ic;
        $old_ic = $data->old_ic;
        $name   = $data->name;
        $police = $data->police_number;

        $emp_name= $data->EmpInfo->empname;
        $dept_name= $data->EmpInfo->dept_name;
        $div_name= $data->EmpInfo->division;

        //address
        $state_address  = $data->StateAddress->alias_name;
        $state_corres   = $data->StateCorres->alias_name;
        $state_emp      = $data->EmpInfo->StateOffice->alias_name;

        $monthly_income = $data->Financial->monthly_income;
        $other_income = $data->Financial->other_income;
        $total_income = $data->Financial->total_income;

        $monthly_com1 = $data->Commitments->monthly_payment1;
        $monthly_com2 = $data->Commitments->monthly_payment2;

        $lahir  =  date('d/m/Y', strtotime($data->dob));
        $date_working   =  date('d/m/Y', strtotime($data->EmpInfo->joined));
        $today=date("Y-m-d");
        $date_working_diff   =  date('Y-m-d', strtotime($data->EmpInfo->joined));
        $d1 = new DateTime($today);
        $d2 = new DateTime($date_working_diff);
        $diff = $d2->diff($d1);
        $total_working=$diff->y;
        $today_date=date("d/m/Y");
        $submitted_date   =  date('d/m/Y', strtotime($data->Terma->file_created));
    ?>
    <table  width="100%" cellpadding='4'>
        <tr>
            <td><img src="{{url('/')}}/img/header.png" style="margin-top: -25px" width="700px" height="55px"></td>
        </tr>
    </table>
    
    <table  width="100%" class="border" style="border: 0px!important" cellpadding='1'>
        <tr>
            <td width="80%" class="">
                <table width="" valign="top" class="">
                    <tr>
                        <td><b>Type of Cutstomer </b><br><i>Jenis Pelanggan</i></td>
                        <td>
                            <table align="left" class="border" cellpadding="1">
                                <tr>
                                    <td class="border">
                                        @if ($dsr_a->existing_mbsb=="NO") 
                                           <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;&nbsp;
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td><b>New/</b><i> Baru </i></td>
                        <td>
                            <table align="left" class="border" cellpadding="1">
                              <tr>
                                <td class="border">
                                    @if ($dsr_a->existing_mbsb=="YES") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;&nbsp;
                                    @endif
                                </td>
                              </tr>
                            </table>
                        </td>
                        <td><b>Existing CIF (No)/</b><br><i>Sedia Ada (No. CIF)</i>
                        </td>
                         @if ($dsr_a->existing_mbsb=="NO") 
                        <td><u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u></td>
                        @else
                        <td><u>{{$dsr_a->cif_no}}</u></td>
                        @endif
                    </tr>
                </table>
            </td>
            <td width="20%" valign="top" class="">
                <table width="" valign="top" class="">
                    <tr>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>
                            <table align="left" class="border" cellpadding="1">
                                <tr>
                                    <td class="border">
                                      @if ($data->title=="Mr. / Encik") 
                                         <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;&nbsp;
                                      @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td><b>Staff</b><br><i>Kakitangan</i></td>
                        <td>&nbsp;</td>
                        <td><b>Staff No./</b><br><i>No Kakitangan </i></td>
                        <td>
                            <table align="left" class="border" cellpadding="1">
                               <tr>
                                    <td class="border">&nbsp; <b></b> &nbsp;</td>
                                    <td class="border">&nbsp; <b></b> &nbsp;</td>
                                    <td class="border">&nbsp; <b></b> &nbsp;</td>
                                    <td class="border">&nbsp; <b></b> &nbsp;</td>
                                    <td class="border">&nbsp; <b></b> &nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="40%" class=""></td>
            <td width="60%" valign="" class=""></td>
        </tr>
    </table>
    <p style="font-size: 9px;color:#2751a6"><b>Classification of residency status under Foreign Exchange Administration Rules./</b> <i>Klasifikasi status pemastautin di bawah Peraturan Pentadbiran Pertukaran Asing.</i><br><b>Please tick ( ) the appropriate boxes below./ </b> <i> Sila tandakan ( ) pada kotak yang bersesuaian di bawah.</i></p>
    
    <table  width="100%" class="border" cellpadding='5' >
        <tr class="border">
            <td width='50%' class="border1">
                <table>
                    <tr>
                        <td>
                            <table class="border">
                                <tr>
                                    <td> 
                                    @if ($data->resident=="Y") 
                                         <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;&nbsp;
                                    @endif</td>
                                </tr>
                            </table>
                        </td>
                        <td><b>Resident /</b> <i>Pemastautin</i></td>
                    </tr>
                    <p align="justify;" style="margin-top: -10px!important">
                        <ul style="list-style-type: disc;text-align: justify;"><b>You are a Resident if /</b><i>Anda adalah Pemastautin jika</i>;
                            <li><b>You are a Malaysian Citizen with no permanent resident status of a territory outside Malaysia; or&nbsp;</b> 
                            <br><i>Anda adalah Warganegara Malaysia tanpa status pemastautin tetap bagi wilayah di luar Malaysia; atau</i></li>
                            <li><b>You are a Non-Malaysian Citizen with permanent resident status in Malaysia and you reside in Malaysia more than 182 days in a calendar year</b> 
                            <br><i>Anda adalah bukan Warganegara Malaysia dengan status pemastautin tetap di Malaysia dan anda tinggal di Malaysia lebih daripada 182 hari dalam satu tahun kalendar.</i></li>
                        </ul>
                    </p>
                    <tr>
                        <td colspan="2">
                            <table></table>
                        </td>
                    </tr>
                </table>
            </td>
            <td width='50%' class="border1">
                <table>
                    <tr>
                        <td>
                            <table class="border">
                                <tr>
                                    <td>@if ($data->resident=="N") 
                                         <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;&nbsp;
                                    @endif</td>
                                </tr>
                            </table>
                        </td>
                        <td><b>Non Resident /</b> <i>Bukan Pemastautin</i></td>
                    </tr>
                    <p align="justify;" style="margin-top: -10px!important">
                        <ul style="list-style-type: disc;text-align: justify;"><b>You are a Resident if /</b><i>Anda adalah Pemastautin jika</i>;
                            <li><b>You are a Malaysian Citizen with no permanent resident status of a territory outside Malaysia; or&nbsp;</b> 
                            <br><i>Anda adalah Warganegara Malaysia tanpa status pemastautin tetap bagi wilayah di luar Malaysia; atau</i></li>
                            <li><b>You are a Non-Malaysian Citizen with permanent resident status in Malaysia and you reside in Malaysia more than 182 days in a calendar year</b> 
                            <br><i>Anda adalah bukan Warganegara Malaysia dengan status pemastautin tetap di Malaysia dan anda tinggal di Malaysia lebih daripada 182 hari dalam satu tahun kalendar.</i></li>
                        </ul>
                    </p>
                    <tr>
                        <td colspan="2">
                            <table></table>
                        </td>
                    </tr>
                </table>
            </td>
            
        </tr>
    </table>
    <table>
        <tr>
            <td style="width:100%;" align="justify"><b>Note: For Ringgit Account Opened by Non-Resident, please complete DECLARATION BY NON-RESIDENTS MAINTAINING / OPERATING RINGGIT ACCOUNT IN MALAYSIA. </b><i>Nota: Bagi Akaun Ringgit yang Dibuka oleh Bukan Pemastautin, sila lengkapkan PENGAKUAN OLEH BUKAN PEMASTAUTIN YANG MENYELENGGARA / MENGURUSKAN AKAUN RINGGIT DI MALAYSIA.</i></td>
        </tr>   
    </table> 
    <div class="row">
        <div class="column">
            <br>
            <span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><strong>SECTION A</strong> /<em> BAHAGIAN A&nbsp;</em><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;</span>
            <table>
                <tr>
                    <td colspan="4" class="" style="width: 700px !important;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5" >
                        <b>PERSONAL PARTICULARS (MAIN APPLICANT) /</b><i> BUTIR-BUTIR PERIBADI (PEMOHON UTAMA)</i>
                    </td>
                </tr>   
            </table>
            <table style="">
                <tr>
                    <td style="width: 80px"><b>Salutation</b><br><i>Gelaran</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="2" style="font-size:10px!important; text-align: center !important;border-collapse: collapse;">
                            <tr>
                                <td class="border">
                                    @if ($data->title=="ENCIK") 
                                      <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;
                                    @endif
                                </td>
                                <td width="50px" style="font-size: 8px !important"><b>Mr / </b><i>Encik</i></td>
                                <td width="13px"></td>
                                <td class="border">
                                    @if ($data->title=="PUAN") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;
                                    @endif
                                </td>
                                <td width="50px" style="font-size: 8px !important"><b>Mdm / </b><i>Puan </i></td>
                                <td width="13px"></td>
                                <td class="border">
                                    @if ($data->title=="CIK") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;
                                    @endif
                                </td>
                                <td width="50px" style="font-size: 8px !important"><b>Miss / </b><i>Cik</i></td>
                                <td width="13px"></td>
                                <td class="border">
                                    @if (($data->title=="ENCIK") OR ($data->title=="CIK") OR ($data->title=="PUAN"))
                                        &nbsp;&nbsp;
                                    @else
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @endif
                                </td>
                                <td width="13px"></td>
                                <td width="80px" style="font-size: 8px !important"><b>Others (Specify)/</b><i>Lain-lain (Nyatakan)</i></td>
                                @if(($data->title=="ENCIK") OR ($data->title=="CIK") OR ($data->title=="PUAN"))
                                <td width="163px" style="border-bottom: 1px solid #2751a6; text-align: left !important;font-weight: bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                @elseif($data->title=='OTH')
                                <td width="163px" style="border-bottom: 1px solid #2751a6; text-align: left !important;font-weight: bold">{{$data->title_others}}</td>
                                @else
                                <td width="163px" style="border-bottom: 1px solid #2751a6; text-align: left !important;font-weight: bold">{{$data->title}}</td>
                                @endif
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 80px"><b>Full Name</b><br><i>Nama Penuh </i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="1" style="font-size:7px!important; text-align: center !important;border-collapse: collapse;">
                            <tr>
                               <?php for ($i=0; $i<=43;$i++) {
                                  if (substr($name,$i,1)=="" OR substr($name,$i,1)==" ") {
                                    echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                  }
                                  else { ?>
                                    <td class="border" style="font-size:9px!important; text-align: center !important;border-collapse: collapse;"> {{substr($name,$i,1)}} </td>
                                    <?php
                                  }
                                }
                                ?>
                            </tr>
                            <tr>
                                <?php for ($i=43; $i<=86;$i++) {
                                  if (substr($name,$i,1)=="") {
                                    if (substr($name,$i-43,1)=="") {
                                      echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                    }
                                    else {
                                      ?>
                                      <td class="border" style="text-align: center !important; font-weight: bold !important;"><font color="white">{{ substr($name,$i-40,1) }}</font> </td>
                                    <?php

                                    }
                                  }
                                  else { ?>
                                    <td class="border" style="text-align: center !important; font-weight: bold !important;">{{ substr($name,$i,1) }}</td>
                                    <?php
                                  }
                                }
                                ?>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td style="width: 80px"><b>MyKad No.</b><br><i>No. MyKad</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellspacing="1" cellpadding="2" style="font-size:10px!important; text-align: center !important;border-collapse: collapse;">
                            <tr>
                                <td class="border"> {{ substr($ic,0,1) }} </td>
                                <td class="border"> {{ substr($ic,1,1) }} </td>
                                <td class="border"> {{ substr($ic,2,1) }} </td>
                                <td class="border"> {{ substr($ic,3,1) }} </td>
                                <td class="border"> {{ substr($ic,4,1) }} </td>
                                <td class="border"> {{ substr($ic,5,1) }} </td>
                                <td>&nbsp;&nbsp;-&nbsp;&nbsp; <br></td>
                                <td class="border"> {{ substr($ic,6,1) }} </td>
                                <td class="border"> {{ substr($ic,7,1) }} </td>
                                <td>&nbsp;&nbsp;-&nbsp;&nbsp; <br></td>
                                <td class="border"> {{ substr($ic,8,1) }} </td>
                                <td class="border"> {{ substr($ic,9,1) }} </td>
                                <td class="border"> {{ substr($ic,10,1) }} </td>
                                <td class="border"> {{ substr($ic,11,1) }} </td>
                                <td>&nbsp;&nbsp;&nbsp;</td>
                                <td style="width: 80px"><b>Date of Birth/</b><i>Tarikh Lahir</i></td>
                                <td style="width: 10px">:</td>
                                <td class="border"> {{substr($lahir,0,1)}} </td>
                                <td class="border"> {{substr($lahir,1,1)}}</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;/&nbsp; </td>
                                <td>&nbsp;</td>
                                <td class="border"> {{substr($lahir,3,1)}} </td>
                                <td class="border"> {{substr($lahir,4,1)}} </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;/&nbsp; </td>
                                <td>&nbsp;</td>
                                <td class="border"> {{substr($lahir,6,1)}} </td>
                                <td class="border"> {{substr($lahir,7,1)}} </td>
                                <td class="border"> {{substr($lahir,8,1)}} </td>
                                <td class="border"> {{substr($lahir,9,1)}} </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 80px"><b>Police/Military No.</b><br><i>No. Polis / Tentera</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellspacing="1" cellpadding="2" style="font-size:10px!important; text-align: center !important;border-collapse: collapse;">
                            <tr>
                                @if(!empty($police))
                                <td class="border"> {{ substr($police,0,1) }} </td>
                                <td class="border"> {{ substr($police,1,1) }} </td>
                                <td class="border"> {{ substr($police,2,1) }} </td>
                                <td class="border"> {{ substr($police,3,1) }} </td>
                                <td class="border"> {{ substr($police,4,1) }} </td>
                                <td class="border"> {{ substr($police,5,1) }} </td>
                                <td class="border"> {{ substr($police,6,1) }} </td>
                                <td class="border"> {{ substr($police,7,1) }} </td>
                                <td class="border"> {{ substr($police,8,1) }} </td>
                                <td class="border"> {{ substr($police,9,1) }} </td>
                                <td class="border"> {{ substr($police,10,1) }} </td>
                                <td class="border"> {{ substr($police,11,1) }} </td>
                                <td class="border"> {{ substr($police,12,1) }} </td>
                                <td class="border"> {{ substr($police,13,1) }} </td>
                                @else
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                @endif
                            </tr>
                        </table>
                    </td>
                </tr>

                <!--<tr>
                    <td style="width: 80px"><b>Police/Military No.</b><i>No. Polis / Tentera</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellspacing="1" cellpadding="2" style="font-size:10px!important; text-align: center !important;border-collapse: collapse;">
                             <tr>
                                <td><br>
                                    <table class="border">
                                        <tr>
                                            <?php for ($i=0; $i<=13;$i++) {
                                              if (substr($police,$i,1)=="" OR substr($police,$i,1)==" ") {
                                                echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                              }
                                              else { ?>
                                                <td class="border">&nbsp; {{substr(strtoupper($police),$i,1)}} &nbsp;</td>
                                                <?php
                                              }
                                            }
                                            ?>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>-->

                <!--<tr>
                    <td style="width: 80px"><b>Gender/</b><i>Jantina</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="1" style="text-align: center !important;">
                            <tr>
                                <td class="border">
                                  @if ($data->gender=="M") 
                                    &nbsp;/&nbsp;
                                  @else
                                    &nbsp;&nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="50px"><b>Male/</b><i>Lelaki </i></td>
                                <td class="border">
                                  @if ($data->gender=="F") 
                                    &nbsp;/&nbsp;
                                  @else
                                    &nbsp;&nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="110px"><b>Female/</b><i> Perempuan </i></td>
                                <td style="width: 120px"><b>Country of Birth/</b><i>Negara Kelahiran</i></td></tr>
                                <tr>
                                    <td style="width: 10px"></td>
                                    <td style="width: 10px"></td>
                                    <td style="width: 10px"></td>
                                    <td style="width: 10px"></td>
                                <td></td>
                                
                                <td class=""><b><u>{{$data->CountryDOB->country_desc}}</u></b><br><b>Country/</b><i>Negara</i></td>
                            </tr>
                        </table>
                    </td>
                </tr>-->
            </table>
    <table  width="100%" class="border" style="border: 0px!important" cellpadding='1'>
        <tr>
            <td width="80%" class="">
                <table width="" valign="top" class="">
                    <tr>
                        <td style="width: 80px"> <b>Gender / </b><i>Jantina</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="left" class="" cellpadding="1">
                                <tr>
                                    <td class="border"> @if ($data->gender=="M") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;&nbsp;
                                      @endif 
                                    </td>
                                    <td class="" width="50px"><b>Male/</b><i>Lelaki </i></td>
                                    <td class="border" style="text-align: center;">
                                        @if ($data->gender=="F") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                        &nbsp;&nbsp;&nbsp;
                                         @endif
                                    </td>
                                    <td class=""  width="150px"><b>Female/</b><i> Perempuan </i></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width: 10px"></td>
                        <td style="width: 80px"><b>Country of Birth</b><br><i>Negara Kelahiran</i>
                        </td>
                        <td>
                            <table align="left" class="" cellpadding="1">
                                <tr>
                                    <td style="width: 10px"></td>
                                    <td style="width: 10px"></td>
                                    <td style="width: 10px"></td>
                                    <td style="width: 10px"></td>
                                    <td></td>
                                    <!--
                                    <td class="" width="100px">
                                        <span style="text-decoration: underline; white-space: pre;">{{$data->CountryDOB->country_desc}} </span>
                                        <br><b>Country/</b><i>Negara</i>
                                    </td>-->
                                    <td width="100px" style="border-bottom: 1px solid #2751a6; text-align: left !important;font-weight: bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></td>
                                    <td width="100px" style="border-bottom: 1px solid #2751a6; text-align: left !important;font-weight: bold">{{$data->CountryDOB->country_desc}}</span></td>
                                </tr>
                                  <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td style="text-align: center;"><b>State/</b><i>Negeri</i></td>
                                    <td style="text-align: center;"><b>Country/</b><i>Negara</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="40%" class=""></td>
            <td width="60%" valign="" class=""></td>
        </tr>
         <tr>
            <td width="40%" class=""></td>
            <td width="60%" valign="" class=""></td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 80px"><b>Home Address </b><i>Alamat Rumah</i></td>
            <td style="width: 10px">:</td>
            <td>
                <table align="" class="border" cellpadding="2" cellspacing="1" style="font-size: 10px; text-align: center !important;">
                    <tr>
                        <?php for ($i=0; $i<=36;$i++) {
                              if (substr($data->address,$i,1)=="" OR substr($data->address,$i,1)==" ") {
                                echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                              }
                              else { ?>
                                <td class="border">{{substr(strtoupper($data->address),$i,1)}}</td>
                                <?php
                              }
                            }
                        ?>
                    </tr>
                    <tr>
                        <?php for ($i=0; $i<=36;$i++) {
                              if (substr($data->address2,$i,1)=="" OR substr($data->address2,$i,1)==" ") {
                                echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                              }
                              else { ?>
                                <td class="border"> {{substr(strtoupper($data->address2),$i,1)}} </td>
                                <?php
                              }
                            }
                        ?>
                    </tr>
                    <tr>
                        <?php for ($i=0; $i<=36;$i++) {
                              if (substr($data->address3,$i,1)=="" OR substr($data->address3,$i,1)==" ") {
                                echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                              }
                              else { ?>
                                <td class="border">{{substr(strtoupper($data->address3),$i,1)}} </td>
                                <?php
                              }
                            }
                        ?>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>

            <td style="width: 70px">&nbsp;&nbsp;&nbsp;</td>
            <td>
                <table align="" class="" cellpadding="2" style="text-align: center !important;border-collapse: collapse;">
                     <tr>
                        <td style="width: 100px"><b>State/</b><i>Negeri</i></td>
                        <td style="width: 10px"></td>
                        <td class="border"> {{ substr($state_address,0,1) }} </td>
                        <td class="border"> {{ substr($state_address,1,1) }} </td>
                        <td class="border"> {{ substr($state_address,2,1) }} </td>
                        <td class="border"> {{ substr($state_address,3,1) }} </td>
                        <td class="border"> {{ substr($state_address,4,1) }} </td>
                        <td class="border"> {{ substr($state_address,5,1) }} </td>
                        <td class="border"> {{ substr($state_address,6,1) }} </td>
                        <td class="border"> {{ substr($state_address,7,1) }} </td>
                        <td class="border"> {{ substr($state_address,8,1) }} </td>
                        <td class="border"> {{ substr($state_address,9,1) }} </td>
                        <td class="border"> {{ substr($state_address,10,1) }} </td>
                        <td class="border"> {{ substr($state_address,11,1) }} </td>
                        <td class="border"> {{ substr($state_address,12,1) }} </td>
                        <td class="border"> {{ substr($state_address,13,1) }} </td>
                        <td class="border"> {{ substr($state_address,14,1) }} </td>
                        <td class="border"> {{ substr($state_address,15,1) }} </td>
                        <td style="width: 100px"><b>Postcode/</b><i>Postcode</i></td>
                        <td style="width: 10px"></td>
                         <td class="border"> {{ substr($data->postcode,0,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,1,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,2,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,3,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,4,1) }} </td>
                     </tr>
                </table>
            </td>
        </tr>
        <!--<tr>
            <td style="width: 80px"><b>State/</b><i>Negeri</i></td>
            <td style="width: 10px">:</td>
            <td>
                <table align="" class="" cellpadding="0" style="text-align: center !important;">
                     <tr>
                        <td class="border"> {{ substr($state_address,0,1) }} </td>
                        <td class="border"> {{ substr($state_address,1,1) }} </td>
                        <td class="border"> {{ substr($state_address,2,1) }} </td>
                        <td class="border"> {{ substr($state_address,3,1) }} </td>
                        <td class="border"> {{ substr($state_address,4,1) }} </td>
                        <td class="border"> {{ substr($state_address,5,1) }} </td>
                        <td class="border"> {{ substr($state_address,6,1) }} </td>
                        <td class="border"> {{ substr($state_address,7,1) }} </td>
                        <td class="border"> {{ substr($state_address,8,1) }} </td>
                        <td class="border"> {{ substr($state_address,9,1) }} </td>
                        <td class="border"> {{ substr($state_address,10,1) }} </td>
                        <td class="border"> {{ substr($state_address,11,1) }} </td>
                        <td class="border"> {{ substr($state_address,12,1) }} </td>
                        <td class="border"> {{ substr($state_address,13,1) }} </td>
                        <td class="border"> {{ substr($state_address,14,1) }} </td>
                        <td class="border"> {{ substr($state_address,15,1) }} </td>
                        <td style="width: 100px"><b>Postcode/</b><i>Postcode</i></td>
                        <td style="width: 10px"></td>
                         <td class="border"> {{ substr($data->postcode,0,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,1,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,2,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,3,1) }} </td>
                        <td class="border"> {{ substr($data->postcode,4,1) }} </td>
                     </tr>
                </table>
            </td>
        </tr>-->
    </table>

    <table  width="100%" class="border" style="border: 0px!important" cellpadding='1'>
        <tr>
            <td width="80%" class="">
                <table width="" valign="top" class="">
                    <tr>
                        <td style="width: 80px"><b>Ownership Status </b><br><i>Taraf Pemilikan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="left" class="border" cellpadding="1">
                                <tr>
                                    <td class="border" style="text-align: center;">
                                        @if ($data->ownership=="OW") 
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;&nbsp;
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td width="150px"><b>Own Premises /</b><i> Milik Sendiri </i><br></td>
                        <td>
                            <table align="left" class="border" cellpadding="1">
                              <tr>
                                <td class="border" style="text-align: center;">
                                   @if ($data->ownership=="LP") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;&nbsp;
                                    @endif
                                </td>
                              </tr>
                            </table>
                        </td>
                        <td width="150px"><b>Living with Family</b><br><i> Tinggal bersama keluarga </i>
                        </td>
                        <td>
                            <table align="left" class="border" cellpadding="1">
                              <tr>
                                <td class="border" style="text-align: center;">
                                    @if ($data->ownership=="R")  
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;&nbsp;
                                    @endif
                                </td>
                              </tr>
                            </table>
                        </td>
                        <td width="80px"><b>Rented / </b><i>Sewa </i><br></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="40%" class=""></td>
            <td width="60%" valign="" class=""></td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 80px"><b>Correspondence Address</b><br><i>Alamat Surat Menyurat</i></td>
            <td style="width: 10px">:</td>
            <td>
                <table align="" class="border" cellpadding="2" cellspacing="1" style="font-size: 10px; text-align: center !important;">
                    <tr>
                        <?php for ($i=0; $i<=36;$i++) {
                              if (substr($data->corres_address1,$i,1)=="" OR substr($data->corres_address1,$i,1)==" ") {
                                echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                              }
                              else { ?>
                                <td class="border">{{substr(strtoupper($data->corres_address1),$i,1)}}</td>
                                <?php
                              }
                            }
                        ?>
                    </tr>
                    <tr>
                        <?php for ($i=0; $i<=36;$i++) {
                              if (substr($data->corres_address2,$i,1)=="" OR substr($data->corres_address2,$i,1)==" ") {
                                echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                              }
                              else { ?>
                                <td class="border"> {{substr(strtoupper($data->corres_address2),$i,1)}} </td>
                                <?php
                              }
                            }
                        ?>
                    </tr>
                    <tr>
                        <?php for ($i=0; $i<=36;$i++) {
                              if (substr($data->corres_address3,$i,1)=="" OR substr($data->corres_address3,$i,1)==" ") {
                                echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                              }
                              else { ?>
                                <td class="border">{{substr(strtoupper($data->corres_address3),$i,1)}} </td>
                                <?php
                              }
                            }
                        ?>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="width: 70px">&nbsp;&nbsp;&nbsp;</td>
            <td>
                <table align="" class="" cellpadding="2" style="text-align: center !important;border-collapse: collapse;">
                     <tr>
                        <td style="width: 100px"><b>State/</b><i>Negeri</i></td>
                        <td style="width: 10px"></td>
                        <td class="border"> {{ substr($state_corres,0,1) }} </td>
                        <td class="border"> {{ substr($state_corres,1,1) }} </td>
                        <td class="border"> {{ substr($state_corres,2,1) }} </td>
                        <td class="border"> {{ substr($state_corres,3,1) }} </td>
                        <td class="border"> {{ substr($state_corres,4,1) }} </td>
                        <td class="border"> {{ substr($state_corres,5,1) }} </td>
                        <td class="border"> {{ substr($state_corres,6,1) }} </td>
                        <td class="border"> {{ substr($state_corres,7,1) }} </td>
                        <td class="border"> {{ substr($state_corres,8,1) }} </td>
                        <td class="border"> {{ substr($state_corres,9,1) }} </td>
                        <td class="border"> {{ substr($state_corres,10,1) }} </td>
                        <td class="border"> {{ substr($state_corres,11,1) }} </td>
                        <td class="border"> {{ substr($state_corres,12,1) }} </td>
                        <td class="border"> {{ substr($state_corres,13,1) }} </td>
                        <td class="border"> {{ substr($state_corres,14,1) }} </td>
                        <td class="border"> {{ substr($state_corres,15,1) }} </td>
                        <td style="width: 100px"><b>Postcode/</b><i>Postcode</i></td>
                        <td style="width: 10px"></td>
                         <td class="border"> {{ substr($data->corres_postcode,0,1) }} </td>
                        <td class="border"> {{ substr($data->corres_postcode,1,1) }} </td>
                        <td class="border"> {{ substr($data->corres_postcode,2,1) }} </td>
                        <td class="border"> {{ substr($data->corres_postcode,3,1) }} </td>
                        <td class="border"> {{ substr($data->corres_postcode,4,1) }} </td>
                     </tr>
                </table>
            </td>
        </tr>
    </table>
    <table  width="100%" class="border" style="font-size: 9px; border: 0px!important" cellpadding='1'>
        <tr>
            <td width="80%" class="">
                <table width="" valign="top" class="">
                    <tr>
                        <td style="width: 100px">&nbsp;</td>
                        <td width="80px"><b>Home Telephone</b><BR><i> Telefon Rumah</i><br></td>
                        <td>
                            <table align="left" class="border" cellpadding="2">
                              <tr>
                                @if(!empty($data->corres_homephone))
                                    <td class="border"> {{ substr($data->corres_homephone,0,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,1,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,2,1) }} </td>
                                    <td class="no-border-top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border"> {{ substr($data->corres_homephone,3,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,4,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,5,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,6,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,7,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,8,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,9,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,10,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_homephone,11,1) }} </td>
                                @else
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="no-border-top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                @endif
                              </tr>
                            </table>
                        </td>
                        <td style="width: 20px">&nbsp;</td>
                       <td width="80px"><b>Mobile Phone</b><BR><i> Telefon Bimbit</i><br></td>
                        <td>
                            <table align="left" class="border" cellpadding="2">
                              <tr>
                                @if(!empty($data->corres_mobilephone))
                                    <td class="border"> {{ substr($data->corres_mobilephone,0,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,1,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,2,1) }} </td>
                                    <td class="no-border-top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,3,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,4,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,5,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,6,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,7,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,8,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,9,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,10,1) }} </td>
                                    <td class="border"> {{ substr($data->corres_mobilephone,11,1) }} </td>
                                @else
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="no-border-top">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    <td class="border">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                @endif
                              </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="40%" class=""></td>
            <td width="60%" valign="" class=""></td>
        </tr>
        <tr>
            <td width="80%" class="">
                <table width="" valign="top" class="">
                    <tr>
                        <td style="width: 100px">&nbsp;</td>
                        <td width="80px"><b>E-mail Address</b><br><i>Almat E-mel</i><br></td>
                        <td>
                            <table align="left" class="" cellpadding="2">
                              <tr>
                                    <td class="clas">{{$data->corres_email}}</td>
                              </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td width="40%" class=""></td>
            <td width="60%" valign="" class=""></td>
        </tr>
    </table>
    <table  width="100%" class="border" style="border: 0px!important" cellpadding='2'>
        <tr>
            <td width="80%" class="">
                <table width="" valign="top" class="">
                    <tr>
                        <td style="width: 80px"> <b>Nationality </b><br><i>Kewarganegaraan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="left" class="" cellpadding="2">
                                <tr>
                                    <td class="border" style="text-align: center;"> 
                                        @if ($data->nationality=="MY") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif 
                                    </td>
                                    <td class="" width="100px"><b>Malaysian/</b><i>Malaysia </i></td>
                                    <td class="border" style="text-align: center;">
                                        @if ($data->nationality=="PR") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                        &nbsp;&nbsp;
                                         @endif
                                    </td>
                                    <td class=""  width="200px"><b>Permanent Resident/</b><i> Penduduk Tetap </i>
                                    </td>
                                    <td class="border" style="text-align: center;">
                                        @if ($data->nationality=="NC") 
                                         <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                        &nbsp;&nbsp;
                                         @endif
                                    </td>
                                    <td class=""  width="250px"><b>Non Citizen (Specify)/</b><i> Bukan Warganegara (nyatakan)</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b style=" font-size: 9px;
    color: #2751a6;

  font-family: Arial, Arial, Sans-serif;">Country of origin</b>/<i style=" font-size: 9px;
    color: #2751a6;

  font-family: Arial, Arial, Sans-serif;">Negara Asal</i>  
   @if ($data->nationality!="NC") 
    <u style=" font-size: 9px;
    color: #2751a6; font-family: Arial, Arial, Sans-serif;">______________________________________________</u>
    @else
    <u style=" font-size: 9px;
    color: #2751a6; font-family: Arial, Arial, Sans-serif;"><b>{{$data->CoOrigin->country_desc}}</b></u>
    @endif
    <table  width="100%" class="border" style="border: 0px!important" cellpadding='0' cellspacing="0">
        <tr>
            <td width="10%"></td>
            <td width="" class="">
                
                
            </td>
        </tr>
        <tr>
            <td width="40%" class=""></td>
            <td width="60%" valign="" class=""></td>
        </tr>
    </table>
    <table>

                <tr>
                    <td style="width: 80px"><b>Race / </b><i>Bangsa</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="2" style="text-align: center !important;">
                            <tr>
                                <td class="border" >
                                  @if ($data->race=="MAL") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="60px"><b>Malay/</b><i> Melayu </i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border">
                                  @if ($data->race=="CHN") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="60px"><b>Chinese/</b><i> China </i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border">
                                  @if ($data->title=="IND") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="50px"><b>Indian/</b><i>India</i></td>
                                <td width="30px">&nbsp;</td>
                                <td class="border">
                                    @if (($data->race!="IND") AND ($data->race!="MAL") AND ($data->race!="CHN"))
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;
                                    @endif
                                </td>
                                <td width="50px"><b>Others (Specify)/</b><i>Lain-lain (Nyatakan)</i></td>
                                <td> <u>{{ $data->title}}</u></td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        <table>
                <tr>
                    <td style="width: 80px"><b>Bumiputera Status</b><br><i>Status Bumiputera</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="2" style="text-align: center !important;">
                            <tr>
                                <td class="border">
                                  @if ($data->bumiputera=="Y") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="30px"><b>Yes / </b><i>Ya</i></td>
                                <td width="37px">&nbsp;</td>
                                <td class="border">
                                  @if ($data->race=="N") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="40px"><b>No / </b><i>Tidak</i></td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        <table>
                <tr>
                    <td style="width: 80px"><b>Religion/</b><i>Agama</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="2" style="text-align: center !important;">
                            <tr>
                                <td class="border">
                                  @if ($data->religion=="I") 
                                   <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="55px"><b>Islam /</b><i> Islam</i></td>
                                <td width="17px">&nbsp;</td>
                                <td class="border">
                                  @if ($data->religion=="H") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="43"><b>Hindu / </b><i>Hindu</i></td>
                                <td width="17px">&nbsp;</td>
                                <td class="border">
                                  @if ($data->religion=="C") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="78px"><b>Christian / </b><i>Kristian</i></td>
                                <td width="3px">&nbsp;</td>
                                <td class="border">
                                  @if ($data->religion=="B") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="70px"><b>Buddhist / </b><i>Buda</i></td>
                                <td width="3px">&nbsp;</td>
                                <td class="border">
                                    @if (($data->religion!="I") AND ($data->religion!="B") AND ($data->religion!="H"))
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                    @else
                                        &nbsp;&nbsp;
                                    @endif
                                </td>
                                <td width="80px"><b>Others /</b><i>Lain-lain </i></td>
                                <td><u>{{ $data->title}}</u></td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
        <table>

                <tr>
                    <td style="width: 80px"><b>Marital Status</b><br><i>Taraf Perkahwinan</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="2" style="text-align: center !important;">
                            <tr>
                                <td class="border">
                                  @if ($data->marital=="M") 
                                   <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="70px"><b>Married / </b><i>Berkahwin </i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border">
                                  @if ($data->marital=="S") 
                                   <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="60px"><b>Single/</b><i>Bujang</i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border">
                                  @if (($data->marital=="D") OR ($data->marital=="W"))
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="80px"><b>Divorced/Widow/Widower / </b><i>Bercerai/Duda/Balu </i></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="width: 80px"><b>No of Dependants</b><br><i>Jumlah Tanggungan</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="2" style="text-align: center !important;">
                            <tr>
                                @if($data->dependents>=10)
                                <td class="border"> {{ substr($data->dependents,0,1) }} </td>
                                <td class="border"> {{ substr($data->dependents,1,1) }} </td>
                                @else
                                <td class="border"> 0</td>
                                <td class="border"> {{ substr($data->dependents,0,1) }} </td>
                                @endif
                                 <td>
                                    <table align="" class="" cellpadding="0" style="text-align: center !important;">
                                        <tr><td><b>Person</b> / <i>Orang</i></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td style="width: 80px"><b>Education Level</b><br><i>Taraf Pendidikan</i></td>
                    <td style="width: 10px">:</td>
                    <td>
                        <table align="" class="" cellpadding="2" >
                            <tr>
                                <td class="border" style="text-align: center !important;">
                                  @if ($data->education=="PS") 
                                   <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="40px"><b>Primary/</b><i>Rendah </i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center !important;">
                                  @if ($data->education=="SS") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="40px"><b>Secondary/</b><i> Menengah </i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center !important;">
                                  @if ($data->education=="DI") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="90px"><b>Diploma/</b><i> Diploma </i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center !important;">
                                  @if ($data->education=="DG") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="80px"><b>Degree/</b><i>Ijazah Sarjana Muda </i></td>
                            </tr>
                            <tr>
                                <br>
                                <td class="border">
                                  @if ($data->education=="MA") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="80px"><b>Masters/</b><i>Ijazah Sarjana </i></td>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center !important;">
                                  @if ($data->education=="PH") 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="80px"><b>PhD/</b><i>Kedoktoran </i></td>
                            </tr>
                        </table>
                    </td>
                </tr>
    </table>
    <!--PAGE 2
    width="100%" class="border" style="border: 0px!important" cellpadding='1'-->
    <table width="100%"  cellspacing="5"  style="page-break-before: always" id="customers"> 
        <div class="row">
            <div class="column">
                <table>
                    <tr>
                        <td colspan="4" class="" style="width: 700px !important;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>EMPLOYMENT DETAILS (MAIN APPLICANT) /</b><i> BUTIR-BUTIR PEKERJAAN (PEMOHON UTAMA)</i>
                        </td>
                    </tr>   
                </table>
                <table style="" style="border-collapse: collapse;">
                    <tr>
                        <td style="width: 100px !important"><b>Employment Type</b><br><i>Jenis Pekerjaan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table style="text-align: center;">
                                <tr>
                                    <td class="border">
                                      @if ($data->EmpInfo->emptype=="115") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="125px"><b>Self-Employed/</b><i>Bekerja Sendiri</i></td>
                                    <td width="5px">&nbsp;&nbsp;</td>
                                    <td class="border">
                                      @if ($data->EmpInfo->emptype=="113") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="195px"><b>Salaried-Private Sector/</b><i>Bergaji-Sektor Swasta</i></td>
                                    <td class="border">
                                      @if ($data->EmpInfo->emptype=="112")
                                      <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                     <td width="170px"><b>Salaried-Government/</b><i>Bergaji-Kerajaan</i></td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td class="border">
                                      @if ($data->EmpInfo->emptype=="PPP") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="50px"><b>Professional</b> /<i> Profesional </i></td>
                                    <td width="22px">&nbsp;&nbsp;</td>
                                    <td class="border">
                                        @if ($data->EmpInfo->emptype=="ZZZ")
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                    </td>
                                    <td width="90px"><b>Others (Specify) /</b><i> Lain-lain (Nyatakan)</i></td>
                                    <td></td>
                                    <td>    
                                        @if ($data->EmpInfo->emptype=="ZZZ") 
                                            <u>{{$data->title}}</u>
                                        @else
                                            <u>_________________</u>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px !important"><b>Employment Status</b><br><i>Status Pekerjaan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="" cellpadding="1" style="text-align: center !important;">
                                <tr>
                                    <td class="border">
                                      @if ($data->EmpInfo->empstatus=="01") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td style="width: 100px !important"><b>Permanent</b> /<i> Tetap </i></td>
                                    <td class="border">
                                      @if ($data->EmpInfo->empstatus=="03")
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;&nbsp;
                                      @endif
                                    </td>
                                     <td style="width: 80px !important"><b>Contract</b> /<i> Kontrak </i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table>
                     <tr>
                        <td style="width: 100px"><b>Name of Employer</b><br><i>Nama Majikan/<br>Perniagaan</i></td>
                        <td style="width: 10px">:</td>
                         <td>
                            <table align="" class="" cellpadding="1" style="font-size:7px!important; text-align: center !important;border-collapse: collapse;">
                            <tr>
                               <?php for ($i=0; $i<=43;$i++) {
                                  if (substr($emp_name,$i,1)=="" OR substr($emp_name,$i,1)==" ") {
                                    echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                  }
                                  else { ?>
                                    <td class="border" style="font-size:9px!important; text-align: center !important;border-collapse: collapse;"> {{substr($emp_name,$i,1)}} </td>
                                    <?php
                                  }
                                }
                                ?>
                            </tr>
                            <tr>
                                <?php for ($i=43; $i<=86;$i++) {
                                  if (substr($emp_name,$i,1)=="") {
                                    if (substr($emp_name,$i-43,1)=="") {
                                      echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                    }
                                    else {
                                      ?>
                                      <td class="border" style="text-align: center !important; font-weight: bold !important;"><font color="white">{{ substr($emp_name,$i-40,1) }}</font> </td>
                                    <?php

                                    }
                                  }
                                  else { ?>
                                    <td class="border" style="text-align: center !important; font-weight: bold !important;">{{ substr($emp_name,$i,1) }}</td>
                                    <?php
                                  }
                                }
                                ?>
                            </tr>
                        </table>
                        </td>

                    </tr>
                    <tr>
                        <td style="width: 80px"><b>Department Name</b><br><i>Nama Jabatan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="border" cellpadding="1">
                                <tr style="color:black !important">
                                   <?php for ($i=0; $i<=40;$i++) {
                                      if (substr($dept_name,$i,1)=="" OR substr($dept_name,$i,1)==" ") {
                                        echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                      }
                                      else { ?>
                                        <td class="border" style="text-align: center !important;"> {{substr($dept_name,$i,1)}} </td>
                                        <?php
                                      }
                                    }
                                    ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 80px"><b>Division & Unit</b><br><i>Bahagian & Unit</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="border" cellpadding="1">
                                <tr style="color:black !important">
                                   <?php for ($i=0; $i<=40;$i++) {
                                      if (substr($div_name,$i,1)=="" OR substr($div_name,$i,1)==" ") {
                                        echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                      }
                                      else { ?>
                                        <td class="border" style="text-align: center !important;"> {{substr($div_name,$i,1)}} </td>
                                        <?php
                                      }
                                    }
                                    ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 100px"><b>Address of Employer/<br>Business</b><br><i>Alamat Majikan/<br>Perniagaan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="border" cellpadding="2" cellspacing="1" style="font-size:7px!important; text-align: center !important;border-collapse: collapse;">
                                <tr>
                                    <?php for ($i=0; $i<=37;$i++) {
                                          if (substr($data->EmpInfo->address,$i,1)=="" OR substr($data->EmpInfo->address,$i,1)==" ") {
                                            echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                          }
                                          else { ?>
                                            <td class="border">{{substr(strtoupper($data->EmpInfo->address),$i,1)}}</td>
                                            <?php
                                          }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <?php for ($i=0; $i<=37;$i++) {
                                          if (substr($data->EmpInfo->address2,$i,1)=="" OR substr($data->EmpInfo->address2,$i,1)==" ") {
                                            echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                          }
                                          else { ?>
                                            <td class="border"> {{substr(strtoupper($data->EmpInfo->address2),$i,1)}} </td>
                                            <?php
                                          }
                                        }
                                    ?>
                                </tr>
                                <tr>
                                    <?php for ($i=0; $i<=37;$i++) {
                                          if (substr($data->EmpInfo->address3,$i,1)=="" OR substr($data->EmpInfo->address3,$i,1)==" ") {
                                            echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                          }
                                          else { ?>
                                            <td class="border">{{substr(strtoupper($data->EmpInfo->address3),$i,1)}} </td>
                                            <?php
                                          }
                                        }
                                    ?>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table  width="100%" class="border" style="font-size: 9px; border: 0px!important;text-align: center;" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 120px">&nbsp;</td>
                                    <td width="50px"><b>State</b><br><i>Negeri</i><br></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">
                                            <tr>
                                                <td class="border"> {{ substr($state_emp,0,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,1,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,2,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,3,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,4,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,5,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,6,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,7,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,8,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,9,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,10,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,11,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,12,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,13,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,14,1) }} </td>
                                                <td class="border"> {{ substr($state_emp,15,1) }} </td> 
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px">&nbsp;</td>
                                    <td width="50px"><b>Postcode</b><br><i>Poskod</i><br></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">
                                            <tr>
                                                <td class="border">{{substr($data->EmpInfo->postcode,0,1)}}</td>
                                                <td class="border">{{substr($data->EmpInfo->postcode,1,1)}}</td>
                                                <td class="border">{{substr($data->EmpInfo->postcode,2,1)}}</td>
                                                <td class="border">{{substr($data->EmpInfo->postcode,3,1)}}</td>
                                                <td class="border">{{substr($data->EmpInfo->postcode,4,1)}}</td>
                                          </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width:100px"><b>Nature of Business</b><br><i>Jenis Perniagaan</i></td>
                        <td style="width:10px">:</td>
                        <td class="classbordernature">{{$data->EmpInfo->Nature->occupation_desc}}</td>
                    </tr>
                </table>
                <table  width="100%" class="border" style="font-size: 9px; border: 0px!important;text-align: center;" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>  
                                    <td style="width: 100px"> <b>Start Date of Work</b><br><i>Tarikh Mula Bekerja</i></td>
                                    <td style="width: 10px">:</td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">
                                            <tr>
                                                <td class="border"> {{substr($date_working,0,1)}} </td>
                                                <td class="border"> {{substr($date_working,1,1)}}</td>
                                                <td style="text-align: center;" width="10px" >/</td>
                                                <td class="border"> {{substr($date_working,3,1)}} </td>
                                                <td class="border"> {{substr($date_working,4,1)}} </td>
                                                <td style="text-align: center;" width="10px">/</td>
                                                <td class="border"> {{substr($date_working,6,1)}} </td>
                                                <td class="border"> {{substr($date_working,7,1)}} </td>
                                                <td class="border"> {{substr($date_working,8,1)}} </td>
                                                <td class="border"> {{substr($date_working,9,1)}} </td> 
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px">&nbsp;</td>
                                    <td width="130px"><b>Total Working Experience</b><br><i>Jumlah Pengalaman Bekerja</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">
                                            <tr>
                                                @if($total_working>=10)
                                                    <td class="border"> {{ substr($total_working,0,1) }} </td>
                                                    <td class="border"> {{ substr($total_working,1,1) }} </td>
                                                @else
                                                    <td class="border"> 0</td>
                                                    <td class="border"> {{ substr($total_working,0,1) }} </td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td style="width: 100px"> <b>Position</b><br><i>Jawatan</i></td>
                        <td style="width: 10px">:</td>
                        <td class="classbordernature"> {{$data->EmpInfo->PositionDesc->position_desc}}  </td>
                    </tr>
                </table>
                <table width="100%" class="border" style="font-size: 9px; border: 0px!important;text-align: center;" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>  
                                    <td style="width: 110px">&nbsp;</td>
                                    <td style="width: 80px"><b>Office Telephone</b><br><i>Telefon Pejabat</i></td>
                                    <td>
                                        <?php
                                            $office_phone1 =substr($data->EmpInfo->office_phone, 0, 1);
                                            $office_phone2 =substr($data->EmpInfo->office_phone, 1, 1);
                                            $office_phone3 =substr($data->EmpInfo->office_phone, 2, 1);
                                            $office_phone4 =substr($data->EmpInfo->office_phone, 3, 1);
                                            $office_phone5 =substr($data->EmpInfo->office_phone, 4, 1);
                                            $office_phone6 =substr($data->EmpInfo->office_phone, 5, 1);
                                            $office_phone7 =substr($data->EmpInfo->office_phone, 6, 1);
                                            $office_phone8 =substr($data->EmpInfo->office_phone, 7, 1);
                                            $office_phone9 =substr($data->EmpInfo->office_phone, 8, 1);
                                            $office_phone10 =substr($data->EmpInfo->office_phone, 9, 1);
                                            $office_phone11 =substr($data->EmpInfo->office_phone, 10, 1);
                                            $office_phone12 =substr($data->EmpInfo->office_phone, 11, 1);
                                        ?>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">{{$office_phone11}}
                                            <tr>
                                                <td class="border">@if(!empty($office_phone1)) {{$office_phone1}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($office_phone2)) {{$office_phone2}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($office_phone3)) {{$office_phone3}} @else &nbsp;&nbsp; @endif</td>
                                                <td style="text-align: center;" width="10px">-</td>
                                                <td class="border">@if(!empty($office_phone4)) {{$office_phone4}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($office_phone5)) {{$office_phone5}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($office_phone6)) {{$office_phone6}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($office_phone7)) {{$office_phone7}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($office_phone8)){{$office_phone8}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($office_phone9)) {{$office_phone9}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($office_phone10)) {{$office_phone10}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($office_phone11)) {{$office_phone11}} @else &nbsp;&nbsp; @endif</td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px">&nbsp;</td>
                                    <td width="70px"><b>Office Fax</b><br> <i>Faks Pejabat</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">
                                            <tr>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,0,1)}} </td>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,1,1)}}</td>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,3,1)}} </td>
                                                <td style="text-align: center;" width="10px">-</td>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,4,1)}} </td>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,6,1)}} </td>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,7,1)}} </td>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,8,1)}} </td>
                                                <td class="border"> {{substr($data->EmpInfo->office_phone,9,1)}} </td>  
                                                <td class="border">{{substr($data->EmpInfo->office_phone,10,1)}} </td>  
                                                <td class="border">{{substr($data->EmpInfo->office_phone,11,1)}} </td>  
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table width="100%" class="border" style="border: 0px!important" cellpadding='1' style="font-size: 9px !important">
                </table>
                <table>
                    <tr>
                        <td colspan="4" class="" style="width: 700px !important;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>PARTICULARS OF SPOUSE /</b><i> MAKLUMAT PASANGAN</i>
                        </td>
                    </tr>   
                </table>
                <table>
                    <tr>
                        <td style="width: 100px"><b>Full Name</b><br><i>Nama Penuh</i></td>
                        <td style="width: 10px">:</td>
                        @if(!empty($data->Spouse->name))
                        <td class="classborderspouse">{{$data->Spouse->name}}</td>
                        @else
                        <td class="classborderspouse">&nbsp;</td>
                        @endif
                    </tr>
                </table>
                <table width="100%" class="border" style="font-size: 9px; border: 0px!important;text-align: center;" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>  
                                    <td style="width: 110px">&nbsp;</td>
                                    <td style="width: 120px"><b>Home/Office Telephone</b><br><i>Telefon  Rumah/Pejabat</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">
                                            <?php
                                                $spphone1 =substr($data->Spouse->homephone, 0, 1);
                                                $spphone2 =substr($data->Spouse->homephone, 1, 1);
                                                $spphone3 =substr($data->Spouse->homephone, 2, 1);
                                                $spphone4 =substr($data->Spouse->homephone, 3, 1);
                                                $spphone5 =substr($data->Spouse->homephone, 4, 1);
                                                $spphone6 =substr($data->Spouse->homephone, 5, 1);
                                                $spphone7 =substr($data->Spouse->homephone, 6, 1);
                                                $spphone8 =substr($data->Spouse->homephone, 7, 1);
                                                $spphone9 =substr($data->Spouse->homephone, 8, 1);
                                                $spphone10 =substr($data->Spouse->homephone, 9, 1);
                                                $spphone11 =substr($data->Spouse->homephone, 10, 1);
                                            ?>
                                            <tr>
                                                <td class="border">@if(empty($spphone1)) &nbsp;&nbsp;  @else {{$spphone1}} @endif</td>
                                                <td class="border">@if(!empty($spphone2)) {{$spphone2}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($spphone3)) {{$spphone3}} @else &nbsp;&nbsp; @endif</td>
                                                <td style="text-align: center;" width="10px">-</td>
                                                <td class="border">@if(!empty($spphone4)) {{$spphone4}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($spphone5)) {{$spphone5}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spphone6)) {{$spphone6}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spphone7)) {{$spphone7}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spphone8)){{$spphone8}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spphone9)) {{$spphone9}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spphone10)) {{$spphone10}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spphone11)) {{$spphone11}} @else &nbsp;&nbsp; @endif</td> 
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px">&nbsp;</td>
                                    <td width="70px"><b>Mobile Phone</b><br> <i>Telefon Bimbit</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="font-size: 9px; border: 0px!important;text-align: center;">
                                            <?php
                                                $spmphone1 =substr($data->Spouse->mobilephone, 0, 1);
                                                $spmphone2 =substr($data->Spouse->mobilephone, 1, 1);
                                                $spmphone3 =substr($data->Spouse->mobilephone, 2, 1);
                                                $spmphone4 =substr($data->Spouse->mobilephone, 3, 1);
                                                $spmphone5 =substr($data->Spouse->mobilephone, 4, 1);
                                                $spmphone6 =substr($data->Spouse->mobilephone, 5, 1);
                                                $spmphone7 =substr($data->Spouse->mobilephone, 6, 1);
                                                $spmphone8 =substr($data->Spouse->mobilephone, 7, 1);
                                                $spmphone9 =substr($data->Spouse->mobilephone, 8, 1);
                                                $spmphone10 =substr($data->Spouse->mobilephone, 9, 1);
                                                $spmphone11 =substr($data->Spouse->mobilephone, 10, 1);
                                            ?>
                                            <tr>
                                                 <td class="border">@if(empty($spmphone1)) &nbsp;&nbsp;  @else {{$spmphone1}} @endif</td>
                                                <td class="border">@if(!empty($spmphone2)) {{$spmphone2}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($spmphone3)) {{$spmphone3}} @else &nbsp;&nbsp; @endif</td>
                                                <td style="text-align: center;" width="10px">-</td>
                                                <td class="border">@if(!empty($spmphone4)) {{$spmphone4}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($spmphone5)) {{$spmphone5}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spmphone6)) {{$spmphone6}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spmphone7)) {{$spmphone7}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spmphone8)){{$spmphone8}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spmphone9)) {{$spmphone9}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spphone10)) {{$spmphone10}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($spmphone11)) {{$spmphone11}} @else &nbsp;&nbsp; @endif</td> 
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table style="">
                    <tr>
                        <td style="width: 100px !important"><b>Employment Type</b><br><i>Jenis Pekerjaan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table style="text-align: center;">
                                <tr>
                                    <td class="border">
                                      @if ($data->Spouse->emptype=="115") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="125px"><b>Self-Employed/</b><i>Bekerja Sendiri</i></td>
                                    <td width="5px">&nbsp;&nbsp;</td>
                                    <td class="border">
                                      @if ($data->Spouse->emptype=="113") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="195px"><b>Salaried-Private Sector/</b><i>Bergaji-Sektor Swasta</i></td>
                                    <td class="border">
                                      @if ($data->Spouse->emptype=="112")
                                      <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                     <td width="170px"><b>Salaried-Government/</b><i>Bergaji-Kerajaan</i></td>
                                </tr>
                            </table>
                            <table>
                                <tr>
                                    <td class="border">
                                      @if ($data->Spouse->emptype=="PPP") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="50px"><b>Professional</b> /<i> Profesional </i></td>
                                    <td width="22px">&nbsp;&nbsp;</td>
                                    <td class="border">
                                        @if ($data->Spouse->emptype=="ZZZ")
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                    </td>
                                    <td width="90px"><b>Others (Specify) /</b><i> Lain-lain (Nyatakan)</i></td>
                                    <td></td>
                                    <td>    
                                        @if ($data->Spouse->emptype=="ZZZ") 
                                            <u>{{$data->Spouse->emptype_others}}</u>
                                        @else
                                            <u>_________________</u>
                                        @endif
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan="4" class="" style="width: 700px !important;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>EMERGENCY CONTACT /</b><i> RUJUKAN KECEMASAN</i>
                        </td>
                    </tr>   
                </table>
                <strong style="font-size: 9px;color: #2751a6">** (Family Members/Close relatives not staying with you) /</strong><em style="font-size: 9px;color: #2751a6"> (Ahli Keluarga/Saudara terdekat yang tidak tinggal bersama anda)</em><br>
                <table width="27%" style="background-color: #adc5f7;font-size: 9px;color: blue" border="0">
                    <tbody>
                    <tr>
                    <td><b>Contact 1 /</b> <i>Rujukan 1</i></td>
                    </tr>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="width: 80px"> <b>Full Name </b><br> <i>Nama Penuh</i></td>
                        <td style="width: 10px">:</td>
                        <td class="classbordercontact">{{$data->Ref->name1}}</td>
                    </tr>
                </table>
                <table  width="100%" class="border" style="font-size: 9px; border: 0px!important" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 100px">&nbsp;</td>
                                    <td width="80px"><b>Home Telephone</b><BR><i> Telefon Rumah</i><br></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                             <?php
                                                $refphone1 =substr($data->Ref->home_phone1, 0, 1);
                                                $refphone2 =substr($data->Ref->home_phone1, 1, 1);
                                                $refphone3 =substr($data->Ref->home_phone1, 2, 1);
                                                $refphone4 =substr($data->Ref->home_phone1, 3, 1);
                                                $refphone5 =substr($data->Ref->home_phone1, 4, 1);
                                                $refphone6 =substr($data->Ref->home_phone1, 5, 1);
                                                $refphone7 =substr($data->Ref->home_phone1, 6, 1);
                                                $refphone8 =substr($data->Ref->home_phone1, 7, 1);
                                                $refphone9 =substr($data->Ref->home_phone1, 8, 1);
                                                $refphone10 =substr($data->Ref->home_phone1, 9, 1);
                                                $refphone11 =substr($data->Ref->home_phone1, 10, 1);
                                            ?>
                                            <tr>
                                                <td class="border">@if(!empty($refphone1)) {{$refphone1}} @else {{$refphone1}}  @endif</td>
                                                <td class="border">@if(!empty($refphone2)) {{$refphone2}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($refphone3)) {{$refphone3}} @else &nbsp;&nbsp; @endif</td>
                                                 <td class="no-border-top">-</td>
                                                <td class="border">@if(!empty($refphone4)) {{$refphone4}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($refphone5)) {{$refphone5}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refphone6)) {{$refphone6}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refphone17)) {{$refphone7}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refphone8)){{$refphone8}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refphone9)) {{$refphone9}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refphone10)) {{$refphone10}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refphone11)) {{$refphone11}} @else &nbsp;&nbsp; @endif</td> 
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px">&nbsp;</td>
                                    <td width="80px"><b>Mobile Phone</b><BR><i> Telefon Bimbit</i><br></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <?php
                                                $refmobilephone1 =substr($data->Ref->mobilephone1, 0, 1);
                                                $refmobilephone2 =substr($data->Ref->mobilephone1, 1, 1);
                                                $refmobilephone3 =substr($data->Ref->mobilephone1, 2, 1);
                                                $refmobilephone4 =substr($data->Ref->mobilephone1, 3, 1);
                                                $refmobilephone5 =substr($data->Ref->mobilephone1, 4, 1);
                                                $refmobilephone6 =substr($data->Ref->mobilephone1, 5, 1);
                                                $refmobilephone7 =substr($data->Ref->mobilephone1, 6, 1);
                                                $refmobilephone8 =substr($data->Ref->mobilephone1, 7, 1);
                                                $refmobilephone9 =substr($data->Ref->mobilephone1, 8, 1);
                                                $refmobilephone10 =substr($data->Ref->mobilephone1, 9, 1);
                                                $refmobilephone11 =substr($data->Ref->mobilephone1, 10, 1);
                                            ?>
                                            <tr>
                                                <td class="border">@if(!empty($refmobilephone1)) {{$refmobilephone1}} @else {{$refmobilephone1}} @endif</td>
                                                <td class="border">@if(!empty($refmobilephone2)) {{$refmobilephone2}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($refmobilephone3)) {{$refmobilephone3}} @else &nbsp;&nbsp; @endif</td>
                                                 <td class="no-border-top">-</td>
                                                <td class="border">@if(!empty($refmobilephone444)) {{$refmobilephone4}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($refmobilephone5)) {{$refmobilephone5}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refmobilephone6)) {{$refmobilephone6}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refmobilephone7)) {{$refmobilephone7}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refmobilephone8)){{$refmobilephone8}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refmobilephone9)) {{$refmobilephone9}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refmobilephone10)) {{$refmobilephone10}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refmobilephone11)) {{$refmobilephone11}} @else &nbsp;&nbsp; @endif</td> 
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table> 
                    <tr>
                        <td style="width: 80px"> <b>Relationship </b><br><i>Hubungan</i></td>
                        <td style="width: 10px">:</td>
                        <td class="classborderrelation">{{$data->Ref->Relationship1->relationship_desc}}</td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table width="27%" style="background-color:#adc5f7;font-size: 9px;color: blue" border="0">
                    <tbody>
                        <tr>
                            <td><b>Contact 2 /</b> <i>Rujukan 2</i></td>
                        </tr>
                    </tbody>
                </table>
                <table>
                    <tr>
                        <td style="width: 80px"> <b>Full Name </b><br> <i>Nama Penuh</i></td>
                        <td style="width: 10px">:</td>
                        <td class="classbordercontact">{{$data->Ref->name2}}</td>
                    </tr>
                </table>
                <table  width="100%" class="border" style="font-size: 9px; border: 0px!important" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 100px">&nbsp;</td>
                                    <td width="80px"><b>Home Telephone</b><BR><i> Telefon Rumah</i><br></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                          <tr>
                                             <?php
                                                $ref2phone1 =substr($data->Ref->home_phone2, 0, 1);
                                                $ref2phone2 =substr($data->Ref->home_phone2, 1, 1);
                                                $ref2phone3 =substr($data->Ref->home_phone2, 2, 1);
                                                $ref2phone4 =substr($data->Ref->home_phone2, 3, 1);
                                                $ref2phone5 =substr($data->Ref->home_phone2, 4, 1);
                                                $ref2phone6 =substr($data->Ref->home_phone2, 5, 1);
                                                $ref2phone7 =substr($data->Ref->home_phone2, 6, 1);
                                                $ref2phone8 =substr($data->Ref->home_phone2, 7, 1);
                                                $ref2phone9 =substr($data->Ref->home_phone2, 8, 1);
                                                $ref2phone10 =substr($data->Ref->home_phone2, 9, 1);
                                                $ref2phone11 =substr($data->Ref->home_phone2, 10, 1);
                                            ?>
                                             <td class="border">@if(!empty($ref2phone1)) {{$ref2phone1}} @else {{$ref2phone1}} @endif</td>
                                                <td class="border">@if(!empty($ref2phone2)) {{$ref2phone2}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($ref2phone3)) {{$ref2phone3}} @else &nbsp;&nbsp; @endif</td>
                                                 <td class="no-border-top">-</td>
                                                <td class="border">@if(!empty($ref2phone4)) {{$ref2phone4}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($ref2phone5)) {{$ref2phone5}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2phone6)) {{$ref2phone6}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2phone17)) {{$refphone7}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2phone8)){{$ref2phone8}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2phone9)) {{$ref2phone9}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2phone10)) {{$ref2phone10}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2phone11)) {{$ref2phone11}} @else &nbsp;&nbsp; @endif</td> 
                                        </table>
                                    </td>
                                    <td style="width: 20px">&nbsp;</td>
                                    <td width="80px"><b>Mobile Phone</b><BR><i> Telefon Bimbit</i><br></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <?php
                                                $ref2mobilephone1 =substr($data->Ref->mobilephone2, 0, 1);
                                                $ref2mobilephone2 =substr($data->Ref->mobilephone2, 1, 1);
                                                $ref2mobilephone3 =substr($data->Ref->mobilephone2, 2, 1);
                                                $ref2mobilephone4 =substr($data->Ref->mobilephone2, 3, 1);
                                                $ref2mobilephone5 =substr($data->Ref->mobilephone2, 4, 1);
                                                $ref2mobilephone6 =substr($data->Ref->mobilephone2, 5, 1);
                                                $ref2mobilephone7 =substr($data->Ref->mobilephone2, 6, 1);
                                                $ref2mobilephone8 =substr($data->Ref->mobilephone2, 7, 1);
                                                $ref2mobilephone9 =substr($data->Ref->mobilephone2, 8, 1);
                                                $ref2mobilephone10 =substr($data->Ref->mobilephone2, 9, 1);
                                                $ref2mobilephone11 =substr($data->Ref->mobilephone2, 10, 1);
                                            ?>
                                            <tr>
                                                <td class="border"> @if(!empty($ref2mobilephone1)) {{$ref2mobilephone1}} @else {{$ref2mobilephone1}}  @endif</td>
                                                <td class="border">@if(!empty($ref2mobilephone2)) {{$ref2mobilephone2}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($ref2mobilephone3)) {{$ref2mobilephone3}} @else &nbsp;&nbsp; @endif</td>
                                                 <td class="no-border-top">-</td>
                                                <td class="border">@if(!empty($ref2mobilephone4)) {{$ref2mobilephone4}} @else &nbsp;&nbsp; @endif</td>
                                                <td class="border">@if(!empty($ref2mobilephone5)) {{$refmobilephone5}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2mobilephone6)) {{$ref2mobilephone6}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2mobilephone7)) {{$ref2mobilephone7}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($refmobilephone8)){{$ref2mobilephone8}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2mobilephone9)) {{$ref2mobilephone9}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2mobilephone10)) {{$ref2mobilephone10}} @else &nbsp;&nbsp; @endif</td>
                                               <td class="border">@if(!empty($ref2mobilephone11)) {{$ref2mobilephone11}} @else &nbsp;&nbsp; @endif</td> 
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table> 
                     <tr>
                        <td style="width: 80px"> <b>Relationship </b><br><i>Hubungan</i></td>
                        <td style="width: 10px">:</td>
                        <td class="classborderrelation">{{$data->Ref->Relationship2->relationship_desc}}</td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <td colspan="4" class="" style="width: 700px !important;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>INCOME INFORMATION (MAIN APPLICANT) / </b><i>MAKLUMAT PENDAPATAN (PEMOHON UTAMA)</i>
                        </td>
                    </tr>   
                </table>

                <table  width="100%" class="border" style="border: 0px!important;font-size: 9px" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 90px!important"><b>Monthly Income</b><i>Pendapatan Bulanan</i></td>
                                    <td><b>RM</b></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2">
                                            <tr>
                                                @if(!empty($monthly_income))
                                                <td class="border"> {{ substr($monthly_income,0,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,1,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,2,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,3,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,4,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,5,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,6,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,7,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,8,1) }} </td>
                                                <td class="border"> {{ substr($monthly_income,9,1) }} </td>
                                                @else
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 76px">&nbsp;&nbsp;&nbsp;</td>
                                    <td style="width: 70px"><b>Other Income</b><br><i>Pendapatan Lain</i>
                                    </td>
                                    <td><b>RM</b></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2">
                                            <tr>
                                                @if(!empty($other_income))
                                                <td class="border"> {{ substr($other_income,0,1) }} </td>
                                                <td class="border"> {{ substr($other_income,1,1) }} </td>
                                                <td class="border"> {{ substr($other_income,2,1) }} </td>
                                                <td class="border"> {{ substr($other_income,3,1) }} </td>
                                                <td class="border"> {{ substr($other_income,4,1) }} </td>
                                                <td class="border"> {{ substr($other_income,5,1) }} </td>
                                                <td class="border"> {{ substr($other_income,6,1) }} </td>
                                                <td class="border"> {{ substr($other_income,7,1) }} </td>
                                                <td class="border"> {{ substr($other_income,8,1) }} </td>
                                                <td class="border"> {{ substr($other_income,9,1) }} </td>
                                                @else
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 80px"> <b>Total Income</b><br> <i>Jumlah Pendapatan</i></td>
                                    <td><b>RM</b></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2">
                                            <tr>
                                                @if(!empty($total_income))
                                                <td class="border"> {{ substr($total_income,0,1) }} </td>
                                                <td class="border"> {{ substr($total_income,1,1) }} </td>
                                                <td class="border"> {{ substr($total_income,2,1) }} </td>
                                                <td class="border"> {{ substr($total_income,3,1) }} </td>
                                                <td class="border"> {{ substr($total_income,4,1) }} </td>
                                                <td class="border"> {{ substr($total_income,5,1) }} </td>
                                                <td class="border"> {{ substr($total_income,6,1) }} </td>
                                                <td class="border"> {{ substr($total_income,7,1) }} </td>
                                                <td class="border"> {{ substr($total_income,8,1) }} </td>
                                                <td class="border"> {{ substr($total_income,9,1) }} </td>
                                                @else
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                </table>

               <!--<table style="font-size: 9px !important">
                     <tr>
                        <td style="width: 80px"> <b>Monthly Income</b><br><i>Pendapatan Bulanan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="" cellpadding="2" style="text-align: center !important;border-collapse: collapse;">
                                 <tr>
                                    <td><b>RM</b>&nbsp;&nbsp;</td>
                                    <td class="border"> {{ substr($monthly_income,0,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,1,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,2,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,3,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,4,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,5,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,6,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,7,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,8,1) }} </td>
                                    <td class="border"> {{ substr($monthly_income,9,1) }} </td>
                                    <td style="width: 30px"></td>
                                    <td style="width: 80px"> <b>Other Income</b><br><i>Pendapatan Lain</i></td>
                                    <td style="width: 10px">:</td>
                                    <td><b>RM</b>&nbsp;&nbsp;</td>
                                    <td class="border"> {{substr($other_income,0,1)}} </td>
                                    <td class="border"> {{substr($other_income,1,1)}}</td>
                                    <td class="border"> {{substr($other_income,2,1)}}</td>
                                    <td class="border"> {{substr($other_income,3,1)}} </td>
                                    <td class="border"> {{substr($other_income,4,1)}} </td>
                                    <td class="border"> {{substr($other_income,6,1)}} </td>
                                    <td class="border"> {{substr($other_income,7,1)}} </td>
                                    <td class="border"> {{substr($other_income,8,1)}} </td>
                                    <td class="border"> {{substr($other_income,9,1)}} </td>
                                 </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 80px"> <b>Total Income</b><br><i>Jumlah Pendapatan</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="" cellpadding="2" style="text-align: center !important;border-collapse: collapse;">
                                 <tr>
                                    <td><b>RM</b>&nbsp;&nbsp;</td>
                                    <td class="border"> {{ substr($total_income,0,1) }} </td>
                                    <td class="border"> {{ substr($total_income,1,1) }} </td>
                                    <td class="border"> {{ substr($total_income,2,1) }} </td>
                                    <td class="border"> {{ substr($total_income,3,1) }} </td>
                                    <td class="border"> {{ substr($total_income,4,1) }} </td>
                                    <td class="border"> {{ substr($total_income,5,1) }} </td>
                                    <td class="border"> {{ substr($total_income,6,1) }} </td>
                                    <td class="border"> {{ substr($total_income,7,1) }} </td>
                                    <td class="border"> {{ substr($total_income,8,1) }} </td>
                                    <td class="border"> {{ substr($total_income,9,1) }} </td>
                                 </tr>
                            </table>
                        </td>
                    </tr>
                </table>-->

                <table>
                    <tr>
                        <td colspan="4" class="" style="width: 700px !important;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>YOUR COMMITMENTS WITH OTHER CREDIT PROVIDERS (NON-BANKS ONLY)** /</b><i>  KOMITMEN DENGAN PEMBIAYA KREDIT LAIN (BUKAN BANK SAHAJA)**</i>
                        </td>
                    </tr>   
                </table>
                <strong style="font-size: 9px;color: #2751a6">** (e.g. AEON Credit, PTPTN, MARA, etc.) /</strong><em style="font-size: 9px;color: #2751a6"> (cth. AEON Credit, PTPTN, MARA, dll.)</em><br>
                    <table width="27%" style="background-color:#adc5f7;font-size: 9px;color: blue" border="0">
                        <tbody>
                        <tr>
                        <td><b>Commitment 1 /</b> <i>Komitmen 1</i></td>
                        </tr>
                        </tbody>
                    </table>

                <!--<table style="width: 670px;font-size: 9px !important">
                     <tr>
                        <td style="width: 80px"> <b>Name of Entity /</b> <i>Nama Entiti</i></td>
                        <td style="width: 10px">:</td>
                        <td style="width: 80px">{{$data->Commitments->name1}}</td>
                        <td style="width: 80px"> <b>Type of Finaning /</b> <i>Jenis Pembiayaan</i></td>
                        <td style="width: 10px">:</td>
                        <td>{{$data->Commitments->name1}}</td>
                    </tr>                </table>-->
                <table  width="100%" class="border" style="border: 0px!important" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 80px"> <b>Name of Entity /</b> <i>Nama Entiti</i></td>
                                    <td style="width: 10px">:</td>
                                    @if(!empty($data->Commitments->name1))
                                    <td style="width: 230px" class="classborder">{{$data->Commitments->name1}}</td>
                                    @else
                                    <td style="width: 230px" class="classborder">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                    <td style="width: 10px">&nbsp;&nbsp;&nbsp;</td>
                                    <td style="width: 100px !important"> <b>Type of Financing </b><br> <i>Jenis Pembiayaan</i></td>
                                    <td style="width: 10px">:</td>
                                    @if(!empty($data->Commitments->name1))
                                    <td style="width: 230px" class="classborder">{{$data->Commitments->name1}}</td>
                                    @else
                                    <td style="width: 230px" class="classborder">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                </tr>
                            </table>
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 80px"> <b>Monthly Payment</b><br> <i>Bayaran Bulanan</i></td>
                                    <td style="width: 10px">:</td>
                                    <td><b>RM</b></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2">
                                            <tr>
                                                @if(!empty($monthly_com1))
                                                <td class="border"> {{ substr($monthly_com1,0,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,1,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,2,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,3,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,4,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,5,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,6,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,7,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,8,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com1,9,1) }} </td>
                                                @else
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 76px">&nbsp;&nbsp;&nbsp;</td>
                                    <td style="width: 140px"><b>Remaining Financing Term</b><br><i>Baki Tempoh Pembiayaan</i>
                                    </td>
                                    <td style="width: 10px">:</td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2">
                                             <tr>
                                                @if(!empty($data->Commitments->remaining1))
                                                @if($data->Commitments->remaining1>=10)
                                                        <td class="border"> {{ substr($data->Commitments->remaining1,0,1) }} </td>
                                                        <td class="border"> {{ substr($data->Commitments->remaining1,1,1) }} </td>
                                                    @else
                                                        <td class="border"> 0</td>
                                                        <td class="border"> {{ substr($data->Commitments->remaining1,0,1) }} </td>
                                                    @endif
                                                @else
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                @endif
                                                Years
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px"><b>Years</b><br><i>Tahun</i>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table width="27%" style="background-color:#adc5f7;font-size: 9px;color: blue" border="0">
                        <tbody>
                        <tr>
                        <td><b>Commitment 2 /</b> <i>Komitmen 2</i></td>
                        </tr>
                        </tbody>
                    </table>
              <table  width="100%" class="border" style="border: 0px!important" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 80px"> <b>Name of Entity </b> <i>Nama Entiti</i></td>
                                    <td style="width: 10px">:</td>
                                    @if(!empty($data->Commitments->name2))
                                    <td style="width: 230px" class="classborder">{{$data->Commitments->name2}}</td>
                                    @else
                                    <td style="width: 230px" class="classborder">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                    <td style="width: 10px">&nbsp;&nbsp;&nbsp;</td>
                                    <td style="width: 100px !important"> <b>Type of Financing </b><br> <i>Jenis Pembiayaan</i></td>
                                    <td style="width: 10px">:</td>
                                    @if(!empty($data->Commitments->name2))
                                    <td style="width: 230px" class="classborder">{{$data->Commitments->name2}}</td>
                                    @else
                                    <td style="width: 230px" class="classborder">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                </tr>
                            </table>
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 80px"> <b>Monthly Payment</b><br> <i>Bayaran Bulanan</i></td>
                                    <td style="width: 10px">:</td>
                                    <td><b>RM</b></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2">
                                            <tr>
                                                @if(!empty($monthly_com2))
                                                <td class="border"> {{ substr($monthly_com2,0,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,1,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,2,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,3,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,4,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,5,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,6,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,7,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,8,1) }} </td>
                                                <td class="border"> {{ substr($monthly_com2,9,1) }} </td>
                                                @else
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 76px">&nbsp;&nbsp;&nbsp;</td>
                                    <td style="width: 140px"><b>Remaining Financing Term</b><br><i>Baki Tempoh Pembiayaan</i>
                                    </td>
                                    <td style="width: 10px">:</td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2">
                                             <tr>
                                                @if(!empty($data->Commitments->remaining2))
                                                    @if($data->Commitments->remaining2>=10)
                                                        <td class="border"> {{ substr($data->Commitments->remaining2,0,1) }} </td>
                                                        <td class="border"> {{ substr($data->Commitments->remaining2,1,1) }} </td>
                                                    @else
                                                        <td class="border"> 0</td>
                                                        <td class="border"> {{ substr($data->Commitments->remaining2,0,1) }} </td>
                                                    @endif
                                                @else
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;&nbsp;</td>
                                                @endif
                                                Years
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px"><b>Years</b><br><i>Tahun</i>
                                </tr>
                            </table>
                        </td>
                    </tr>
                   <!-- <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>-->
                </table>
             </div>
        </div>
    </table>
   
    <!--PAGE 3-->
    <table width="100%"  cellspacing="2" style="page-break-before: always" id="customers"> 
        <div class="row">
            <div class="column">
                <p><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><strong>SECTION B</strong> /<em> BAHAGIAN B&nbsp;</em><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;</span></p>
                <table>
                    <tr>
                         <td colspan="4" class="" style="width: 700px !important;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>FINANCING DETAILS</b> /<i> MAKLUMAT PEMBIAYAAN</i>
                        </td>
                    </tr>   
                </table>
                <table width="27%" style="background-color: #adc5f7;font-size: 9px;color: blue" border="0">
                    <tbody>
                    <tr>
                    <td><b>Facility Applied /</b> <i>Kemudahan yang dipohon</i></td>
                    </tr>
                    </tbody>
                </table>
                <table  width="100%" class="border" style="border: 0px!important" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 80px"> <b>Package Applied</b> <i>Pakej Dipohon</i></td>
                                    <td style="width: 10px">:</td>
                                    <td class="classborder">{{$pra->package->name}}</td>
                                    <td style="width: 10px">&nbsp;&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="40%" class=""></td>
                        <td width="60%" valign="" class=""></td>
                    </tr>
                </table>
                <table style="">
                    <tr>
                        <td style="width: 80px"><b>Product Bundling</b><br><i>Gabungan Produk</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="" cellpadding="2" style="text-align: center !important;">
                                <tr>
                                    <td class="border">
                                      @if($data->Financial->product_bundling=="1") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="50px"><b>Yes (Specify) / </b><i>Ya (Nyatakan)</i></td>
                                    @if($data->Financial->product_bundling=="0")
                                    <td>________________________________________________________</td>
                                    @else
                                    <td width="275px" style="border-bottom: 1px solid #2751a6 !important">{{$data->Financial->product_bundling_specify}}</td>
                                    @endif
                                    <td width="10px">&nbsp;</td>
                                    <td class="border">
                                      @if($data->Financial->product_bundling=="0") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="40px"><b>No</b><i>Tidak</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 80px"><b>Cross Selling</b><br><i>Jualan Silang</i></td>
                        <td style="width: 10px">:</td>
                        <td>
                            <table align="" class="" cellpadding="1" style="text-align: center !important;">
                                <tr>
                                    <td class="border">
                                      @if($data->Financial->cross_selling=="1") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                      @else
                                        &nbsp;&nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="50px"><b>Yes (Specify) / </b><i>Ya (Nyatakan)</i></td>
                                    @if($data->Financial->cross_selling=="0")
                                    <td>________________________________________________________</td>
                                    @else
                                    <td width="279px" style="border-bottom: 1px solid #2751a6 !important">{{$data->Financial->cross_selling_specify}}</td>
                                    @endif
                                    <td width="10px">&nbsp;</td>
                                    <td class="border">
                                      @if($data->Financial->cross_selling=="0") 
                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                      @else
                                        &nbsp;&nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="40px"><b>No</b><i>Tidak</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table style="background-color: #adc5f7;font-size: 9px;color: blue" border="0" width="44%">
                    <tbody>
                    <tr>
                    <td><b>Information on Takaful Coverage /</b> <i>Maklumat untuk Perlindungan Takaful</i></td>
                    </tr>
                    </tbody>
                </table>
                <table style="">
                    <tr>
                        <td style="width: 90px"><b>Takaful Coverage</b><br><i>Perlindungan Takaful</i></td>
                        <td style="width: 10px">:</td>
                        <td>   <span> <b>Group Credit Term Takaful (GCTT)/</b><i>/ Perlindungan Takaful Berkelompok Bertempoh (GCTT)</i>&nbsp; &nbsp; &nbsp; &nbsp;</em> &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp;
                            <table align="" class="" cellpadding="1" style="text-align: center !important;">
                                <tr>
                                    <td class="border">
                                      @if($data->Financial->takaful_coverage=="1") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="40px"><b>Yes / </b><i>Ya</i></td>
                                     <td width="10px">&nbsp;</td>
                                    <td class="border">
                                      @if($data->Financial->takaful_coverage=="0") 
                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                      @else
                                        &nbsp;&nbsp;
                                      @endif
                                    </td>
                                    <td width="40px"><b>No</b><i>Tidak</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table style="background-color: #adc5f7;font-size: 9px;color: blue" border="0" width="29%">
                    <tbody>
                    <tr>
                    <td><b>Details of Financing /</b> <i>Butir-butir Pembiayaan</i></td>
                    </tr>
                    </tbody>
                </table>
                <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 90px"> <b>Financing Amount</b><br> <i>Jumlah Pembiayaan</i></td>
                                    <td style="width: 10px">:</td>
                                    <td><b>RM</b></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <tr>
                                                @if(!empty($data->LoanAmount->loanammount))
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,0,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,1,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,2,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,3,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,4,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,5,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,6,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,7,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,8,1) }} </td>
                                                <td class="border"> {{ substr($data->LoanAmount->loanammount,9,1) }} </td>
                                                @else
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                @endif
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 110px">&nbsp;&nbsp;&nbsp;</td>
                                    <td style="width: 80px"><b>Tenure</b><br><i>Tempoh</i></td>
                                    <td>
                                        <table align="" class="border" cellpadding="2" style="text-align: center;">
                                             <tr>
                                                @if(!empty($data->LoanAmount->new_tenure))
                                                <td class="border"> {{substr($data->LoanAmount->new_tenure,0,1)}}</td>
                                                <td class="border"> {{substr($data->LoanAmount->new_tenure,1,1)}} </td>
                                                @else
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                @endif
                                                Years
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 20px"><b>Years</b><br><i>Tahun</i>
                                </tr>
                            </table>
                <p><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><strong>SECTION C</strong> /<em> BAHAGIAN C&nbsp;</em><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;</span></p>            
                <table>
                <tr>
                    <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                        <b>APPLICATION FOR PERSONAL FINANCING-i FACILITY /</b><i> PERMOHONAN UNTUK KEMUDAHAN PEMBIAYAAN PERIBADI</i>
                    </td>
                    </tr>   
                </table>
                <table  width="100%" class="border" style="border: 0px!important" cellpadding='1'>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="" style="text-align: center;">
                                <tr>
                                    <td style="width: 80px"><b>Purpose of Facility</b><br><i>Tujuan Pembiayaan</i></td>
                                    <td style="width: 10px">:</td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <tr>
                                                <td class="border">
                                                    @if ($data->Financial->purpose_facility=="PF01") 
                                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                                    @else
                                                        &nbsp;&nbsp;
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 60px"><b>Education</b><br><i>Pendidikan</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="1">
                                          <tr>
                                            <td class="border">
                                                @if ($data->Financial->purpose_facility=="PF02") 
                                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                @else
                                                    &nbsp;&nbsp;
                                                @endif
                                            </td>
                                          </tr>
                                        </table>
                                    </td>
                                    <td style="width: 80px"><b>Education</b><br><i>Pendidikan</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="1">
                                          <tr>
                                            <td class="border">
                                                @if ($data->Financial->purpose_facility=="PF03") 
                                                       <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                    @else
                                                        &nbsp;&nbsp;
                                                    @endif
                                            </td>
                                          </tr>
                                        </table>
                                    </td>
                                    <td style="width: 80px"><b>Business</b><br><i>Perniagaan</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="1">
                                          <tr>
                                            <td class="border">
                                                @if ($data->Financial->purpose_facility=="PF04") 
                                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                @else
                                                    &nbsp;&nbsp;
                                                @endif
                                            </td>
                                          </tr>
                                        </table>
                                    </td>
                                    <td style="width: 70px"><b>Others</b><br><i>Lain-lain</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 80px"><b>Type of Customer</b><br><i>Jenis Pelanggan</i></td>
                                    <td style="width: 10px">:</td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <tr>
                                                <td class="border">
                                                    @if ($data->Financial->type_customer=="Y") 
                                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                    @else
                                                        &nbsp;&nbsp;
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 60px"><b>Biro</b><br><i>Biro</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                          <tr>
                                            <td class="border">
                                                @if ($data->Financial->type_customer=="N") 
                                                   <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                @else
                                                    &nbsp;&nbsp;
                                                @endif
                                            </td>
                                          </tr>
                                        </table>
                                    </td>
                                    <td style="width: 80px"><b>Non-Biro</b><br><i>Non-Biro</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                          <tr>
                                            <td class="border">
                                                @if ($data->Financial->type_customer=="P") 
                                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                @else
                                                    &nbsp;&nbsp;
                                                @endif
                                            </td>
                                          </tr>
                                        </table>
                                    </td>
                                    <td style="width: 80px"><b>Private Sector</b><br><i>Swasta</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                          <tr>
                                            <td class="border">
                                               @if($data->Financial->type_customer=="G") 
                                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                @else
                                                    &nbsp;&nbsp;
                                                @endif
                                            </td>
                                          </tr>
                                        </table>
                                    </td>
                                    <td style="width: 70px"><b>Federal/State AG</b><br><i>Akauntan Negara / Negeri</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td width="80%" class="">
                            <table width="" valign="top" class="">
                                <tr>
                                    <td style="width: 80px"><b>Payment Mode</b><br><i>Cara Bayaran</i></td>
                                    <td style="width: 10px">:</td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <tr>
                                                <td class="border">
                                                    @if ($data->Financial->payment_mode=="BA") 
                                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                    @else
                                                        &nbsp;&nbsp;
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td style="width: 100px!important"><b>Biro Angkasa Salary Deduction</b><br><i> Potongan Gaji melalui Biro Angkasa</i></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <tr>
                                                <td class="border">
                                                    @if ($data->Financial->payment_mode=="FG") 
                                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                    @else
                                                        &nbsp;&nbsp;
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><b>Federal/Sate AG Salary Deduction</b><br><i>Potongan Gaji Melalui Akauntan Negara/negeri</i></td>
                                </tr>
                                <tr>
                                    <td style="width: 60px"></td>
                                    <td style="width: 10px"></td>
                                    <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <tr>
                                                <td class="border">
                                                    @if ($data->Financial->payment_mode=="P") 
                                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                    @else
                                                        &nbsp;&nbsp;
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><b>Employer Salary Deduction</b><br><i> Potongan Gaji Majikan</i></td>
                                   <td>
                                        <table align="left" class="border" cellpadding="2" style="text-align: center;">
                                            <tr>
                                                <td class="border">
                                                    @if ($data->Financial->payment_mode=="OC") 
                                                        <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px"/>
                                                    @else
                                                        &nbsp;&nbsp;
                                                    @endif
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td><b>Over the Counter</b><br><i>Melalui Kaunter</i></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
     
                <hr>
                <b style="color: #2751a6">Application for Settlement (If any)</b><br><i style="color: #2751a6">  Permohonan Penyelesaian Pembiayaan Lain (Jika Berkenaan)</i>
                <table style="background-color: #adc5f7;font-size: 9px;color: blue" border="0" width="83%">
                    <tbody>
                    <tr>
                    <td><b>Name of Banks/Non Banks** /</b> <i>Nama Bank/Bukan Bank**</i> <b>(e.g. AEON Credit, PTPTN, MARA, etc.)/</b><i> (cth. AEON Credit, PTPTN, MARA, dll.)</i></td>
                    </tr>
                    </tbody>
                </table>
                <table style="width: 670px;font-size: 9px !important">
                     <tr>
                        <td style="width: 10px">1.</td>
                        <td>
                            <table align="" class="" cellpadding="1" >
                                 <tr>
                                     @if(!empty($data->Financial->bank1))
                                        <td class="classbank">{{$data->Financial->bank1}}</td>
                                     @else
                                        <td class="classbank">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                 </tr>
                            </table>
                        </td>
                         <td style="width: 10px">4.</td>
                        <td>
                            <table align="" class="" cellpadding="1" >
                                 <tr>
                                     @if(!empty($data->Financial->bank4))
                                        <td class="classbank">{{$data->Financial->bank4}}</td>
                                     @else
                                        <td class="classbank">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                 </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 10px">2.</td>
                        <td>
                            <table align="" class="" cellpadding="1" >
                                 <tr>
                                     @if(!empty($data->Financial->bank2))
                                        <td class="classbank">{{$data->Financial->bank2}}</td>
                                     @else
                                        <td class="classbank">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                 </tr>
                            </table>
                        </td>
                         <td style="width: 10px">5.</td>
                        <td>
                           <table align="" class="" cellpadding="1" >
                                 <tr>
                                     @if(!empty($data->Financial->bank5))
                                        <td class="classbank">{{$data->Financial->bank5}}</td>
                                     @else
                                        <td class="classbank">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                 </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td style="width: 10px">3.</td>
                        <td>
                            <table align="" class="" cellpadding="1" >
                                 <tr>
                                     @if(!empty($data->Financial->bank3))
                                        <td class="classbank">{{$data->Financial->bank3}}</td>
                                     @else
                                        <td class="classbank">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                 </tr>
                            </table>
                        </td>
                        <td style="width: 10px">6.</td>
                        <td>
                            <table align="" class="" cellpadding="1" >
                                 <tr>
                                     @if(!empty($data->Financial->bank6))
                                        <td class="classbank">{{$data->Financial->bank6}}</td>
                                     @else
                                        <td class="classbank">&nbsp;&nbsp;&nbsp;</td>
                                    @endif
                                 </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <hr>
                <b style="font-size: 8px;color: #2751a6">I agree if there are excess payments upon settlement of the financing, the amount of such payments will be credited into my savings accounts as per details below.</b><br>
                <i style="font-size: 7.5px;color: #2751a6">Saya bersetuju sekiranya terdapat lebihan bayaran selepas penyelesaian pembiayaan dibuat, jumlah lebihan bayaran tersebut akan dikreditkan ke dalam akaun simpanan saya seperti butiran di bawah.</i><br><br>
                <b  style="font-size: 8px;color: #2751a6">Account to Credit Financing Amount</b><br>
                <i  style="font-size: 8px;color: #2751a6">Akaun di mana Amaun Pembiayaan Dikreditkan </i>

                <table  width="100%" class="border" style="border: 0px!important" cellpadding='2'>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="10%"><b>Bank's Name</b><br><i>Nama Bank</i></td>
                                    <td><br>
                                        <table class="border" style="text-align: center;">
                                            <tr>
                                                <?php for ($i=0; $i<=17;$i++) {
                                                  if (substr($data->Financial->bank_name,$i,1)=="" OR substr($data->Financial->bank_name,$i,1)==" ") {
                                                    echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                                  }
                                                  else { ?>
                                                    <td class="border">{{substr(strtoupper($data->Financial->bank_name),$i,1)}} </td>
                                                    <?php
                                                  }
                                                }
                                                ?>
                                            </tr>
                                        </table>
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;<b>Account No</b><br><i>&nbsp;&nbsp;&nbsp;No. Akaun</i></td>
                                    <td><br>
                                        <table class="border" style="text-align: center;">
                                            <tr>
                                                <?php for ($i=0; $i<=17;$i++) {
                                                  if (substr($data->Financial->account_no,$i,1)=="" OR substr($data->Financial->account_no,$i,1)==" ") {
                                                    echo "<td class='border'>&nbsp; &nbsp; &nbsp;</td>";
                                                  }
                                                  else { ?>
                                                    <td class="border">{{substr(strtoupper($data->Financial->account_no),$i,1)}}</td>
                                                    <?php
                                                  }
                                                }
                                                ?>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>PURCHASE APPLICATION AND PROMISE TO BUY</b><i> PERMOHONAN BELIAN DAN AKUJANJI UNTUK MEMBELI</i>
                        </td>
                    </tr>   
                </table>
                <b style="font-size: 7.5px;text-align: justify;width: 700px;color: #2751a6" >Upon approval of financing and my acceptance to the Terms and Conditions, I hereby order and request MBSB Bank to purchase commodity with the purchase price that will be approved by MBSB Bank as per SMS ‘Aqad and promise to buy the same commodity at cost plus profit, depending on the financing facility and I will be held responsible for any violation to the agreement.</b><br>
                <i style="font-size: 8px;color: #2751a6">Setelah mendapat kelulusan pembiayaan dan penerimaan saya kepada Terma dan Syarat, saya dengan ini membuat pesanan dan memohon MBSB Bank untuk membeli komoditi dengan harga belian yang akan diluluskan oleh MBSB Bank seperti tertera di dalam 'Aqad SMS dan berjanji untuk membeli komoditi tersebut pada kos tambah keuntungan, bergantung kepada kelulusan pembiayaan, di mana saya bertanggungjawab sepenuhnya akibat mengingkari perjanjian ini.</i>
                <table class="" width="100%" >
                <tr>
                    <td class="border" style="width: 50%!important"  bgcolor="#d0d5dd" valign="top">
                        <b>Signature of Applicant</b><br>
                        <i>Tandatangan Pemohon</i>
                      </td>
                    <td class="no-border-top" style="width: 50% !important" >
                        <table align="" class="" cellpadding="1" style="" >
                            <tr>
                                <td style="width: 40px !important"><b>Name</b><br><i>Nama</i></td>
                                <td class="signature" colspan="1">{{$name}}</td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>MyKad No./</b><br><i>No. MyKad</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                      <tr>
                                            <td class="border"> {{ substr($ic,0,1) }} </td>
                                            <td class="border"> {{ substr($ic,1,1) }} </td>
                                            <td class="border"> {{ substr($ic,2,1) }} </td>
                                            <td class="border"> {{ substr($ic,3,1) }} </td>
                                            <td class="border"> {{ substr($ic,4,1) }} </td>
                                            <td class="border"> {{ substr($ic,5,1) }} </td>
                                            <td class="no-border-top">-</td>
                                            <td class="border"> {{ substr($ic,6,1) }} </td>
                                            <td class="border"> {{ substr($ic,7,1) }} </td>
                                            <td class="no-border-top">-</td>
                                            <td class="border"> {{ substr($ic,8,1) }} </td>
                                            <td class="border"> {{ substr($ic,9,1) }} </td>
                                            <td class="border"> {{ substr($ic,10,1) }} </td>
                                            <td class="border"> {{ substr($ic,11,1) }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>Date</b><br><i>Tarikh</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                      <tr>
                                            <td class="border"> {{ substr($submitted_date,0,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,1,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,3,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,4,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,6,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,7,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,8,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,9,1) }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                        <b>APPOINTMENT OF MBSB BANK AS A SALES AGENT/ </b><i> PERLANTIKAN MBSB BANK SEBAGAI EJEN JUALAN</i>
                    </td>
                </tr>   
            </table>
                <b style="font-size: 7.5px;text-align: justify;width: 700px;color: #2751a6" >Subject to MBSB Bank’s acceptance, I hereby irrevocably and unconditionally appoint MBSB Bank as my agent under the shariah contract of Wakalah to sell the commodities to any third party purchaser/ commodity trader as MBSB Bank may deem fit and in accordance with such terms acceptable to MBSB Bank. I shall be bound by any contract or agreement that MBSB Bank may
                enter into with the said third party purchaser/trader for the purpose of the sale of the commodities on my behalf. I hereby agree to pay MBSB Bank a sum of RM34.00 (subject to Goods and  Tax) as Wakalah Fee. I hereby undertake to indemnify MBSB Bank to make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB Bank may suffer or incur as a result of fulfilling MBSB Bank’s agency function as set out above. </b><br>
                <i style="font-size: 7.5px;text-align: justify;width: 700px;color: #2751a6" >Tertakluk kepada penerimaan MBSB Bank, saya dengan ini secara tidak boleh batal dan tanpa syarat melantik MBSB Bank sebagai ejen saya di bawah kontrak syariah Wakalah untuk menjual komoditi kepada mana-mana pembeli / peniaga komoditi pihak ketiga sebagaimana yang difikirkan patut oleh MBSB Bank dan mengikut apa-apa terma yang diterima oleh MBSB Bank. Saya akan terikat dengan mana-mana
                kontrak atau perjanjian yang dibuat oleh MBSB Bank dengan pembeli / peniaga komoditi pihak ketiga tersebut bagi tujuan penjualan komoditi bagi pihak saya. Saya dengan ini bersetuju untuk membayar MBSB Bank sebanyak RM34.00 (tertakluk kepada Cukai Barangan dan Perkhidmatan) sebagai fi Wakalah. Saya dengan ini mengaku janji untuk menanggung segala kerugian MBSB Bank dengan membayar sepenuhnya
                semua kerugian, kos dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB Bank akibat
                memenuhi fungsi agensi seperti yang dinyatakan di atas.</i>
                <table class="" width="100%" >
                <tr>
                    <td class="border" style="width: 50%!important"  bgcolor="#d0d5dd" valign="top">
                        <b>Signature of Applicant</b><br>
                        <i>Tandatangan Pemohon</i>
                      </td>
                    <td class="no-border-top" style="width: 50%!important;">
                        <table align="" class="" cellpadding="1" style="">
                            <tr>
                                <td style="width: 40px !important"><b>Name</b><br><i>Nama</i></td>
                                <td class="signature" colspan="1">{{$name}}</td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>MyKad No./</b><br><i>No. MyKad</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                      <tr>
                                            <td class="border"> {{ substr($ic,0,1) }} </td>
                                            <td class="border"> {{ substr($ic,1,1) }} </td>
                                            <td class="border"> {{ substr($ic,2,1) }} </td>
                                            <td class="border"> {{ substr($ic,3,1) }} </td>
                                            <td class="border"> {{ substr($ic,4,1) }} </td>
                                            <td class="border"> {{ substr($ic,5,1) }} </td>
                                            <td class="no-border-top">-</td>
                                            <td class="border"> {{ substr($ic,6,1) }} </td>
                                            <td class="border"> {{ substr($ic,7,1) }} </td>
                                            <td class="no-border-top">-</td>
                                            <td class="border"> {{ substr($ic,8,1) }} </td>
                                            <td class="border"> {{ substr($ic,9,1) }} </td>
                                            <td class="border"> {{ substr($ic,10,1) }} </td>
                                            <td class="border"> {{ substr($ic,11,1) }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>Date</b><br><i>No. MyKad</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                        <tr>
                                            <td class="border"> {{ substr($submitted_date,0,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,1,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,3,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,4,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,6,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,7,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,8,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,9,1) }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

            </div>

          </td>
        </tr>
      </table>
            </div>
        </div>
    </table>
    <!--PAGE 4-->
    <table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
        <div class="row">
            <div class="column">
                <table style="background-color: #adc5f7;font-size: 9px;color: blue" border="0" width="83%">
                    <tbody>
                    <tr>
                    <td> <b>Acknowledgement by MBSB Bank on the appointment as a Sales Agent/</b><i> Perakuan MBSB Bank untuk pelantikan sebagai Ejen Jualan</i></td>
                    </tr>
                    </tbody>
                </table>
                <br>
                 <table class="" width="100%" >
                <tr>
                    <td class="border" style="width: 50%!important"  bgcolor="#d0d5dd" valign="top">
                        <b>Signature of Marketing Officer/Master Agent/Financial Advisor</b><br>
                        <i>Tandatangan Pegawai Pemasaran/Ejen Jualan/Penasihat Kewangan</i>
                      </td>
                    <td class="no-border-top" style="width: 50%!important;">
                        <table align="" class="" cellpadding="1" style="">
                            <tr>
                                <td style="width: 40px !important"><b>Name</b><br><i>Nama</i></td>
                                @if($data->Terma->referral_id==0)
                                <td class="signature" colspan="1">NETXPERT SDN BHD</td>
                                @else
                                <td class="signature" colspan="1">{{$data->Terma->MO->desc_mo}}</td>
                                @endif
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>Staff No.</b><br><i>No. Kakitangan</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                     @if($data->Terma->referral_id!=0)
                                      <tr>
                                            <td class="border"> {{ substr($data->Terma->MO->id_mo,0,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->id_mo,0,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->id_mo,0,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->id_mo,0,1) }} </td>
                                        </tr>
                                      @else
                                      <tr>
                                            <td class="border"> 0</td>
                                            <td class="border"> 0</td>
                                            <td class="border"> 0 </td>
                                            <td class="border"> 0</td>
                                        </tr>
                                      @endif
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>Branch</b><br><i>Cawangan</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                     @if($data->Terma->referral_id!=0)
                                        <tr>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,0,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,1,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,2,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,3,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,4,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,5,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,6,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,7,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,8,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,9,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,10,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,11,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,12,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,13,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,14,1) }} </td>
                                            <td class="border"> {{ substr($data->Terma->MO->Branch->branchname,15,1) }} </td>
                                        </tr>
                                        @else
                                         <tr>
                                            <td class="border"> K</td>
                                            <td class="border">U</td>
                                            <td class="border"> A</td>
                                            <td class="border"> L </td>
                                            <td class="border"> A</td>
                                            <td class="border"> &nbsp;</td>
                                            <td class="border"> L</td>
                                            <td class="border">U </td>
                                            <td class="border"> M</td>
                                            <td class="border"> P</td>
                                            <td class="border"> U</td>
                                            <td class="border"> R</td>
                                            <td class="border"> &nbsp;</td>
                                            <td class="border"> &nbsp;</td>
                                           <td class="border"> &nbsp;</td>
                                            <td class="border"> &nbsp;</td>
                                        </tr>
                                        @endif
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>Date</b><br><i>Tarikh</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                      <tr>
                                            <td class="border"> {{ substr($submitted_date,0,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,1,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,3,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,4,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,6,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,7,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,8,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,9,1) }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <p><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><strong>SECTION D</strong> /<em> BAHAGIAN D&nbsp;</em><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;</span></p> 
                <table>
                    <tr>
                        <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>PRIVACY CONSENT NOTICE /</b><i> NOTIS PRIVASI DATA & PERSETUJUAN</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 7.5px !important">
                    <tr>
                        <td style="width: 80px;text-align: justify;" > 
                            <b>
                                You hereby agree and consent to MBSB Bank collecting, using, processing and disclosing your personal information including and not limited to your sensitive personal data as stated in the application form for the purposes disclosed in detail in the MBSB Bank Privacy Notice that can be accessed at www.mbsbbank.com.
                                <br><br>
                                In the event you do not wish us, our affiliates, consultants or any third parties that we have contracted or engaged to communicate marketing offers to you and/or you wish to make any inquiries or complaints, request access, amend or you elect to limit our right to process your personal data; you may contact us at 03-2096 3000 or e-mail us at enquiry@mbsbbank.com.
                                <br><br>
                                Any request to access or amend personal data may be subject to a fee and also requirements under the Personal Data Protection Act 2010. We may review and update this Notice from time to time to reflect changes in the law, changes in our business practices, procedures and structure, and the community’s changing privacy expectations. While it is not generally feasible to notify you of changes to this Notice, the latest version of the MBSB Bank Privacy Notice will be available on MBSB Bank’s website. By signing this application form, you represent and warrant that you have read and understood this Notice and MBSB Bank’s Privacy Notice.
                            </b><br><br>
                            <i>
                                Anda dengan ini bersetuju dan membenarkan MBSB Bank mengumpul, menggunakan, memproses dan mendedahkan maklumat peribadi anda termasuk dan tidak terhad kepada data peribadi yang sensitif seperti yang dinyatakan di dalam borang permohonan bagi tujuan yang dibincangkan secara terperinci di dalam Notis Privasi MBSB Bank yang boleh dilayari di laman web rasmi MBSB Bank; www.mbsbbank.com.
                                <br><br>
                                Sekiranya anda tidak ingin kami dan juga anak-anak syarikat, syarikat-syarikat bersekutu, perunding atau mana-mana pihak ketiga yang telah dilantik oleh MBSB Bank untuk berkomunikasi dengan anda bagi menawarkan pemasaran dan/atau anda ingin membuat sebarang pertanyaan atau aduan, memohon akses, meminda, dan/atau anda memilih untuk menghadkan hak kami untuk memproses data peribadi anda, anda boleh menghubungi kami di 03-2096 3000 atau emel di enquiry@mbsbbank.com.
                                <br><br>
                                Sebarang permintaan untuk mengakses atau meminda data peribadi adalah tertakluk kepada bayaran dan juga syarat-syarat di bawah Akta Perlindungan Data Peribadi 2010. Kami boleh mengkaji semula dan mengemas kini Notis ini dari semasa ke semasa untuk mencerminkan perubahan di dalam Undang-Undang, perubahan di dalam amalan perniagaan, prosedur dan struktur dan perubahan keperluan privasi masyarakat. Walaupun sebarang perubahan terhadap Notis ini pada lazimnya adalah tidak dimaklumkan kepada anda, versi terkini Notis Privasi MBSB Bank boleh didapati di laman web rasmi MBSB Bank. Dengan menandatangani borang permohonan ini, anda mewakili dan menjamin bahawa anda telah membaca dan memahami Notis ini dan Notis Privasi MBSB Bank.
                            </i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p style="text-align: justify; line-height: 1;"><span style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: rgb(147, 101, 184);"></p>
                        </td>
                    </tr>
                </table>
                <p><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span><strong>SECTION E</strong> /<em> BAHAGIAN E&nbsp;</em><span style="background-color: #adc5f7;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp;</span></p> 
                <table>
                    <tr>
                       <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>DECLARATION/DISCLOSURE BY APPLICANT/CO-APPLICANT/GUARANTOR/</b> /<i>  PERAKUAN/PENDEDAHAN OLEH PEMOHON/PEMOHON BERSAMA/PENJAMIN</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 7.5px !important">
                    <tr>
                        <td style="width: 80px">
                            <ol>
                                <li style="text-align: justify; line-height: 1; font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 8px; color: rgb(41, 105, 176);"><strong>I/We hereby declare that I/we am/are NOT a bankrupt.</strong>
                                        <em><br>Saya/Kami mengesahkan bahawa saya/kami TIDAK muflis.</em>
                                        <br>
                                        <br>
                                    </span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I/We declare that the information furnished in this form are completely true and accurate and I/we have not withheld any information which may prejudice my/our financing application or have a bearing on your financing decision.</strong><em>
                                    <br>Saya/Kami mengesahkan bahawa maklumat yang disediakan di dalam borang ini adalah benar, tepat dan lengkap dan saya/kami tidak menyembunyikan sebarang maklumat yang mungkin prejudis terhadap permohonan pembiayaan saya/kami atau mempunyai kesan ke atas keputusan pembiayaan anda.</em>
                                        <br>
                                        <br>
                                    </span></span></span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>In the event, I/we resign from my/our current employment, MBSB Bank shall, at their absolute discretion, revise the profit rate in order to match the prevailing market rate.</strong>
                                    <br><em>Sekiranya, saya/kami meletakkan jawatan daripada pekerjaan saya/kami, MBSB Bank akan, mengikut budi bicara mutlak mereka, mengubah kadar keuntungan sama dengan kadar pasaran semasa.</em>
                                        <br>
                                        <br>
                                    </span></span></span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);"><span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);">
                                    <span style="font-family: Arial,Helvetica,sans-serif;"><strong>I/We hereby authorize you/your representative to obtain the relevant information relating to this application from any relevant source as deemed suitable by MBSB Bank but not limited to any bureaus or agencies established by Bank Negara Malaysia (”BNM”) or other parties. The authorization to obtain the relevant information is also extended to prospective guarantors, security providers, authorized depository agent and other party relating to this application as deemed necessary by MBSB Bank.</strong>
                                    <br><em>Saya/Kami memberi kebenaran kepada MBSB Bank/wakil MBSB Bank untuk mendapatkan maklumat yang relevan terhadap permohonan ini dari sumber-sumber yang relevan dan yang dianggap sesuai oleh MBSB Bank termasuk dan tidak terhad kepada mana-mana biro atau agensi yang ditubuhkan oleh Bank Negara Malaysia (”BNM”) atau pihak yang lain. Kebenaran untuk mendapatkan maklumat yang relevan juga merangkumi bakal penjamin dan/atau pemberi sekuriti dan mana-mana wakil penyimpan yang dibenarkan dan pihak yang berkenaan dengan permohonan ini yang dianggap sesuai oleh MBSB Bank.</em>
                                    <br>
                                    <br>
                                    </span></span></span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I/We understand that MBSB Bank reserves the absolute right to approve or decline this application as MBSB Bank deems fit without assigning any reason.</strong>
                                    <br><em>Saya/Kami faham bahawa pihak MBSB Bank mempunyai hak mutlak untuk meluluskan atau menolak permohonan tanpa menyatakan sebarang alasan.
                                    <br></em>
                                    <br>
                                    </span></span></span></li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);"><span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);">
                                    <span style="font-family: Arial,Helvetica,sans-serif;"><strong>I/We expressly irrevocably consent and authorize MBSB Bank to furnish all relevant information relating to or arising from or in connection with the financing facilities to any subsidiary companies of MBSB Bank, its agents and/or such persons or BNM, Cagamas Berhad and debt collection agents or such other authority or body established by BNM, or such other authority having jurisdiction over MBSB Bank as MBSB Bank may absolutely deem fit and such other authority as may be authorized by law.</strong>
                                    <br><em>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB Bank untuk mendedahkan sebarang maklumat yang diperlukan berkaitan dengan atau berbangkit daripada apa-apa hubungan dengan kemudahan pembiayaan kepada mana-mana anak syarikat MBSB Bank, ejennya, dan/atau mana-mana individu atau BNM, Cagamas Berhad dan ejen pemungut hutang atau mana-mana pihak berkuasa lain yang mempunyai bidang kuasa ke atas MBSB Bank di mana MBSB Bank secara mutlak berhak memberi kata putus dan mana-mana pihak berkuasa yang dibenarkan selaras dengan undang-undang.</em>
                                    <br>
                                    <br>
                                    </span></span></span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);">
                                        <span style="font-family: Arial,Helvetica,sans-serif;"><strong>I/We hereby further expressly irrevocably consent and authorise MBSB Bank to seek any information concerning me/us with or from any credit reference/reporting agencies, including but not limited to CCRIS, CTOS, RAMCI, FIS and/or Inland Revenue Authorities or any authority as MBSB Bank may from time to time deem fit</strong>.
                                        <br><em>Saya/Kami bersetuju tanpa hak menarik balik dan tanpa syarat memberi kebenaran kepada MBSB Bank untuk mendapatkan sebarang maklumat berkaitan saya/kami daripada mana-mana agensi rujukan kredit, termasuk dan tidak terhad kepada CCRIS, CTOS, RAMCI, FIS dan/atau Lembaga Hasil Dalam Negeri atau mana-mana pihak berkuasa yang diputuskan oleh MBSB Bank dari masa ke semasa.</em>
                                      <br>
                                      <br>
                                    </span></span></span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I/We also acknowledge that it is a requirement that all information relating to this application must be transmitted and/or updated to the Central Credit Reference Information System ("CCRIS"), a database maintained by BNM as and when necessary.</strong>
                                     <br><em>Saya/Kami juga mengesahkan bahawa semua maklumat berkaitan permohonan ini mestilah dimaklumkan dan/atau dikemaskini kepada Sistem Maklumat Rujukan Kredit Pusat ("CCRIS"), pangkalan data yang diuruskan oleh BNM apabila perlu.</em></span></span></span>
                                    <br>
                                    <br>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I /We shall comply with MBSB Bank's requirements in respect of my/our application and I/we understand that MBSB Bank's offer of the financing shall be subject to MBSB Bank performing the necessary verification.</strong>
                                      <br><em>Saya/Kami akan mematuhi segala keperluan MBSB Bank untuk permohonan saya/kami dan saya/kami memahami bahawa tawaran pembiayaan oleh MBSB Bank adalah tertakluk kepada pengesahan yang diperlukan oleh MBSB Bank.</em></span></span></span>
                                    <br><span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"></span></span></span>
                                    <br>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I/We hereby undertake to notify and/or inform the emergency contact person and my/our spouse that their personal data has been provided to MBSB Bank and undertake to indemnify and hold MBSB Bank harmless in the event of any legal repercussions arising from my/our failure to notify the said emergency contact person/spouse.</strong>
                                    <br><em>Saya/Kami dengan ini bersetuju untuk memberitahu dan/atau menghubungi penama rujukan kecemasan dan pasangan saya/kami bahawa data peribadi mereka telah diberi kepada MBSB Bank dan berjanji tidak akan mengambil sebarang tindakan ke atas MBSB Bank sekiranya timbul sebarang tindakan undang-undang daripada kegagalan saya/kami untuk memberitahu penama rujukan kecemasan/pasangan saya/kami.</em>
                                    <br>
                                    <br>
                                    </span></span></span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-size: 8px;"><span style="color: rgb(41, 105, 176);"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>This application form and all supporting documents that were submitted to MBSB Bank shall be the sole property of MBSB Bank and MBSB Bank is entitled to retain the same irrespective of whether my/our application is approved or rejected by MBSB Bank.</strong>
                                    <br><em>Borang permohonan ini dan semua dokumen sokongan yang telah diserahkan kepada MBSB Bank adalah hak milik mutlak MBSB Bank dan MBSB Bank berhak untuk mengekalkan semua dokumen tanpa mengira samaada permohonan saya/kami diluluskan atau ditolak oleh MBSB Bank</em>
                                    <br>
                                    <br>
                                    </span></span></span>
                                </li>
                                <li style="text-align: justify; line-height: 1;font-size: 8px; color: rgb(41, 105, 176);">
                                    <span style="font-family: Arial, Helvetica, sans-serif; font-size: 8px; color: rgb(41, 105, 176);"><strong>I/We hereby irrevocably agree to waive my/our rights to a refund where the amount is not exceeding RM5.00 arising from but not limited to early settlement or closure of my/our financing account. I/We further consent and authorize MBSB Bank to donate the said amount to charitable organisations deemed appropriate by MBSB Bank. </strong>
                                    <br><em>Saya/Kami bersetuju tanpa hak menarik balik bagi bayaran balik di mana jumlahnya tidak melebihi RM5.00, hasil daripada tetapi tidak terhad kepada penyelesaian awal atau penutupan akaun pembiayaan saya/kami. Saya/Kami seterusnya membenar dan memberi kuasa kepada MBSB Bank untuk menderma jumlah tersebut kepada badan-badan kebajikan yang difikirkan wajar oleh pihak MBSB Bank </em></span>
                                </li>
                            </ol>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </table>
    <!--PAGE 5-->
    <table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
        <div class="row">
            <div class="column">
                <table>
                    <tr>
                         <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>PERMISSION TO DEDUCT FROM PERSONAL FINANCING-i FACILITY /</b><i>  KEBENARAN PEMOTONGAN DARI KEMUDAHAN PEMBIAYAAN PERIBADI-i</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            
                                <li type='1' style="margin-left: 20px;">
                                    <strong><span style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: rgb(41, 105, 176);">I hereby authorise and allow MBSB Bank to deduct and pay directly from the financing amount approved by MBSB Bank, the following:-</span></strong>
                                    <span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;">
                                    <br><em>Saya dengan ini memberi kuasa dan membenarkan MBSB Bank melakukan pemotongan / pembayaran secara terus daripada jumlah pembiayaan yang diluluskan oleh MBSB Bank, seperti berikut:-</em> 
                                    <br> 
                                    
                                      <li type='i' style="text-align: justify;margin-left: 30px;"><strong><span style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: rgb(41, 105, 176);">Security Deposit</span></strong><span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;">/ <em>Deposit Sekuriti </em></span></span></span></li>
                                      <li type='i' style="text-align: justify;margin-left: 30px;"><span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>GCTT Takaful Contribution (if applicable)</strong>/ <em>Sumbangan Takaful GCTT (sekiranya berkenaan) </em></span></span></span></li>
                                      <li type='i' style="text-align: justify;margin-left: 30px;"><span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>Wakalah Fee/</strong> <em>Fi Wakalah </em></span></span></span></li>
                                      <li type='i' style="text-align: justify;margin-left: 30px;"><span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>Redemption for other financing (if applicable)/</strong> <em>Penyelesaian pembiayaan lain (sekiranya berkenaan)</em></span></span></span></li>
                                      <li type='i' style="text-align: justify;margin-left: 30px;"><span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;">&nbsp;<strong>Banca Takaful Product (if applicable)</strong>/</span></span></span><em><span style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: rgb(41, 105, 176);">&nbsp;Produk Banca Takaful (sekiranya berkenaan) Other charges (if applicable)/ Lain-lain caj (sekiranya berkenaan)</span></em></li>
                                    </span></span></span>
                                    <br>
                                </li>
                                <li type='1' style="text-align: justify; line-height: 1;font-size: 9px; color: rgb(41, 105, 176);margin-left: 20px;">
                                    <span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;">
                                    <strong>The deductions to be made are subject to the package taken by me and based on the terms imprinted in the SMS `aqad.</strong> 
                                    <br><em>Potongan yang akan dibuat adalah tertakluk kepada pakej yang saya ambil dan sebagaimana yang tertera didalam`aqad SMS.</em> 
                                    <br> 
                                    <br>
                                    </span></span></span>
                                </li>
                                <li type='1' style="text-align: justify; line-height: 1;font-size: 9px; color: rgb(41, 105, 176);margin-left: 20px;">
                                    <span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I also agree that the net financing amount (after deductions) will be paid by MBSB Bank to me by crediting my account details of which I have provided or by any other mode deemed suitable by MBSB Bank.</strong> 
                                    <br><em>Saya juga bersetuju bahawa baki amaun pembiayaan di atas (selepas potongan) akan dibayar oleh MBSB Bank kepada saya dengan mengkreditkan akaun saya mengikut butiran yang telah disertakan atau melalui apa-apa cara yang difikirkan sesuai oleh MBSB Bank.</em> 
                                    <br> 
                                    <br>
                                    </span></span></span>
                                </li>
                                <li  type='1' style="text-align: justify; line-height: 1;font-size: 9px; color: rgb(41, 105, 176);margin-left: 20px;">
                                    <span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I also confirm and undertake that this authorization is irrevocable in the event the amount to settle other debts has been paid to any third party on my behalf by MBSB Bank and I further agree and undertake to pay all indebtedness owing to MBSB Bank in the event I cancel the facility.&nbsp;</strong> 
                                    <br><em>Saya juga dengan ini akujanji bahawa kebenaran ini adalah kebenaran tidak boleh batal sekiranya kesemua amaun tebus hutang telah pun dibayar kepada mana-mana pihak ketiga bagi pihak saya oleh MBSB Bank dan saya juga bersetuju serta berakujanji akan membayar kesemua amaun tebus hutang tersebut kepada MBSB Bank jika saya membatalkan kemudahan tersebut.</em> 
                                    <br> 
                                    <br>
                                    </span></span></span>
                                </li>
                                <li type='1' style="text-align: justify; line-height: 1;font-size: 9px; color: rgb(41, 105, 176);margin-left: 20px;">
                                    <span style="color: rgb(41, 105, 176);"><span style="font-size: 9px;"><span style="font-family: Arial,Helvetica,sans-serif;"><strong>I shall be responsible to pay in full any cost incurred. I hereby further undertake to keep MBSB Bank fully indemnified and make good in full all losses, costs and expenses resulting from any claims, proceedings, actions, requests or any form of damages that MBSB Bank may suffer or incur as a result of fulfilling its function as set out above.&nbsp;</strong> 
                                    <br>
                                    </span></span></span><em><span style="font-family: Arial, Helvetica, sans-serif; font-size: 9px; color: rgb(41, 105, 176);font-size: 9px; color: rgb(41, 105, 176);margin-left: 20px;">
                                    Saya akan bertanggungjawab untuk membayar sepenuhnya semua kos yang ditanggung. Saya juga mengaku janji akan menanggung segala kerugian MBSB Bank dengan membayar sepenuhnya semua kerugian, kos dan perbelanjaan yang timbul daripada apa-apa tuntutan, prosiding, tindakan, permintaan atau apa-apa bentuk kerosakan yang mungkin dialami atau ditanggung oleh MBSB Bank akibat memenuhi fungsi seperti yang dinyatakan di atas.</span></em>
                                </li>
                            </ol>
                        </td>
                    </tr>
                </table>

                <table>
                    <tr>
                        <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px !important" bgcolor="#0055a5">
                            <b>CREDIT TRANSACTIONS AND EXPOSURES WITH CONNECTED PARTIES / </b><i>TRANSAKSI KREDIT DAN PENDEDAHAN DENGAN PIHAK BERKAITAN</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            <b style="text-align: justify; font-size: 7.5px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">Do you have any immediate family or close relatives (including parents, brother/sister and their spouses, dependent's spouse and own/step/adopted child) that are employees of MBSB Bank?</b>
                            <br>
                            <i style="font-size: 7.5px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">Adakah anda mempunyai ahli keluarga atau saudara terdekat (termasuk ibubapa, abang/kakak/adik dan pasangan, pasangan dibawah tanggungan, anak dan saudara tiri/angkat) yang sedang bekerja dengan MBSB Bank?</i>
                        </td>
                        <table cellpadding="2" style="text-align: center;">
                            <tr>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->credit_transactions!='0') 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="30px"><b>Yes / </b><i>Ya</i></td>
                                <td width="20px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->credit_transactions=='0')
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                 <td width="40px"><b>No / </b><i>Tidak</i></td>
                            </tr>
                        </table>
                    </tr>
                </table>
                <b style=" font-size: 9px; color: #2751a6;">If Yes, please complete the information below: /
                </b><i  style=" font-size: 9px; color: #2751a6;"> Jika Ya, sila lengkapkan maklumat dibawah:</i><br>

                @if((empty($data->Terma->credit_transactions)) OR ($data->Terma->credit_transactions=='0'))
                <table style="width: 670px;font-size: 9px !important">
                     <tr>
                        <td style="width: 10px"><b>Name</b><br><i>Nama</i></td>
                        <td>
                            <table align="" class="" cellpadding="2" style="text-align: center !important;">
                                 <tr>
                                    <td class="border" style="width: 50%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                            </table>
                        </td>
                        <td style="width: 10px">&nbsp;&nbsp;<b>Relationship</b><br>&nbsp;&nbsp;<i>Hubungan</i></td>
                        <td>
                            <table align="" class="" cellpadding="1" style="text-align: center !important;">
                                 <tr>
                                    <td class="border" style="width: 50%"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                 </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                            <td style="width: 80px"><b>MyKad No. </b><br><i>No. MyKad</i></td>
                            <td>
                                <table align="" class="border" cellpadding="1" style="text-align: center !important;">
                                    <tr>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="no-border-top">-</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="no-border-top">-</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                         <td style="width: 80px">&nbsp;&nbsp;<b>Passport No</b><br>&nbsp;&nbsp;<i>No. Pasport</i></td>
                            <td>
                                <table align="" class="border" cellpadding="1" style="text-align: center !important;">
                                    <tr>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                    </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                @elseif($data->Terma->credit_transactions!='0')
                <table style="width: 670px;font-size: 9px !important">
                     <tr>
                        <td style="width: 10px"><b>Name</b><br><i>Nama</i></td>
                        <td>
                            <table align="" class="" cellpadding="2">
                                 <tr>
                                    <td class="classborder50">{{$data->Credit->fullname}}</td>
                                 </tr>
                            </table>
                        </td>
                        <td style="width: 10px">&nbsp;&nbsp;<b>Relationship</b><br>&nbsp;&nbsp;<i>Hubungan</i></td>
                        <td>
                            <table align="" class="" cellpadding="2">
                                 <tr>
                                    <td class="classborder50" >{{$data->Credit->relationship}} </td>
                                 </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                            <td style="width: 80px"><b>MyKad No. </b><br><i>No. MyKad</i></td>
                            <td>
                                <table align="" class="border" cellpadding="1" style="text-align: center !important;">
                                   @if(!empty($data->Credit->mykad))
                                    <tr>
                                        <td class="border"> {{ substr($data->Credit->mykad,0,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,1,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,2,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,3,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,4,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,5,1) }} </td>
                                        <td>&nbsp;- <br></td>
                                        <td class="border"> {{ substr($data->Credit->mykad,6,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,7,1) }} </td>
                                        <td>&nbsp;- <br></td>
                                        <td class="border"> {{ substr($data->Credit->mykad,8,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,9,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,10,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->mykad,11,1) }} </td>
                                </tr>
                                @else
                                 <tr>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="no-border-top">-</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="no-border-top">-</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                        <td class="border">&nbsp;&nbsp;</td>
                                </tr>
                                @endif
                            </table>
                        </td>

                         <td style="width: 80px">&nbsp;&nbsp;<b>Passport No</b><br>&nbsp;&nbsp;<i>No. Pasport</i></td>
                            <td>
                                <table align="" class="border" cellpadding="1" style="text-align: center !important;">
                                   @if(!empty($data->Credit->passport))
                                    <tr>
                                        <td class="border"> {{ substr($data->Credit->passport,0,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,1,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,2,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,3,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,4,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,5,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,6,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,7,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,8,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,9,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,10,1) }} </td>
                                        <td class="border"> {{ substr($data->Credit->passport,11,1) }} </td>
                                </tr>
                                @else
                                 <tr>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                                <td class="border">&nbsp;&nbsp;</td>
                                            </tr>
                                @endif
                            </table>
                        </td>
                    </tr>
                </table>
                @endif
                <table>
                    <tr>
                         <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>CONSENT FOR CROSS-SELLING, MARKETING, PROMOTIONS, ETC/ </b><i> PERSETUJUAN UNTUK JUALAN SILANG, PEMASARAN, PROMOSI DAN LAIN-LAIN</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            <b style="font-size: 9px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">I/We expressly consent and authorize MBSB Bank to process any information that I/we have provided to MBSB Bank for the purposes of cross-selling, marketing and promotions including disclosure to its strategic partners or such persons or third parties as MBSB Bank deem fit.</b>
                            <br>
                            <i style="font-size: 9px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">Saya/Kami mengesahkan bahawa saya/kami memberi kebenaran dan kuasa kepada MBSB Bank yang tidak boleh dibatal tanpa kebenaran untuk mendedahkan sebarang maklumat yang telah saya/kami kemukakan kepada MBSB Bank bagi tujuan jualan silang, pemasaran dan promosi termasuk pendedahan kepada rakan strategik atau mana-mana individu atau pihak ketiga yang difikirkan wajar oleh MBSB Bank.</i>
                        </td>
                        <table cellpadding="2" style="text-align: center;">
                            <tr>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->consent_for!='0') 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="30px"><b>Yes / </b><i>Ya</i></td>
                                <td width="20px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->consent_for=='0')
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                 <td width="40px"><b>No / </b><i>Tidak</i></td>
                            </tr>
                        </table>
                    </tr>
                </table>
                 <table>
                    <tr>
                         <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>HIGH NETWORTH INDIVIDUAL CUSTOMER ("HNWI")/ </b><i>/ INDIVIDU YANG BERPENDAPATAN TINGGI ("HNWI")</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            <b style="text-align: justify;font-size: 9px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">HNWI means an individual whose total net personal assets, or total net joint assets with his or her spouse, exceeds RM3 million or its equivalent in foreign currencies, excluding the value of the individual's primary residence. Calculation of HNWI is total asset less total liabilities.</b>
                            <br>
                            <i style="font-size: 9px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">HNWI bermaksud seseorang individu di mana jumlah bersih aset-aset peribadi, atau jumlah bersih aset-aset bersama dengan pasangan, melebihi RM3 juta atau yang setaraf dengannya dalam mata wang asing, tidak termasuk nilai kediaman utama individu tersebut. Pengiraan HNWI adalah berdasarkan jumlah keseluruhan aset tolak jumlah keseluruhan liabiliti.</i>
                            <br>
                            <b>Does your total net personal assets or total net joint assets with your spouse exceeds RM3 million? </b>
                            <i>Adakah jumlah bersih aset-aset peribadi anda atau jumlah bersih aset bersama dengan pasangan anda melebihi RM3 juta?</i>
                          </p>
                        </td>
                         <table cellpadding="2" style="text-align: center;">
                            <tr>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->high_networth!='0') 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="30px"><b>Yes / </b><i>Ya</i></td>
                                <td width="20px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->high_networth=='0')
                                   <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                 <td width="40px"><b>No / </b><i>Tidak</i></td>
                            </tr>
                        </table>
                    </tr>
                </table>
                <table>
                    <tr>
                         <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px" bgcolor="#0055a5">
                            <b>POLITICALLY EXPOSED PERSON ("PEP") /</b><i> INDIVIDU BERKAITAN POLITIK ("PEP")</i>
                        </td>
                    </tr>   
                </table>
                 <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            <b style="text-align: justify; font-size: xx-small;">PEP - Individuals who are or who have been entrusted with prominent public functions domestically or internationally. Family members of PEPs are defined as those who may be expected to influence or be influenced by that PEP, as well as dependents of the PEP. This includes the PEP&rsquo;s:</b><br>
                            <i style="font-size: xx-small;">PEP &ndash; seseorang Individu yang diamanahkan dengan &ldquo;Fungsi Awam Yang Penting&rdquo; samaada domestik atau antarabangsa. Ahli keluarga PEP adalah ditakrifkan sebagai mereka yang dijangka boleh mempengaruhi atau dipengaruhi oleh PEP tersebut dan juga tanggungan PEP. Ianya termasuk:</i>
                           <ol type='I'>
                                <li><span style="font-size: xx-small;font-weight: bold">Spouse and dependents of the spouse; </span><br /><span style="font-size: xx-small;">Pasangan suami atau isteri berserta tanggungannya; </span></li>
                                <li><span style="font-size: xx-small;font-weight: bold">Child (including step children and adopted children) and spouse of the child; </span><br /><span style="font-size: xx-small;">Anak (termasuk anak tiri atau anak angkat yang sah) berserta pasangan suami atau isteri kepada anak-anak tersebut;</span></li>
                                <li><span style="font-size: xx-small;font-weight: bold"> Parent; and </span><br /><span style="font-size: xx-small;">Ibu bapa; dan</span></li>
                                <li><span style="font-size: xx-small;font-weight: bold">Brother or sister and their spouses. </span><br /><span style="font-size: xx-small;">Adik beradik berserta pasangan suami atau isteri mereka.</span></li>
                           </ol>
                             <b style="text-align: justify;font-size: xx-small;">Definition of Related Closed Associates of PEPs:/</b><em>Definisi Kenalan-Kenalan yang Berkait Rapat dengan PEPs:</em>
                            <ul>
                                <li style="text-align: justify;"><span style="font-size: xx-small;"><strong>Related close associate to PEP is defined as individual who is closely connected to a PEP, either socially or professionally.</strong> </span><br /><span style="font-size: xx-small;"><em>Kenalan-kenalan yang berkait rapat dengan PEP ditakrifkan sebagai individu yang saling berhubung dan berkait rapat dengan PEP, samada secara sosial atau profesional</em></span></li>
                            </ul>
                            <table style="background-color: #adc5f7;font-size: 9px;color: blue" border="0" width="29%">
                                <tbody>
                                <tr>
                                <td><b>For Individual /</b> <i>Untuk Individu</i></td>
                                </tr>
                                </tbody>
                            </table>
                            <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            <b style="text-align: justify;font-size: 9px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">Are you a PEP or a Family Member(s) of PEP or a Related Close Associate(s) of PEP?</b>
                            <br>
                            <i style="font-size: 9px; font-family: Arial, Helvetica, sans-serif; color: rgb(85, 57, 130);">Adakah anda seorang PEP atau ahli keluarga PEP atau kenalan-kenalan berkait rapat dengan PEP?</i>
                          </p>
                        </td>
                         <table cellpadding="2" style="text-align: center;">
                            <tr>
                                <td width="10px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->politically!='0') 
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                <td width="30px"><b>Yes / </b><i>Ya</i></td>
                                <td width="20px">&nbsp;</td>
                                <td class="border" style="text-align: center;">
                                  @if ($data->Terma->politically=='0')
                                    <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                  @else
                                    &nbsp;&nbsp;
                                  @endif
                                </td>
                                 <td width="40px"><b>No / </b><i>Tidak</i></td>
                            </tr>
                        </table>
                    </tr>
                </table>
                           
                            <b>If Yes, please complete the information below:/</b><i> Jika Ya, sila lengkapkan maklumat di bawah:</i><br>S
                              @if((empty($data->Terma->politically)) OR ($data->Terma->politically=='0'))
                             <table style="height: 80px; width: 659px;" border="1" cellspacing="0" align="center">
                                <tbody>
                                    <tr>
                                        <td style="text-align: center;"><strong>No</strong><br/><br/></td>
                                        <td style="text-align: center;"><strong>Name</strong><br/><em>Name<br/><br/></em></td>
                                        <td style="text-align: center;"><strong>Relationship with customer</strong><br/><em>Hubungan dengan Pelanggan<br/><br/></em></td>
                                        <td style="text-align: center;"><strong>Status**</strong><br/><em>Status**<br/><br/></em></td>
                                        <td style="text-align: center;"><strong>Prominent Public Position</strong><br/><em>Kedudukan Awan yang Penting<br/><br/></em></td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </tbody>
                            </table>
                            @else
                             <table style="height: 80px; width: 659px; font-size: 9px;text-align: justify;" border="1" cellspacing="0" align="center">
                                <tbody>
                                    <tr>
                                        <td style="text-align: center;"><strong>No</strong><br/><br/></td>
                                        <td style="text-align: center;"><strong>Name</strong><br/><em>Name<br/><br/></em></td>
                                        <td style="text-align: center;"><strong>Relationship with customer</strong><br/><em>Hubungan dengan Pelanggan<br/><br/></em></td>
                                        <td style="text-align: center;"><strong>Status**</strong><br/><em>Status**<br/><br/></em></td>
                                        <td style="text-align: center;"><strong>Prominent Public Position</strong><br/><em>Kedudukan Awan yang Penting<br/><br/></em></td>
                                    </tr>
                                    @if(!empty($data->Pep->name1))
                                    <tr>
                                        <td>@if(!empty($data->Pep->name1)) 1 &nbsp;@endif</td>
                                        <td>{{$data->Pep->name1}}</td>
                                        <td>{{$data->Pep->relationship1}}</td>
                                        <td>{{$data->Pep->status1}}</td>
                                        <td>{{$data->Pep->prominent1}}</td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    @endif
                                    @if(!empty($data->Pep->name2))
                                    <tr>
                                        <td>@if(!empty($data->Pep->name2)) 2 &nbsp; @endif</td>
                                        <td>{{$data->Pep->name2}}</td>
                                        <td>{{$data->Pep->relationship2}}</td>
                                        <td>{{$data->Pep->status2}}</td>
                                        <td>{{$data->Pep->prominent2}}</td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    @endif
                                    @if(!empty($data->Pep->name3))
                                    <tr>
                                        <td>@if(!empty($data->Pep->name3)) 3 @else &nbsp; @endif</td>
                                        <td>{{$data->Pep->name3}}</td>
                                        <td>{{$data->Pep->relationship3}}</td>
                                        <td>{{$data->Pep->status3}}</td>
                                        <td>{{$data->Pep->prominent3}}</td>
                                    </tr>
                                    @else
                                    <tr>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            @endif
                        </td>
                    </tr>
                </table>
                <b>** Currently holding/ is actively seeking/ is being considered/ Previously holding</b><br>
                <i>** Memegang jawatan buat masa ini/ masih aktif mencari/ dalam pertimbangan/ memegang jawatan sebelum ini</i>
            </div>
        </div>
    </table>
    <!--PAGE 6-->
    <table width="100%"  cellspacing="4" style="page-break-before: always" id="customers"> 
        <div class="row">
            <div class="column">
                <table>
                    <tr>
                        <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px !important" bgcolor="#0055a5">
                            <b>DECLARATION RELATING TO FOREIGN EXCHANGE ADMINISTRATION RULES ISSUED BY BANK NEGARA MALAYSIA/</b><i> PENGISYTIHARAN BERKAITAN DENGAN PERATURAN PENTADBIRAN PERTUKARAN ASING YANG DIKELUARKAN OLEH BANK NEGARA MALAYSIA</i>
                        </td>
                    </tr>   
                </table>
                   <div a style="font-size: 7.5px;color: #2751a6;"><b>I hereby:/</b>Saya dengan ini:
                  
                        <li type="a" style="font-weight: bold; margin-left: 30px;"><strong>Declare and conﬁrm that all personal data and information provided in and in connection with this Personal Financing-i Application and any other banking documents I/we may sign or enter into from time to time is true and correct in full compliance with the Islamic Financial Services Act 2013 and Central Bank of Malaysia Act 2009. I/we shall be fully responsible for any inaccurate, untrue or incomplete information provided in this Form. I/we also authorise the Bank to make this information available to Bank Negara Malaysia in compliance with Islamic Financial Services Act 2013 and Central Bank of Malaysia Act 2009.</strong></li>
                        <li type="a" style="font-weight: bold; margin-left: 30px;"><strong>Agree and undertake that we shall at all times comply and be responsible for the compliance with BANK NEGARA MALAYSIA&rsquo;s (BNM) previling ForeignExchange Administration Notices &amp; Rules as made available at BNM's website : www.bnm.gov.my<br ></strong><em style="font-weight: normal;">Bersetuju dan mengaku bahawa kami akan sentiasa mematuhi dan bertanggungjawab terhadap pematuhan bagi Notis &amp; Peraturan Pentadbiran Pertukaran Asing yang dibuat oleh BANK NEGARA MALAYSIA (BNM) seperti yang terdapat di laman web BNM: <a href="http://www.bnm.gov.my/">www.bnm.gov.my</a></em></li>
                       </div>
                        <b style="font-size: 7.5px;color: #2751a6;">This section is to be completed if the applicant is a Malaysian citizen and an oversea address is provided in the application form/ </b><i style="font-size: 7.5px;color: #2751a6;">Seksyen ini hendaklah dilengkapkan jika pemohon adalah warganegara Malaysia dan beralamat di luar negara seperti yang disediakan di dalam borang permohonan ini.</i><br><br>

                        <b style="font-size: 7.5px;color: #2751a6;">I hereby declare the following/ </b><i style="font-size: 7.5px;color: #2751a6;">Saya dengan ini mengisytiharkan seperti berikut:<br/></i>
                        <b style="font-size: 7.5px;color: #2751a6;">(Please &#10004; the appropriate boxes below) </b>
                        <i style="font-size: 7.5px;color: #2751a6;">(Sila &#10004; pada kotak yang bersesuaian di bawah)</i><br><br>
                
                <table style="font-size: 8px">
                    <tr>

                        <td class="border">
                            @if ($foreigner->foreign1=='1')
                                <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                            @else
                                &nbsp;&nbsp;
                            @endif
                        </td>
                        <td style="font-size: 8px">
                             @if (!empty($foreigner->country))
                                <b >I have been granted permanent residence status in the country of <u>{{$foreigner->Country->country_desc}}</u>/</b><i> Saya telah diberikan status kediaman tetap di negara <u>{{$foreigner->Country->country_desc}}</u></i>
                            @else
                               <b >I have been granted permanent residence status in the country of___________________/</b><i> Saya telah diberikan status kediaman tetap di negara ___________________</i>
                            @endif
                            
                        </td>>
                    </tr>
                    <tr>
                        <td class="border">
                            @if ($foreigner->foreign2=='1')
                                <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                            @else
                                &nbsp;&nbsp;
                            @endif
                        </td>
                        <td style="font-size: 8px">
                            <b>I have not been granted any permanent residence status in a territory outside Malaysia./ </b><i>Saya tidak pernah diberikan mana-mana status kediaman tetap di wilayah luar Malaysia</i>
                        </td>>
                    </tr>
                    <tr>
                        <td class="border">
                            @if ($foreigner->foreign3=='1')
                                <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                            @else
                                &nbsp;&nbsp;
                            @endif
                        </td>
                        <td style="font-size: 8px">
                            <b>I reside in Malaysia more than 182 days in a calendar year</b><i> Saya tinggal di Malaysia lebih daripada 182 hari dalam satu tahun kalendar</i>
                        </td>>
                    </tr>
                    <tr>
                        <td class="border">
                            @if ($foreigner->foreign4=='1')
                                <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                            @else
                                &nbsp;&nbsp;
                            @endif
                        </td>
                        <td style="font-size: 8px">
                            <b>I reside outside Malaysia more than 182 days in a calendar year/</b><i> Saya tinggal di luar Malaysia lebih daripada 182 hari dalam satu tahun kalendar</i>
                        </td>>
                    </tr>
                </table>
                <table>
                    <tr>
                       <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px !important" bgcolor="#0055a5">
                            <b>DECLARATION FOR SUBSCRIPTION OF WEALTH MANAGEMENT PRODUCT/</b><i> PERAKUAN UNTUK LANGGANAN PRODUK PENGURUSAN KEKAYAAN</i>
                        </td>
                    </tr>   
                </table>
                    <b style="font-size: 7.5px!important">Do you wish to subscribe to the following Wealth Management Product(s)/ </b>
                    <i>Adakah anda ingin melanggan Produk Pengurusan Kekayaan yang berikut:</i>
                        </td>
                        <table cellspacing="0">
                            <tr>
                                <td width="50px">&nbsp;&nbsp;&nbsp;</td>
                                <td class="border">
                                 @if ($subs->sub_wealth=='1')
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                </td>
                                <td width="70px">Yes /<i> Ya </i></td>
                                <td class="border">
                                 @if ($subs->sub_wealth=='0')
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                </td>
                                <td width="70px">No /<i> Tidak </i></td>
                            </tr>
                        </table><br>
                    </tr>

                <table style="font-size: 7.5px !important;text-align: justify; border-spacing: 0;" border="0" cellspacing="0">
                    <tr style="font-size: 7.5px !important">
                        <td></td>
                        <td>
                            <table cellspacing="0" style="border-spacing: 0;" cellpadding="1">
                                <tr>
                                    <td class="border">
                                        @if ($subs->gpa=='1')
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                    </td>
                                    <td width="80%">&nbsp;<b>Declaration if agree to subscribe to Group Personal Accident (GPA) Takaful Plan/ </b><i>Pengakuan sekiranya bersetuju untuk mengambil pelan Takaful Kemalangan Diri Berkelompok (TKDB)</i></td>
                                    <ol>
                                        <li type="a" style="font-weight: bold;margin-top: -20px !important">
                                            <b>I hereby understand and agree that the cost of Coverage for GPA – Syarikat Takaful Malaysia Berhad (STMB) is only RM350 (cost inclusive of GST) of a single payment for 1 year protection./ </b>
                                            <i style="font-weight: normal;">Saya memahami dan bersetuju bahawa kos Perlindungan GPA – Syarikat Takaful Malaysia Berhad adalah dengan hanya RM350 (harga termasuk GST) sekali pembayaran untuk 1 tahun perlindungan.</i>
                                        </li>
                                        <li type="a" style="font-weight: bold;">
                                            <b>This coverage is provided by STMB with cooperation from MBSB Bank. I hereby understand and agree that contributions and other charges are subject to goods and services tax ("GST") in accordance with the Goods and Services Tax Act 2014 ("GST Act") and I undertake to pay the GST fee that is levied in addition to the contribution and other charges (if applicable). I also declared that I have carefully read and understood the details of the Product Disclosure Sheet of GPA which can be found on the MBSB Bank website www.mbsbbank.com. /</b>
                                            <i style="font-weight: normal;">Perlindungan ini disediakan oleh Syarikat Takaful Malaysia Berhad di atas kerjasama dengan MBSB Bank. Saya faham dan bersetuju bahawa sumbangan dan lain-lain caj yang dikenakan adalah tertakluk kepada cukai barangan dan perkhidmatan ("GST") selaras dengan Akta Cukai Barangan dan Perkhidmatan 2014 ("Akta GST") dan saya bertanggungjawab untuk membayar jumlah bayaran GST yang dikenakan sebagai tambahan ke atas sumbangan dan caj-caj lain ( jika berkenaan). Saya juga mengaku bahawa saya telah membaca dengan teliti dan memahami butiran Penyata Pendedahan Produk untuk GPA dan ianya boleh didapati di laman www.mbsbbank.com.</i>
                                        </li>
                                    </ol>
                                </tr>
                                
                            </table>
                           
                        </td>
                    </tr>
                </table>
                <table style="font-size: 7.5px;text-align: justify;" border="0">
                    <tr style="font-size: 7.5px !important">
                        <td></td>
                        <td>
                            <table cellspacing="0">
                                <tr>
                                    <td class="border">
                                       @if ($subs->annur=='1')
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                    </td>
                                    <td width="80%"><b>&nbsp;Declaration if agree to subscribe to AN-NUR Takaful Plan/  </b><i> Pengakuan sekiranya bersetuju untuk mengambil pelan Takaful AN NUR</i></td>
                                </tr>
                            </table>
                            <ol>
                                <li style="margin-top: -20px !important"><b>I hereby understand and agree that the cost of Coverage for Takaful AN-NUR cover is only RM350 (cost inclusive of GST) of a single payment for 1 year protection./ </b><i>Saya memahami dan bersetuju bahawa kos Perlindungan Takaful AN-NUR adalah dengan hanya RM350 (harga termasuk GST) sekali pembayaran untuk 1 tahun perlindungan</i></li>
                                <li><b>This coverage is provided by Great Eastern Takaful Berhad (GETB) with cooperation from MBSB Bank. I hereby understand and agree that contributions and other charges are subject to goods and services tax ("GST") in accordance with the Goods and Services Tax Act 2014 ("GST Act") and I undertake to pay the GST fee that is levied in addition to the contribution and other charges (if applicable). I also declare that I have carefully read and understood the details of the Product Disclosure Sheet of Takaful AN-NUR which can be found on the MBSB Bank website www.mbsbbank.com./ </b>
                                <i>Perlindungan ini disediakan oleh Great Eastern Takaful Berhad (GETB) di atas kerjasama dengan MBSB Bank. Saya faham dan bersetuju bahawa sumbangan dan lain-lain caj yang dikenakan adalah tertakluk kepada cukai barangan dan perkhidmatan ("GST") selaras dengan Akta Cukai Barangan dan Perkhidmatan 2014 ("Akta GST") dan saya bertanggungjawab untuk membayar jumlah bayaran GST yang dikenakan sebagai tambahan ke atas sumbangan dan caj-caj lain ( jika berkenaan). Saya juga mengaku bahawa saya telah membaca dengan teliti dan memahami butiran Penyata Pendedahan Produk untuk Takaful AN-NUR dan ianya boleh didapati di laman www.mbsbbank.com.</i></li>
                            </ol>
                        </td>
                    </tr>
                </table>
                <table style="font-size: 7.5px;text-align: justify;" border="0" >
                    <tr style="font-size: 7.5px !important">
                        <td></td>
                        <td>
                            <table cellspacing="0">
                                <tr>
                                    <td class="border">
                                        @if ($subs->hasanah=='1')
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                    </td>
                                    <td>&nbsp;<b>Declaration if agree to subscribe to HASANAH Takaful Plan/</b><i> Pengakuan sekiranya bersetuju untuk mengambil pelan Takaful Hasanah </i></td>
                                </tr>
                            </table>
                            <ol>
                                <li style="margin-top: -20px !important"><b>I hereby understand and agree that the cost of Coverage for Takaful HASANAH cover is only RM350 (cost inclusive of GST) of a single payment for 1 year protection./ </b><i>Saya memahami dan bersetuju bahawa kos Perlindungan Takaful HASANAH adalah dengan hanya RM350 (harga termasuk GST) sekali pembayaran untuk 1 tahun perlindungan</i></li>
                                <li><b>This coverage is provided by eTiQa with cooperation from MBSB Bank. I hereby understand and agree that contributions and other charges are subject to goods and services tax ("GST") in accordance with the Goods and Services Tax Act 2014 ("GST Act") and I undertake to pay the GST fee that is levied in addition to the contribution and other charges (if applicable). I also declared that I have carefully read and understood the details of the Product Disclosure Sheet of Takaful HASANAH which can be found on the MBSB Bank website www.mbsbbank.com./ P</b><i>Perlindungan ini disediakan oleh eTiQa di atas kerjasama dengan MBSB Bank. Saya faham dan bersetuju bahawa sumbangan dan lain-lain caj yang dikenakan adalah tertakluk kepada cukai barangan dan perkhidmatan ("GST") selaras dengan Akta Cukai Barangan dan Perkhidmatan 2014 ("Akta GST") dan saya bertanggungjawab untuk membayar jumlah bayaran GST yang dikenakan sebagai tambahan ke atas sumbangan dan caj-caj lain ( jika berkenaan). Saya juga mengaku bahawa saya telah membaca dengan teliti dan memahami butiran Penyata Pendedahan Produk untuk Takaful Hasanah dan ianya boleh didapati di laman www.mbsbbank.com.</i></li>
                            </ol>
                        </td>
                    </tr>
                </table>
                <table style="font-size: 7.5px" border="0">
                    <tr style="font-size: 7.5px !important">
                        <td></td>
                        <td>
                            <table cellspacing="0">
                                <tr>
                                    <td class="border">
                                        @if ($subs->wasiat=='1')
                                            <img src="https://img.icons8.com/ios/50/000000/checkmark.png" width="10px" />
                                        @else
                                            &nbsp;&nbsp;
                                        @endif
                                    </td>
                                    <td><b>Declaration if agree to subscribe to WASIAT Product/</b><i> Pengakuan sekiranya bersetuju untuk mengambil Wasiat WASIAT</i></td>
                                </tr>
                            </table>
                            <ol>
                                <li style="margin-top: -20px !important">&nbsp;<b>I hereby understand and agree that the cost of WASIAT is only RM500 (cost inclusive of GST) for a life time subscription./</b><i> Saya memahami dan bersetuju bahawa kos WASIAT adalah dengan hanya RM500 (harga termasuk GST) untuk sekali sahaja seumur hidup.</i></li>
                                <li><b> This coverage is provided by Amanah Raya Berhad with cooperation from MBSB Bank. I hereby understand and agree that contributions and other charges are subject to goods and services tax ("GST") in accordance with the Goods and Services Tax Act 2014 ("GST Act") and I undertake to pay the GST fee that is levied in addition to the contribution and other charges (if applicable). I also declared that I have carefully read and understood the details of the Product Disclosure Sheet of WASIAT which can be found on the MBSB Bank website www.mbsbbank.com./</b><i> Perlindungan ini disediakan oleh Amanah Raya Berhad di atas kerjasama dengan MBSB Bank. Saya faham dan bersetuju bahawa sumbangan dan lain-lain caj yang dikenakan adalah tertakluk kepada cukai barangan dan perkhidmatan ("GST") selaras dengan Akta Cukai Barangan dan Perkhidmatan 2014 ("Akta GST") dan saya bertanggungjawab untuk membayar jumlah bayaran GST yang dikenakan sebagai tambahan ke atas sumbangan dan caj-caj lain ( jika berkenaan). Saya juga mengaku bahawa saya telah membaca dengan teliti dan memahami butiran Penyata Pendedahan Produk untuk WASIAT dan ianya boleh didapati di laman www.mbsbbank.com.</i></li>
                            </ol>
                        </td>
                    </tr>
                </table>
                <table>
                    <tr>
                        <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px !important" bgcolor="#0055a5">
                            <b>FOR GOODS AND SERVICES TAX ("GST") EFFECTIVE 1 APRIL 2015/</b><i>UNTUK CUKAI BARANGAN DAN PERKHIDMATAN (“CBP”) BERKUATKUASA 1 APRIL 2015</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            <b>I/We hereby agree that I/We shall be liable for all Goods and Services Tax (GST) payable in connection with this application or any account or any service in connection therein and MBSB Bank shall be authorized to debit my/our account for the same.</b><br>
                            <i>Saya/Kami bersetuju bahawa saya/kami akan bertanggungjawab ke atas Cukai Barangan dan Perkhidmatan (“CBP”) yang berkaitan dengan permohonan ini atau mana-mana akaun atau apa-apa perkhidmatan yang berkaitan dan MBSB Bank dibenarkan untuk mendebit akaun saya/kami bagi tujuan yang sama.</i>
                        </td>
                    </tr>
                </table>
                 <table>
                    <tr>
                        <td colspan="3" class="" style="width: 700px;color: #FFFFFF;font-size: 9px !important" bgcolor="#0055a5">
                            <b>PRODUCT DISCLOSURE SHEET/</b><i>LEMBARAN PENJELASAN PRODUK</i>
                        </td>
                    </tr>   
                </table>
                <table style="font-size: 8px !important">
                    <tr>
                        <td style="width: 80px"> 
                            <b>I/We hereby declare that I/ we have been briefed on the information contained in the Product Disclosure Sheet that has been given to me/us for the product applied herein</b><br>
                            <i>Saya/Kami mengesahkan bahawa saya/kami telah diberitahu mengenai maklumat yang terkandung di dalam Lembaran Penjelasan Produk yang telah diberikan kepada saya/kami berkaitan dengan produk yang dipohon di sini</i>
                        </td>
                    </tr>
                </table>

            <table class="" width="100%" >
                <tr>
                    <td class="border" style="width: 50%!important"  bgcolor="#d0d5dd" valign="top">
                        <b>Signature of Applicant</b><br>
                        <i>Tandatangan Pemohon</i>
                      </td>
                    <td class="no-border-top" style="width: 50%!important;">
                        <table align="" class="" cellpadding="1" style="">
                            <tr>
                                <td style="width: 40px !important"><b>Name</b><br><i>Nama</i></td>
                                <td class="signature" colspan="1">{{$name}}</td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>MyKad No./</b><br><i>No. MyKad</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                      <tr>
                                            <td class="border"> {{ substr($ic,0,1) }} </td>
                                            <td class="border"> {{ substr($ic,1,1) }} </td>
                                            <td class="border"> {{ substr($ic,2,1) }} </td>
                                            <td class="border"> {{ substr($ic,3,1) }} </td>
                                            <td class="border"> {{ substr($ic,4,1) }} </td>
                                            <td class="border"> {{ substr($ic,5,1) }} </td>
                                            <td class="no-border-top">-</td>
                                            <td class="border"> {{ substr($ic,6,1) }} </td>
                                            <td class="border"> {{ substr($ic,7,1) }} </td>
                                            <td class="no-border-top">-</td>
                                            <td class="border"> {{ substr($ic,8,1) }} </td>
                                            <td class="border"> {{ substr($ic,9,1) }} </td>
                                            <td class="border"> {{ substr($ic,10,1) }} </td>
                                            <td class="border"> {{ substr($ic,11,1) }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 80px"><b>Date</b><br><i>No. MyKad</i></td>
                                <td>
                                    <table align="left" class="border" cellpadding="2"  
                                    style="text-align: center !important;">
                                        <tr>
                                            <td class="border"> {{ substr($submitted_date,0,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,1,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,3,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,4,1) }} </td>
                                             <td class="no-border-top">/</td>
                                            <td class="border"> {{ substr($submitted_date,6,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,7,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,8,1) }} </td>
                                            <td class="border"> {{ substr($submitted_date,9,1) }} </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table>
                <tr>
                    <td></td>
                </tr>
            </table>
            <table class="border" width="700px" style="text-align: center;">
                <tr>
                    <td><b style="font-size: 7.5px">Note : You are encouraged to immediately contact MBSB Bank to discuss payment alternatives if you experience any difficulties in meeting the financing commitments /</b><br>
            <i style="font-size: 7.5px">Nota: Anda digalakkan menghubungi MBSB Bank dengan segera untuk membincangkan bayaran alternatif jika anda mengalami sebarang kesulitan dalam memenuhi komitmen pembiayaan</i></td>
                </tr>
            </table>
            
                </div></div></table></span></p></td></tr></table></div></div></table></span></td></tr></table></div></div></table></td></tr></table></td></tr></table>
            </div></div></table></div></div></body></html>
          <!-- </div>
        </div>
    </table>
    </div>
</div>


    
    
  </tr></table></td></tr></table>
</body></html>-->/table>
</body></html>-->/table>
</body></html>-->