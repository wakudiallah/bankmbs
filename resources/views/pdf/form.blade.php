<html>
<head>
<style type="text/css">

body {

        font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
}
	
th {
		color:#0055a5;
    font-size: 14px;
  

}

br.border2  {
   display: block;
   margin: 20px 0;
}

td {
	color:#0055a5;
    font-size: 11px;
    line-height: 150%;

}
p {
    
    line-height: 130%;

}

ul
{
    list-style-type: none;
}

table.border {
	color:#0055a5;
  border-collapse: collapse;
    border: 1px solid black;
}

td.border {
	color:#0055a5;
  border-collapse: collapse;
    border: 1px solid black;
}
td.border2 {
   
}
td.header {
        	color:white;
        	
        	 background-color: #0055a5;
        }
</style>
</head>
<body>

<table  width="530" >
	<tr>
		<td width="20%" align="top">	<img src="{{url('/')}}/img/logo_small.png" ></td>
		<td width="70%" valign="top" align="center">
			<font size="1"><b>APPLICATION FORM FOR PERSONAL FINANCING-i FACILITY FOR INDIVIDUAL</b><i><br>BORANG PERMOHONAN UNTUK KEMUDAHAN PEMBIAYAAN PERIBADI-i BAGI INDIVIDU</i> </font>
		 </td>
		 <td width="10%" >&nbsp;</td>
	</tr>
</table>
<table  width="530" class="border" >
	<tr>
		<td valign="top" class="border">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>SECTION A /</b> <i>BAHAGIAN A</i></td>
	</tr>
	<tr>
		<td valign="top"  class="header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>PERSONAL PARTICULARS (MAIN APPLICANT)/</b> <i>BUTIR-BUTIR PERIBADI (PEMOHON UTAMA)</i></td>
	</tr>
	<tr>
		<td valign="top">
			<ul>
			  <li>1. <b>Salutation /</b> <i> Gelaran </i> :  </li>
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$data->title}} @if($data->title=="Others")  <u>({{$data->title_others}})</u> @endif @if(empty($data->title))  -  @endif</li>
			  	</ul>
			  <li>2. <b>Full Name (As per ID Document) /</b> <i>Nama Penuh (Seperti dalam Dokumen Pengenalan Diri) </i> : </li> 
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$data->name}} @if(empty($data->name))  -  @endif</li>
			  	</ul>
			  
			  <li>3. <b>MyKad No. /</b>  <i>No. MyKad</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->new_ic}}
				 @if(empty($data->new_ic))  -  @endif
				</li></ul>
			  <li>4. <b>Date of Birth /</b> <i>Tarikh Lahir</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{date('d-m-Y', strtotime($data->dob))}}  @if(empty($data->dob))  -  @endif </li></ul>
			  <li>5. <b>Police/Military No /</b> <i>No. Polis/Tentera</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->police_number}}
				 @if(empty($data->police_number))  -  @endif</li></ul>
			  <li>6. <b>Gender /</b> <i> Jantina</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->gender}}</li></ul>
			  <li>7.  <b>Country of Birth /</b> <i>Tempat Lahir</i> :</li>
			  	<ul style=" margin-left: -10px;"> <li > {{$data->name_prefered}}
			  	 <?php if(empty($data->name_prefered)) { echo"-";} ?> </li></ul>
			  <li>8.  <b>Home Address  /</b> <i>Alamat Rumah</i> :</li>
			  	<ul style=" margin-left: -10px;"> 
			  		@if(empty($data->address))  

			  		@else
			  			<li > {{$data->address}} </li>
			  		@endif

			  		@if(empty($data->address2))  

			  		@else
			  			<li > {{$data->address2}} </li>
			  		@endif

			  		@if(empty($data->address3))  

			  		@else
			  			<li > {{$data->address3}} </li>
			  		@endif
			  		@if(empty($data->address) && empty($data->address) && empty($data->address)) 
			  			<li> - </li>
			  		@endif
			
			  <li><b>Postcode /</b> <i>Poskod</i> :{{$data->postcode}}  @if(empty($data->postcode))  -  @endif</li>

			  <li><b>State /</b> <i>Negeri</i> : {{$data->state}}  @if(empty($data->state))  -  @endif</li>

			  	</ul>

			  <li>9. <b>Ownership Status /</b> <i>Taraf Pemilikan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->ownership}} @if(empty($data->ownership))  -  @endif</li></ul>
			  <li>10.<b> Correspondence Address /</b> <i>Alamat Surat Menyurat</i> : </li>
			  		<ul style=" margin-left: -10px;"> 
			  		@if(empty($data->corres_address1))  

			  		@else
			  			<li > {{$data->corres_address1}} </li>
			  		@endif

			  		@if(empty($data->corres_address2))  

			  		@else
			  			<li > {{$data->corres_address2}} </li>
			  		@endif

			  		@if(empty($data->corres_address3))  

			  		@else
			  			<li > {{$data->corres_address3}} </li>
			  		@endif

			  		@if(empty($data->corres_address1) && empty($data->corres_address2) && empty($data->corres_address3)) 
			  			<li> - </li>
			  		@endif
			  		
			  <li><b>Postcode /</b> <i>Poskod</i> : {{$data->corres_postcode}} @if(empty($data->corres_postcode))  -  @endif</li>

		

			  <li><b>State  /</b> <i>Negeri </i> : {{$data->corres_state}} @if(empty($data->corres_state))  -  @endif</li>
			  <li><b> Home Telephone /</b> <i>Telefon Rumah </i> : {{$data->corres_homephone}} @if(empty($data->corres_homephone))  -  @endif</li>
			  <li><b> Mobile Phone /</b> <i>Telefon Bimbit</i> : {{$data->corres_mobilephone}} @if(empty($data->corres_mobilephone))  -  @endif </li>
			  <li><b> E-mail Address /</b> <i>Alamat E-mel </i> : {{$pra->email}} @if(empty($pra->email))  -  @endif
				</li>
				</ul>

				<li>11. <b>Nationality /</b> <i>Kewarganegaraan </i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->country}} @if(empty($data->country))  -  @endif</li></ul>

			  	 <li>12. <b>Race /</b>  <i>Bangsa</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->race}}
				 @if(empty($data->race))  -  @endif
				</li></ul>

				 <li>13. <b>Bumiputera Status /</b>  <i>Status Bumiputera</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->bumiputera}}
				 @if(empty($data->bumiputera))  -  @endif
				</li></ul>

				 <li>14. <b>Religion /</b>  <i>Agama</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->religion}}
				 @if(empty($data->religion))  -  @endif
				</li></ul>

				 <li>15. <b>Marital Status /</b>  <i>Taraf Perkahwinan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->marital}}
				 @if(empty($data->marital))  -  @endif
				</li></ul>

				 <li>16. <b>No of Dependants /</b>  <i>Jumlah Tanggungan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->dependents}}
				 @if(empty($data->dependents))  -  @endif
				</li></ul>

				<li>17. <b>Education Level /</b>  <i>Taraf Pendidikan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$data->education}}
				 @if(empty($data->education))  -  @endif
				</li></ul>


			</ul>
				</td>
						

		</tr>

</table>

<table  width="530" class="border" style="page-break-before: always" >

	<tr>
		<td valign="top"  class="header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMPLOYMENT DETAILS (MAIN APPLICANT)/</b> <i>BUTIR-BUTIR PEKERJAAN (PEMOHON UTAMA)</i></td>
	</tr>
	<tr>
		<td valign="top">
			<ul>
			  <li>1. <b>Employment Type /</b> <i> Jenis Pekerjaan </i> :  </li>
			  	<ul style=" margin-left: -10px;"> 
			  		<li >
			

			  		@if($empinfo->emptype=="Others (Specify) / Lain-lain (Nyatakan)")  <u>({{$empinfo->emptype_others}})</u> 

			  		@else
			  		{{$empinfo->emptype}} 

			  		 @endif</li>
			  	</ul>
			  <li>2. <b>Employment Status /</b> <i>Status Pekerjaan </i> : </li> 
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$empinfo->empstatus}} @if(empty($empinfo->empstatus))  -  @endif</li>
			  	</ul>
			  
			  <li>3. <b>Name of Employer/ Business /</b>  <i>Nama Majikan/ Perniagaan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >@if(!empty($empinfo->empname)) {{$empinfo->empname}} @else {{$pra->employer->name}}  @endif



				</li></ul>
			  <li>4. <b>Department Name /</b> <i>Nama Jabatan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$empinfo->dept_name}}
				 @if(empty($empinfo->dept_name))  -  @endif </li></ul>

			  <li>5. <b>Division & Unit /</b> <i>Bahagian & Unit</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$empinfo->division}}
				 @if(empty($empinfo->division))  -  @endif</li></ul>
			  <li>6. <b>Address of Employer/ Business /</b> <i> Alamat Majikan/ Perniagaan</i> : </li>
			 
			  	<ul style=" margin-left: -10px;"> 
			  		@if(empty($empinfo->address))  

			  		@else
			  			<li > {{$empinfo->address}} </li>
			  		@endif

			  		@if(empty($empinfo->address2))  

			  		@else
			  			<li > {{$empinfo->address2}} </li>
			  		@endif

			  		@if(empty($empinfo->address3))  

			  		@else
			  			<li > {{$empinfo->address3}} </li>
			  		@endif
			  		@if(empty($empinfo->address) && empty($empinfo->address) && empty($empinfo->address)) 
			  			<li> - </li>
			  		@endif
			
			  <li><b>Postcode /</b> <i>Poskod</i> :{{$empinfo->postcode}}  @if(empty($empinfo->postcode))  -  @endif</li>

			  <li><b>State /</b> <i>Negeri</i> : {{$empinfo->state}}  @if(empty($empinfo->state))  -  @endif</li>

			  	</ul>

			  <li>7. <b>Nature of Business /</b> <i>Jenis Perniagaan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$empinfo->nature_business}} @if(empty($empinfo->nature_business))  -  @endif</li></ul>

			  	<li>8. <b>Start Date of Work /</b> <i>Tarikh Mula Bekerja </i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{date('d-m-Y', strtotime($empinfo->joined))}} @if(empty($empinfo->joined))  -  @endif</li></ul>
			 
				<li>9. <b>Total Working Experience /</b> <i>Jumlah Pengalaman Bekerja </i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$empinfo->joined}} @if(empty($empinfo->joined))  -  @endif</li></ul>

			  	 <li>10. <b>Position /</b>  <i>Jawatan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$empinfo->position}}
				 @if(empty($empinfo->position))  -  @endif
				</li></ul>

				 <li>11. <b>Office Telephone /</b>  <i>Telefon Pejabat</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$empinfo->office_phone}}
				 @if(empty($empinfo->office_phone))  -  @endif
				</li></ul>

				 <li>12. <b>Office Fax /</b>  <i>Faks Pejabat</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$empinfo->office_fax}}
				 @if(empty($empinfo->office_fax))  -  @endif
				</li></ul>

				

			</ul>
				</td>
						

		</tr>

		<tr>
				<td valign="top"  class="header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>PARTICULARS OF SPOUSE/</b> <i> MAKLUMAT SUAMI-ISTERI</i></td>
		</tr>
		<tr>
			<td valign="top">
				<ul>
				
				  <li>1. <b>Full Name /</b> <i> Nama Penuh </i> :  </li>
				  	<ul style=" margin-left: -10px;"> 
				  		<li >{{$spouse->name}} @if(empty($spouse->name))  -  @endif</li>
				  	</ul>
				  <li>2. <b>Home/Office Telephone/</b> <i>Telefon Rumah/Pejabat</i> : </li> 
				  	<ul style=" margin-left: -10px;"> 
				  		<li >{{$spouse->homephone}} @if(empty($spouse->homephone))  -  @endif</li>
				  	</ul>
				  
				  <li>3. <b>Mobile Phone /</b>  <i>Telefon Bimbit</i> : </li>
				  	<ul style=" margin-left: -10px;"> <li >{{$spouse->mobilephone}} @if(empty($spouse->mobilephone))  -  @endif</li>
				  	</ul>
				  <li>4. <b>Employment Type /</b> <i>Jenis Pekerjaan</i> : </li>
				  	<ul style=" margin-left: -10px;"> <li >
					  	@if($data->marital != 'Married / Berkahwin')
				  			-
				  		@else
				  			  	@if($spouse->emptype=="Others (Specify)/ Lain-lain (Nyatakan)")  
				  			  		<u>({{$spouse->emptype_others}})</u> 
				  			  	@else
				  				{{$spouse->emptype}} 
				  				@endif

				  		 @endif
			  		  </li>
			  		 </ul>
				</ul>
			</td>
		</tr>
		<tr>
				<td valign="top"  class="header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMERGENCY CONTACT**/</b> <i> RUJUKAN KECEMASAN**</i></td>
		</tr>

		<tr>


		<td valign="top">
		

			<ul> <b>** (Family Members/Relatives not staying with you)/ </b>(Ahli Keluarga/Saudara terdekat yang tidak tinggal bersama anda)<br>
		<b>Contact 1/</b> Rujukan 1<br> <br>
			  <li>1. <b>Full Name /</b> <i> Nama Penuh </i> :  </li>
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$reference->name1}} @if(empty($reference->name1))  -  @endif</li>
			  	</ul>
			  <li>2. <b>Home Telephone/</b> <i>Telefon Rumah</i> : </li> 
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$reference->home_phone1}} @if(empty($reference->home_phone1))  -  @endif</li>
			  	</ul>
			  
			  <li>3. <b>Mobile Phone /</b>  <i>Telefon Bimbit</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$reference->mobilephone1}} @if(empty($reference->mobilephone1))  -  @endif</li>
			  	</ul>
			  <li>4. <b>Relationship /</b> <i>Hubungan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$reference->relationship1}}
				 @if(empty($reference->relationship1))  -  @endif </li></ul>
			</ul>
		</td>
	</tr>
</table>

<table class="border" width="530"  style="page-break-before: always" >
	<tr>
		<td valign="top">			
			<ul>	
				<b>Contact 2/</b> Rujukan 2 <br> <br>
			 	  <li>1. <b>Full Name /</b> <i> Nama Penuh </i> :  </li>
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$reference->name2}} @if(empty($reference->name2))  -  @endif</li>
			  	</ul>
			  <li>2. <b>Home Telephone/</b> <i>Telefon Rumah</i> : </li> 
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$reference->home_phone2}} @if(empty($reference->home_phone2))  -  @endif</li>
			  	</ul>
			  
			  <li>3. <b>Mobile Phone /</b>  <i>Telefon Bimbit</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$reference->mobilephone2}} @if(empty($reference->mobilephone2))  -  @endif</li>
			  	</ul>
			  <li>4. <b>Relationship /</b> <i>Hubungan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$reference->relationship2}}
				 @if(empty($reference->relationship2))  -  @endif </li></ul>
			</ul>
		</td>
	</tr>
	<tr>
		<td valign="top"  class="header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>INCOME INFORMATION (MAIN APPLICANT)/</b> <i> MAKLUMAT PENDAPATAN (PEMOHON UTAMA)</i></td>
	</tr>

	<tr>
		<td valign="top">			
			<ul>	
			 	  <li>1. <b>Monthly Income/</b> <i> Pendapatan Bulanan </i> :  </li>
			  	<ul style=" margin-left: -10px;"> 
			  		<li >RM {{number_format((float)$financial->monthly_income, 2, '.', '')}}</li>
			  	</ul>
			  <li>2. <b>Other Income/</b> <i>Pendapatan Lain</i> : </li> 
			  	<ul style=" margin-left: -10px;"> 
			  		<li >RM {{number_format((float)$financial->other_income, 2, '.', '')}}</li>
			  	</ul>
			  
			  <li>3. <b>Total Income /</b>  <i>Jumlah Pendapatan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >RM {{number_format((float)$financial->monthly_income + $financial->other_income, 2, '.', '')}}</li>
			  	</ul>
			</ul>
		</td>
	</tr>

	<tr>
		<td valign="top"  class="header">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>YOUR COMMITMENTS WITH OTHER CREDIT PROVIDERS (NON-BANKS ONLY)**/</b> <i>  KOMITMEN DENGAN PEMBIAYA KREDIT LAIN <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(BUKAN BANK SAHAJA)**</i></td>
	</tr>

	<tr>
		<td valign="top">			
			<ul><b>** (e.g. AEON Credit, PTPTN, MARA, etc.)/</b> (cth. AEON Credit, PTPTN, MARA, dll.)<br>
				<b>Commitment 1/</b> Komitmen 1	<br><br>	
			 	  <li>1. <b>Name of Entity/</b> <i> Nama Entiti </i> :  </li>
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$commitments->name1}}
				 @if(empty($commitments->name1))  -  @endif </li>
			  	</ul>
			 
			  
			  	<li>2. <b>Type of Financing/</b>  <i>Jenis Pembiayaan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$commitments->financing1}}
				 @if(empty($commitments->financing1))  -  @endif </li>
			  	</ul>

		  	 	<li>3. <b>Monthly Payment/</b> <i>Bayaran Bulanan</i> : </li> 
			  	<ul style=" margin-left: -10px;"> 
			  		<li >
			  			@if($commitments->monthly_payment1==0) 
			  				-
					 	@else
					 		RM {{number_format((float)$commitments->monthly_payment1, 2, '.', '')}}
					  	@endif 
				  </li>
			  	</ul>
			  	<li>4. <b>Remaining Financing Term/</b>  <i>Baki Tempoh Pembiayaan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >
				 @if($commitments->remaining1==0)  - 

				 @else
				 	{{$commitments->remaining1}} Years/ Tahun
				  @endif </li>
			  	</ul>

			</ul>

				<ul>
				<b>Commitment 2/</b> Komitmen 2	<br><br>	
			 	  <li>1. <b>Name of Entity/</b> <i> Nama Entiti </i> :  </li>
			  	<ul style=" margin-left: -10px;"> 
			  		<li >{{$commitments->name2}}
				 @if(empty($commitments->name2))  -  @endif </li>
			  	</ul>
			 
			  
			  	<li>2. <b>Type of Financing/</b>  <i>Jenis Pembiayaan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >{{$commitments->financing2}}
				 @if(empty($commitments->financing2))  -  @endif </li>
			  	</ul>

		  	 	<li>3. <b>Monthly Payment/</b> <i>Bayaran Bulanan</i> : </li> 
			  	<ul style=" margin-left: -10px;"> 
			  		<li >
			  			@if($commitments->monthly_payment2==0) 
			  				-
					 	@else
					 		RM {{number_format((float)$commitments->monthly_payment2, 2, '.', '')}}
					  	@endif 
				  </li>
			  	</ul>
			  	<li>4. <b>Remaining Financing Term/</b>  <i>Baki Tempoh Pembiayaan</i> : </li>
			  	<ul style=" margin-left: -10px;"> <li >
				 @if($commitments->remaining2==0)  - 

				 @else
				 	{{$commitments->remaining2}} Years/ Tahun
				  @endif </li>
			  	</ul>

			</ul>
		</td>
	</tr>

</table>


						<table style="page-break-before: always" >
						<tr>
						<td colspan="3" style="line-height: 120%"><p  align="center"><b>SEKSYEN G - PENGISYTIHARAN/ <i> SECTION G - DECLARATION</b></p>
							 <b>Saya/kami dengan ini mengaku dan mengisytiharkan bahawa : </b><br>
                                                          <i>  I/we hereby agreed and declare that: <br></i> <br>
                                                          <ol type="i">
                                                        <li>  <b>Maklumat yang diberikan di dalam borang permohonan pembiayaan ini dan dokumen-dokumen lain adalah benar tanpa menyembunyikan maklumat yang mungkin mempengaruhi permohonan saya/kami dan pihak Co-opbank Persatuan berhak menolak / menarik balik sekiranya maklumat tersebut tidak benar dan palsu. </b> <br>
                                                          <i> All the information stated in the application form and the relevant supporting documents are true/genuine. Co-opbank Persatuan reserve the right to reject/withdraw this application in any case of fraud/forged documents. </i> </li>

                                                           <li>   <b>  Segala transaksi yang akan dilaksanakan tidak berkaitan apa jua perkara yang tidak dibenarkan dibawah 'AMLATFA 2001' (Akta Pencegahan Pengubahan Wang Haram, Pencegahan Keganasan dan Hasil daripada Akta Haram 2001).</b> <br>
                                                          <i>All the related transaction hereafter is not connected to Anti Money Laundering, Anti-Terrorism Financing and Proceed of Unlawful Activities Act 2001. <br></i> </li>

                                                          <li>    <b>Tidak dikenakan tindakan kebankrapan dan bukanlah seorang yang muflis seperti yang tertakluk dibawah Seksyen 3 Akta Kebankrapan 1967. Saya/kami akan memaklumkan kepada pihak  Co-opbank Persatuan dalam masa 7 hari sekiranya saya/kami berada dalam proses kebankrapan. </b><br><i>No bankruptcy proceeding are being taken against me/us and I/we are not a bankrupt under Section 3 of the Bankruptcy Act 1967, I undertake to notify  Co-opbank Persatuan within 7 days should there be any bankruptcy proceeding is taken against me/us. </i> </li>
                                                           <li>   <b>Membenarkan pihak  Co-opbank Persatuan menghubungi majikan  atau mana-mana pihak untuk mendapatkan sebarang keterangan mengenai saya/kami dan seterusnya membenarkan mereka  memberi keterangan yang diperlukan oleh pihak  Co-opbank Persatuan.</b> <br>
                                                          <i>Allow  Co-opbank Persatuan to contact my employer or any related parties to obtain the relevant information on me / us and thereafter allow them to provide the necessary information.</i> </li>
                                                          <li>  <b>Tiada sebarang permohonan pembiayaan di institusi kewangan lain serentak/dalam tempoh masa yang terdekat ketika borang permohonan ini dihantar ke  Co-opbank Persatuan.</b> <br> <i>I/we declare that I/we did not submit any other financing request to other financial institution simultaneously. 
                                                          </i> </li>
                                                          </ol>
                                                          <ol>
 <b>Saya/kami dengan ini memberi kebenaran kepada  Co-opbank Persatuan yang tidak boleh dibatal tanpa syarat untuk mendedahkan maklumat berkaitan dengan kemudahan kewangan saya/kami di  Co-opbank Persatuan bagi apa-apa tujuan sekalipun kepada Cawangan  Co-opbank Persatuan , Subsidiari, Bank Negara Malaysia (BNM), Unit Kredit Pusat (Central Credit Unit), Biro Maklumat Guaman dan agensi lain yang diluluskan oleh BNM atau mana-mana pihak berkuasa lain yang mempunyai bidang kuasa ke atas  Co-opbank Persatuan, dan pemegang  serahan hak yang dibenarkan dan diberikuasa oleh  Co-opbank Persatuan. Saya/kami membenarkan pihak  Co-opbank Persatuan mendapatkan maklumat peribadi kredit saya/kami dari CTOS, RAM dan mana-mana pihak berkuasa lain yang berkaitan.</b> <b> Maklumat yang diperolehi hanya boleh digunakan bagi tujuan permohonan pembiayaan sahaja. </b> <br> <i> I/we agreed to allow  Co-opbank Persatuan to reveal /forward all relevant information relating to this financing to any  Co-opbank Persatuan Branch, Subsidiaries, Central Bank of Malaysia (BNM), Central Credit Bureau and any other agencies approved by Central Bank of Malaysia (BNM) or any other authorities supervising  Co-opbank Persatuan. I/we hereby agree to allow  Co-opbank Persatuan to obtain the relevant information about me/us from CTOS, RAM or any other parties. All relevant information obtained are meant to be used for this specific financing only. <br></i>
 </ol>
							
						</td>
						</tr>
						
						<tr>
						<td colspan="3"> <br><br><br><br><br><br><br></td>
						</tr>

						<tr>
						<td align="center" style="line-height: 5%"> ---------------------------------- <p> Nama / <i>Name </i>  </p></td>
						<td align="center"  style="line-height: 5%">---------------------------------- <p> Tandatangan Pemohon /<i>Applicant's Signature</i> </p></td>
						<td align="center"  style="line-height: 5%"> ---------------------------------- <p>Tarikh / <i>Date</i> </p> </td>
						</tr>
						</table>

						
</body>
</html>



