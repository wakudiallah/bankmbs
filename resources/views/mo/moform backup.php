<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Add New Applicant";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>
<style>
.not-active {
   pointer-events: none;
   cursor: default;
}
input:read-only { 
    background-color: #eee;
}


</style>

    <?php
    include("asset/inc/nav.php");
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
 <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">

<script>
var apiGeolocationSuccess = function(position) {
  showLocation(position);
};

var tryAPIGeolocation = function() {
  jQuery.post( "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyDCa1LUe1vOczX1hO_iGYgyo8p_jYuGOPU", function(success) {
    apiGeolocationSuccess({coords: {latitude: success.location.lat, longitude: success.location.lng}});
  })
  .fail(function(err) {
    // alert("API Geolocation error! \n\n"+err);
    window.location.href = "/geolocation/error/"+err;
  });
};

var browserGeolocationSuccess = function(position) {
  showLocation(position);
};

var browserGeolocationFail = function(error) {
  switch (error.code) {
    case error.TIMEOUT:
      alert("Browser geolocation error !\n\nTimeout.");
      break;
    case error.PERMISSION_DENIED:
      if(error.message.indexOf("Only secure origins are allowed") == 0) {
        tryAPIGeolocation();
      }
         window.location.href = "{{url('/')}}/geolocation/error/"+error.code;
      break;
    case error.POSITION_UNAVAILABLE:
      alert("Browser geolocation error !\n\nPosition unavailable.");
      break;
  }
};

var tryGeolocation = function() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      browserGeolocationSuccess,
      browserGeolocationFail,
      {maximumAge: 50000, timeout: 20000, enableHighAccuracy: true});
  }
};

tryGeolocation();

function showLocation(position) {
  var latitude = position.coords.latitude;
  var longitude = position.coords.longitude;
  var _token = $('#_token').val();
  $.ajax({
    type:'POST',
    url: "{{url('/')}}/getLocation",
    data: { latitude: latitude, _token : _token, longitude : longitude },
    success:function(data){
            if(data){
              $("#latitude").val(data.latitude);
              $("#longitude").val(data.longitude);
               $("#location").val(data.location);
                 

            }else{
                $("#location").html('Not Available');
            }
    }
  });
}
</script>  
<div id="main" role="main">
    <div id="content">
<section id="widget-grid" class="">
                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                
                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"
                
                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>New Application - {{$data->first()->name}}</h2>
                
                                </header>
                
                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body">
                          
                                        <div class="row">
                                            <form id="wizard-1" novalidate="novalidate">
                                                <div id="bootstrap-wizard-1" class="col-sm-12">
                                                    <div class="form-bootstrapWizard">
                                                
                                                        <div class="clearfix"></div>
                                                    </div>
                                                    
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                              
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                   
                                                    <div class="tab-content">
                                 
                                                         
                                                         <div class="tab-pane active" id="tab1">
                                                            <br>
                                                            <h3><strong>Muat Naik Dokumen / <i>  Upload Document </i> </strong></h3>
                                                               <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <fieldset>
                                            
                                            <div class="container" style="">
       
                                                 <input name="id_praapplication" type="hidden"  value="{{$pra->id}}" >

                                                
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">E-mel Pelanggan *</label>
                                                    <div class="col-md-3 col-offset-4">
                                                    <input type="text" class="form-control" size="16" required maxlength="40" autocomplete="new-email" id="acus_email"  name="acus_email" placeholder="email@domain.com" value="{{$pra->acus_email}}">
                                                      <input type="hidden" size="16" maxlength="40" id="email_exists"  name="email_exists" value="{{$pra->email_exists}}">
                                                    </div>
                                                  </div>
                                                  <br><br><hr><br>
                                                   
                                              

                                            

                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Salinan Kad Pengenalan *</label>
                                                    <div class="col-md-8">
                            <input id="fileupload6"  @if(empty($document6->name)) required @endif  class="btn btn-default" type="file" name="file6"  >
                            <input type="hidden" name="document6"   id="documentx6"  value="Salinan Kad Pengenalan">
                            &nbsp; <span id="document6"> </span> 
                            <input type='hidden' value='@if(!empty($document6->name)){{$document6->upload}} @endif' id='a6' name='a6'/>
                            @if(!empty($document6->name))
                              <span id="document6a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document6->upload}}"> {{$document6->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif
                                  
                                                    </div>
                                                </div> &nbsp; <hr><br>

                             <div class="form-group">
                            <label class="col-md-4 control-label">Salinan Penyata Gaji Untuk 3 Bulan Terkini *</label>
                            <div class="col-md-8">
                           <input id="fileupload7"  @if(empty($document7->name)) required @endif   class="btn btn-default" type="file" name="file7" >
                            <input type="hidden" name="document7"  id="documentx7"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini">
                            &nbsp; <span id="document7"> </span> 

                            <input type='hidden' value='@if(!empty($document7->name)){{$document7->upload}} @endif' id='a7' name='a7'/>
                            @if(!empty($document7->name))
                              <span id="document7a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document7->upload}}"> {{$document7->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif
                            <br>
                             <i> Jika dokumen penyata gaji lebih daripada satu, sila muat naik dibawah </i>
                                                    
                             <input id="fileupload10"  class="btn btn-default" type="file" name="file10" >
                            <input type="hidden" name="document10"  id="documentx10"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini (optional 1))">
                            &nbsp; <span id="document10"> </span> 

                            @if(!empty($document10->name))
                              <span id="document10a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document10->upload}}"> {{$document10->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif

                            <input id="fileupload11"  class="btn btn-default" type="file" name="file11" >
                            <input type="hidden" name="document11"  id="documentx11"  value="Salinan Penyata Gaji Untuk 3 Bulan Terkini (optional 2)">
                            &nbsp; <span id="document11"> </span> 
                            @if(!empty($document11->name))
                              <span id="document11a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document11->upload}}"> {{$document11->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif
            
             
                                                    </div>
                                                </div>
                         
                                               
                                        
                         
                                               
                                                      &nbsp; <hr><br>
                                                <div class="form-group">
                                                    <label class="col-md-4 control-label">Surat Pengesahan Majikan</label>
                                                    <div class="col-md-8">
                            <input id="fileupload4"  class="btn btn-default" type="file" name="file4"  >
                            <input type="hidden" name="document4"   id="documentx4"  value="Surat Pengesahan Majikan">
                            &nbsp; <span id="document4"> </span> 
                            <input type='hidden' value='@if(!empty($document4->name)){{$document4->upload}} @endif' id='a4' name='a4'/>
                            @if(!empty($document4->name))
                              <span id="document4a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document4->upload}}"> {{$document4->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif
                                  
                                                    </div>
                                                </div>

                                                               &nbsp; <hr><br>

                         
                             <div class="form-group">
                            <label class="col-md-4 control-label">Penyata Penyelesaian Awal (Jika berkenaan)</label>
                            <div class="col-md-8">
                           <input id="fileupload1"  @if(empty($document1->name)) @endif   class="btn btn-default" type="file" name="file1" >
                            <input type="hidden" name="document1"  id="documentx1"  value="Salinan Penyata Penyelesaian Awal">
                            &nbsp; <span id="document1"> </span> 
                            @if(!empty($document1->name))
                              <span id="document1a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document1->upload}}"> {{$document1->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif
                            <br>
                             <i> Jika dokumen penyata penyelesaian awal lebih daripada satu, sila muat naik dibawah </i>
                                                    
                             <input id="fileupload2"  class="btn btn-default" type="file" name="file2" >
                            <input type="hidden" name="document2"  id="documentx2"  value="Salinan Penyata Penyelesaian Awal (optional 1)">
                            &nbsp; <span id="document2"> </span> 
                            @if(!empty($document2->name))
                              <span id="document2a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document2->upload}}"> {{$document2->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif

                            <input id="fileupload3"  class="btn btn-default" type="file" name="file3" >
                            <input type="hidden" name="document3"  id="documentx3"  value="Salinan Penyata Penyelesaian Awal  (optional 2)">
                            &nbsp; <span id="document3"> </span> 
                            @if(!empty($document3->name))
                              <span id="document3a"><a target='_blank' href="{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/{{$document3->upload}}"> {{$document3->name}}</a><i class="glyphicon glyphicon-ok txt-color-green"></i></span> 
                            @endif
            
             
                                                    </div>
                                                </div>

                                                    <div class="row"><br>
                                                               <div class="col-lg-4">
                                                               </div>
                                                               <div class="col-lg-5">
                                                               </div>
                                                                <div class="col-lg-3">
                                                                  <p class="right"><a id="agree" class="btn btn-lg btn-primary" ><i class="fa fa-check"></i> 
                                                            Hantar / <i> Submit </i>
                                                           </a> </p>
                                                               </div>
                                                          
                                                        </div>

                                            </div>
                                               
                                            </fieldset>
                                                          
                                                        </div>

                
                                                        <div class="form-actions">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <ul class="pager wizard no-margin">
                                                                        <!--<li class="previous first ">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-default"> First </a>
                                                                        </li>-->
                                                                 
                                                                        <!--<li class="next last">
                                                                        <a href="javascript:void(0);" class="btn btn-lg btn-primary"> Last </a>
                                                                        </li>-->
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                
                                    </div>
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                
                            </div>
                            <!-- end widget -->
                
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET END -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
     </div>
</div>
            <br>
        <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
            
            
    
            //Bootstrap Wizard Validations

              var $validator = $("#wizard-1").validate({
                
                rules: {
                  acus_email: {
                    required: false,
                    email: "Your email address must be in the format of name@domain.com"
                  },
                  
                  fname: {
                    required: true
                  },
                  lname: {
                    required: true
                  },
                  country: {
                    required: true
                  },
                  country_v: {
                    required: true
                  },
        
                  wphone: {
                    required: true,
                    minlength: 10
                  },
                  hphone: {
                    required: true,
                    minlength: 10
                  }
                },
                
               
                
                messages: {
                  fname: "Please specify your First name",
                  lname: "Please specify your Last name",
                  email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                  }
                },
                
                highlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                  } else {
                    error.insertAfter(element);
                  }
                }
              });
              
              $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                  var $valid = $("#wizard-1").valid();
                  if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                  } else {


                    if (index=='2') {
                          var email_exists = $('#email_exists').val();

                          if(email_exists==1) {
                             bootbox.alert('[No.10] Customer with this email is already exists, Try another email address');
                                 $validator.focusInvalid();
                                  return false;
                              
                          }
                       
                                
                    }
                        
               

                      $.ajax({

                            type: "PUT",
                           
                            url: '{{ url('/agentform/') }}'+'/'+index,
                            data: $('#tab'+index+' :input').serialize(),

                            cache: false,
                            beforeSend: function () {

                               
                            },

                            success: function () {
                             

                            
                             

                            },
                            error: function () {
                               
                                 

                            }
                        });
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                  
                }
                }
              });
              
        
            // fuelux wizard
              var wizard = $('.wizard').wizard();
              
              wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
                
              });

        
        })

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

        </script>
<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

 <script type="text/javascript">

$( "#acus_emailzz" ).keyup(function() {
    var emel = $('#acus_email').val();
    var id_praapplication = $('#id_pra2').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/email_check/"+emel+"/"+id_praapplication,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {
                    $("#email_exists").val(data.status);
                
                }
            });

   
});
// save email while key in
$( "#acus_email" ).change(function() {
    var emel = $('#acus_email').val();
    var id_praapplication = $('#id_pra2').val();
     $("#acus_email2").val(emel);

     
 /* $.ajax({
                url: "<?php  print url('/'); ?>/email_save/"+emel+"/"+id_praapplication,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {
                
                }
            }); */

   
});


$( "#postcode" ).change(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city").val(data[k].post_office );
                        $("#state").val(data[k].state.state_name );
                    });

                }
            });

   
});
</script>
<script type="text/javascript">

$( "#postcode3" ).change(function() {
    var postcode = $('#postcode3').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city3").val(data[k].post_office );
                        $("#state3").val(data[k].state.state_name );
                    });

                }
            });

   
});
</script>
  <script type="text/javascript">

$( "#postcode2" ).change(function() {
    var postcode = $('#postcode2').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city2").val(data[k].post_office+' / '+data[k].state.state_name );
                  
                    });

                }
            });

   
});
</script>

<script type="text/javascript">

$( "#postcode4" ).change(function() {
    var postcode = $('#postcode4').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#city4").val(data[k].post_office );
                        $("#state4").val(data[k].state.state_name );
                    });

                }
            });

   
});
</script>

  <script type="text/javascript">

$( "#status" ).change(function() {
    var status = $('#status').val();
     
    if(status > 1 ) {

         $("#tab99").hide();
          $("#tab98").hide();
    }
    else {

         $("#tab99").show();
         $("#tab98").show();
    }

   
});
</script>


<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : '{{url('/')}}/form/upload/{{$x}}';
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
              $("#a{{$x}}").val(data.file);
            

             document.getElementById("fileupload{{$x}}").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>

<script type="text/javascript">



$( ".promosi" ).click(function() {
    var pernyataan = $('input[name="pernyataan"]:checked').val();
    $('#pernyataan').val(pernyataan);

});

$( "#agree" ).click(function() {
     var icnumber = $('#new_ic').val();
var acus_email = $('#acus_email').val();
var dua = $('#dua:checked').val();
var tiga = $('#tiga:checked').val();
var pertama = $('#pertama:checked').val();



if(acus_email=="") {
    alert("Please enter customer email address");
    return false;
}

if ($("#a6").val()=='') {
    alert('Sila muat naik salinan kad pengenalan');
    return false;
}

if ($("#a7").val()=='') {
    alert('Sila muat naik salinan penyata gaji untuk 3 bulan terkini');
    return false;
}


$('#myModal').modal('show');
              
   
});


</script>

    <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Pengesahan</h4>
                    </div>
                    <div class="modal-body">
        {!! Form::open(['url' => 'agentform/term','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                      <fieldset>
                                      
                                          <input type="hidden" autocomplete="false" name="pernyataan" id="pernyataan">
                                           <input type="hidden" autocomplete="false" name="acus_email" id="acus_email2">
                                          <input type="hidden" id="id_pra_submit" name="id_praapplication" value='{{$pra->id}}'>
                                          <input type="hidden" id="token" name="_token" value='{{csrf_token()}}'>

                                            <input type='hidden' name='latitude' id='latitude'>
                                             <input type='hidden' name='longitude' id='longitude'>
                                             <input type='hidden' name='location' id='location'>
                                        
                                        <span id="message"></span>


                                        <section >
                                            <label class="label">Kata Laluan</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" autocomplete="off" id="password" name="password" placeholder="Password" >
                                                <b class="tooltip tooltip-bottom-right">Password</b>
                                            </label>

                                        </section>
                             
                                 
                               
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      
                                     
                                </fieldset>
                       
                       
        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <a id="submit_application" class="btn btn-primary">
                                       Submit
                                    </a>
                                   
                           {!! Form::close() !!}   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



<script type="text/javascript">


$( "#submit_application" ).click(function() {
    var pernyataan = $('input[name="pernyataan"]:checked').val();
    var _token = $('#token').val();
    var id_praapplication = $('#id_pra_submit').val();
    var password = $('#password').val();
    var acus_email = $('#acus_email').val();
    var latitude = $('#latitude').val();
    var longitude = $('#longitude').val();
    var location = $('#location').val();

    $.ajax({

        type: "POST",
       
        url: '{{ url('/moform/term/') }}',
        data:  { id_praapplication: id_praapplication, _token : _token, pernyataan : pernyataan, password : password, acus_email : acus_email, latitude : latitude, longitude :longitude, location : location},

        cache: false,
        beforeSend: function () {
            $("#message").html("<div class='alert alert-default alert-dismissable'>"+
                    "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                    "Please Wait..</div>");
        },

        success: function (data) {
         
              if(data.status==0) {
                 
                    $("#message").html("<div class='alert alert-danger alert-dismissable'>"+
                    "<a href='#'' class='close' data-dismiss='alert' aria-label='close'>&times;</a>"+
                    data.message+"</div>");
                                    
                }
                else {
                     alert("Success!");
                     window.location.href='{{url('/')}}/admin';

                }
         

        },
        error: function () {
           
             

        }

    })
 });


   

</script>
   
<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}

function minmaxtempoh(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 1; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}

</script>

<script>
function toFloat(z) {
    var x = document.getElementById(z);
    x.value = parseFloat(x.value).toFixed(2);
}
</script>
   <script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("select option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });

</script>

<script>
   $('#tgl').datepicker({
                dateFormat : 'dd/mm/yy',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

</script>
<script>
    $('.startmonth').datepicker({
                    changeMonth: true,
                    changeYear: true,
                    changeDate: false,
                
                    dateFormat: 'MM yy',
                    onClose: function(dateText, inst) { 
                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                        $(this).datepicker('setDate', new Date(year, month, 1));
                    }
                });
     $(".startmonth").focus(function () {
        $(".ui-datepicker-calendar").hide();
        $("#ui-datepicker-div").position({
            my: "center top",
            at: "center bottom",
            of: $(this)
        });
    });
</script>

<script type="text/javascript">
$( "#name" ).keyup(function() {
   var cal_name = $('#name').val();
    $("#cal_name").val(cal_name);
    });
</script>

<script type="text/javascript">
            function show(str){
            
        document.getElementById('add_redemption_button').style.display = 'none';
        document.getElementById('status_penyelesaian').value = '0';
        
            }
            function show2(sign){
               
        document.getElementById('add_redemption_button').style.display = 'block';
        document.getElementById('status_penyelesaian').value = '1';
             
            }


    
</script>

