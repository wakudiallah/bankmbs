@if($user->role==10)
<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Marketing List";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


include("asset/inc/nav.php");


?>
@else
<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Marketing List";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["master"]["sub"]["package"]["active"] = true;
include("asset/inc/nav.php");


?>
@endif
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
        @include('sweetalert::alert')
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><br>
            <a href='' data-toggle='modal' data-target='#addBranch' class='btn btn-success'><i class='fa fa-plus'></i> Add MO</a>
            <div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title" id="addBranch">New Marketing Officer</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::open(['url' => 'marketing','method' => 'post','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                            <fieldset>
                                <section>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" id="Name" name="name" placeholder="Full Name" required onkeyup="this.value = this.value.toUpperCase()" onkeypress="return FullName(event);">
                                        <b class="tooltip tooltip-bottom-right">Full Name</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="text" id="Employer_ID" name="employer_id" placeholder="Employer ID" required>
                                        <b class="tooltip tooltip-bottom-right">Employer ID</b>
                                    </label>
                                </section>
                                @if($user->role==4) 
                                <section>
                                    <label class="select">
                                        <i class="icon-append fa fa-user"></i>
                                        <select class="form-control" name="manager_id" id="manager_id" required="required">
                                            <option selected="">select manager</option>
                                                @foreach ($manager as $manager)
                                                    <option  value="{{$manager->id}}">{{$manager->name}}</option>
                                                @endforeach
                                        </select>
                                    </label>
                                </section>
                                @else
                                <section>             
                                    <label class="input">
                                        <i class="icon-append fa fa-user"></i>
                                        <input type="hidden" id="manager_id" name="manager_id" placeholder="Employer ID" readonly="" value="{{$user->id}}">
                                        <input type="text" id="Employer_ID" name="manager_name" placeholder="Employer ID" readonly="" value="{{$user->name}}">
                                        <b class="tooltip tooltip-bottom-right">Manager Name</b>
                                    </label>
                                </section>
                                @endif
                                <section>          
                                    <label class="input">
                                        <i class="icon-append fa fa-phone"></i>
                                        <input type="text" id="phone" name="phone" placeholder="Phone No" required i pattern="/^-?\d+\.?\d*$/" onKeyPress="if(this.value.length==12) return false;" minlength="7" maxlength="14" >
                                        <b class="tooltip tooltip-bottom-right">Phone No</b>
                                    </label>
                                </section>
                                <section>
                                    <label class="input">
                                        <i class="icon-append fa fa-envelope"></i>
                                        <input type="Email" id="Email" name="email" placeholder="Email" required>
                                        <b class="tooltip tooltip-bottom-right">Email</b>
                                    </label>
                                </section>
                                <section>
                                    Set Password ?
                                    <label class="select">
                                        <select name="set_password" id="set_password">
                                            <option value="1" >Yes</option>
                                            <option value="0">No</option>
                                        </select>
                                        <b class="tooltip tooltip-bottom-right">Set Password?</b>
                                    </label>
                                </section>
                                <div id="section_password">
                                    <section>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>
                                            <input type="password" autocomplete="off" id="password" name="password" placeholder="Password" required>
                                            <b class="tooltip tooltip-bottom-right">Password</b>
                                        </label>
                                    </section>
                                    <section>
                                        <label class="input">
                                            <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Password Confirmation" required>
                                            <b class="tooltip tooltip-bottom-right">Password Confirmation</b>
                                        </label>
                                    </section>
                                </div>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            </fieldset>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                Cancel
                            </button>
                            <button type="submit" name="submit" class="btn btn-primary">
                               Submit
                            </button>
                            {!! Form::close() !!}   
                        </div>
                    </div>
                </div>
            </div><br><br>
                            
            <div class="jarviswidget well" id="wid-id-0">
                <header>
                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                    <h2>Widget Title </h2> 
                </header>
                <div>
                    <div class="jarviswidget-editbox"></div>
                    <div class="widget-body no-padding">
                        <table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Employer ID</th>
                                    <th>Full Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Manager</th>
                                    <th>Status</th>
                                    <th>Detail/Action</th>
                               
                                </tr>
                            </thead>
                            <tbody>
                            <?php $i=1; ?>
                            @foreach ($marketing_user as $a)
                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $a->id_mo }}</td>
                                    <td>{{ $a->desc_mo }}</td>
                                    <td>{{ $a->email }}</td>
                                    <td>{{ $a->phone }}</td>
                                    <td>@if(!empty($a->id_manager))
                                            {{ $a->Manager->name }}
                                        @endif
                                    </td>
                                    <td>
                                        @if($a->active_user==1) 
                                            <span class='label label-success'><b>Active</b></span>
                                        @else
                                            <span class='label label-default'><b>Not Active</b></span>
                                        @endif
                                    </td>
                                    <td> <a href='#' class="btn btn-primary btn-sm" data-toggle='modal' data-target='#myModal{{$a->id}}'> <i class="fa fa-cog fa-lg"></i> </a> &nbsp; | &nbsp; 
                                        <!--
                                        <a href='#' data-toggle='modal' data-target='#deleteModal{{$i}}'> <i class="fa fa-trash-o fa-lg"></i></a>
                                        <div class="modal fade" id="deleteModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal{{$i}}" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                        &times;
                                                    </button>
                                                    <h4 class="modal-title" id="DeleteBranch"><b>Are You Sure to Delete  
                                                    {{ $a->name }} ? </b></h4>
                                                </div> 
                                                <div class="modal-body">
                                                    {!! Form::open(['url' => 'agent/'.$a->id,'method' => 'DELETE' ]) !!}
                                                    <div align='right'>
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <input type="hidden" name="name" value="{{ $a->name }}">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                                            No
                                                        </button>
                                                       <button type="submit" name="submit" class="btn btn-primary">
                                                            Yes
                                                        </button></div>
                                                        {!! Form::close() !!}   
                                                    </div>
                                                </div>
                                            </div>
                                       </div> -->
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            @endforeach
                            </tbody>
                        </table>	
                    </div><br><br><br><br>
                </div>
            </div>
        </article>
    </div>
</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        pageSetUp();
            var responsiveHelper_dt_basic = undefined;
            var responsiveHelper_datatable_fixed_column = undefined;
            var responsiveHelper_datatable_col_reorder = undefined;
            var responsiveHelper_datatable_tabletools = undefined;
            
            var breakpointDefinition = {
                tablet : 1024,
                phone : 480
            };
            $('#datatable_col_reorder').dataTable({
            "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
                    "t"+
                    "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
            "autoWidth" : true,
            "preDrawCallback" : function() {
                // Initialize the responsive datatables helper once.
                if (!responsiveHelper_datatable_col_reorder) {
                    responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
                }
            },
            "rowCallback" : function(nRow) {
                responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
            },
            "drawCallback" : function(oSettings) {
                responsiveHelper_datatable_col_reorder.respond();
            }           
        });
    })
</script>
<script>
  /* BASIC ;*/
    var responsiveHelper_dt_basic = undefined;
    var responsiveHelper_datatable_fixed_column = undefined;
    var responsiveHelper_datatable_col_reorder = undefined;
    var responsiveHelper_datatable_tabletools = undefined;
    
    var breakpointDefinition = {
        tablet : 1024,
        phone : 480
    };
    $('#dt_basic').dataTable({
        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
            "t"+
            "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
        "autoWidth" : true,
        "preDrawCallback" : function() {
            // Initialize the responsive datatables helper once.
            if (!responsiveHelper_dt_basic) {
                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
            }
        },
        "rowCallback" : function(nRow) {
            responsiveHelper_dt_basic.createExpandIcon(nRow);
        },
        "drawCallback" : function(oSettings) {
            responsiveHelper_dt_basic.respond();
        }
    });        
</script>

<script>
  $(document).ready(function() {
    // show the alert
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
            $(this).remove(); 
        });
        }, 2800);
});
</script>
    
<script type="text/javascript">
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({
            // Rules for form validation
           rules : {
                Email: {
                    required : true,
                    email : true
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                 Branch : {
                    required : true
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },
            // Messages for form validation
             messages : {

                    Email: {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                    Branch: {
                    required : 'Please select a Branch'
                },
      
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
            }
        });
    });
</script>


@foreach ($marketing_user as $b)

<div class="modal fade" id="myModal{{$b->id}}" tabindex="-1" role="dialog" aria-labelledby="myModal{{$i}}" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="addBranch"> {{ $b->name }} </h4>
            </div>
            <div class="modal-body">
             {!! Form::open(['url' => 'activate_agent','class' => 'smart-form client-form', 'id' =>'smart-form-register3','method'=>'POST', 'enctype' =>'multipart/form-data' ]) !!} 
              <fieldset>
              <input type="hidden" name="marketing_id" value="{{$b->id}}">
                    <section >
                          <label class="label">Full Name: </label>
                             <label class="input">
                                <i class="icon-append fa fa-user"></i>
                                <input type="text" id="fullname" name="fullname" placeholder="Full Name " value="{{ $b->desc_mo }}" required onkeyup="this.value = this.value.toUpperCase()">
                                <b class="tooltip tooltip-bottom-right">Full Name</b>
                            </label>
                        </section>
                        <section>
                          <label class="label">Employer ID: </label>
                             <label class="input">
                                <i class="icon-append fa fa-address-card"></i>
                                <input type="text" maxlength="12" minlength="12" id="employer_id" name="employer_id" placeholder="Employer ID " value="{{ $b->id_mo }}" required>
                                <b class="tooltip tooltip-bottom-right">Employer ID</b>
                            </label>
                        </section>
                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-envelope"></i>
                                <input type="Email" id="Email" name="email" placeholder="Email" value="{{ $b->email }}" required>
                                <b class="tooltip tooltip-bottom-right">Email</b>
                            </label>
                        </section>
                        <section>
                              <label class="label">Phone No: </label>
                            <label class="input">
                            
                                <i class="icon-append fa fa-mobile fa-lg"></i>
                                <input type="text" id="phoneno" name="phoneno" placeholder="Handphone" value="{{ $b->phone }}" required>
                                <b class="tooltip tooltip-bottom-right">Handphone</b>
                            </label>
                        </section>
                       <section>
                        <label class="label">Manager:</label>
                        <label class="select">
                            <i class="icon-append fa fa-user"></i>
                             <select class="form-control" name="manager_id" id="manager_id" required="required">
                                 <option selected=""></option>
                                @foreach ($manager1 as $mg1)
                                    @if($b->id_manager !=NULL)
                                        <?php 
                                            if($b->id_manager==$mg1->id) {
                                              $selected = "selected";
                                            }
                                            else {
                                               $selected = "";
                                            } 
                                        ?>
                                            <option {{$selected}} value="{{$mg1->id}}">{{$mg1->name}}</option>
                                    @else
                                        <option value="{{$mg1->id}}">{{$mg1->name}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </label>
                    </section>
                    <section>
                        <label class="label">Current Status:</label>
                        <label class="input">
                            @if($b->active_user==0) 
                            <span class='label label-warning'><b>Inactive</b></span>
                            @elseif($b->active_user==1) 
                             <span class='label label-success'><b>Active</b></span>
                            @endif
                        </label>
                    </section>
                    <section>
                        <label class="label">Set New Status:</label>
                        <label class="select">
                            <i class="icon-append fa fa-history"></i>
                            <select name="status" id="status{{$b->id}}">
                                <option value="-99" selected disabled>-Select-</option> 
                                <option value="0">Inactive</option>
                                <option value="1">Active </option>
                            </select>
                            <b class="tooltip tooltip-bottom-right">Set New Status</b>
                        </label>
                    </section>
                    <section>
                        Set Password ?
                        <label class="select">
                            <select name="set_password" id="set_password2">
                                <option value="1" >Yes</option>
                                <option value="0">No</option>
                            </select>
                            <b class="tooltip tooltip-bottom-right">Set Password?</b>
                        </label>
                    </section>

                    <div id="section_password2">
                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-lock"></i>
                                <input type="password" autocomplete="off" id="password2" name="password" placeholder="Password" required>
                                <b class="tooltip tooltip-bottom-right">Password</b>
                            </label>
                        </section>
                        <section>
                            <label class="input">
                                <i class="icon-append fa fa-lock"></i>
                                <input type="password" id="password_confirmation2" name="password_confirmation" placeholder="Password Confirmation" required>
                                <b class="tooltip tooltip-bottom-right">Password Confirmation</b>
                            </label>
                        </section>
                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 </fieldset>
            </div>
            <div class="modal-footer">
                <button type="submit" name="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancel
                </button>
                {!! Form::close() !!}   
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endforeach

<script type="text/javascript">
$( "#set_password" ).change(function() {
    var set_password = $('#set_password').val();
    if(set_password == 1) {
        $("#section_password").show();
        $("#password").prop('required',true);
        $("#password_confirmation").prop('required',true);
    }
    else {
        $("#section_password").hide();
        $("#password").prop('required',false);
        $("#password_confirmation").prop('required',false);
    }
});
</script>
<script type="text/javascript">
$( "#set_password2" ).change(function() {
    var set_password2 = $('#set_password2').val();
    if(set_password2 == 1) {
        $("#section_password2").show();
        $("#password2").prop('required',true);
        $("#password_confirmation2").prop('required',true);
    }
    else {
        $("#section_password2").hide();
        $("#password2").prop('required',false);
        $("#password_confirmation2").prop('required',false);
    }
});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $("#phone").keypress(function(event){
            var inputValue = event.charCode;
            if(!(inputValue >= 48 && inputValue <= 57)){
                event.preventDefault();
            }
        });
    });
</script>