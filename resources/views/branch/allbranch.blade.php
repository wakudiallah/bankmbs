<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title =  "Branch Master";


$page_css[] = "your_style.css";
include("asset/inc/header.php");


$page_nav["master"]["sub"]["mbsb"]["sub"]["branchmaster"]["active"] = true;
include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	    @if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                            <br>
                            <a href='' data-toggle='modal' data-target='#addBranch' class='btn btn-success'><i class='fa fa-bank'></i> Add Branch</a>
                            	<!-- Modal -->
                                <div class="modal fade" id="addBranch" tabindex="-1" role="dialog" aria-labelledby="addBranch" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="addBranch">Add Branch</h4>
                                            </div>
                                            <div class="modal-body">
                                             {!! Form::open(['url' => 'admin/addbranch','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                                              <fieldset>
                                                    <section >
                                                          <label class="label">Branch Name </label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-bank"></i>
                                                                <input type="text" id="Name" name="Name" placeholder="Branch Name " required>
                                                                <b class="tooltip tooltip-bottom-right">Branch Name</b>
                                                            </label>
                                                        </section>
                                                        <section >
                                                          <label class="label">Address </label>
                                                             <label class="input">
             
                                                                <textarea id='Address' name='Address' class='form-control' rows="4" cols="50" required> </textarea>
                                                                <b class="tooltip tooltip-bottom-right">Address</b>
                                                            </label>
                                                        </section>
                                                        <section >
                                                          <label class="label">Telephone Number </label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-phone"></i>
                                                                <input type="text" id="Telephone" name="Phone" placeholder="Telephone Number" required>
                                                                <b class="tooltip tooltip-bottom-right">Telephone Number</b>
                                                            </label>
                                                        </section>
                                                            <label class="label">Fax </label>
                                                             <label class="input">
                                                                <i class="icon-append fa fa-fax"></i>
                                                                <input type="text" id="Fax" name="Fax" placeholder="Fax">
                                                                <b class="tooltip tooltip-bottom-right">Fax</b>
                                                            </label>
                                                        </section>
                                                       
                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                 </fieldset>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Cancel
                                                </button>
                                                <button type="submit" name="submit" class="btn btn-primary">
                                                               Submit
                                                            </button>
                                                           
                                                   {!! Form::close() !!}   
                                            </div>
                                        </div><!-- /.modal-content -->
                                    </div><!-- /.modal-dialog -->
                                </div><!-- /.modal -->
                            
                            <br><br>

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>
                                                <tr>
                                                    <th>No</th>
                                                    <th>Branch</th>
                                                    <th>Address</th>
                                                    <th>Telephone No.</th>
                                                    <th>Fax</th>
                                                  
													<th>Edit</th>
                                                    <th>Delete</th>
                                               
                                                </tr>
											</thead>
                                            <tbody>
                                            <?php $i=1; ?>
                                            @foreach ($branch as $a)
                                                <tr>
                                                    <td>{{ $i }}</td>
                                                    <td>{{ $a->branchname }}</td>
                                                    <td>{{ $a->address }}</td>
                                                    <td>{{ $a->phone }}</td>
                                                    <td>{{ $a->fax }}</td>
                                           
													<td> <a href='#' data-toggle='modal' data-target='#myModal{{$i}}'>Edit</a>
													 <!-- Modal -->
													<div class="modal fade" id="myModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="myModal{{$i}}" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="addBranch">Branch {{ $a->branchname }} </h4>
																</div>
																<div class="modal-body">
																 {!! Form::open(['url' => 'admin/editbranch','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
																  <fieldset>
																		<section >
																			  <label class="label">Branch Name </label>
																				 <label class="input">
																					<i class="icon-append fa fa-bank"></i>
																					<input type="text" id="Name" name="Name" placeholder="Branch Name " value="{{ $a->branchname }}" required>
																					<b class="tooltip tooltip-bottom-right">Branch Name</b>
																				</label>
																			</section>
																			<section >
																			  <label class="label">Address </label>
																				 <label class="input">
								 
																					<textarea id='Address' name='Address' class='form-control' rows="4" cols="73" required>{{ $a->address }}</textarea>
																					<b class="tooltip tooltip-bottom-right">Address</b>
																				</label>
																			</section>
																			<section >
																			  <label class="label">Telephone Number </label>
																				 <label class="input">
																					<i class="icon-append fa fa-phone"></i>
																					<input type="text" id="Telephone" name="Phone" placeholder="Telephone Number" value="{{ $a->phone }}" required>
																					<b class="tooltip tooltip-bottom-right">Telephone Number</b>
																				</label>
																			</section>
																			<section>
																				<label class="label">Fax </label>
																				 <label class="input">
																					<i class="icon-append fa fa-fax"></i>
																					<input type="text" id="Fax" name="Fax" value="{{ $a->fax }}" placeholder="Fax">
																					<b class="tooltip tooltip-bottom-right">Fax</b>
																				</label>
																			</section>
																			<section >
																			  <label class="label">Status </label>
																				<label class="input">
																			   
																					<select name='Status' id='Status' required class='form-control'>
																				
																						   @if($a->status=='1')
																							<option selected value="1">Active</option> 
																						     <option value="0">Inactive</option>
																							@else
																							 <option  value="1">Active</option> 
																						     <option selected value="0">Inactive</option>
																							@endif
																			
																					</select>
																					<b class="tooltip tooltip-bottom-right">Select Branch</b>
																				</label>
																			</section>
																		   
																		 <input type="hidden" name="_token" value="{{ csrf_token() }}">
																		  <input type="hidden" name="idBranch" value="{{ $a->id }}">
																	 </fieldset>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Cancel
																	</button>
																	<button type="submit" name="submit" class="btn btn-primary">
																				   Submit
																				</button>
																			   
																	   {!! Form::close() !!}   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
													</td>
                                                    <td>
                                                        <a href='#' data-toggle='modal' data-target='#deleteModal{{$i}}'>Delete</a>
													 <!-- Modal -->
													<div class="modal fade" id="deleteModal{{$i}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal{{$i}}" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="DeleteBranch"><b>Are You Sure to Delete Branch 
                                                                      {{ $a->branchname }} ? </b></h4>
																</div>
														
																<div class="modal-body">
                                                                     {!! Form::open(['url' => 'admin/deletebranch' ]) !!}
                                                                    <div align='right'>
                                                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
																		  <input type="hidden" name="idBranch" value="{{ $a->id }}">
                                                                        <input type="hidden" name="branchname" value="{{ $a->branchname }}">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		No
																	</button>
																	<button type="submit" name="submit" class="btn btn-primary">
																				   Yes
																				</button></div>
																			   
																	   {!! Form::close() !!}   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
                                                    </td>
                                                  
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
											</tbody>
                                        </table>
																					   
										
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->

<!-- PAGE FOOTER -->
<?php
    // include page footer
    include("asset/inc/footer.php");
?>
<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>
    
    <script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Name: {
                    required : true,
                    minlength : 1,
                    maxlength : 50
                },
                TelephoneNumber: {
                    required : true,
                    minlength : 11,
                    maxlength : 11
                }
            },

            // Messages for form validation
             messages : {

                    Name: {
                    required : 'Please enter Branch Name',
                    email : 'Please enter a VALID email address'
                }
                    }
        });

    });
</script>



          
