<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Pinjaman Peribadi i-Destinasi ";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
    <div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
                     @if (Session::has('message'))
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>{{ Session::get('message') }}</strong> 
    </div>
            
            @endif             
            @if (count($errors) > 0)
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
           
          
                <div class="row">
              <div class="col-xs-10 col-sm-10 col-md-6 col-lg-3">
            </div>

          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
            <div class="well no-padding">

               {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
                <header>
                  <p class="txt-color-white"><b>   Semak Kelayakan Pembiayaan / Pinjaman Anda  </b> </p>
                </header>

                <fieldset>
                                   <div class="row">
                                    
                                     <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                          <label class="label"> <b> Nama Penuh </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="FullName" name="FullName" placeholder="Full Name"    @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                                            </label>
                                        </section>
                                        </div>
                                        <div class="col-xs-6 col-12">
                                             <section class="col col-12">
                                            <label class="label"> <b> No Kad Pengenalan </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                            </label>
                                        </section>

                                        </div>
                                    </div>
                                        <div class="row">
                                         <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                            <label class="label"> <b> Nombor Telefon Bimbit</b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-mobile-phone"></i>
                                                <input type="text" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number"  minlength="7" maxlength="12"  onkeypress="return isNumberKey(event)"  @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>
                                                <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>
                                            </label>
                                        </section>
                                        </div>
                                
                                    
                                          <div class="col-xs-6 col-12">
                                        <section class="col col-10 col-lg-10 col-md-10 col-xs-10">
                                            <label class="label"> <b> Jenis Pekerjaan </b></label>
                                                <label class="select" style="width: 206px !important">
                                                    <select name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employment')) 
                                                    <option  value="{{ Session::get('employment') }}">{{ Session::get('employment2') }}</option>
                                                     
                                                     
                                                     
                                                     
                                                     
                                                    @endif
                                                       
                                                         @foreach ($employment as $employment)
                            <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                              @endforeach
                                                    </select> <i></i>

                                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                                </label>
                                            </section>
                                        </div>
                                      
                                        </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                         <div class="col-xs-6 col-12">
                                             <section class="col col-12 col-lg-10">
                                            <label class="label"> <b> Majikan</b></label>
                                                <label class="select" id="majikan">
                                                    <select name="Employer" id="Employer" class="form-control" onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employer')) 
                                                    <option  value="{{ Session::get('employer') }}">{{ Session::get('employer2') }}</option>
                                                     
                                                     
                                                    @endif

                                                   
                                                       
                                                    </select> <i></i>
                                                    <input type="hidden" name="Employer2" id="Employer2" value="" />
                                                </label>

                                                 <label id="majikan2" class="input @if (Session::has('basicsalary')) state-error @endif ">
                                                <i class="icon-append fa fa-briefcase"></i>
                                                <input type="text" id="majikantext" name="majikan"  placeholder="Majikan" maxlength="70"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right"> Majikan</b>
                                            </label>

                                            </section>
                                            </div>
                                          <div class="col-xs-6 col-12">
                                                      <section class="col col-12">
                                            <label class="label"> <b> Gaji Asas (RM) </b>  </label>
                                            <label class="input @if (Session::has('basicsalary')) state-error @endif ">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="BasicSalary" id="BasicSalary" placeholder="Basic Salary (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Gaji Asas (RM) </b>
                                            </label>
                                        </section>
                                            </div>
                                        </div>
                                            <div class="row">
                                                    <div class="col-xs-6 col-12">
                                                       <section class="col col-12">
                                                    <label class="label"> <b> Elaun (RM) </b></label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                        <input type="text" name="Allowance" id="Allowance" placeholder="Allowance (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif >
                                                        <b class="tooltip tooltip-bottom-right">Elaun (RM)</b>
                                                    </label>
                                                </section>
                                      
                                        </div>
                                                 <div class="col-xs-6 col-12">
                                                          <section class="col col-12">
                                                    <label class="label"><b> Potongan Bulanan (RM) </b> </label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                        <input type="text" name="Deduction" id="Deduction" placeholder="Existing Total Deduction" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif >
                                                        <b class="tooltip tooltip-bottom-right"> Jumlah Potongan Bulanan Semasa (RM)  </b>
                                                    </label>
                                                </section>
                                                </div>
                                              
                                                
                                            </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                         <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-lg-10 col-md-10">
                                            <label class="label"> <b> Pakej</b><div class="visible-xs"><br></div> </label>
                                               <label class="select" style="width: 206px !important">
                                                    <select name="Package" id="Package" class="form-control">
                                                        
                                                           @foreach ($package as $package)
                            <option value="{{ $package->id }}">{{ $package->name }}</option>
                              @endforeach
                                                    </select> <i></i>
                                                </label>
                                           </section>
                                            </div>
                                        <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                            <label class="label"> <b> Jumlah Pembiayaan(RM) </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="LoanAmount" id="LoanAmount" placeholder="Loan Amount (RM)" onkeypress="return isNumberKey(event)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif>
                                                 
                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan (RM)</b>
                                            </label>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </section>
                                        </div>
                                        </div>


                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                     <b>    Kira Kelayakan  </b>
                                    </button>
                                    <div id="response"></div>
                                </footer>

                <div class="message">
                  <i class="fa fa-check"></i>
                  <p>
                    Thank you for your registration!
                  </p>
                </div>
              </form>
            
                                   
        </div>
      </div>

    </div></div>

            
        
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-black inline-block">
                        <span class="txt-color-black">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>
  

    
        

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

    $(document).ready(function() {
        
          $("#smart-form-register2").hide();
        $("#smart-form-register").validate({

          // Rules for form validation
          rules : {
              FullName: {
              required : true
            },
            ICNumber : {
              required : true
            },
            
            PhoneNumber: {
                required: true
            },
            Deduction: {
                required: true
            },
            
            Allowance: {
                required: true
            },
            Package: {
                required: true
            },
            Employment: {
                required: true
            },
            Employer: {
                required: true
            },
            
            
            BasicSalary: {
                required: true
                            
            },
            LoanAmount: {
                required: true
            }
          },

          // Messages for form validation
          messages : {

              FullName: {
              required : 'Please enter your full name'
            },
            
            ICNumber: {
                required: 'Please enter your ic number'
            },
            PhoneNumber: {
                required: 'Please enter your phone number'
            },
            Allowance: {
                required: 'Please enter  yor allowance'
            },
            Deduction: {
                required: 'Please enter your total deduction'
            },
            Package: {
                required: 'Please select package'
            },
            Employment: {
                required: 'Please select employement type'
            },

            Employer: {
                required: 'Please select employer'
            },


            BasicSalary: {
                required: 'Please enter your basic salary'
            },
            LoanAmount: {
                required: 'Please select your loan amount'
            }
          }
        });

      });
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                ICNumber : {
                    required : true,
                    minlength : 12,
                    maxlength : 13
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email2 : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>
<?php 
  //include required scripts
  include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    

<script type="text/javascript">

 @if (Session::has('employer')) 
 
   $("#majikan2").hide();
    $("#majikan").show();
  @else
   $("#majikan").hide();
   $("#majikan2").show();
  @endif

$( "#Employment" ).change(function() {
    var Employment = $('#Employment').val();
    if( Employment != '2') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
    }
    else {
         
        $("#majikan2").hide();
         $("#majikan").show();
            
          $("#majikan").simulate('click');
         


    }
  $.ajax({
                url: "<?php  print url('/'); ?>/employer/"+Employment,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });

                }
            });

   
});
</script>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("#Employment option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });



</script>

<script type="text/javascript">

function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}


         var Employment = getSelectedText('Employment');
         var Employer = getSelectedText('Employer');
          $("#Employment2").val(Employment);
           $("#Employer2").val(Employer); 

</script>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>