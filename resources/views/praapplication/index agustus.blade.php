<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "MBSB Personal Financing-i";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
    <div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
             @if (Session::has('message'))

                <script>
           
                       function pesan() {
                            bootbox.alert("<b>{{Session::get('message')}}</b>");
                        }
                        window.onload = pesan;

                        
                </script>
            @endif             
            @if (count($errors) > 0)
                <script>
   

                     function pesan() {
                          bootbox.alert("<b>@foreach ($errors->all() as $error) {{ $error }} <br> @endforeach</b>");
                      }
                      window.onload = pesan;

          
              </script>

          @endif
           
          
				<div class="row">
					<div class="col-xs-11 col-sm-12 col-md-7 col-lg-7">


        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">

<script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
                 ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
    
        
            //responsive code end
        });
    </script>
<script>
        jQuery(document).ready(function ($) {
            
            var jssor_2_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_2_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_2_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_2_slider = new $JssorSlider$("jssor_2", jssor_2_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_2_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_2_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
     ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
        });
    </script>
<script>
        jQuery(document).ready(function ($) {
            
            var jssor_3_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_3_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_3_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_3_slider = new $JssorSlider$("jssor_3", jssor_3_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_3_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_3_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
                 ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
    
        
            //responsive code end
        });
    </script>

    <style>
        
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('{{url('/')}}/img/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        /* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        */
        .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('{{url('/')}}/img/a22.png') center center no-repeat;
            overflow: hidden;
        }
        .jssora22l { background-position: -10px -31px; }
        .jssora22r { background-position: -70px -31px; }
        .jssora22l:hover { background-position: -130px -31px; }
        .jssora22r:hover { background-position: -190px -31px; }
        .jssora22l.jssora22ldn { background-position: -250px -31px; }
        .jssora22r.jssora22rdn { background-position: -310px -31px; }
    </style>


    <div class="well" id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('{{url('/')}}/img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 650px; height: 320px; overflow: hidden;">
          <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/teguh.png">
          <img data-u="image" src="{{ url('/') }}/img/teguh.png" />
        </a>
            </div>

            <div data-p="225.00" style="display: none;">
            <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/slidembsb.png">    
          <img data-u="image" src="{{ url('/') }}/img/slidembsb.png" />
        </a>
            </div>

            <div data-p="225.00" style="display: none;">
                 <a data-toggle="modal"  data-toggle="modal" data-target="#payment">
          <img data-u="image" src="{{ url('/') }}/img/payment_method.png" />
        </a>
            </div>
           
            <div data-p="225.00" style="display: none;">
                <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/blue.jpg">
          <img data-u="image" src="{{ url('/') }}/img/blue.jpg" />
        </a>
            </div>
   
            
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-05.jpg"> 
         <img data-u="image" src="{{ url('/') }}/img/WEB-05.jpg" />
        </a>
            </div>
              
            <div data-p="225.00" style="display: none;">
        <a class="fancybox-buttons" data-fancybox-group="button" href="{{ url('/') }}/img/WEB-07.jpg"> 
          <img data-u="image" src="{{ url('/') }}/img/WEB-07.jpg" />
        </a>
            </div>        
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:1px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>

    <h1><font color='#005aac'>Personal Financing-i</font></h1>
<p align='justify'>Learn about our Islamic personal financing solutions through our host of attractive packages that best suit your needs.</p>

<ul>
  <li>Competitive profit rates as low as ECOF-i -0.20% p.a.*</li>
  <li>No guarantor required.</li>
  <li>No hidden charges.</li>
  <li>Quick approval</li>
  <li>Open to public servant and private sector employees.</li>
  <li>Monthly instalment via Biro Angkasa (Biro) or Accountant General (AG) or Employer Salary Deduction or Over-the-counter</li>
</ul>
<font size='2'>
<p><i>* Terms and conditions apply.<br>
ECOF-i = Islamic Effective Cost of Fund</i></p></font>

    <br>
                
            </div>
                        
                        
          </div>

          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
            <div class="well no-padding">

               {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
                <header>
                  <p class="txt-color-white"><b>  Mohon Sekarang  </b> </p>
                </header>

                <fieldset>
                                     <div class="row">
                                     <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                          <label class="label"> <b> Nama Penuh </b></label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="FullName" name="FullName" placeholder="Full Name"    @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                                            </label>
                                        </section>
                                        </div>
                                        <div class="col-xs-6 col-12">
                                             <section class="col col-12">
                                            <label class="label"> <b> No Kad Pengenalan </b></label>
                                            <label class="input @if (Session::has('icnumber_error')) state-error  @endif">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">No Kad Pengenalan</b>
                                            </label>
                                        </section>

                                        </div>
                                    </div>
                                        <div class="row">
                                         <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                            <label class="label"> <b> Nombor Telefon Bimbit</b> </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-mobile-phone"></i>
                                                <input type="text" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number"  minlength="7" maxlength="12"  onkeypress="return isNumberKey(event)"  @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>
                                                <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>
                                            </label>
                                        </section>
                                        </div>
                                
                                    
                                          <div class="col-xs-6 col-12">
                                       <section class="col col-12">
                                            <label class="label"> <b> Jenis Pekerjaan </b></label>
                                                <label class="select">
                                                    <select name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                                
                                                       
                                                         @foreach ($employment as $employment)
                            <option value="{{ $employment->id }}">{{ $employment->name }}</option>
                              @endforeach
                                                    </select> <i></i>

                                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                                </label>
                                            </section>
                                        </div>
                                      
                                        </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                         <div class="col-xs-6 col-12">
                                             <section class="col col-12">
                                            <label class="label"> <b> Majikan</b></label>
                                                <label class="select" id="majikan">
                                                    <select name="Employer" id="Employer" class="form-control" onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employer')) 
                                                    <option  value="{{ Session::get('employer') }}">{{ Session::get('employer2') }}</option>
                                                     
                                                     
                                                    @endif

                                                   
                                                       
                                                    </select> <i></i>
                                                    <input type="hidden" name="Employer2" id="Employer2" value="" />
                                                </label>

                                                 <label id="majikan2" class="input">
                                                <i class="icon-append fa fa-briefcase"></i>
                                                <input type="text" id="majikantext" name="majikan"  placeholder="Majikan" maxlength="70"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right"> Majikan</b>
                                            </label>

                                            </section>
                                            </div>
                                          <div class="col-xs-6 col-12">
                                                      <section class="col col-12">
                                            <label class="label"> <b> Gaji Asas (RM) </b>  </label>
                                            <label class="input @if (Session::has('hadpotongan')) state-error  @endif ">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="BasicSalary" id="BasicSalary" placeholder="Basic Salary (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Gaji Asas (RM) </b>
                                            </label>
                                        </section>
                                            </div>
                                        </div>
                                            <div class="row">
                                                    <div class="col-xs-6 col-12">
                                                       <section class="col col-12">
                                                    <label class="label"> <b> Elaun (RM) </b></label>
                                                    <label class="input @if (Session::has('hadpotongan')) state-error  @endif">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                        <input type="text" name="Allowance" id="Allowance" placeholder="Allowance (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif >
                                                        <b class="tooltip tooltip-bottom-right">Elaun (RM)</b>
                                                    </label>
                                                </section>
                                      
                                        </div>
                                                 <div class="col-xs-6 col-12">
                                                          <section class="col col-12">
                                                    <label class="label"><b> Potongan Bulanan (RM) </b> </label>
                                                    <label class="input @if (Session::has('hadpotongan')) state-error  @endif">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                        <input type="text" name="Deduction" id="Deduction" placeholder="Existing Total Deduction (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif >
                                                        <b class="tooltip tooltip-bottom-right"> Jumlah Potongan Bulanan Semasa (RM)  </b>
                                                    </label>
                                                </section>
                                                </div>
                                              
                                                
                                            </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                         <div class="col-xs-6 col-12">
                                        <section class="col col-12 col-lg-12 col-md-12">
                                            <label class="label"> <b> Pakej</b><div class="visible-xs"><br></div> </label>
                                                <label class="input">
                         
                                                    <input type="text" name="Package" id="Package" @if (Session::has('package_name'))  value="{{ Session::get('package_name') }}" @else value="Mumtaz-i" @endif class="form-control" disabled>

                                                </label>
                                           </section>
                                            </div>
                                        <div class="col-xs-6 col-12">
                                        <section class="col col-12">
                                            <label class="label"> <b> Jumlah Pembiayaan(RM) </b></label>
                                            <label class="input @if (Session::has('hadpotongan')) state-error  @endif"">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="LoanAmount" id="LoanAmount" placeholder="Loan Amount (RM)" onkeypress="return isNumberKey(event)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif>
                                                 
                                                <b class="tooltip tooltip-bottom-right">Jumlah Pembiayaan (RM)</b>
                                            </label>
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        </section>
                                        </div>
                                        </div>

                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                     <b>    Kira Kelayakan  </b>
                                    </button>
                                    <div id="response"></div>
                                </footer>

                <div class="message">
                  <i class="fa fa-check"></i>
                  <p>
                    Thank you for your registration!
                  </p>
                </div>
              </form>
            
                                   
        </div>
      </div>

    </div>
    </div>



    <!-- Modal -->
    <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel"><b>Payment Methods</b></h4>
                    </div>
                    <div class="modal-body">
   
                 You can make payment through the channels stated below:
                 <ol type='1'>
                   <li>MBSB nationwide branches</li>
                   <li>Maybank2u.com (Online bill payment services)</li>
                   <li>Cimbclicks.com (Online bill payment services)</li>
                   <li>RHB.com.my (Online bill payment services)</li>
                   <li>Bank Simpanan Nasional (under Giro Services)</li>
                   <li>Standing Instruction from respective banks (where applicable)</li>
                  
                </ol>
                <p><i>*Please state your name and account number on all payments made to MBSB.</i></p>
                  <p>
                  Disclaimer: Payment via Online bill payment shall be credited into your account in the following business day subject to successful transaction.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                          Close
                        </button> 
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        
       <div class="page-footer">
            <div class="row">
             

                <div class="col-xs-12 col-sm-12 text-left ">
                    <div class="txt-color-black inline-block">
                        <span class="txt-color-black">NetXpert Sdn Bhd  © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>
  

    
        

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

    $(document).ready(function() {
        
          $("#smart-form-register2").hide();
        $("#smart-form-register").validate({

          // Rules for form validation
          rules : {
              FullName: {
              required : true
            },
            ICNumber : {
              required : true
            },
            
            PhoneNumber: {
                required: true
            },
            Deduction: {
                required: true
            },
            
            Allowance: {
                required: true
            },
            Package: {
                required: true
            },
            Employment: {
                required: true
            },
            Employer: {
                required: true
            },
            
            
            BasicSalary: {
                required: true
                            
            },
            LoanAmount: {
                required: true
            }
          },

          // Messages for form validation
          messages : {

              FullName: {
              required : 'Please enter your full name'
            },
            
            ICNumber: {
                required: 'Please enter your ic number'
            },
            PhoneNumber: {
                required: 'Please enter your phone number'
            },
            Allowance: {
                required: 'Please enter  yor allowance'
            },
            Deduction: {
                required: 'Please enter your total deduction'
            },
            Package: {
                required: 'Please select package'
            },
            Employment: {
                required: 'Please select employement type'
            },

            Employer: {
                required: 'Please select employer'
            },


            BasicSalary: {
                required: 'Please enter your basic salary'
            },
            LoanAmount: {
                required: 'Please select your loan amount'
            }
          }
        });

      });
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                ICNumber : {
                    required : true,
                    minlength : 12,
                    maxlength : 13
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email2 : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>
<?php 
  //include required scripts
  include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
    

<script type="text/javascript">

 @if (Session::has('employer')) 
 
   $("#majikan2").hide();
    $("#majikan").show();
  @else
   $("#majikan").hide();
   $("#majikan2").show();
  @endif

$( "#Employment" ).change(function() {
    var Employment = $('#Employment').val();

    if( Employment == '1') {

      $("#Employer").html(" ");
      $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
       $("#Package").val("Mumtaz-i");
    }
      else if( Employment == '2') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Afdhal-i");
    }

    else if( Employment == '3') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Afdhal-i");
    }
      else if( Employment == '4') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();
         $("#Package").val("Private Sector PF-i");
    }
    else if( Employment == '5') {
         
         $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
        $("#majikantext").focus();
         $("#majikan").hide();

          $("#Package").val("Afdhal-i");
            
        //  $("#majikan").simulate('click');
         


    }
  $.ajax({
                url: "<?php  print url('/'); ?>/employer/"+Employment,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });

                }
            });

   
});
</script>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("#Employment option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });



</script>

<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("select option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });

</script>

<script type="text/javascript">

function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}


         var Employment = getSelectedText('Employment');
         var Employer = getSelectedText('Employer');
          $("#Employment2").val(Employment);
           $("#Employer2").val(Employer); 

</script>
<!--Add the following script at the bottom of the web page (before </body></html>)-->
<script type="text/javascript" async="async" defer="defer" data-cfasync="false" src="https://mylivechat.com/chatinline.aspx?hccid=21343764"></script>