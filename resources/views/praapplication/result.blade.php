<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "MBSB Personal Financing-i";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
    <!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
<?php
include ("asset/inc/header-home.php");
?>
    <div id="main" role="main">
<br><br><br><br>
      <!-- MAIN CONTENT -->
      <div id="content" class="container">
           @if (Session::has('message'))
    
                    <script>
                        function pesan() {
                            bootbox.alert("<b>{{Session::get('message')}}</b>");
                        }

                       
                           window.onload = pesan;
                     


                       

                    </script>
            @endif             
            @if (count($errors) > 0)
                <script>
                     function pesan() {
                          bootbox.alert("<b>@foreach ($errors->all() as $error) {{ $error }} <br> @endforeach</b>");
                      }
                      window.onload = pesan;

                      
              </script>

          @endif
           
          
        <div class="row">
          <div class="col-xs-11 col-sm-12 col-md-6 col-lg-6">


        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">


    <br>
                
            </div>
                        
                        
          </div>

          </div>
          
          <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" >
            <div class="well no-padding">

							  @foreach($pra as $pra)

        <?php 
        $icnumber = $pra->icnumber;
        $tanggal = substr($icnumber,4, 2);
        $bulan =  substr($icnumber,2, 2);
        $tahun = substr($icnumber,0, 2); 

        if($tahun > 30) {

            $tahun2 = "19".$tahun;
        }
        else {
             $tahun2 = "20".$tahun;

        }
       
         $lahir = $tahun2.'-'.$bulan.'-'.$tanggal;
        $lahir =  date('Y-m-d', strtotime($lahir. ' - 16 month'));
        $oDateNow = new DateTime();
        $oDateBirth = new DateTime($lahir);
        $oDateIntervall = $oDateNow->diff($oDateBirth);

        $umur =  $oDateIntervall->y;
        $durasix = 60 - $oDateIntervall->y;
        if( $durasix  > 10){ $durasi = 10 ;} 
        else { $durasi = $durasix ;}
 
        ?>
						
    <div class='smart-form client-form'id='smart-form-register2'>
        <header> <p class="txt-color-white"><b>   Loan Eligibility </b> </p> </header>
        <form action='{{url('/')}}/praapplication/{{$id}}' method='post' enctype="multipart/form-data">
            <fieldset>
                <div class="row">
                    <section class="col col-6">
                        <label class="label"> <b>  Package </b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>
                            <b><input type="text" id="Package2" value="{{$pra->package->name}}"  name="Package2" placeholder="Package" disabled="disabled"></b>
                            <input name="id_praapplication" id="id_praapplication" type="hidden"  value="{{$pra->id}}" >
                             <input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
                            <b class="tooltip tooltip-bottom-right">Package</b>
                        </label>
                    </section>
                    <section class="col col-6">
                        <label class="label"> <b> <font color="red" size="2.5" > Financing amount up to  </font>  </b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>

                            @foreach($loan as $loan)
                            <?php
                                $salary_dsr = ($zbasicsalary * ($loan->dsr / 100)) - $zdeduction;
                                $ndi        = ($zbasicsalary - $zdeduction) -  1300;
                                 $max       = $salary_dsr * 12 * 10 ;

                                 function pembulatan($uang) {
                                    $puluhan = substr($uang, -3);
                                    
                                    if($puluhan<500) {
                                        $akhir = $uang - $puluhan; 
                                    } 
                                    else {
                                        $akhir = $uang - $puluhan;
                                    }
                                    return $akhir;
                                }

                                if(!empty($loan->max_byammount))  {
                                    $ansuran = intval($salary_dsr)-1;
                                    if($pra->package->id=="1") {
                                        //$a= $pra->package->effective_rate;
                                        //$bunga = ((($a/12)+0.025) *10)/100;
                                        $bunga = 3.7/100;
                                        //$bunga = $pra->package->effective_rate / 100;
                                    }
                                    elseif($pra->package->id=="2") {
                                        //$a= $pra->package->effective_rate;
                                        //$bungas = ((($a/12)+0.025) *10)/100;
                                        $bunga = 4.45/100;
                                         //$bunga = $pra->package->effective_rate / 100;
                                    }
                                    else {
                                        //$a= $pra->package->effective_rate;
                                        //$bungas = ((($a/12)+0.025) *10)/100;
                                        $bunga = 7.45/100;
                                        //$bunga = $pra->package->effective_rate / 100;
                                    }
                                      
                                    $pinjaman = 0;
                                    for ($i = 0; $i <= $loan->max_byammount; $i++) {
                                        $bungapinjaman  = $i  * $bunga * $durasi ;
                                        $totalpinjaman  = $i + $bungapinjaman ;
                                        $durasitahun  = $durasi * 12;
                                        $ansuran2    = intval($totalpinjaman / ($durasi * 12));
                                        
                                        //echo $ansuran2."<br>";
                                        if ($ansuran2 < $ndi)
                                            {
                                                $pinjaman = $i;
                                            }
                                    }   

                                    if($pinjaman > 1) {
                                        $bulat = pembulatan($pinjaman);
                                        $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                        $loanz = $bulat;
                                    }
                                    else {
                                        $loanx =  number_format($loan->max_byammount, 0 , ',' , ',' ) ; 
                                        $loanz = $loan->max_byammount;
                                    }
                                }
                                else { 
                                    $bulat = pembulatan($loan->max_bysalary * $total_salary);
                                    $loanx =  number_format($bulat, 0 , ',' , ',' ) ; 
                                    $loanz = $bulat;
                                    if ($loanz > 199000) {
                                        $loanz  = 250000;
                                        $loanx =  number_format($loanz, 0 , ',' , ',' ) ; 
                                    }
                                }
                            ?>
                                    <!--{{$pra->package->effective_rate}}-->
                            @endforeach
                            <b><input readonly type="text" id="MaxLoan"  
                                value=" RM {{$loanx}}" name="MaxLoan" placeholder="Max Loan Eligibility (RM)"
                                 class="merah" requierd> </b>
                                <input readonly type="hidden" id="maxloanz"  
                                value="{{$loanz}}" name="maxloanz" placeholder="Max Loan Eligibility (RM)" requierd> 
                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                        </label>
                    </section>
                </div>

                <div class="row">
                    <section class="col col-6">
                        <label class="label"><b>  Financing Amount (RM) </b> </label>
                        <label class="input state-<?php if( $pra->loanamount <= $loanz ) { print "success"; } else { print "error"; }?>">
                            <i class="icon-append fa fa-credit-card"></i>
                            <input type="text" name="LoanAmount2"  id="LoanAmount2" onkeypress="return isNumberKey(event)" value="{{ $pra->loanamount }}"  placeholder="RM " 
                            onkeyup="this.value = minmax(this.value, 0, {{$loanz}})">
                            <b class="tooltip tooltip-bottom-right">Financing Amount </b>
                        </label>
                    </section>
                                        
                    <section class="col col-6">
                        <label class="label"> <b>Total income</b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>
                            <input type="text" id="pendapatan" value="RM {{ number_format($total_salary, 0 , ',' , ',' )}}" name="pendapatan" placeholder="Loan Amount" disabled="disabled">
                            <b class="tooltip tooltip-bottom-right">Total income</b>
                             <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        </label>
                    </section>
                    <section class="col col-6">
                        <label class="label"> <b>Maximum Installment </b> </label>
                        <label class="input">
                            <i class="icon-append fa fa-credit-card"></i>
                            <input type="text" id="ansuran_maksima" value="RM {{ number_format($ndi, 0 , ',' , ',' )  }}   / month" name="ansuran_maksima" placeholder="Maximum Installment" readonly>
                            <b class="tooltip tooltip-bottom-right">Maximum Installment</b>
                        </label>
                    </section>
                    <section class="col col-6">
                        <label class="label"> &nbsp; </label>
                            <footer>
                                <button class="btn btn-primary " type="submit">
                                    <b> Recalculate </b>
                                </button>
                            </footer>
                    </section>
                </div>
            </fieldset>
        </form> 

        <fieldset>
            <table class="table table-bordered table-hover" border='1'>
                <thead>
                    <tr>
                        <th valign="middle"><b>Tenures</b></th>
                        <th class="hidden-xs"><b>Financing Amount</b></th>
                        <th><b>Monthly installment</b></th>
                        <th><b>Choose</b></th>
                     </tr>
                </thead>
                <tbody>
                    <?php
                        $total_loan = $pra->loanamount;
                        $loans = $total_loan;
                        $interest =$pra->package->effective_rate;
                        $interest1 =$pra->package->effective_rate/12*10;
                        $interest11 =  number_format($interest1, 2 ) ;
                        $interest3 =(($pra->package->effective_rate/12)+0.025)*10;
                        $interest31 =  number_format($interest3, 2 ) ; 
                        $year2 = 2;
                        $year3 = 3;
                        $year4 = 4;
                        $year5 = 5;
                        $year6 = 6;
                        $year7 = 7;
                        $year8 = 8;
                        $year9 = 9;
                        $year10 =10;

                       function pmt2($interest, $year2, $loans) {
                           $months = 12*$year2;
                           $interests = $interest / 1200;
                           $amount2 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount2,0,",","");
                        }

                        function pmt3($interest, $year3, $loans) {
                           $months = 12*$year3;
                           $interests = $interest / 1200;
                           $amount3 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount3,0,",","");
                        }
                        function pmt4($interest3, $year4, $loans) {
                           $months = 12*$year4;
                           $interests = $interest3 / 1000;
                           $amount4 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount4,0,",","");
                        }
                        function pmt5($interest3, $year5, $loans) {
                           $months = 12*$year5;
                           $interests = $interest3 / 1000;
                           $amount5 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount5,0,",","");
                        }
                         function pmt6($interest3, $year6, $loans) {
                           $months = 12*$year6;
                           $interests = $interest3 / 1000;
                           $amount6 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount6,0,",","");
                        }
                         function pmt7($interest3, $year7, $loans) {
                           $months = 12*$year7;
                           $interests = $interest3 / 1000;
                           $amount7 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount7,0,",","");
                        }
                         function pmt8($interest3, $year8, $loans) {
                           $months = 12*$year8;
                           $interests = $interest3 / 1000;
                           $amount8 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount8,0,",","");
                        }
                         function pmt9($interest3, $year9, $loans) {
                           $months = 12*$year9;
                           $interests = $interest3 / 1000;
                           $amount9 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));
                           return number_format($amount9,0,",","");
                        }
                         function pmt10($interest3, $year10, $loans) {
                           $months = 12*$year10;
                           $interests = $interest3 / 1000;
                           $amount10 = $interests * -$loans * pow((1 + $interests), $months) / (1 - pow((1 + $interests), $months));

                           return number_format($amount10,0,",","");
                        }

                        $amount2 = pmt2($interest, $year2, $loans);
                        $amount3 = pmt3($interest, $year3, $loans);
                        $amount4 = pmt4($interest3, $year4, $loans);
                        $amount5 = pmt5($interest3, $year5, $loans);
                        $amount6 = pmt6($interest3, $year6, $loans);
                        $amount7 = pmt7($interest3, $year7, $loans);
                        $amount8 = pmt8($interest3, $year8, $loans);
                        $amount9 = pmt9($interest3, $year9, $loans);
                        $amount10 = pmt10($interest3, $year10, $loans);

                        //echo "Your payment will be &pound;" . round($amount2,0) . " a month, for " . $months . " months";
                    ?>
                    <?php
                        if(($amount2 <= $ndi)){
                            $y = '2';
                        }
                        elseif(($amount3 <= $ndi)){
                             $y = '3';
                        }
                        elseif(($amount4 <= $ndi)){
                             $y = '4';
                         }
                        elseif(($amount5 <= $ndi)){
                             $y = '5';
                         }
                        elseif(($amount6 <= $ndi)){
                             $y = '6';
                         }
                        elseif(($amount7 <= $ndi)){
                             $y = '7';
                         }
                        elseif(($amount8 <= $ndi)){
                             $y = '8';
                         }
                        elseif(($amount9 <= $ndi)){
                             $y = '9';
                         }
                        elseif(($amount10 <= $ndi)){
                             $y = '10';
                        }
                    ?>
                    <?php for ($x = $y; $x <= 10; $x++) {  ?>
                    <tr>
                        <td>@if(($x==2) AND ($amount2 <= $ndi))
                                2 Tahun
                            @elseif(($x==3) AND ($amount3 <= $ndi))
                                3 Tahun
                            @elseif(($x==4) AND ($amount4 <= $ndi))
                                4 Tahun
                            @elseif(($x==5) AND ($amount5 <= $ndi))
                                5 Tahun
                            @elseif(($x==6) AND ($amount6 <= $ndi))
                                6 Tahun
                            @elseif(($x==7) AND ($amount7 <= $ndi))
                                7 Tahun
                            @elseif(($x==8) AND ($amount8 <= $ndi))
                                8 Tahun
                            @elseif(($x==9) AND ($amount9 <= $ndi))
                                9 Tahun
                            @elseif(($x==10) AND ($amount10 <= $ndi))
                                10 Tahun
                            @endif</td>
                        <td class="hidden-xs"> RM {{ number_format( $pra->loanamount, 0 , ',' , ',' )  }}  </td>
                        <td>
                            @if($x==2)
                                RM {{$amount2}}
                            @elseif($x==3)
                                RM {{$amount3}}
                            @elseif($x==4)
                                RM {{$amount4}}
                            @elseif($x==5)
                                RM {{$amount5}}
                            @elseif($x==6)
                                RM {{$amount6}}
                            @elseif($x==7)
                                RM {{$amount7}}
                            @elseif($x==8)
                                RM {{$amount8}}
                            @elseif($x==9)
                                RM {{$amount9}}
                            @elseif($x==10)
                                RM {{$amount10}}
                            @endif
                        </td>
                        <td align="center">  <input type="radio" name="tenure" id="tenure" class="tenure" value="{{$x}}" required>
                        <input type="hidden" name="rates" id="rates" class="rates" value="{{$interest31}}">
                        <input type="hidden" name="rate" id="rate" class="rate" value="{{$interest11}}">
                        <input type="hidden" name="amount2" id="amount2" class="amount2" value="{{$amount2}}">
                        <input type="hidden" name="amount3" id="amount3" class="amount3" value="{{$amount3}}">
                        <input type="hidden" name="amount4" id="amount4" class="amount4" value="{{$amount4}}">
                        <input type="hidden" name="amount5" id="amount5" class="amount5" value="{{$amount5}}">
                        <input type="hidden" name="amount6" id="amount6" class="amount6" value="{{$amount6}}">
                        <input type="hidden" name="amount7" id="amount7" class="amount7" value="{{$amount7}}">
                        <input type="hidden" name="amount8" id="amount8" class="amount8" value="{{$amount8}}">
                        <input type="hidden" name="amount9" id="amount9" class="amount9" value="{{$amount9}}">
                        <input type="hidden" name="amount10" id="amount10" class="amount10" value="{{$amount10}}">
                         <input type="hidden" name="interests" id="interests" class="interests" value="{{$pra->package->effective_rate}}">
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </fieldset>
        <i style="font-size: 12px; margin-left: 12px">  <sup>*</sup>{{$pra->package->effective_rate}} % p.a (Effective Rate) /  {{$pra->package->flat_rate}} % p.a (Flat Rate equivalent for 3 years)</i><br>
        <i style="font-size: 12px; margin-left: 12px">  <sup>*</sup>MBSB Bank’s current Base Rate (BR) is 3.90% p.a. and ceiling profit rate is 15%</i>
        
        
        <fieldset>
            <footer>
                <button type="submit" class="btn btn-primary btn-lg" id="submit_tenure">
                    <b> Submit</b>
                </button>
            </footer>
        <div>
    </form>
</div>
						
					
					</div>
					
					
				</div>
								
				
			</div>
		

		</div>
		

        </div>
    </div>
           


		<!-- Modal -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        &times;
                    </button>
                    <h4 class="modal-title" id="myModalLabel">Register</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => 'user','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                        <fieldset>
                            <section >
                                <label class="label"><br>E-mail</label>
                                <label class="input">
                                    <i class="icon-append fa fa-envelope"></i>
                                    <input type="hidden" name="idpra" value="{{$pra->id}}">
                                    <input type="Email" id="Email" name="Email" required placeholder="Email" >
                                    <b class="tooltip tooltip-bottom-right">E-mail</b>
                                </label>
                            </section>
                            <input type="hidden" id="FullName2" name="FullName2"  value="{{$pra->fullname}}" placeholder="Full Name" readonly >
                            @endforeach     
                            <section>
                                <label class="label">Password</label>
                                <label class="input">
                                    <i class="icon-append fa fa-key "></i>
                                    <input type="Password" required id="password" name="password" placeholder="Password" >
                                    <b class="tooltip tooltip-bottom-right">
                                    Must be between 8 – 36 characters long - combination of numbers (0 to 9) and letters  (a to z) with at least one capital letter (A to Z)</b>
                                </label>
                            </section>
                            <section>
                                <label class="label">Password Confirmation</label>
                                <label class="input">
                                    <i class="icon-append fa fa-key "></i>
                                    <input type="Password" id="password_confirmation" name="password_confirmation" required placeholder="Password Confirmation" >
                                    <b class="tooltip tooltip-bottom-right">Password Confirmation</b>
                                </label>
                            </section>
                            <input type="hidden" name="_token" value="{{ csrf_token()}}">
                        </fieldset>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">
                            Register
                        </button>
                    {!! Form::close() !!}   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Modal -->
        <div class="modal fade" id="payment" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel"><b>Payment Methods</b></h4>
                    </div>
                    <div class="modal-body">
   
                 You can make payment through the channels stated below:
                 <ol type='1'>
                   <li>MBSB nationwide branches</li>
                   <li>Maybank2u.com (Online bill payment services)</li>
                   <li>Cimbclicks.com (Online bill payment services)</li>
                   <li>RHB.com.my (Online bill payment services)</li>
                   <li>Bank Simpanan Nasional (under Giro Services)</li>
                   <li>Standing Instruction from respective banks (where applicable)</li>
                  
                </ol>
                <p><i>*Please state your name and account number on all payments made to MBSB.</i></p>
                  <p>
                  Disclaimer: Payment via Online bill payment shall be credited into your account in the following business day subject to successful transaction.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                          Close
                        </button> 
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<!-- ==========================CONTENT ENDS HERE ========================== -->

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                password : {
                    required : true,
                    minlength : 8,
                    maxlength : 36
                },
                password_confirmation : {
                    required : true,
                   
                    maxlength : 36,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
             messages : {

                    email : {
                    required : 'Please enter your email address',
                    Email2 : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                password : {
                    required : 'Please enter your password'
                },
                password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>

<script type="text/javascript">

$( ".tenure" ).change(function() {
    var tenure = $('input[name=tenure]:checked').val();
    var id_praapplication = $('#id_praapplication').val();
    var _token = $('#token').val();
    var maxloanz = $('#maxloanz').val();
    var package = $('#Package2').val();
    var loanamount = $('#LoanAmount2').val();
     var interests = $('#interests').val();
    if(tenure==2){
         var installment = $('#amount2').val();
        var rate = $('#rate').val();
    } 
    else if(tenure==3){
        var installment = $('#amount3').val();
        var rate = $('#rate').val();
    }
    else if(tenure==4){
        var installment = $('#amount4').val();
        var rate = $('#rates').val();
    }
    else if(tenure==5){
        var installment = $('#amount5').val();
        var rate = $('#rates').val();
    }
    else if(tenure==6){
        var installment = $('#amount6').val();
        var rate = $('#rates').val();
    }
    else if(tenure==7){
        var installment = $('#amount7').val();
        var rate = $('#rates').val();
    }
    else if(tenure==8){
        var installment = $('#amount8').val();
        var rate = $('#rates').val();
    }
    else if(tenure==9){
        var installment = $('#amount9').val();
        var rate = $('#rates').val();
    }
    else if(tenure==10){
        var installment = $('#amount10').val();
        var rate = $('#rates').val();
    }

  $.ajax({
        type: "PUT",
        url: '{{ url('/form/') }}'+'/99',
        data: { id_praapplication: id_praapplication, tenure: tenure, _token : _token, maxloan : maxloanz, package : package, rate : rate, installment : installment,loanamount:loanamount,interests:interests
        },
        success: function (data, status) {
        }
    });
});
</script>

<script type="text/javascript">

    $(document).ready(function() {

    var id_praapplication = $('#id_praapplication').val();
    var _token = $('#token').val();
    var LoanAmount = $('#LoanAmount2').val();
    var maxloanz = $('#maxloanz').val();
      
    $.ajax({
        type: "PUT",
        url: '{{ url('/form/') }}'+'/9',
        data: { id_praapplication: id_praapplication,  _token : _token, loanammount : LoanAmount, maxloan : maxloanz 
        },
        success: function (data, status) {
        }
        });
    });
</script>

<?php 
	//include required scripts
	include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
	
<script>
$(document).ready(function(){
    $("#submit_tenure").click(function(){
  var tenure = $('input[name=tenure]:checked').val();
     
        if (tenure>0) { 
      $('#myModal').modal('show');
    }
    else{
      bootbox.alert('Sila Pilih Tempoh Pembiayaan');
      
    }
    });
});
</script>


<script type="text/javascript">
function minmax(value, min, max) 
{
    if(parseInt(value) < min || isNaN(value)) 
        return 0; 
    else if(parseInt(value) > max) 
        return max; 
    else return value;
}
</script>


<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>


