<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");

/*---------------- PHP Custom Scripts ---------

YOU CAN SET CONFIGURATION VARIABLES HERE BEFORE IT GOES TO NAV, RIBBON, ETC.
E.G. $page_title = "Custom Title" */

$page_title = "Get Egibity Result";

/* ---------------- END PHP Custom Scripts ------------- */

//include header
//you can add your custom css in $page_css array.
//Note: all css files are inside css/ folder
$page_css[] = "your_style.css";
$no_main_header = true;
$page_html_prop = array("id"=>"extr-page");
include("asset/inc/header.php");

?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
		<!-- possible classes: minified, no-right-panel, fixed-ribbon, fixed-header, fixed-width-->
		<header id="header">
			<!--<span id="logo"></span>-->

			<div id="logo-group">

				<span id="logo"> <img src="<?php echo ASSETS_URL; ?>/img/logo.png" alt="SmartAdmin" width="300"> </span>

				<!-- END AJAX-DROPDOWN -->
			</div>

<?php if(empty($user->name)) { ?> 
			<span id="extr-page-header-space"> <span class="hidden-mobile hiddex-xs">Already registered?</span> 
            <a href="login" class="btn btn-danger">Sign In</a> </span>

            <?php } else {?>
                <span id="extr-page-header-space"> 

            <a href="logout" class="btn btn-danger">Log Out</a> </span>

            <?php } ?>

		</header>

		<div id="main" role="main">

			<!-- MAIN CONTENT -->
			<div id="content" class="container">
                     @if (Session::has('message'))
    

    <div class="alert adjusted alert-info fade in">
    <button class="close" data-dismiss="alert">
         ×
    </button>
     <i class="fa-fw fa-lg fa fa-exclamation"></i>
      <strong>{{ Session::get('message') }}</strong> 
    </div>
            
            @endif             
            @if (count($errors) > 0)
    <div class="alert alert-danger">
          <button class="close" data-dismiss="alert">
         ×
    </button>  
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
           
          
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-7 col-lg-7 hidden-xs hidden-sm">

						<h1 class="txt-color-red login-header-big"> Pembiayaan Persendirian-i Lestari  </h1>
						<div class="hero">
<script>
        jQuery(document).ready(function ($) {
            
            var jssor_1_SlideoTransitions = [
              [{b:5500,d:3000,o:-1,r:240,e:{r:2}}],
              [{b:-1,d:1,o:-1,c:{x:51.0,t:-51.0}},{b:0,d:1000,o:1,c:{x:-51.0,t:51.0},e:{o:7,c:{x:7,t:7}}}],
              [{b:-1,d:1,o:-1,sX:9,sY:9},{b:1000,d:1000,o:1,sX:-9,sY:-9,e:{sX:2,sY:2}}],
              [{b:-1,d:1,o:-1,r:-180,sX:9,sY:9},{b:2000,d:1000,o:1,r:180,sX:-9,sY:-9,e:{r:2,sX:2,sY:2}}],
              [{b:-1,d:1,o:-1},{b:3000,d:2000,y:180,o:1,e:{y:16}}],
              [{b:-1,d:1,o:-1,r:-150},{b:7500,d:1600,o:1,r:150,e:{r:3}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:10000,d:2000,x:-379,e:{x:7}}],
              [{b:-1,d:1,o:-1,r:288,sX:9,sY:9},{b:9100,d:900,x:-1400,y:-660,o:1,r:-288,sX:-9,sY:-9,e:{r:6}},{b:10000,d:1600,x:-200,o:-1,e:{x:16}}]
            ];
            
            var jssor_1_options = {
              $AutoPlay: true,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $CaptionSliderOptions: {
                $Class: $JssorCaptionSlideo$,
                $Transitions: jssor_1_SlideoTransitions
              },
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };
            
            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
            
            //responsive code begin
            //you can remove responsive code if you don't want the slider scales while window resizing
            function ScaleSlider() {
                var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
                if (refSize) {
                    refSize = Math.min(refSize, 1920);
                    jssor_1_slider.$ScaleWidth(refSize);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }
            ScaleSlider();
            $(window).bind("load", ScaleSlider);
            $(window).bind("resize", ScaleSlider);
            $(window).bind("orientationchange", ScaleSlider);
            //responsive code end
        });
    </script>

    <style>
        
        /* jssor slider bullet navigator skin 05 css */
        /*
        .jssorb05 div           (normal)
        .jssorb05 div:hover     (normal mouseover)
        .jssorb05 .av           (active)
        .jssorb05 .av:hover     (active mouseover)
        .jssorb05 .dn           (mousedown)
        */
        .jssorb05 {
            position: absolute;
        }
        .jssorb05 div, .jssorb05 div:hover, .jssorb05 .av {
            position: absolute;
            /* size of bullet elment */
            width: 16px;
            height: 16px;
            background: url('img/b05.png') no-repeat;
            overflow: hidden;
            cursor: pointer;
        }
        .jssorb05 div { background-position: -7px -7px; }
        .jssorb05 div:hover, .jssorb05 .av:hover { background-position: -37px -7px; }
        .jssorb05 .av { background-position: -67px -7px; }
        .jssorb05 .dn, .jssorb05 .dn:hover { background-position: -97px -7px; }

        /* jssor slider arrow navigator skin 22 css */
        /*
        .jssora22l                  (normal)
        .jssora22r                  (normal)
        .jssora22l:hover            (normal mouseover)
        .jssora22r:hover            (normal mouseover)
        .jssora22l.jssora22ldn      (mousedown)
        .jssora22r.jssora22rdn      (mousedown)
        */
        .jssora22l, .jssora22r {
            display: block;
            position: absolute;
            /* size of arrow element */
            width: 40px;
            height: 58px;
            cursor: pointer;
            background: url('img/a22.png') center center no-repeat;
            overflow: hidden;
        }
        .jssora22l { background-position: -10px -31px; }
        .jssora22r { background-position: -70px -31px; }
        .jssora22l:hover { background-position: -130px -31px; }
        .jssora22r:hover { background-position: -190px -31px; }
        .jssora22l.jssora22ldn { background-position: -250px -31px; }
        .jssora22r.jssora22rdn { background-position: -310px -31px; }
    </style>


    <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden; visibility: hidden;">
        <!-- Loading Screen -->
        <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
            <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
        </div>
        <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 1300px; height: 500px; overflow: hidden;">
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="img/red.jpg" />
                <div style="position: absolute; top: 30px; left: 30px; width: 480px; height: 120px; font-size: 50px; color: #ffffff; line-height: 60px;">TOUCH SWIPE SLIDER</div>
                <div style="position: absolute; top: 300px; left: 30px; width: 480px; height: 120px; font-size: 30px; color: #ffffff; line-height: 38px;">Build your slider with anything, includes image, content, text, html, photo, picture</div>
                <div data-u="caption" data-t="0" style="position: absolute; top: 100px; left: 600px; width: 445px; height: 300px;">
                    <img src="img/c-phone.png" style="position: absolute; top: 0px; left: 0px; width: 445px; height: 300px;" />
                    <img src="img/c-jssor-slider.png" data-u="caption" data-t="1" style="position: absolute; top: 70px; left: 130px; width: 102px; height: 78px;" />
                    <img src="img/c-text.png" data-u="caption" data-t="2" style="position: absolute; top: 153px; left: 163px; width: 80px; height: 53px;" />
                    <img src="img/c-fruit.png" data-u="caption" data-t="3" style="position: absolute; top: 60px; left: 220px; width: 140px; height: 90px;" />
                    <img src="img/c-navigator.png" data-u="caption" data-t="4" style="position: absolute; top: -123px; left: 121px; width: 200px; height: 155px;" />
                </div>
                <div data-u="caption" data-t="5" style="position: absolute; top: 120px; left: 650px; width: 470px; height: 220px;">
                    <img src="img/c-phone-horizontal.png" style="position: absolute; top: 0px; left: 0px; width: 470px; height: 220px;" />
                    <div style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px; overflow: hidden;">
                        <img src="img/c-slide-1.jpg" data-u="caption" data-t="6" style="position: absolute; top: 0px; left: 0px; width: 379px; height: 213px;" />
                        <img src="img/c-slide-3.jpg" data-u="caption" data-t="7" style="position: absolute; top: 0px; left: 379px; width: 379px; height: 213px;" />
                    </div>
                    <img src="img/c-navigator-horizontal.png" style="position: absolute; top: 4px; left: 45px; width: 379px; height: 213px;" />
                    <img src="img/c-finger-pointing.png" data-u="caption" data-t="8" style="position: absolute; top: 740px; left: 1600px; width: 257px; height: 300px;" />
                </div>
            </div>
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="img/purple.jpg" />
            </div>
            <div data-p="225.00" style="display: none;">
                <img data-u="image" src="img/blue.jpg" />
            </div>
            <a data-u="ad" href="http://www.jssor.com" style="display:none">jQuery Slider</a>
        
        </div>
        <!-- Bullet Navigator -->
        <div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
            <!-- bullet navigator item prototype -->
            <div data-u="prototype" style="width:16px;height:16px;"></div>
        </div>
        <!-- Arrow Navigator -->
        <span data-u="arrowleft" class="jssora22l" style="top:0px;left:12px;width:40px;height:58px;" data-autocenter="2"></span>
        <span data-u="arrowright" class="jssora22r" style="top:0px;right:12px;width:40px;height:58px;" data-autocenter="2"></span>
    </div>

						 <ul>
                                <li>  Jumlah pembiayaan sehingga RM200,000  </li>
                                <li> Kadar keuntungan serendah 3.90% setahun</li>
                                <li> Tempoh pembiayaan sehingga 10 tahun</li>
                                <li>  Kelulusan segera</li>
                                <li>  Hadiah-hadiah menarik menanti anda</li>
                               
                            </ul>
							
						
							
						</div>
                        
                        
					

					</div>
					<div class="col-xs-12 col-sm-12 col-md-5 col-lg-5">
						<div class="well no-padding">

							 {!! Form::open(['url' => 'praapplication','class' => 'smart-form client-form', 'id' =>'smart-form-register' ]) !!}
								<header>
									Pembiayaan Persendirian i-Lestari
								</header>

								<fieldset>
                                     <div class="row">
                                        <section class="col col-6">
                                          <label class="label">Nama Penuh</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="FullName" name="FullName" placeholder="Full Name"    @if (Session::has('fullname'))  value="{{ Session::get('fullname') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Nama Penuh</b>
                                            </label>
                                        </section>
                                         <section class="col col-6">
                                          <label class="label">Email</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope"></i>
                                                <input type="email" id="Email2" name="Email2" placeholder="Email" required="required"  @if (Session::has('email'))  value="{{ Session::get('email') }}" @endif  >
                                                <b class="tooltip tooltip-bottom-right">Email</b>
                                            </label>
                                        </section>
                                    </div>
                                        <div class="row">
                                        <section class="col col-6">
                                            <label class="label">No Kad Pengenalan </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="ICNumber" name="ICNumber" minlength="12" maxlength="12" placeholder="IC Number"  @if (Session::has('icnumber'))  value="{{ Session::get('icnumber') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">IC Number</b>
                                            </label>
                                        </section>
                                
                                    
                                        <section class="col col-6">
                                            <label class="label">Nombor Telefon Bimbit </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-mobile-phone"></i>
                                                <input type="text" id="PhoneNumber" name="PhoneNumber" placeholder="Handphone Number"  minlength="7" maxlength="12"  onkeypress="return isNumberKey(event)"  @if (Session::has('phone'))  value="{{ Session::get('phone') }}" @endif>
                                                <b class="tooltip tooltip-bottom-right">Nombor Telefon Bimbit </b>
                                            </label>
                                        </section>
                                      
                                        </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Jenis Pekerjaan </label>
                                                <label class="select">
                                                    <select name="Employment" id="Employment" class="form-control"  onchange="document.getElementById('Employment2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employment')) 
                                                    <option  value="{{ Session::get('employment') }}">{{ Session::get('employment2') }}</option>
                                                     
                                                     @else 
                                                     <option value=""> </option>
                                                    @endif
                                                       
                                                         @foreach ($employment as $employment)
   								 					<option value="{{ $employment->id }}">{{ $employment->name }}</option>
															@endforeach
                                                    </select> <i></i>

                                                    <input type="hidden" name="Employment2" id="Employment2" value="" />
                                                </label>
                                            </section>
                                         <section class="col col-6">
                                            <label class="label">Majikan</label>
                                                <label class="select" id="majikan">
                                                    <select name="Employer" id="Employer" class="form-control" onchange="document.getElementById('Employer2').value=this.options[this.selectedIndex].text"  >
                                                    @if (Session::has('employer')) 
                                                    <option  value="{{ Session::get('employer') }}">{{ Session::get('employer2') }}</option>
                                                     
                                                     
                                                    @endif

                                                   
                                                       
                                                    </select> <i></i>
                                                    <input type="hidden" name="Employer2" id="Employer2" value="" />
                                                </label>

                                                 <label id="majikan2" class="input @if (Session::has('basicsalary')) state-error @endif ">
                                                <i class="icon-append fa fa-briefcase"></i>
                                                <input type="text" name="majikan"  placeholder="Majikan" maxlength="70"  @if (Session::has('majikan'))  value="{{ Session::get('majikan') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Gaji Bulanan (RM)</b>
                                            </label>

                                            </section>
                                        </div>
                                            <div class="row">
                                                  <section class="col col-6">
                                            <label class="label">Gaji Bulanan (RM) </label>
                                            <label class="input @if (Session::has('basicsalary')) state-error @endif ">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="BasicSalary" id="BasicSalary" placeholder="Basic Salary (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('basicsalary'))  value="{{ Session::get('basicsalary') }}" @endif >
                                                <b class="tooltip tooltip-bottom-right">Gaji Bulanan (RM)</b>
                                            </label>
                                        </section>
                                                <section class="col col-6">
                                                    <label class="label">Elaun (RM) </label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                        <input type="text" name="Allowance" id="Allowance" placeholder="Allowance (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('allowance'))  value="{{ Session::get('allowance') }}" @endif >
                                                        <b class="tooltip tooltip-bottom-right">Allowance (RM)</b>
                                                    </label>
                                                </section>
                                                <section class="col col-6">
                                                    <label class="label">Jumlah Potongan Bulanan Semasa (RM) </label>
                                                    <label class="input">
                                                        <i class="icon-append fa fa-credit-card"></i>
                                                        <input type="text" name="Deduction" id="Deduction" placeholder="Existing Total Deduction (RM)" onkeypress="return isNumberKey(event)" maxlength="7"  @if (Session::has('deduction'))  value="{{ Session::get('deduction') }}" @endif >
                                                        <b class="tooltip tooltip-bottom-right">Existing Total Deduction (RM)</b>
                                                    </label>
                                                </section>
                                            </div>
                                </fieldset>
                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Pakej </label>
                                                <label class="select">
                                                    <select name="Package" id="Package" class="form-control">
                                                        
                                                           @foreach ($package as $package)
   								 					<option value="{{ $package->id }}">{{ $package->name }}</option>
															@endforeach
                                                    </select> <i></i>
                                                </label>
                                            </section>
                                        <section class="col col-6">
                                            <label class="label">Jumlah Pembiayaan(RM) </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" name="LoanAmount" id="LoanAmount" placeholder="Loan Amount (RM)" onkeypress="return isNumberKey(event)" @if (Session::has('loanAmount'))  value="{{ Session::get('loanAmount') }}" @endif>
                                                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <b class="tooltip tooltip-bottom-right">Loan Amount (RM)</b>
                                            </label>
                                        </section>
                                        </div>

                                </fieldset>

                                <footer>
                                    <button type="submit" class="btn btn-primary">
                                        Kira Kelayakan Anda
                                    </button>
                                    <div id="response"></div>
                                </footer>

								<div class="message">
									<i class="fa fa-check"></i>
									<p>
										Thank you for your registration!
									</p>
								</div>
							</form>
						
                                   <div class='smart-form client-form'id='smart-form-register2'>
                                <header>Egibilty Result</header>


                                <fieldset>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Package</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="Package2" name="Package2" placeholder="Package" disabled="disabled">
                                                
                                                <b class="tooltip tooltip-bottom-right">Package</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">Loan Amount</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-user"></i>
                                                <input type="text" id="LoanAmount2" name="LoanAmount2" placeholder="Loan Amount" disabled="disabled">
                                                <b class="tooltip tooltip-bottom-right">Loan Amount</b>
                                            </label>
                                        </section>
                                    </div>
                                    <div class="row">
                                        <section class="col col-6">
                                            <label class="label">Max Loan Eligibility </label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="MaxLoan" name="MaxLoan" placeholder="Max Loan Eligibility (RM)" requierd>
                                                <b class="tooltip tooltip-bottom-right">Max Loan Eligibility (RM)</b>
                                            </label>
                                        </section>
                                        <section class="col col-6">
                                            <label class="label">Interest Rate</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-credit-card"></i>
                                                <input type="text" id="InterestRete" name="Rate" placeholder="Interest Rate" value="3.65" requierd>
                                                <b class="tooltip tooltip-bottom-right">Interest Rate</b>
                                            </label>
                                        </section>
                                        </div>
                                </fieldset>
                                <fieldset>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Tenure(Year)</th>
                                                <th>Installment(RM)</th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>3</td>
                                                <td>934.00</td>
                                                <td><input type="radio" name="Installmen" value="3"></td>

                                            </tr>
                                            <tr>
                                                <td>4</td>
                                                <td>725.00</td>
                                                <td><input type="radio" name="Installmen" value="4"></td>
                                            </tr>
                                            <tr>
                                                <td>5</td>
                                                <td>600.00</td>
                                                <td><input type="radio" name="Installmen" value="5"></td>
                                            </tr>
                                            <tr>
                                                <td>6</td>
                                                <td>517.00</td>
                                                <td><input type="radio" name="Installmen" value="6"></td>
                                            </tr>
                                            <tr>
                                                <td>7</td>
                                                <td>457.00</td>
                                                <td><input type="radio" name="Installmen" value="7"></td>
                                            </tr>
                                            <tr>
                                                <td>8</td>
                                                <td>413.00</td>
                                                <td><input type="radio" name="Installmen" value="8"></td>
                                            </tr>
                                            <tr>
                                                <td>9</td>
                                                <td>378.00</td>
                                                <td><input type="radio" name="Installmen" value="9"></td>
                                            </tr>
                                            <tr>
                                                <td>10</td>
                                                <td>350.00</td>
                                                <td><input type="radio" name="Installmen" value="10"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <fieldset>
                                    

                                <footer>
                                        <button class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
                                    Continue
                                </button>
                                   
                                  
                                </footer>

                    <div>
                                      

						</div>
					
					</div>
				</div>
			</div>

		</div>
        </div>
        </div>
       <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-white inline-block">
                        <span class="txt-color-white">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title" id="myModalLabel">Register</h4>
                    </div>
                    <div class="modal-body">
        {!! Form::open(['url' => 'user','class' => 'smart-form client-form', 'id' =>'smart-form-register3' ]) !!}
                      <fieldset>
                                      <section >
                                          <label class="label">Pealase Register For Continue<br><br>Email</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-envelope"></i>
                                                <input type="Email" id="Email" name="Email" placeholder="Email" readonly>
                                                <b class="tooltip tooltip-bottom-right">Email</b>
                                            </label>
                                        </section>
                                        
                                        <section >
                                            <label class="label">Password</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="Password" name="Password" placeholder="Password" >
                                                <b class="tooltip tooltip-bottom-right">Password</b>
                                            </label>
                                        </section>
                                        <section >
                                            <label class="label">Password Confirmation</label>
                                            <label class="input">
                                                <i class="icon-append fa fa-key "></i>
                                                <input type="Password" id="PasswordConfirmation" name="PasswordConfirmation" placeholder="Password Confirmation" >
                                                <b class="tooltip tooltip-bottom-right">Password Confirmation</b>
                                            </label>
                                        </section>
                                     <input type="hidden" id="FullName2" name="FullName2" placeholder="Full Name" readonly >
                                     <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                      
                                     
                                </fieldset>
                       
                       
        
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Cancel
                        </button>
                        <button type="submit" name="submit" class="btn btn-primary">
                                       Register
                                    </button>
                                   
                           {!! Form::close() !!}   
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        

<!-- ==========================CONTENT ENDS HERE ========================== -->
<script type="text/javascript">

		$(document).ready(function() {
				
					$("#smart-form-register2").hide();
				$("#smart-form-register").validate({

					// Rules for form validation
					rules : {
					    FullName: {
							required : true
						},
						ICNumber : {
							required : true
						},
						
						PhoneNumber: {
						    required: true
						},
						Deduction: {
						    required: true
						},
						
						Allowance: {
						    required: true
						},
						Package: {
						    required: true
						},
						Employment: {
						    required: true
						},
						Employer: {
						    required: true
						},
						
						
						BasicSalary: {
						    required: true
                            
						},
						LoanAmount: {
						    required: true
						}
					},

					// Messages for form validation
					messages : {

					    FullName: {
							required : 'Please enter your full name'
						},
						
						ICNumber: {
						    required: 'Please enter your ic number'
						},
						PhoneNumber: {
						    required: 'Please enter your phone number'
						},
						Allowance: {
						    required: 'Please enter  yor allowance'
						},
						Deduction: {
						    required: 'Please enter your total deduction'
						},
						Package: {
						    required: 'Please select package'
						},
						Employment: {
						    required: 'Please select employement type'
						},

						Employer: {
						    required: 'Please select employer'
						},


						BasicSalary: {
						    required: 'Please enter your basic salary'
						},
						LoanAmount: {
						    required: 'Please select your loan amount'
						}
					}
				});

			});
    </script>

<script type="text/javascript">
   
    // Validation
    $(function() {
         runAllForms();
        $("#smart-form-register3").validate({

            // Rules for form validation
           rules : {
                
                Email2 : {
                    required : true,
                    email : true
                },
                BasicSalary : {
                    required : true,
                    min :2000

                    
                },
                Password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                ICNumber : {
                    required : true,
                    minlength : 12,
                    maxlength : 13
                },
                PasswordConfirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#Password'
                }
            },

            // Messages for form validation
             messages : {

                    Email2 : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                BasicSalary : {
                    required : 'Please enter your email address'
                },
                Password : {
                    required : 'Please enter your password'
                },
                PasswordConfirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                }
                    }
        });

    });
</script>
<?php 
	//include required scripts
	include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) 
<script src="..."></script>-->
		

<script type="text/javascript">

 @if (Session::has('employer')) 
 
   $("#majikan2").hide();
    $("#majikan").show();
  @else
   $("#majikan").hide();
   $("#majikan2").show();
  @endif

$( "#Employment" ).change(function() {
    var Employment = $('#Employment').val();
    if( Employment != '2') {
   
     $("#Employer").html(" ");
     $("#Employer2").val(" ");
        $("#majikan2").show();
         $("#majikan").hide();
    }
    else {
          $("#Employer").html("<option value=''>  </option> ");
        $("#majikan2").hide();
         $("#majikan").show();


    }
  $.ajax({
                url: "<?php  print url('/'); ?>/employer/"+Employment,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        $("#Employer").append("<option value='" + data[k].id + "'>" + data[k].name + "</option>");
                    });

                }
            });

   
});
</script>

<script>
function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}
</script>

<script type="text/javascript">
    
     $(document).ready(function() {

   var found = [];
$("#Employment option").each(function() {
  if($.inArray(this.value, found) != -1) $(this).remove();
  found.push(this.value);
});
     });



</script>

<script type="text/javascript">

function getSelectedText(elementId) {
    var elt = document.getElementById(elementId);

    if (elt.selectedIndex == -1)
        return null;

    return elt.options[elt.selectedIndex].text;
}


         var Employment = getSelectedText('Employment');
         var Employer = getSelectedText('Employer');
          $("#Employment2").val(Employment);
           $("#Employer2").val(Employer); 

</script>