<p>Dear {{$nameho}} </p>
<p>A New customer has submit a loan application and have been approved by Ezlestari Administrator.
The application is now ready to start the verification process by <b>{{$branchname}} Branch</b></p>
<p>Short detail of customer :</p>
<p> Name : {{$cus_name}}</p>
<p> IC No. : {{$cus_ic}} </p>
<p>please make sure the application are reviewed by {{$branchname}} Branch as soon as possible</p>
<br>
<p>Thank You,</p>
<br>
<p>Ezlestari.com.my</p>
