<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "User Verification";


$page_css[] = "your_style.css";
include("asset/inc/header.php");





?>

<style>
.not-active {
   pointer-events: none;
   cursor: default;
}
table.border {
    border-collapse: separate;
    border-spacing: 10px; /* cellspacing */
    *border-collapse: expression('separate', cellSpacing = '10px');
}

td.border {
    padding: 10px; /* cellpadding */
}
</style>

    <?php
  include("asset/inc/nav.php");
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>

<div id="main" role="main">
    <div id="content">
<section id="widget-grid" class="">

                
                    <!-- row -->
                    <div class="row">
                
                        <!-- NEW WIDGET START -->
                        <article class="col-sm-12 col-md-12 col-lg-12">
                
                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget jarviswidget-color-blue" id="wid-id-0" data-widget-editbutton="false" data-widget-deletebutton="false">
                                <!-- widget options:
                                usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                
                                data-widget-colorbutton="false"
                                data-widget-editbutton="false"
                                data-widget-togglebutton="false"
                                data-widget-deletebutton="false"
                                data-widget-fullscreenbutton="false"
                                data-widget-custombutton="false"
                                data-widget-collapsed="true"
                                data-widget-sortable="false"
                
                                -->
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-check"></i> </span>
                                    <h2>Customer Verification</h2>
                
                                </header>
                
                                <!-- widget div-->
                                <div>
                
                                    <!-- widget edit box -->
                                    <div class="jarviswidget-editbox">
                                        <!-- This area used as dropdown edit box -->
                
                                    </div>
                                    <!-- end widget edit box -->
                
                                    <!-- widget content -->
                                    <div class="widget-body">
                          
                                       <div class="row">
<div class="col-lg-6">
    <div class="box dark">
        <header>
            <div class="icons"><i class="fa fa-edit"></i></div>
            <h5>Input Text Fields</h5>
            <!-- .toolbar -->
            <div class="toolbar">
              <nav style="padding: 8px;">
                  <a href="javascript:;" class="btn btn-default btn-xs collapse-box">
                      <i class="fa fa-minus"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-default btn-xs full-box">
                      <i class="fa fa-expand"></i>
                  </a>
                  <a href="javascript:;" class="btn btn-danger btn-xs close-box">
                      <i class="fa fa-times"></i>
                  </a>
              </nav>
            </div>            <!-- /.toolbar -->
        </header>
        <div id="div-1" class="body">
            <form class="form-horizontal">

                <div class="form-group">
                    <label for="text1" class="control-label col-lg-4">Normal Input Field</label>

                    <div class="col-lg-8">
                        <input type="text" id="text1" placeholder="Email" class="form-control">
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label for="pass1" class="control-label col-lg-4">Password Field</label>

                    <div class="col-lg-8">
                        <input class="form-control" type="password" id="pass1"
                               data-original-title="Please use your secure password" data-placement="top"/>
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-label col-lg-4">Read only input</label>

                    <div class="col-lg-8">
                        <input type="text" value="read only" readonly class="form-control">
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label class="control-label col-lg-4">Disabled input</label>

                    <div class="col-lg-8">
                        <input type="text" value="disabled" disabled class="form-control">
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label for="text2" class="control-label col-lg-4">With Placeholder</label>

                    <div class="col-lg-8">
                        <input type="text" id="text2" placeholder="placeholder text" class="form-control">
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label for="limiter" class="control-label col-lg-4">Input limiter</label>

                    <div class="col-lg-8">
                        <textarea id="limiter" class="form-control"></textarea>
                    </div>
                </div>
                <!-- /.row -->

                <div class="form-group">
                    <label for="text4" class="control-label col-lg-4">Default Textarea</label>

                    <div class="col-lg-8">
                        <textarea id="text4" class="form-control"></textarea>
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label for="autosize" class="control-label col-lg-4">Textarea With Autosize</label>

                    <div class="col-lg-8">
                        <textarea id="autosize" class="form-control"></textarea>
                    </div>
                </div>
                <!-- /.form-group -->

                <div class="form-group">
                    <label for="tags" class="control-label col-lg-4">Tags</label>

                    <div class="col-lg-8">
                        <input name="tags" id="tags" value="foo,bar,baz" class="form-control">
                    </div>
                </div>
                <!-- /.form-group -->
            </form>
        </div>
    </div>
</div>
                
                                    </div>
                                    <!-- end widget content -->
                
                                </div>
                                <!-- end widget div -->
                
                            </div>
                            <!-- end widget -->
                
                        </article>
                        <!-- WIDGET END -->
                
                        <!-- NEW WIDGET START -->
                        <a
                        <!-- WIDGET ENffD -->
                
                    </div>
                
                    <!-- end row -->
                
                </section>
            </div>
            <br>
                <div class="page-footer">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                   
                </div>

                <div class="col-xs-6 col-sm-6 text-right hidden-xs">
                    <div class="txt-color-black inline-block">
                        <span class="txt-color-black">NetXpert Data Solution © All rights reserved   </span>
                        
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
        
        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        
        $(document).ready(function() {
            
            pageSetUp();
            
            
    
            //Bootstrap Wizard Validations

              var $validator = $("#wizard-1").validate({
                
                rules: {
                  email: {
                    required: false,
                    email: "Your email address must be in the format of name@domain.com"
                  },
                  fname: {
                    required: true
                  },
                  lname: {
                    required: true
                  },
                  country: {
                    required: true
                  },
                  city: {
                    required: false
                  },
                 
                  wphone: {
                    required: true,
                    minlength: 10
                  },
                  hphone: {
                    required: true,
                    minlength: 10
                  }
                },
                
                messages: {
                  fname: "Please specify your First name",
                  lname: "Please specify your Last name",
                  email: {
                    required: "We need your email address to contact you",
                    email: "Your email address must be in the format of name@domain.com"
                  }
                },
                
                highlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                  $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                  if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                  } else {
                    error.insertAfter(element);
                  }
                }
              });
              
              $('#bootstrap-wizard-1').bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                  var $valid = $("#wizard-1").valid();
                  if (!$valid) {
                    $validator.focusInvalid();
                    return false;
                  } else {

                   

                      $.ajax({

                            type: "PUT",
                           
                             url: '{{ url('/verifiedForm/') }}'+'/'+index,
                            data: $('#tab'+index+' :input').serialize(),

                            cache: false,
                            beforeSend: function () {
                               
                            },

                            success: function () {
                             
                            
                             

                            },
                            error: function () {
                               
                                 

                            }
                        });
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                      'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                    .html('<i class="fa fa-check"></i>');
                  }
                }
              });
              
        
            // fuelux wizard
              var wizard = $('.wizard').wizard();
              
              wizard.on('finished', function (e, data) {
                //$("#fuelux-wizard").submit();
                //console.log("submitted!");
                $.smallBox({
                  title: "Congratulations! Your form was submitted",
                  content: "<i class='fa fa-clock-o'></i> <i>1 seconds ago...</i>",
                  color: "#5F895F",
                  iconSmall: "fa fa-check bounce animated",
                  timeout: 4000
                });
                
              });

        
        })

        </script>

        <!-- Your GOOGLE ANALYTICS CODE Below -->
        <script type="text/javascript">
            var _gaq = _gaq || [];
                _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
                _gaq.push(['_trackPageview']);
            
            (function() {
                var ga = document.createElement('script');
                ga.type = 'text/javascript';
                ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0];
                s.parentNode.insertBefore(ga, s);
            })();


                $('.startdate').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: '1950:2017',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });

                 $('.date').datepicker({
                dateFormat : 'dd/mm/yy',
                 changeYear: true,
                 changeMonth: true,
                 yearRange: '1950:2017',
                prevText : '<i class="fa fa-chevron-left"></i>',
                nextText : '<i class="fa fa-chevron-right"></i>',
                onSelect : function(selectedDate) {
                    $('#finishdate').datepicker('option', 'minDate', selectedDate);
                }
            });


        </script>


<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

       <script type="text/javascript">
       
$( "#postcode2" ).change(function() {
    var postcode = $('#postcode2').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#state2").val(data[k].state.state_name );
                    });

                }
            });

   
});

</script>

  <script type="text/javascript">

$( "#postcode" ).keyup(function() {
    var postcode = $('#postcode').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

    
                        $("#state").val(data[k].state.state_name );
                  
                    });

                }
            });

   
});
</script>

      <script type="text/javascript">
       
$( "#postcode3" ).change(function() {
    var postcode = $('#postcode3').val();
     
  $.ajax({
                url: "<?php  print url('/'); ?>/postcode/"+postcode,
                dataType: 'json',
                data: {
                   
                },
                success: function (data, status) {

                    jQuery.each(data, function (k) {

                        //$("#city").val(data[k].post_office );
                        $("#state3").val(data[k].state.state_name );
                    });

                }
            });

   
});

</script>

<?php for ($x = 1; $x <= 12; $x++) {  ?>

<script>
/*jslint unparam: true */
/*global window, $ */
$(function () {
    'use strict';

    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : '{{url('/')}}/form/upload/{{$x}}';
    $('#fileupload{{$x}}').fileupload({
        url: url,
        dataType: 'json',
        success: function ( data) {
             var text = $('#documentx{{$x}}').val();
            $("#document{{$x}}").html("<a target='_blank' href='"+"{{url('/')}}/admin/downloaddocpdf/{{str_replace('/', '', $pra->fullname)}}/"+data.file+"'>"+text+"</a><i class='glyphicon glyphicon-ok txt-color-green'></i>");
             $("#document{{$x}}a").hide();
            

             document.getElementById("fileupload{{$x}}").required = false;
             
            
            

        }
       
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');

        
});
</script>

<?php } ?>


  <script type="text/javascript">

$( "#agree" ).click(function() {

    
      var id_praapplication = $('#id_praapplication_term').val();
      var _token = $('#token_term').val();
      var branch = $('#branch').val();
      var declaration = 2;
      var remark = $('#remark_verification').val();
      
      if ((remark=='') || (branch=='')) {
      //code
       bootbox.alert("Please Select Branch AND Fill a Remark!");
      }
      else {
      var r = confirm("Are You Sure to Route this Application to Branch?");
      if (r == true) {
         $.ajax({

               type: "POST",
              
               url: '{{ url('/verifiedForm/term/') }}',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () {
                  
               },

               success: function () {
                
                   alert("Verification Success, Application Approved Route to Branch !");
                   location.href='{{url('/')}}/admin';
                

               },
               error: function () {
                  
               }
           });
     }
   }
});
</script>

<script type="text/javascript">

$( "#disagree" ).click(function() {
   var id_praapplication = $('#id_praapplication_term').val();
   var _token = $('#token_term').val();
   var declaration = 3;
   var branch = $('#branch').val();
   var remark = $('#remark_verification').val();
   if (remark=='') {
      //code
       alert("Please fill Remark !");
   }
   else {
      var r = confirm("Are You Sure to Rejected this Application?");
      if (r == true) {
     
         $.ajax({
               type: "POST",
               url: '{{ url('/verifiedForm/term/') }}',
               data: { branch: branch, id_praapplication: id_praapplication, _token : _token, declaration : declaration, remark : remark},
     
               cache: false,
               beforeSend: function () { 
               },
               success: function () {
                   alert("Application Rejected !");
                   location.href='{{url('/')}}/admin';
               },
               error: function () {
               }
           });
     }
   }
});
</script>








<script>
function isNumberKey(evt) {
          var theEvent = evt || window.event;
          var key = theEvent.keyCode || theEvent.which;
          key = String.fromCharCode(key);
          if (key.length == 0) return;
          var regex = /^[0-9.,\b]+$/;
          if (!regex.test(key)) {
              theEvent.returnValue = false;
              if (theEvent.preventDefault) theEvent.preventDefault();
          }
}
</script>

<script>
function toFloat(z) {
    var x = document.getElementById(z);
    x.value = parseFloat(x.value).toFixed(2);
}
</script>


<script>
function alertWithoutNotice(message){
    setTimeout(function(){
        alert(message);
    }, 1000);
}
</script>





@if($view=='view')
<script type="text/javascript">
    $(document).ready(function(){
        $("#wizard-1 :input").prop("disabled", true);
    });

</script>
@endif









