
        <script type="text/javascript">
            $('input.Currency_basicsalary1').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });

            $('input.Currency_allowance1').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });


            $( ".fn" ).keyup(function() {
             var a = $('#a1').val().replace(/,/g,'');
             
              var b = $('#b1').val().replace(/,/g,'');

              var total = parseFloat(a) + parseFloat(b);
              $("#total_nya").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            });

        </script>

        <!-- end of total salary 1 -->


        <!-- total salary 2 -->
        <script type="text/javascript">
            $('input.Currency_basicsalary2').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });

            $('input.Currency_allowance2').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });


            $( ".fn" ).keyup(function() {
             var a = $('#a2').val().replace(/,/g,'');
             
              var b = $('#b2').val().replace(/,/g,'');

              var total = parseFloat(a) + parseFloat(b);
              $("#total_nya2").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            });

        </script>

        <!-- end of total salary 2 -->


        <!-- total salary 3 & variable income -->
        <script type="text/javascript">
            $('input.Currency_basicsalary3').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });

            $('input.Currency_allowance3').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });


            $('input.variable_income').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });


            $( ".fn" ).keyup(function() {
              var a = $('#a3').val().replace(/,/g,'');
              var b = $('#b3').val().replace(/,/g,'');
              var ndi_variable_income = $('#variable_income').val().replace(/,/g,'');
              var ndi_non_salaried = $('#additional_income').val().replace(/,/g,'');




              var total = parseFloat(a) + parseFloat(b);
              $("#total_nya3").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
              
              $("#total_earnings").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
              
              /* -- gmi -- */
              var gmi = $('#variable_income').val().replace(/,/g,'');
              var total_gmi = parseFloat(a) + parseFloat(b) + parseFloat(gmi);
              
              $("#gmi").val(parseFloat(total_gmi).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
              $("#gmi_use").val(parseFloat(total_gmi).toFixed(2));
              
              $("#ndi_basic_salary").val(parseFloat(a).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
              $("#ndi_fixed_allowance").val(parseFloat(b).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
              $("#ndi_variable_income").val(parseFloat(ndi_variable_income).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              $("#ndi_non_salaried").val(parseFloat(ndi_non_salaried).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));


              var ndi_ite = $('#ite').val().replace(/,/g,'');
              $("#ndi_ite").val(parseFloat(ndi_ite).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var ndi_gross_income = $('#gmi').val().replace(/,/g,'');
              $("#ndi_gross_income").val(parseFloat(ndi_gross_income).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var ndi_deduction = $('#total_deduction1').val().replace(/,/g,'');
              $("#ndi_deduction").val(parseFloat(ndi_deduction).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var ndi_net_income = $('#net_income').val().replace(/,/g,'');
              $("#ndi_net_income").val(parseFloat(ndi_net_income).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var ndi_commitment = $('#total_commitment').val().replace(/,/g,'');
              $("#ndi_commitment").val(parseFloat(ndi_commitment).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var ndi_repayment = $('#monthly_repayment').val().replace(/,/g,'');
              $("#ndi_repayment").val(parseFloat(ndi_repayment).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var ndi_deduction = $('#total_deduction3').val().replace(/,/g,'');
              $("#ndi_deduction").val(parseFloat(ndi_deduction).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var ndi_ndi = $('#ndi_use').val().replace(/,/g,'');
              $("#ndi_ndi").val(parseFloat(ndi_ndi).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              $("#rr_dsr").val($('#dsr').val());
    $("#rr_ndi").val(parseFloat($('#ndi').val()).toFixed(2));

               

           
            });

            

          

        </script>

        <!-- end of total salary 3 -->

      <!-- total salary 1 -->
<script type="text/javascript">
    function getDecision() {

    var category = $('#category').val();

    var existing_mbsb = $('#existing_mbsb').val();

    var ndi_use = $('#ndi_use').val();

    var gmi_use = $('#gmi_use').val();

    var dsr = $('#dsr_bnm').val();

    var sector = $('#sector').val();



    if(sector=="Government") {
       if(category=="Biro") {
            if(existing_mbsb=="No") {
                if(gmi_use<3000) {
                    $("#decision").val("FAIL. New BIRO Customer, Gross Income < RM3,000."); 
                }
                else if(gmi_use>=3000) {
                    if(dsr>85.0000) {
                        $("#decision").val("FAIL. Max DSR 85%");
                    }
                    else {
                        if(ndi_use<1300) {
                            $("#decision").val("FAIL. New BIRO Customer, NDI < RM1,300.");
                        }
                        else if(isNaN(ndi_use)) {
                            $("#decision").val(" ");
                        }
                        else {
                            $("#decision").val("PASS");  
                        }
                    }
                }
            }
            else if(existing_mbsb=="Yes") {
                if(gmi_use<1500) {
                    $("#decision").val("FAIL. Existing BIRO Customer, Gross Income < RM1,500.");
                }
                else if(gmi_use>=1500) {
                    if(gmi_use>=2000 && gmi_use<=3000 && dsr>60.000) {
                        $("#decision").val("FAIL. Gross Income 2000 - 3000, Max DSR 60%");
                    }
                    else if(gmi_use>3000 && dsr>85.000) {
                        $("#decision").val("FAIL. Gross Income > 3000, Max DSR 85%");
                    }
                    else {
                        if(ndi_use<1000) {
                            $("#decision").val("FAIL. Existing BIRO Customer, NDI < RM1,000.");
                        }
                        else if(isNaN(ndi_use)) {
                            $("#decision").val(" ");
                        }
                        else {
                            $("#decision").val("PASS");  
                        }
                    }
                 }
            }
            else {
                $("#decision").val("Please select Existing MBSB PF-I customer refinancing their existing PF-I facility with MBSB (internal overlap) option in Section A");  
            }
        }
        else if(category=="Non-Biro") {
            if(gmi_use<3000) {
                $("#decision").val("FAIL. Non-Biro Customer, Gross Income < RM3,000.");
            }
            else if(gmi_use>=3000) {
                if(dsr>85.000) {
                    $("#decision").val("FAIL. Max DSR 85%");
                }
                else {
                    if(ndi_use<1300) {
                        $("#decision").val("FAIL. Non-Biro Customer, NDI < RM1,300.");
                    }
                    else if(isNaN(ndi_use)) {
                        $("#decision").val(" ");
                    }
                    else {
                        $("#decision").val("PASS");
                    }
                }
            }
        }
        else {
            $("#decision").val("Please select category option in Section A");  
        }
    }
    else if(sector=="Private") {
        if(gmi_use<3500) {
            $("#decision").val("FAIL. Private Gross Income < RM3,500.");
        }
        else if(gmi_use>=3500) {
            if(gmi_use<10000 && dsr>65.000) {
                $("#decision").val("FAIL. For Gross Income less than RM 10,000, Max DSR is 65%");
            }
            else if(gmi_use>=10000 && dsr>80.000) {
                $("#decision").val("FAIL. For Gross Income more than RM 10,000, Max DSR is 80%");
            }
            else {
                if(ndi_use<1300) {
                    $("#decision").val("FAIL. Customer's NDI < RM1,300.");
                }
                else if(isNaN(ndi_use)) {
                    $("#decision").val(" ");
                }
                else {
                    $("#decision").val("PASS");  
                }
            }
        }
    }
}
</script>
        <!-- Deductions -->
         <script type="text/javascript">
            $('input.Currency').on('blur', function() {
              const value = this.value.replace(/,/g, '');
              this.value = parseFloat(value).toLocaleString('en-US', {
                style: 'decimal',
                maximumFractionDigits: 2,
                minimumFractionDigits: 2
              });
            });

            

            $( ".Currency" ).keyup(function() {
              var epf1 = $('#epf1').val().replace(/,/g,'');
              var socso1 = $('#socso1').val().replace(/,/g,'');
              var income_tax1 = $('#income_tax1').val().replace(/,/g,'');
              var other_contribution1 = $('#other_contribution1').val().replace(/,/g,'');

              var epf2 = $('#epf2').val().replace(/,/g,'');
              var socso2 = $('#socso2').val().replace(/,/g,'');
              var income_tax2 = $('#income_tax2').val().replace(/,/g,'');
              var other_contribution2 = $('#other_contribution2').val().replace(/,/g,'');

              var epf3 = $('#epf3').val().replace(/,/g,'');
              var socso3 = $('#socso3').val().replace(/,/g,'');
              var income_tax3 = $('#income_tax3').val().replace(/,/g,'');
              var other_contribution3 = $('#other_contribution3').val().replace(/,/g,'');


              var total = parseFloat(epf1) + parseFloat(socso1) + parseFloat(income_tax1) + parseFloat(other_contribution1);
              $("#total_deduction1").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var total = parseFloat(epf2) + parseFloat(socso2) + parseFloat(income_tax2) + parseFloat(other_contribution2);
              $("#total_deduction2").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var total_d3 = parseFloat(epf3) + parseFloat(socso3) + parseFloat(income_tax3) + parseFloat(other_contribution3);
              $("#total_deduction3").val(parseFloat(total_d3).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              $("#total_deduction").val(parseFloat(total_d3).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              /* Total nmi */
              var gmi = $('#gmi_use').val().replace(/,/g,'');
              var total_deduction = $('#total_deduction').val().replace(/,/g,'');
              var total = parseFloat(gmi) - parseFloat(total_deduction);
              $("#nmi").val(parseFloat(total).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));


            var additional_income = $('#additional_income').val().replace(/,/g,'');
            var ite = $('#ite').val().replace(/,/g,'');


            var total_gross_income = parseFloat(gmi) + parseFloat(additional_income) ;
            $("#total_gross_income").val(parseFloat(total_gross_income).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

            var total_net_income = parseFloat(total) + parseFloat(additional_income) - parseFloat(ite) ;
            $("#total_net_income").val(parseFloat(total_net_income).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

            var financing_amount = $('#financing_amount').val().replace(/,/g,'');
            var tenure = $('#tenure').val().replace(/,/g,'');
            var existing_loan = $('#existing_loan').val().replace(/,/g,'');
            var discountCode = 'DISTR50';
            var codeEntered = $("#discount").val();
            
            if (tenure >= 4) {
                var effective_rate = $("#rate").val().replace(/,/g,'');
                var rates= ((parseFloat(effective_rate)/12) + 0.025)*10;
            }
            else{
                var effective_rate = $("#rate").val().replace(/,/g,'');
                var rates= parseFloat(effective_rate)/12*10;
            }

            var monthly_repayments = (parseFloat(financing_amount) * (parseFloat(rate)/100) * parseFloat(tenure) + parseFloat(financing_amount))/ (parseFloat(tenure)* 12);
            $("#monthly_repayments").val(parseFloat(monthly_repayments).toFixed(0));

            var months = 12* parseFloat(tenure);
            var interests = parseFloat(rates) / 1000;

            var monthly_repayment = (parseFloat(interests) * -parseFloat(financing_amount) *Math.pow((1 + parseFloat(interests)), parseFloat(months)) / (1 - Math.pow((1 + parseFloat(interests)), parseFloat(months))));
            $("#monthly_repayment").val(parseFloat(monthly_repayment).toFixed(0));
            
            var total_group_exposure = parseFloat(financing_amount) + parseFloat(existing_loan);
            $("#total_group_exposure").val(parseFloat(total_group_exposure).toFixed(0));

            var mbsb_housing = $('#mbsb_housing').val().replace(/,/g,'');
            var mbsb_hire = $('#mbsb_hire').val().replace(/,/g,'');
            var mbsb_personal = $('#mbsb_personal').val().replace(/,/g,'');
            var de_angkasa = $('#de_angkasa').val().replace(/,/g,'');
            var de_govhousing = $('#de_govhousing').val().replace(/,/g,'');
            var de_koperasi = $('#de_koperasi').val().replace(/,/g,'');
            var de_nonbiro = $('#de_nonbiro').val().replace(/,/g,'');
            var de_otherloan = $('#de_otherloan').val().replace(/,/g,'');
            var de_other = $('#de_other').val().replace(/,/g,'');
            var ccris_housing = $('#ccris_housing').val().replace(/,/g,'');
            var ccris_creditcard = $('#ccris_creditcard').val().replace(/,/g,'');
            var ccris_hire = $('#ccris_hire').val().replace(/,/g,'');
            var ccris_otherdraft = $('#ccris_otherdraft').val().replace(/,/g,'');
            var ccris_other = $('#ccris_other').val().replace(/,/g,'');

            var total_commitment = parseFloat(mbsb_housing) + parseFloat(mbsb_hire) + parseFloat(mbsb_personal) + parseFloat(de_angkasa) + parseFloat(de_govhousing) + parseFloat(de_koperasi) + parseFloat(de_nonbiro) + parseFloat(de_otherloan) + parseFloat(de_other) + parseFloat(ccris_housing) + parseFloat(ccris_creditcard) + parseFloat(ccris_hire) + parseFloat(ccris_otherdraft) + parseFloat(ccris_other);

            $("#total_commitment").val(parseFloat(total_commitment).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

            var total_debt = parseFloat(total_commitment) + parseFloat(monthly_repayment);
            $("#total_debt").val(parseFloat(total_debt).toFixed(0));

            var ndi = parseFloat(total_net_income) - parseFloat(total_commitment) - parseFloat(monthly_repayment).toFixed(0);
            $("#ndi").val(parseFloat(ndi).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            $("#ndi_use").val(parseFloat(ndi).toFixed(2));



            var ndi_ndi = $('#ndi').val().replace(/,/g,'');
              $("#ndi_ndi").val(parseFloat(ndi_ndi).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
              $("#rr_ndi").val(parseFloat(ndi_ndi).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

              var rr_dsr = $('#dsr').val().replace(/,/g,'');
              $("#rr_dsr").val(parseFloat(rr_dsr).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

               

            $("#net_income").val(parseFloat(total_net_income).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
            var dsr = (parseFloat(total_debt) / parseFloat(total_net_income))*100;

            //  $("#dsr").val(parseFloat(dsr).toFixed(2)+'%');

            $("#dsr").val(parseFloat(dsr).toFixed(2));
            $("#dsr_bnm").val(parseFloat(dsr).toFixed(4));

            


            getDecision();
            ndi_statement();

            });
            $( ".fin" ).change(function() {
            getDecision();
        });

        </script>

        <!--End of Deductions -->



