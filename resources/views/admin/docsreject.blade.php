<?php

//initilize the page
require_once("asset/inc/init.php");

//require UI configuration (nav, ribbon, etc.)
require_once("asset/inc/config.ui.php");


$page_title = "Administrator [Second Level Verification]";


$page_css[] = "your_style.css";
include("asset/inc/header.php");



include("asset/inc/nav.php");


?>
<!-- ==========================CONTENT STARTS HERE ========================== -->
<!-- MAIN PANEL -->
<div id="main" role="main">
  <?php
        //configure ribbon (breadcrumbs) array("name"=>"url"), leave url empty if no url
        //$breadcrumbs["New Crumb"] => "http://url.com"
        $breadcrumbs["Home"] = "";
        include("asset/inc/ribbon.php");
    ?>
    <!-- MAIN CONTENT -->
    <div id="content">
	   	@if (Session::has('error'))
    

        <div class="alert adjusted alert-danger fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('error') }}</strong> 
        </div>
        @elseif (Session::has('success'))
           <div class="alert adjusted alert-success fade in">
        <button class="close" data-dismiss="alert">
             ×
        </button>
         <i class="fa-fw fa-lg fa fa-exclamation"></i>
          <strong>{{ Session::get('success') }}</strong> 
        </div>
            
      @endif

						<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                            
                         

                            <!-- Widget ID (each widget will need unique ID)-->
                            <div class="jarviswidget well" id="wid-id-0">
                                <!-- widget options:
                                    usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">
                                    
                                    data-widget-colorbutton="false" 
                                    data-widget-editbutton="false"
                                    data-widget-togglebutton="false"
                                    data-widget-deletebutton="false"
                                    data-widget-fullscreenbutton="false"
                                    data-widget-custombutton="false"
                                    data-widget-collapsed="true" 
                                    data-widget-sortable="false"
                                    
                                -->
                                
                                
                                <header>
                                    <span class="widget-icon"> <i class="fa fa-comments"></i> </span>
                                    <h2>Widget Title </h2>              
                                    
                                </header>

								<!-- widget div-->
								<div>
				
									<!-- widget edit box -->
									<div class="jarviswidget-editbox">
										<!-- This area used as dropdown edit box -->
				
									</div>
									<!-- end widget edit box -->
				
									<!-- widget content -->
									<div class="widget-body no-padding">
				
										<table id="dt_basic" class="table table-striped table-bordered table-hover" width="100%">
											<thead>			                
												<tr>
													<th>No.</th>
                                                    <th>IC Number</th>
                                                    <th>Name</th>
													<th>Phone</th>                                                
                                                    <th>Submit Date</th>
                                                    <th>Status</th>                                           
                                                  
                                                    <th>Last Activity</th>
                                                    <th>Action</th>
                                                     <th>Download</th>
                                                      

												</tr>
											</thead>
											<tbody>
												<?php $i=1; ?>
                                                 @foreach ($terma as $term)
												
                                                  
                                              <tr>
                                              
                                                <td>
                                                   {{ $i }}                              
                                                </td>
                                                  
                                                  <td>
                                              {{ $term->Basic->new_ic }}
                                              <input type='hidden' id='ic99{{$i}}' name='ic' value='{{ $term->Basic->new_ic }}'/>
                                                <div id='block{{$i}}'></div>
                                                <script type="text/javascript">

                                                    $(document).ready(function() {
                                                    
                                                    var old_icx = $('#ic99{{$i}}').val();
                                                    
                                                       
                                                    if(old_icx == '661013075335' | old_icx =='690719085131'  | old_icx =='720609085274' | old_icx== '55060207511'  | old_icx== '560703085625' | old_icx== '711004085963' | old_icx== '761123086729'| old_icx== '820508105756'| old_icx== '590717105964'| old_icx== '760127086668'| old_icx== '781007055405'| old_icx== '590102055532' | old_icx== '800514035960' | old_icx== '790929085867')
                                                    {
                                                    $('#block{{$i}}').html("<span class='label label-danger'>IC Blocked </span>");
                                                    
                                                    }
                                                    });
                                                  </script>

                                                  
                                                  
                                                  </td>
                                                <td>{{ $term->Basic->name }}</td>
												<td>{{ $term->PraApplication->phone }}</td>
                                                <td>{{$term->file_created }}</td>
                                                <td><div align='center'>
													<span data-toggle="tooltip" title="{{$term->verification_remark}}"  class='label label-danger'>Documents Rejected</span>
                                                   
												   </div>
                                                </td>
                                         
                                                
                                                  <td>
                                                  <?php if(!empty($term->Log_download->first()->id)) {
                                                  $max = 0;
                                                  $k=0;
												
                                                      foreach ($term->Log_download as $log) {
                                                      
                                                              
                                                                  if($max < $log->id ) {
                                                                  $max = $log->id ;
                                                                  $tanggal = $log->downloaded_at ;
                                                                  $user_download =$log->user->name;
                                                                // echo $user_download; //echo "<br>";
                                                                  }
                                                            $k++;
                                                          
                                                    
                        
                                                       }
                                                       if ($k<>'0') {
                                                        echo "<a href='#' data-toggle='modal' data-target='#myModal".$i."'>".$tanggal." by ".$user_download."</a>";
                                                       
                                                       }
                                                       else {
                                                         echo "-";
                                                       }
													
													}
													else {
													  echo "-";
													}
													
													?>
													
                                                  
                                                  
                                                  
                                                  </td>
                                                <td align='center'>
													 <a href="{{ url('/admin/step1/'.$term->id_praapplication.'/view') }}" class='btn btn-sm btn-default'>View Application</a>
                                                    
                                                </td>
                                              
                                                <td> <a href="{{url('/')}}/admin/downloadzip/{{$term->id_praapplication}}"
                                                ><img width='24' height='24' src="{{ url('/') }}/asset/img/zip.png"></img></a>&nbsp;&nbsp; 
                                                  <a href="{{url('/')}}/admin/downloadpdf/{{$term->id_praapplication}}"
                                                ><img width='24' height='24' src="{{ url('/') }}/asset/img/pdf.png"></img></a> </td>
                                            </tr>
											
                                              <?php
                                              $i++; ?>
                                            @endforeach
											</tbody>
										</table>
                                          
                                          <br>
                                            <?php $j=1; foreach($terma as $termxz) { 

                                                  if(!empty($termxz->Log_download->first()->id)) { ?>
                                                    <!-- Modal -->
													<div class="modal fade" id="myModal{{$j}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
														<div class="modal-dialog">
															<div class="modal-content">
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
																		&times;
																	</button>
																	<h4 class="modal-title" id="myModalLabel"><b>Log Download : {{ $termxz->Basic->name }} - {{ $termxz->Basic->new_ic }} of @if($termxz->id_branch=='0')
                                                         - 
                                                    @else                                            
                                                        <b>{{ $termxz->Branch->branchname }}</b>
                                                    @endif  Branch</b></h4>
																
                                                                
                                                                </div>
																<div class="modal-body">												
																	  			<table id="datatable_col_reorder{{$j}}" class='table table-striped table-bordered table-hover'>
																		<thead>
																		  <tr>
																			<th>Date</th>
																		  <th>User</th>
                                                                            <th>Activity</th>
                                                                              <th>Remark</th>
																			  <th>Reason</th>
																		  </tr>
																		
																		</thead>
																		  	<tbody>
																		  @if(!empty($termxz->Log_download->first()->id))
												
																			@foreach ($termxz->Log_download as $logfull)
																		
											
																					
																						  <tr>
 																							<td> {{  $logfull->downloaded_at  }}</td>
																							<td> {{ $logfull->user->name }}</td>
                                                                                              <td> {{ $logfull->activity }}</td>
                                                                                                <td> {{ $logfull->log_remark }}</td>
																						  	@if ($logfull->log_reason=="")
																								<td> &nbsp; </td>
																								@else
																								<td> {{ $logfull->log_reason }}</td>
																								@endif
																						  </tr>
																						
																						
																			
																			 @endforeach
																		</tbody>
																			
																		  @else 
																			<td colspan='2'>No Data</td>
																		  @endif
																		  
																							  
																	</table>							   																   													
																  </div>
																  <div class="modal-footer">
																	<button type="button" class="btn btn-default" data-dismiss="modal">
																		Close
																	</button>
																																	   
																</div>
															</div><!-- /.modal-content -->
														</div><!-- /.modal-dialog -->
													</div><!-- /.modal -->
													<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/jquery.dataTables.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.colVis.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.tableTools.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatables/dataTables.bootstrap.min.js"></script>
<script src="<?php  print url('/'); ?>/asset/js/plugin/datatable-responsive/datatables.responsive.min.js"></script>



		<script type="text/javascript">
		
		// DO NOT REMOVE : GLOBAL FUNCTIONS!
		
		$(document).ready(function() {
			
			pageSetUp();
		
			
			/* // DOM Position key index //
		
			l - Length changing (dropdown)
			f - Filtering input (search)
			t - The Table! (datatable)
			i - Information (records)
			p - Pagination (paging)
			r - pRocessing 
			< and > - div elements
			<"#id" and > - div with an id
			<"class" and > - div with a class
			<"#id.class" and > - div with an id and class
			
			Also see: http://legacy.datatables.net/usage/features
			*/	
	
			/* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
	
				
	
			/* END BASIC */
			
				/* COLUMN SHOW - HIDE */
	$('#datatable_col_reorder{{$j}}').dataTable({
		"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-6 hidden-xs'C>r>"+
				"t"+
				"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-sm-6 col-xs-12'p>>",
		"autoWidth" : true,
		"preDrawCallback" : function() {
			// Initialize the responsive datatables helper once.
			if (!responsiveHelper_datatable_col_reorder) {
				responsiveHelper_datatable_col_reorder = new ResponsiveDatatablesHelper($('#datatable_col_reorder{{$j}}'), breakpointDefinition);
			}
		},
		"rowCallback" : function(nRow) {
			responsiveHelper_datatable_col_reorder.createExpandIcon(nRow);
		},
		"drawCallback" : function(oSettings) {
			responsiveHelper_datatable_col_reorder.respond();
		}			
	});
	
	/* END COLUMN SHOW - HIDE */
		})
		</script>
          
													
													<?php
													}
													$j++;
													}
													?>

									</div>
									<br><br><br><br>
									<!-- end widget content -->
				
								</div>
								<!-- end widget div -->
                                
                            </div>
                            <!-- end widget -->

                        </article>
    </div>
    <!-- END MAIN CONTENT -->

</div>
<!-- END MAIN PANEL -->
<!-- ==========================CONTENT ENDS HERE ========================== -->


<!-- END PAGE FOOTER -->

<?php 
    //include required scripts
    include("asset/inc/scripts.php"); 
?>

<!-- PAGE RELATED PLUGIN(S) -->
<script>
  /* BASIC ;*/
				var responsiveHelper_dt_basic = undefined;
				var responsiveHelper_datatable_fixed_column = undefined;
				var responsiveHelper_datatable_col_reorder = undefined;
				var responsiveHelper_datatable_tabletools = undefined;
				
				var breakpointDefinition = {
					tablet : 1024,
					phone : 480
				};
				$('#dt_basic').dataTable({
					"sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>"+
						"t"+
						"<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
					"autoWidth" : true,
					"preDrawCallback" : function() {
						// Initialize the responsive datatables helper once.
						if (!responsiveHelper_dt_basic) {
							responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
						}
					},
					"rowCallback" : function(nRow) {
						responsiveHelper_dt_basic.createExpandIcon(nRow);
					},
					"drawCallback" : function(oSettings) {
						responsiveHelper_dt_basic.respond();
					}
				});
				
</script>
<script>
  $(document).ready(function() {
    // show the alert
    window.setTimeout(function() {
    $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
        $(this).remove(); 
    });
}, 2800);
 
});
</script>




          
