<!DOCTYPE html>
<html lang="en-us" <?php echo implode(' ', array_map(function($prop, $value) {
			return $prop.'="'.$value.'"';
		}, array_keys($page_html_prop), $page_html_prop)) ;?>>
	<head>
		<meta charset="utf-8">
		<!--<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">-->

		<title>Personal Financing-i | MBSB Bank</title>

	
		<meta charset="UTF-8">
<meta name="description" content="Portal online pinjaman peribadi ">
<meta name="keywords" content="MBSB, Personal loan MBSB, Personal financing-i MBSB, apply loan online, pembiayaan peribadi, Semak Kelayakan Pembiayaan / Pinjaman Anda,  Pinjaman Peribadi, portal pinjaman online, personal loan online, bank persatuan, pinjaman lulus segera, loan koperasi online,pinjaman peribadi segera,loan segera lulus,pembiayaan peribadi segera,portal pembiayaan online,Permohanan Pembiayaan Peribadi Online,Permohonan Loan Online,Semak kelayakan pinjaman ">
<meta name="author" content="MBSB Bank Berhad">

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/font-awesome.min.css">

		<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-production-plugins.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-production.min.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-skins.min.css">
		<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>/css/jquery.fileupload.css">
		
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/normalize.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/main.css">
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/jquery.steps.css">
		<!-- SmartAdmin RTL Support is under construction-->
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/smartadmin-rtl.min.css">
	
		<!-- We recommend you use "your_style.css" to override SmartAdmin
		     specific styles this will also ensure you retrain your customization with each SmartAdmin update.
		<link rel="stylesheet" type="text/css" media="screen" href="<?php echo ASSETS_URL; ?>/css/your_style.css"> -->

		<?php

			if ($page_css) {
				foreach ($page_css as $css) {
					echo '<link rel="stylesheet" type="text/css" media="screen" href="'.ASSETS_URL.'/css/'.$css.'">';
				}
			}
		?>


	

		<!-- FAVICONS -->
		<link rel="shortcut icon" href="<?php echo ASSETS_URL; ?>/img/favicon/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?php echo ASSETS_URL; ?>/img/favicon/favicon.ico" type="image/x-icon">

		<!-- GOOGLE FONT -->
		<link rel="stylesheet" href="<?php echo ASSETS_URL; ?>/css/css.css">

		<!-- Specifying a Webpage Icon for Web Clip
			 Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
		<link rel="apple-touch-icon" href="<?php echo ASSETS_URL; ?>/img/splash/sptouch-icon-iphone.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?php echo ASSETS_URL; ?>/img/splash/touch-icon-ipad.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?php echo ASSETS_URL; ?>/img/splash/touch-icon-iphone-retina.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?php echo ASSETS_URL; ?>/img/splash/touch-icon-ipad-retina.png">

		<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">

		<!-- Startup image for web apps -->
		<link rel="apple-touch-startup-image" href="<?php echo ASSETS_URL; ?>/img/splash/ipad-landscape.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
		<link rel="apple-touch-startup-image" href="<?php echo ASSETS_URL; ?>/img/splash/ipad-portrait.png" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
		<link rel="apple-touch-startup-image" href="<?php echo ASSETS_URL; ?>/img/splash/iphone.png" media="screen and (max-device-width: 320px)">

		<!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
		<script>
			if (!window.jQuery) {
				document.write('<script src="<?php echo ASSETS_URL; ?>/js/libs/jquery-2.1.1.min.js"><\/script>');
			}
		</script>

		<script>
			if (!window.jQuery.ui) {
				document.write('<script src="<?php echo ASSETS_URL; ?>/js/libs/jquery-ui-1.10.3.min.js"><\/script>');
			}
		</script>


	</head>
	<body   class="smart-style-1 fixed-header" <?php echo implode(' ', array_map(function($prop, $value) {
			return $prop.'="'.$value.'"';
		}, array_keys($page_body_prop), $page_body_prop)) ;?>>

		<!-- POSSIBLE CLASSES: minified, fixed-ribbon, fixed-header, fixed-width
			 You can also add different skin classes such as "smart-skin-1", "smart-skin-2" etc...-->
		<?php
			if (!$no_main_header) {

		?>
				<!-- HEADER -->
				<header id="header">
					
			<div id="logo-group">

				<span id="logo"> <a href="<?php print url('/') ?>"><img src="<?php echo ASSETS_URL; ?>/img/logo.jpg" alt="SmartAdmin" width="100%"> </a></span>

				<!-- END AJAX-DROPDOWN -->
			</div>
					

					<!-- projects dropdown -->
					
					<!-- end projects dropdown -->

					<!-- pulled right: nav area -->
					<div class="pull-right">

					<!-- collapse menu button -->
						<div id="hide-menu" class="btn-header pull-right">
					<span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
				</div>
						<!-- end collapse menu -->
						
					<div id="project-context">

						<span><font color="black"> Welcome, </font></span>
						<span id="project-selector" class="popover-trigger-element dropdown-toggle" data-toggle="dropdown">
						 <?php if (!empty($statusx)) {print $user->name ; }  else { if(!empty($data))  {  if(!empty($data->first()->name)) {print $data->first()->name ;}  }
						 else  {print $user->name ; }  } ?> <i class="fa fa-angle-down"></i></span> 
					

						<!-- Suggestion: populate this list with fetch and push technique -->
						<ul class="dropdown-menu">
							<?php if($user->role=='0') { ?>
							<li>
								<a href="<?php print url('/'); ?>/faq" >FAQ</a>
							</li>
							<?php } ?>
							<li>
								<a href="<?php print url('/'); ?>/user" >Update Account</a>
							</li>
						
							<li class="divider"></li>
							
							<li id="logout">
								<?php if($user->role=="5" || $user->role=="10") {?>
									<a    href="<?php print url('/'); ?>/mbsb_logout" title="Sign Out"  data-action="userLogout" ata-logout-msg="You can improve your security further after logging out by closing this opened browser">Sign Out <i class="fa fa-power-off"></i></a>

								<?php } else { ?>
									<a    href="<?php print url('/'); ?>/mbsb_logout" title="Sign Out"  data-action="userLogout" ata-logout-msg="You can improve your security further after logging out by closing this opened browser">Sign Out <i class="fa fa-power-off"></i></a>

								<?php } ?>
							
						
						</li>
						</ul>
						<ul class="dropdown-menu">
							<?php if($user->role=='0') { ?>
							<li>
								<a href="<?php print url('/'); ?>/faq" >FAQ</a>
							</li>
							<?php } ?>
							<li>
								<a href="<?php print url('/'); ?>/user" >Update Account</a>
							</li>
						
							<li class="divider"></li>
							
							<li id="logout">
								<?php if($user->role=="5" || $user->role=="10") {?>
									<a    href="<?php print url('/'); ?>/mbsb_logout" title="Sign Out"  data-action="userLogout" ata-logout-msg="You can improve your security further after logging out by closing this opened browser">Sign Out <i class="fa fa-power-off"></i></a>

								<?php } else { ?>
									<a    href="<?php print url('/'); ?>/mbsb_logout" title="Sign Out"  data-action="userLogout" ata-logout-msg="You can improve your security further after logging out by closing this opened browser">Sign Out <i class="fa fa-power-off"></i></a>

								<?php } ?>
							
						
						</li>
						</ul>
						<!-- end dropdown-menu-->

					</div>
					

					</div>
					<!-- end pulled right: nav area -->

				</header>
				<!-- END HEADER -->

				<!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
				Note: These tiles are completely responsive,
				you can add as many as you like
				-->
				

		<?php
			}
		?>
		        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-163298716-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-163298716-1');
</script>