<?php

//CONFIGURATION for SmartAdmin UI

//ribbon breadcrumbs config
//array("Display Name" => "URL");
$breadcrumbs = array(
	"Home" => APP_URL
);


if (!empty($user)) {
	if ($user->role=='1') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"Route Back Status" => array(
				"title" => "Route Back Status",
				"url" => url('/')."/routeback_status",
				"icon" => "fa fa-chevron-circle-left"
			),
			"master" => array(
				"title" => "Master",
				"icon" => "fa-puzzle-piece",
				"sub" => array(	
					"bankpersatuan" => array(
						"title" => "Bank Persatuan",
						"icon" => "fa fa-bank",
						"sub" => array(
							"branchmaster" => array(
								"title" => "Branch Master",
								"url" => url('/')."/admin/allbranch/index",
								"icon" => "fa-share-alt"
							),
							/*"branchuser" => array(
								"title" => "Branch User",
								"url" => url('/')."/admin/allbranch_user/index",
								"icon" => "fa-users"
							),*/
							"hquser" => array(
								"title" => "HQ User",
								"url" => url('/')."/admin/allhq_user/index",
								"icon" => "fa-user"
							),
						)
					),
						"Employer" => array(
						"title" => "Employer",
						"icon" => "fa-suitcase ",
						"url" => url('/')."/employer",
					
					),

				)
			)	
			,
			"report" => array(
				"title" => "Report",
				"icon" => "fa-file",
				"sub" => array(	
					
				
						"calculated" => array(
						"title" => "Just Calculated Cust",
						"icon" => "fa-file ",
						"url" => url('/')."/report/calculated",
					
					),


						"registered" => array(
						"title" => "Registered",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered",
					
					),

					"submitted" => array(
						"title" => "Submitted",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted",
					
					),
					"by status" => array(
						"title" => "By Status",
						"icon" => "fa-file ",
						"url" => url('/')."/report/bystatus",
					
					),

				)
			)	
		);
	}
	else if ($user->role=='4') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			/*"Documents Rejected" => array(
				"title" => "Documents Rejected",
				"url" => url('/')."/docsreject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Rejected" => array(
				"title" => "Application Rejected",
				"url" => url('/')."/reject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Approved" => array(
				"title" => "Application Approved",
				"url" => url('/')."/approved",
				"icon" => "fa fa-check-square"
			),*/
			
			
			"master" => array(
				"title" => "Master",
				"icon" => "fa-puzzle-piece",
				"sub" => array(	
					"mbsb" => array(
						"title" => "MBSB",
						"icon" => "fa fa-bank",
						"sub" => array(
							"branchmaster" => array(
								"title" => "Branch Master",
								"url" => url('/')."/admin/allbranch/index",
								"icon" => "fa-share-alt"
							),
							/*"branchuser" => array(
								"title" => "Branch User",
								"url" => url('/')."/admin/allbranch_user/index",
								"icon" => "fa-users"
							),*/
							"hquser" => array(
								"title" => "HQ User",
								"url" => url('/')."/admin/allhq_user/index",
								"icon" => "fa-user"
							),
							"package" => array(
						"title" => "Marketing List",
						"url" => url('/')."/marketing",
						"icon" => "fa-users"
					),
						"score" => array(
						"title" => "Manager",
						"url" => url('/')."/manager",
						"icon" => "fa-user"
					)
						
						)
					),
						
						"package" => array(
						"title" => "Package",
						"url" => url('/')."/admin/master/package",
						"icon" => "fa-tags"
					),
						"job" => array(
						"title" => "Job Sector",
						"url" => url('/')."/admin/master/job-sector",
						"icon" => "fa-suitcase"
					),
						"score" => array(
						"title" => "Score Rating",
						"url" => url('/')."/admin/master/scorerating",
						"icon" => "fa-bar-chart"
					),
						"scorecard" => array(
						"title" => "Score Card",
						"url" => url('/')."/admin/master/scorecard",
						"icon" => "fa-credit-card"
					),


				)
			),	
			"report" => array(
				"title" => "Report",
				"icon" => "fa-file",
				"sub" => array(	
					
				
						"calculated" => array(
						"title" => "Just Calculated Cust",
						"icon" => "fa-file ",
						"url" => url('/')."/report/calculated",
					
					),


						"registered" => array(
						"title" => "Registered",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered",
					
					),
						"submitted" => array(
						"title" => "Submitted",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted",
					
					),
					"by status" => array(
						"title" => "By Status",
						"icon" => "fa-file ",
						"url" => url('/')."/report/bystatus",
					
					),

				)
			)	
		);
	}
	else if ($user->role=='8') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"Documents Rejected" => array(
				"title" => "Documents Rejected",
				"url" => url('/')."/docsreject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Rejected" => array(
				"title" => "Application Rejected",
				"url" => url('/')."/reject",
				"icon" => "fa fa-chevron-circle-left"
			),
			"Application Approved" => array(
				"title" => "Application Approved",
				"url" => url('/')."/approved",
				"icon" => "fa fa-check-square"
			),
			
			
			/*"master" => array(
				"title" => "Master",
				"icon" => "fa-puzzle-piece",
				"sub" => array(	
					"mbsb" => array(
						"title" => "MBSB",
						"icon" => "fa fa-bank",
						"sub" => array(
							"branchmaster" => array(
								"title" => "Branch Master",
								"url" => url('/')."/admin/allbranch/index",
								"icon" => "fa-share-alt"
							),
							/*"branchuser" => array(
								"title" => "Branch User",
								"url" => url('/')."/admin/allbranch_user/index",
								"icon" => "fa-users"
							),
							"hquser" => array(
								"title" => "HQ User",
								"url" => url('/')."/admin/allhq_user/index",
								"icon" => "fa-user"
							),
							"package" => array(
						"title" => "Marketing List",
						"url" => url('/')."/marketing",
						"icon" => "fa-user"
					),
						"score" => array(
						"title" => "Manager",
						"url" => url('/')."/manager",
						"icon" => "fa-user"
					)
						
						)
					),
						
						"package" => array(
						"title" => "Package",
						"url" => url('/')."/admin/master/package",
						"icon" => ""
					),
						"score" => array(
						"title" => "Score Rating",
						"url" => url('/')."/admin/master/scorerating",
						"icon" => ""
					),
						"scorecard" => array(
						"title" => "Score Card",
						"url" => url('/')."/admin/master/scorecard",
						"icon" => ""
					),


				)
			),	*/
			"report" => array(
				"title" => "Report",
				"icon" => "fa-file",
				"sub" => array(	
					
				
						"calculated" => array(
						"title" => "Just Calculated Cust",
						"icon" => "fa-file ",
						"url" => url('/')."/report/calculated",
					
					),


						"registered" => array(
						"title" => "Registered",
						"icon" => "fa-file ",
						"url" => url('/')."/report/registered",
					
					),
						"submitted" => array(
						"title" => "Submitted",
						"icon" => "fa-file ",
						"url" => url('/')."/report/submitted",
					
					),
					"by status" => array(
						"title" => "By Status",
						"icon" => "fa-file ",
						"url" => url('/')."/report/bystatus",
					
					),

				)
			)	
		);
	}
	else if ($user->role=='5') {
		$page_nav = array(
			"utama" => array(
				"title" => "Main Page",
				"url" => url('/')."/",
				"icon" => "fa-home"
			),
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-dashboard"
			),
			"New Applicant" => array(
				"title" => "Add New Applicant",
				"url" => url('/')."/moapplication",
				"icon" => "fa fa-plus"
			),
			"Anti Attrition" => array(
				"title" => "Anti Attrition",
				"url" => url('/')."/agent/anti-attrition",
				"icon" => "fa fa-user"
			),
			"MO Info" => array(
				"title" => "MO Info",
				"url" => url('/')."/moinfo",
				"icon" => "fa fa-user"
			),
			"Log Login" => array(
				"title" => "Login History",
				"url" => url('/')."/loglogin",
				"icon" => "fa fa-sign-in"
			)/*,
			"Logout" => array(
				"title" => "Logout",
				"url" => url('/')."/mbsb_logout",
				"icon" => "fa fa-sign-in"
			)*/

		);
	}
	else if ($user->role=='10') {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"Marketing List" => array(
				"title" => "Marketing List",
				"url" => url('/')."/marketing",
				"icon" => "fa fa-user"
			),
			"Direct Application" => array(
				"title" => "Direct Application",
				"url" => url('/')."/direct_application",
				"icon" => "fa fa-file"
			)
		);
	}
	else {
		$page_nav = array(
			"dashboard" => array(
				"title" => "Dashboard",
				"url" => url('/')."/admin",
				"icon" => "fa-home"
			),
			"Assign to MO" => array(
				"title" => "Assign to MO",
				"url" => url('/')."/clrt/new-loan-offer",
				"icon" => "fa fa-user"
			),
			"Anti Attrition" => array(
				"title" => "Anti Attrition",
				"url" => url('/')."/clrt/anti-attrition",
				"icon" => "fa fa-user"
			)
		);
	}
}

//configuration variables
$page_title = "";
$page_css = array();
$no_main_header = false; //set true for lock.php and login.php
$page_body_prop = array(); //optional properties for <body>
$page_html_prop = array(); //optional properties for <html>
?>